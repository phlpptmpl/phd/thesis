%!TEX program=lualatex
%!TEX file=thesis.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% document class

% Additional package options
\input{config/packages-options}

% Main document class definition
\documentclass[%
  tikz,%
  mathematics,%
  code,%
  biblatex,%
  ngerman,%
  english,%,
  % draft,%
  final,%
  accepted,%
  colorful,%
  fontsize=12pt,%
]{iswdctrt}

%% END document class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Document Properties

\title{Dynamics of Cable-Driven Parallel Robots with Elastic and Flexible, Time-Varying Length Cables}
% \subtitle{Did you know you can add a subtitle to your thesis, too?}

\mainexaminer{\textPD \textDrIng Andreas Pott}
% @TODO check names have correct titles
\secondexaminer{%
  \textProf \textDr Bernard Haasdonk \and %
  Marc Gouttefarde, \textPhD \and %
  \textProf \textDr ir.\@ habil.\@ Remco I.\@ Leine%
}

\author{Philipp Tempel}
\shortauthor{Ph.\@ Tempel}
\city{Stuttgart}
\placeofbirth{Stuttgart}
% \date{\printdate{2019-04-04}}
\date{\printdate{2019-07-19}}

% Dedication (to whom am I going to make this thesis)
\dedication{To... me?}
\dedication{
  \epigraph{%
    All models are wrong.
    
    But some are useful.
  }{%
    \textsc{George E.\@ P.\@ Box}\\%
    \textit{1976, Journal of the American Statistical Association}\\%
    \autocite{Box.1976b}%
  }
}

%% END Document Properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Custom Includes

% Load my custom additional packges
\input{config/packages}

% Load my custom colors
\input{config/colors}

% Load my custom environments
\input{config/environments}

% Load my custom commands
\input{config/commands}

% Load unknown hyphenations
\input{config/hyphenation}

% Load all TikZ related stuff
\input{config/tikz}

\addbibresource{thesis.bib}

%% END Custom Includes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% document configuration

\input{config/configuration}

%% END document configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TikZ Configuration

\tdplotsetmaincoords{80}{110}

\input{config/tikz/externalize}

\tikzset{/tikz/external/mode=list and make}

%% END TikZ Configuration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% glossaries and indices

\input{config/glossaries}

%% END glossaries and indices
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Cleveref Names

\AtBeginDocument{%
  \crefname{equation}{equation}{equations}%
  \crefname{chapter}{chapter}{chapters}%
  \crefname{section}{section}{sections}%
  \crefname{appendix}{appendix}{appendices}%
  \crefname{enumi}{item}{items}%
  \crefname{footnote}{footnote}{footnotes}%
  \crefname{figure}{figure}{figures}%
  \crefname{table}{table}{tables}%
  \crefname{theorem}{theorem}{theorems}%
  \crefname{lemma}{lemma}{lemmas}%
  \crefname{corollary}{corollary}{corollaries}%
  \crefname{proposition}{proposition}{propositions}%
  \crefname{definition}{definition}{definitions}%
  \crefname{result}{result}{results}%
  \crefname{example}{example}{examples}%
  \crefname{remark}{remark}{remarks}%
  \crefname{note}{note}{notes}%
  %
  \Crefname{equation}{Equation}{Equations}%
  \Crefname{chapter}{Chapter}{Chapters}%
  \Crefname{section}{Section}{Sections}%
  \Crefname{appendix}{Appendix}{Appendices}%
  \Crefname{enumi}{Item}{Items}%
  \Crefname{footnote}{Footnote}{Footnotes}%
  \Crefname{figure}{figure}{Figures}%
  \Crefname{table}{Table}{Tables}%
  \Crefname{theorem}{Theorem}{Theorems}%
  \Crefname{lemma}{Lemma}{Lemmas}%
  \Crefname{corollary}{Corollary}{Corollaries}%
  \Crefname{proposition}{Proposition}{Propositions}%
  \Crefname{definition}{Definition}{Definitions}%
  \Crefname{result}{Result}{Results}%
  \Crefname{example}{Example}{Examples}%
  \Crefname{remark}{Remark}{Remarks}%
  \Crefname{note}{Note}{Notes}%
}

%% END Cleveref Names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





% Order and Components according to UNC GradSchool
% @see https://gradschool.unc.edu/academics/thesis-diss/guide/ordercomponents.html
%   Title Page
%   Copyright Page
%   Abstract
%   Dedication, Acknowledgements, and Preface (each optional)
%   Table of Contents, with page numbers
%   List of Tables, List of Figures, or List of Illustrations, with titles and page numbers (if applicable)
%   List of Abbreviations (if applicable)
%   List of Symbols (if applicable)
%   Chapters, including:
%       Introduction, if any
%       Main body, with consistent subheadings as appropriate
%   Appendices (if applicable)
%   Endnotes (if applicable)
%   References (see section on References for options)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \chapter        80
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section        70
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection     60
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsubsection  50
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \paragraph      40
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subparagraph



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% document
\begin{document}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% mainmatter

\mainmatter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{chapter}{5}
\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--estimation--mechanical-parameters}
  \includegraphics[%
    ]{stress-strain/estimation/mechanical-parameters}
  \tikzexternaldisable%
  \caption{
    Elastic and viscous parameters of the \gls{acr:GMW} model with~$n_{\msmaxwell} \in \numset{2;3;4}$ estimated over~$n_{\ms{trial}} = 10$ trials.
    Purely elastic behavior, parametrized solely by~$\youngsmodulus_{1}$ is estimated to~$\youngsmodulus_{1} \approxeq \SI{7}{\giga\pascal}$, conforming well with data sheets by the manufacturer.
    Further mechanical parameters~$\youngsmodulus_{\loopindex}$ and~$\viscousmodulus_{\loopindex}$,~$\parentheses*{ \loopindex \in \numset{2;3;4} }$ are not available in data sheets.
    Numeric values are given in~\cref{tbl:stress-strain:results:estimation:number-of-elements:mechanical-parameters}.
    Note the first \gls{acr:MW} element does not contain a damper which can be expressed with~$\viscousmodulus_{1} = \infty$.
  }
  \label{fig:stress-strain:results:estimation:number-of-elements:mechanical-parameters}
\end{figure}

%% END mainmatter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





\end{document}

%% END document
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
