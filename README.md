# PhD Thesis: Dynamics of Cable-Driven Parallel Robots with Elastic and Flexible, Time-varying Length Cables [Working Title]

[![pipeline status](https://git.isw.uni-stuttgart.de/dissertationen/tempel-philipp/thesis/badges/develop/pipeline.svg)](https://git.isw.uni-stuttgart.de/dissertationen/tempel-philipp/thesis/commits/develop)

There are two files `gitdate.txt` and `gitrev.txt` which are configured as
```bash
git update-index --skip-worktree gitdate.txt
git update-index --skip-worktree gitrev.txt
```
such that the file itself will be in the repo but changes to its content won't be tracked.

