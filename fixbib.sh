FILES=`find . -type f -name "*.bib" -depth 1`

sed -i.bak '/shorthand = /d' ${FILES}
sed -i.bak '/language = /d' ${FILES}
sed -i.bak '/shortjournal = /d' ${FILES}
sed -i.bak 's/{\\$}\\mathcal{H}{\\_}2{\\$}/$\\mathcal{H}_{2}$/g' ${FILES}
sed -i.bak 's/H{\\$}{\\_}$\\backslash$infty{\\$}/$\\mathcal{H}_{\\infty}$/g' ${FILES}
sed -i.bak 's/{\\$}{\\$}46$\\backslash$,\\%${\\$}46{\\%}/$46\\,\\%$/g' ${FILES}
sed -i.bak 's/{\\$}$\\backslash$int \\sb{0}\\sp{1}-{\\rm ln}(x)f(x)$\\backslash$,dx{\\$}/$\\int_{0}^{1}{(\\ln x) \\, f(x) \\, \\mathrm{d}x}$/g' ${FILES}
sed -i.bak 's/{\\$}$\\backslash$int \\sb{0}\\sp{1}(-{\\rm Log}$\\backslash$ x)·x\\sp{\\alpha }·f(x)·dx{\\$}/$\\int_{0}^{1}{(- \\log x x^{a} f(x) \\, \\mathrm{d} x}$/g' ${FILES}
sed -i.bak 's/{\\$}$\\backslash$int \\sb{0}\\sp{\\infty } E\\sb{m}(x)·f(x)·dx{\\$}/$\\int_{0}^{\\infty}{E_m (f) f(x) \\mathrm{d} x}$/g' ${FILES}
dos2unix ${FILES}
