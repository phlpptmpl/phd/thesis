#!/usr/bin/env python3

import mimetypes
import pathlib
import progressbar as pb
import subprocess

def compress(d):
  # Init mimetype handling
  mimetypes.init()
  # Loop over all directories
  fs = find_images(d)
  
  # List of commands/subprocesses
  c = [];
  
  # Loop over each image
  for s in fs:
    # Get a new target path
    t = pathlib.Path(*[p for p in s.parts if p is not "original"]);
    # Make sure target directory exists
    if not ( t.parent.is_dir() and t.parent.exists() ):
      t.parent.mkdir(parents=True, exist_ok=True)
    
    # Build the command
    c.append(['convert', '-units', 'PixelsPerInch', str(s.resolve()), '-density', '300', '-resize', '2000x', str(t.resolve())])
  
  # initialize timer
  widgets = ['Compressing file (', pb.Counter(), '/' , str(len(c)) , ') ',
             pb.Bar(), ' ', pb.AdaptiveETA()]
  timer = pb.ProgressBar(widgets=widgets, maxval=len(c)).start()

  # Loop over each command and trigger it
  for idx, c_ in enumerate(c):
      p = subprocess.Popen(c_)
      p.wait()
      p.poll()
      timer.update(idx)
      
  # Done
  timer.finish()
  
              
def find_images(d):
  # Holds images
  fs = [];
  
  # Get a resolved pathlib.Path object for the path
  p = pathlib.Path(d).resolve();
  
  # Loop over all directories
  for d in p.glob('**'):
    # Loop over all files in each directory
    for f in d.iterdir():
      t, e = mimetypes.guess_type(str(f))
      if not t is None and t.startswith('image/'):
        fs.append(f)
  
  return fs
  
  
if __name__ == "__main__":
  compress("images/original/");
