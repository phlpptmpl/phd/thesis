%!TEX root=../../../../../thesis.tex
%!TEX file=figures/caro-dynamics/named/ipanema-3/motion_up/cable-forces.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{1.00\linewidth/2 - 2\subplothsep}
\setlength{\subplotheight}{1.00\subplotwidth}

\pgfplotstableread[%
  col sep=comma,%
]{data/cdpr/named/motion_up/ipanema_3/degree-7/segment-11/cable.forces.dat}\plotdata

\begin{tikzpicture}[
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cable forces
\begin{axis}[
    % General adjustments
    timeseries plot,%
    name={caro-dynamics ipanema-3 motion_up cable-forces},%
    % yyaxis left,%
    cycle multiindex* list={%
      /phlpptmpl/linestyles*\nextlist%
      /phlpptmpl/mark list even*%
    },%
    %
    % Legend
    legend to name={caro-dynamics ipanema-3 motion_up cable-forces legend},%
    legend style={%
      legend columns=4,%
      % transpose legend,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
    % 
    % X-Axis
    enlarge x limits=0.02,%
    % 
    % Y-Axis
    ymin=100,%
    ymax=3000,%
    ylabel={Cable force $\cabforce_{\cabindex} / \si{\newton}$},%
    scaled y ticks=base 10:-3,%
    % axis y discontinuity=parallel,%
    ytickmin=100,%
    enlarge y limits=0.02,%
  ]

% X
\foreach \cid in {1,2,3,4,5,6,7,8}{%
  \addplot+[%
    ]table[%
        col sep=comma,%
        x=t,%
        y=c\cid,%
      ]{\plotdata};
  \addlegendentryexpanded{$\cabindex = \cid$};
    \expandafter\label\expandafter{plt:caro-dynamics:dynamics:ipanema-3:motion-up:forces:c\cid}
}

\end{axis}

\node[%
    yshift=-3.00\baselineskip,%
    anchor=north,%
  ]%
  at (caro-dynamics ipanema-3 motion_up cable-forces.south)%
  {%
    \ref{caro-dynamics ipanema-3 motion_up cable-forces legend}%
  };

\end{tikzpicture}
