%!TEX root=../../../../../thesis.tex
%!TEX file=figures/caro-dynamics/named/ipanema-cuboid/kinetostatics/shape.tikz

\begin{tikzpicture}[%
    /phlpptmpl/cable robot 3d,%
  ]

\begin{axis}[%
    % General adjustments
    cable robot 3d,%
    %
    % X-Axis
    xmin=-1.00,%
    xmax=1.00,%
    %
    % Y-Axis
    ymin=-1.00,%
    ymax=1.00,%
    %
    % Z-Axis
    zmin=-1.00,%
    zmax=1.00,%
  ]

\pgfmathsetmacro{\caronumcab}{8}
\pgfmathsetmacro{\caropolydeg}{7}
\pgfmathsetmacro{\caronumsegment}{11}

% Cables background
\foreach \cableid in {1,5}{%
  \addplot3[%
      /phlpptmpl/cable robot cable,%
    ]%
    table[%
        x=x\cableid,%
        y=y\cableid,%
        z=z\cableid,%
        col sep=comma,%
      ]%
      {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/cables.dat};

  \pgfmathsetmacro{\cableidindex}{\cableid-1}
  % Platform anchors
  \addplot3[%
      /phlpptmpl/cable robot platform anchor background,%
      select only coord at index=\cableidindex,%
    ]%
    table[%
        x=x,%
        y=y,%
        z=z,%
        col sep=comma,%
      ]%
      {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/platform.vertices.dat};
}

% Platform
\addplot3[%
    /phlpptmpl/cable robot platform,%
    patch table=data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/platform.faces.dat,%
    % patch type=rectangle,,%
  ]%
  table[%
      x=x,%
      y=y,%
      z=z,%
      col sep=comma,%
    ]%
    {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/platform.vertices.dat};

% Cables foreground
\foreach \cableid in {2,3,4,6,7,8}{%
  \addplot3[%
      /phlpptmpl/cable robot cable,%
    ]%
    table[%
        x=x\cableid,%
        y=y\cableid,%
        z=z\cableid,%
        col sep=comma,%
      ]%
      {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/cables.dat};

  \pgfmathsetmacro{\cableidindex}{\cableid-1}
  % Platform anchors
  \addplot3[%
      /phlpptmpl/cable robot platform anchor,%
    ]%
    table[%
        x=x,%
        y=y,%
        z=z,%
        col sep=comma,%
      ]%
      {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/platform.vertices.dat};
}

% Winches
\foreach \winchid in {1,2,...,\caronumcab}{%
  \addplot3[%
      /phlpptmpl/cable robot frame anchor,%
    ]%
    table[%
        x=x\winchid,%
        y=y\winchid,%
        z=z\winchid,%
        col sep=comma,%
      ]%
      {data/cdpr/named/kinetostatics/ipanema_cuboid/degree-\caropolydeg/segment-\caronumsegment/winches.dat};
}

\end{axis}

\end{tikzpicture}
