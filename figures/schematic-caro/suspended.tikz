%!TEX root=../../thesis.tex
%!TEX file=figures/schematic-caro/suspended.tikz

\tdplotsetmaincoords{80}{110}

\pgfmathsetmacro{\framewidth}{6.00}
\pgfmathsetmacro{\framedepth}{6.00}
\pgfmathsetmacro{\frameheight}{4.00}
\pgfmathsetmacro{\platwidth}{1.00}
\pgfmathsetmacro{\platdepth}{1.00}
\pgfmathsetmacro{\platheight}{0.50}


\begin{tikzpicture}[%
    %
    tdplot_main_coords,%
    %
    line cap=round,%
    line join=round,%
    %
    every path/.append style={%
      line cap=round,%
      line join=round,%
    },%
  ]

% Calculate coordinates of the winches
\coordinate%
  (a1)%
  at ({-0.50*\framewidth},  {0.50*\framedepth},  {1.00*\frameheight});
\coordinate%
  (a2)%
  at ( {0.50*\framewidth},  {0.50*\framedepth},  {1.00*\frameheight});
\coordinate%
  (a3)%
  at ( {0.50*\framewidth}, {-0.50*\framedepth},  {1.00*\frameheight});
\coordinate%
  (a4)%
  at ({-0.50*\framewidth}, {-0.50*\framedepth},  {1.00*\frameheight});
\coordinate%
  (a5)%
  at ({-0.50*\framewidth},  {0.50*\framedepth},  {0.98*\frameheight});
\coordinate%
  (a6)%
  at ( {0.50*\framewidth},  {0.50*\framedepth},  {0.98*\frameheight});
\coordinate%
  (a7)%
  at ( {0.50*\framewidth}, {-0.50*\framedepth},  {0.98*\frameheight});
\coordinate%
  (a8)%
  at ({-0.50*\framewidth}, {-0.50*\framedepth},  {0.98*\frameheight});

% Calculate coordinates of the ground floor
\coordinate%
  (a01)%
  at ({-0.50*\framewidth},  {0.50*\framedepth}, 0);
\coordinate%
  (a02)%
  at ( {0.50*\framewidth},  {0.50*\framedepth}, 0);
\coordinate%
  (a03)%
  at ( {0.50*\framewidth}, {-0.50*\framedepth}, 0);
\coordinate%
  (a04)%
  at ({-0.50*\framewidth}, {-0.50*\framedepth}, 0);

% Set platform coordinate
\coordinate%
  (r)%
  at (0, 0, {0.50*\frameheight});


% Set coordinates of the target 3D sub-plot (i.e., platform plot)
\tdplotsetrotatedcoords{0}{0}{0}
\tdplotsetrotatedcoordsorigin{(r)}

% Define platform coordinates in the target 3D sub-plot
\begin{scope}[%
  tdplot_rotated_coords,%
]
  \coordinate%
    (b1)%
    at ( {0.50*\platwidth},  {0.50*\platdepth},  {0.50*\platheight});
  \coordinate%
    (b2)%
    at ( {0.50*\platwidth}, {-0.50*\platdepth},  {0.50*\platheight});
  \coordinate%
    (b3)%
    at ({-0.50*\platwidth}, {-0.50*\platdepth},  {0.50*\platheight});
  \coordinate%
    (b4)%
    at ({-0.50*\platwidth},  {0.50*\platdepth},  {0.50*\platheight});
  \coordinate%
    (b5)%
    at ({-0.50*\platwidth}, {-0.50*\platdepth}, {-0.50*\platheight});
  \coordinate%
    (b6)%
    at ({-0.50*\platwidth},  {0.50*\platdepth}, {-0.50*\platheight});
  \coordinate%
    (b7)%
    at ( {0.50*\platwidth},  {0.50*\platdepth}, {-0.50*\platheight});
  \coordinate%
    (b8)%
    at ( {0.50*\platwidth}, {-0.50*\platdepth}, {-0.50*\platheight});

\end{scope}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Ground

\draw[%
    draw,%
    /cdpr/floor,%
  ]%
  (a01)%
  -- (a02)%
  -- (a03)%
  -- (a04)%
  -- cycle;

%% END ground
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% frame background

% left
\draw[%
    draw,%
    fill=White,%
  ]%
  (a3)%
  -- (a4)%
  -- (a8)%
  -- (a7)%
  -- cycle;

% rear
\draw[%
    draw,%
    fill=White,%
  ]%
  (a4)%
  -- (a1)%
  -- (a5)%
  -- (a8)%
  -- cycle;

%% END frame background
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% background cables

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a5)%
  -- (b5);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a6)%
  -- (b6);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a8)%
  -- (b8);

%% END background cables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% platform

% bottom
\draw[%
    /cdpr/rigid body,%
  ]%
  (b5)
  -- (b6)
  -- (b7)
  -- (b8)
  -- cycle;

% left
\draw[%
    /cdpr/rigid body,%
  ]%
  (b2)
  -- (b3)
  -- (b5)
  -- (b8)
  -- cycle;

% rear
\draw[%
    /cdpr/rigid body,%
  ]%
  (b3)
  -- (b4)
  -- (b6)
  -- (b5)
  -- cycle;

% right
\draw[%
    /cdpr/rigid body,%
  ]%
  (b4)
  -- (b1)
  -- (b7)
  -- (b6)
  -- cycle;

% front
\draw[%
    /cdpr/rigid body,%
  ]%
  (b1)
  -- (b2)
  -- (b8)
  -- (b7)
  -- cycle;

% top
\draw[%
    /cdpr/rigid body,%
  ]%
  (b1)
  -- (b2)
  -- (b3)
  -- (b4)
  -- cycle;

%% END platform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% foreground cables

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a1)%
  -- (b1);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a2)%
  -- (b2);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a3)%
  -- (b3);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a4)%
  -- (b4);

\draw[%
    /cdpr/cable,%
    % very thick,%
  ]%
  (a7)%
  -- (b7);

%% END foreground cables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% frame foreground

% right
\draw[%
    draw,%
    fill=White,%
  ]%
  (a1)%
  -- (a2)%
  -- (a6)%
  -- (a5)%
  -- cycle;

% front
\draw[%
    draw,%
    fill=White,%
  ]%
  (a2)%
  -- (a3)%
  -- (a7)%
  -- (a6)%
  -- cycle;

%% END frame foreground
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\end{tikzpicture}
