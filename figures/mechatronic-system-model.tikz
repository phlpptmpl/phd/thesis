%!TEX root=../thesis.tex
%!TEX file=figures/mechatronic-system-model.tikz

\begin{tikzpicture}[%
    node distance=6.00em and 8.00em,%
    on grid,%
    /phlpptmpl/mindmap node/.append style={%
      text width=5.00em,%
      minimum height=3.25em,%
    },%
    /phlpptmpl/mindmap line/.append style={
      ->,%
    },%
    /phlpptmpl/mindmap line annotation/.append style={%
    },%
  ]



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% nodes

\node[%
    /phlpptmpl/mindmap node,%
  ]%
  (numerical control)%
  {\trans{Numerical Control}{Numerische Steuerung}};

\node[%
    /phlpptmpl/mindmap node,%
    below=of numerical control,%
  ]%
  (cascaded control)%
  {\trans{Cascaded Control}{Kaskadenregelung}};

\node[%
    % coordinate,%
    below=of cascaded control,%
    /phlpptmpl/mindmap node,%
    draw=none,%
    fill=none,%
  ]%
  (below cascaded control)%
  {};

\node[%
    /phlpptmpl/mindmap node,%
    right=of cascaded control,%
  ]%
  (electrodynamics)%
  {\trans{Electrodynamics}{Elektrodynamik}};

\draw[%
  ]%
  (electrodynamics.south)%
    node[%
        /phlpptmpl/mindmap node,%
        anchor=north,%
        yshift=\pgflinewidth,%
      ]%
      (ammeter)%
      {\trans{Ammeter}{Amp{\'e}remeter}};

\node[%
    /phlpptmpl/mindmap node,%
    right=10.00em of electrodynamics,%
    % rectangle split,%
    % rectangle split parts=2,%
  ]%
  (winch mechanics)%
  {\trans{Winch Mechanics}{Windenmechanik}};

\draw[%
  ]%
  (winch mechanics.south)%
    node[%
        /phlpptmpl/mindmap node,%
        anchor=north,%
        yshift=\pgflinewidth,%
      ]%
      (position encoder)%
      {\trans{Rotary Encoder}{Drehgeber}};

\node[%
    below=0.50em of position encoder.250,%
    coordinate,%
  ]%
  (below position encoder left)%
  {};

\node[%
    below=1.00em of position encoder.290,%
    coordinate,%
  ]%
  (below position encoder right)%
  {};

\node[%
    right=of winch mechanics,%
    /phlpptmpl/mindmap node,%
  ]%
  (cables)%
  {\trans{Cables}{Seile}};

\node[%
    right=of cables,%
    /phlpptmpl/mindmap node,%
  ]%
  (platform)%
  {\trans{Platform}{Plattform}};

\node[%
    below=of platform,%
    /phlpptmpl/mindmap node,%
  ]%
  (inverse kinematics)%
  {\trans{Inverse Kinematics}{Inverse Kinematik}};

\node[%
    above=of platform,%
    /phlpptmpl/mindmap node,%
  ]%
  (environment)%
  {\trans{Environment}{Umgebung}};

%% END nodes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% connections

\draw[%
    /phlpptmpl/mindmap line,%
  ]%
  (numerical control)%
  -- (cascaded control)%
  node[%
      /phlpptmpl/mindmap line annotation,%
      pos=0.33,%%
      anchor=east,%
    ]%
    {$\actuang_{\ms{\trans{cmd}{strg}}, \cabindex}$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (cascaded control)%
  -- (electrodynamics)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=south,%
      ]%
      {$\vect{\voltage}_{\ms{dq}, \cabindex}$};

\draw[%
    /phlpptmpl/mindmap line,%
  ]%
  (electrodynamics)%
  -- (winch mechanics)%
    node[
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=south,%
      ]%
      {$\torque_{\subservo, \cabindex}$};

\draw[
    /phlpptmpl/mindmap line,%
    arrows={%
      -Stealth[harpoon]%
    },%
  ]%
  (winch mechanics.5)%
  -- (cables.175)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=south,%
      ]%
      {$\actuang_{\subwinch, \cabindex}$};

\draw[
    /phlpptmpl/mindmap line,%
    arrows={%
      -Stealth[harpoon]%
    },%
  ]%
  (cables.185)
  -- (winch mechanics.355)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=north,%
      ]%
      {$\cabforce_{\cabindex}$};

\draw[%
    /phlpptmpl/mindmap vector,%
    thick,%
  ]%
  (cables)%
  -- (platform)%
    node[
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=south,%
      ]%
      {$\cabforces$};

\draw[%
    /phlpptmpl/mindmap vector,%
    arrows={%
      -Stealth[harpoon,swap]%
    },%
  ]%
  (inverse kinematics.82)%
  -- (platform.278)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=west,%
      ]%
      {$\structmat$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (inverse kinematics)%
  -| (cables)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%%
        anchor=south west,%
      ]%
      {$\cablens$};

\draw[%
    /phlpptmpl/mindmap vector,%
  ]%
  (environment)%
  -- (platform)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.33,%%
        anchor=east,%
      ]%
      {$\wrench_{\subgravity}, \wrench_{\subprocess}$};

\draw[%
    /phlpptmpl/mindmap line,%
  ]%
  (ammeter)%
  -| (cascaded control.300)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=south west,%
      ]%
      {$\current_{\ms{dq}, \cabindex}$};

\draw[%
    /phlpptmpl/mindmap line,%
  ]%
  (position encoder.250)%
  -- (below position encoder left)
  -| (cascaded control.270)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=south west,%
      ]%
      {$\actuang_{\ms{\trans{enc}{enk}}, \cabindex}$};

\draw[%
    /phlpptmpl/mindmap line,%
  ]%
  (position encoder.290)%
  -- (below position encoder right)
  -| (cascaded control.240)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.50,%
        anchor=north west,%
      ]%
      {$\dotactuang_{\ms{\trans{enc}{enk}}, \cabindex}$};

\draw[%
    /phlpptmpl/mindmap vector,%
    arrows={
      -Stealth[harpoon,swap]%
    },%
  ]%
  (platform.262)%
  -- (inverse kinematics.98)%
    node[%
        /phlpptmpl/mindmap line annotation,%
        pos=0.5,%
        anchor=east,%
      ]%
      {$\platpose, \dotplatpose, \ddotplatpose$};%
  % -- (platform.335);

%% END connections
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% fits

\begin{scope}[on background layer]

\node[%
    /phlpptmpl/mindmap collection node,%
    fit=(cascaded control) (below cascaded control) (electrodynamics) (ammeter),%
    inner ysep=1.00em,%
    inner xsep=0.50em,%
  ]%
  (electrical subsystem)%
  {};
\node[%
    annotation,%
    anchor=north west,%
  ]%
  at (electrical subsystem.south west)%
  {\trans{Electrical Subsystem}{Elektrisches Teilsystem}};%

% \node[%
%     anchor=south east,%
%     /phlpptmpl/no margin,%
%     /phlpptmpl/normal padding,%
%     % draw,%
%   ]%
%   at (electrical subsystem.north east)%
%   {%
%     \begin{tikzpicture}[%
%         % transform shape,%
%       ]%
%       %
%       \begin{axis}[%
%           % General adjustments
%           % scale only axis,%
%           samples=101,%
%           domain=0:1,%
%           restrict x to domain=0:1,%
%           restrict y to domain=0:1.25,%
%           width=4.00em,%
%           height=2.50em,%
%           no marks,%
%           axis line style={%
%             draw=PrimaryColor,%
%           },%
%           % X-Axis
%           xmin=0,%
%           xmax=1,%
%           xtick=\empty,%
%           xlabel={},%
%           % Y-Axis
%           ymin=0,%
%           ymax=1.25,%
%           ytick=\empty,%
%           ylabel={},%
%         ]
        
%         \pgfmathsetmacro\tfK{1.000}
%         \pgfmathsetmacro\tfT{0.175}
%         \pgfmathsetmacro\tfD{0.975}
%         % \pgfmathsetmacro\tfW{sqrt(1/(\tfT^2))}
%         \pgfmathsetmacro\tfW{1/\tfT}
        
%         \addplot+[] plot(\x, {\tfK*(1-1/(sqrt(1 - \tfD^2))*exp(-\tfD*\tfW*\x)*sin(\tfW*sqrt(1-\tfD^2)*\x+acos(\tfD)))});
      
%       \end{axis}
    
%     \end{tikzpicture}
%   };

\node[%
    /phlpptmpl/mindmap collection node,%
    fit=(winch mechanics) (position encoder) (cables) (platform) (inverse kinematics),%
    inner ysep=1.00em,%
    inner xsep=0.50em,%
  ]%
  (mechanical subsystem)%
  {};
\node[%
    annotation,%
    anchor=north west,%
  ]%
  at (mechanical subsystem.south west)%
  {\trans{Mechanical Subsystem}{Mechanisches Teilsystem}};%

\end{scope}

%% END fits
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{tikzpicture}
