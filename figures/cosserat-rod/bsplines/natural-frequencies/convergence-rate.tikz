%!TEX root=../../../../thesis.tex
%!TEX file=figures/cosserat-rod/bsplines/natural-frequencies/convergence-rate.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
% \setlength{\subplotvsep}{4.00\baselineskip}
\setlength{\subplotwidth}{0.96\linewidth/2 - 2.00\subplothsep}
\setlength{\subplotheight}{0.50\subplotwidth}

\begin{tikzpicture}[
    mark size=2.75pt,%
  ]

\begin{groupplot}[%
    group style={%
      % General stuff
      group name={natural frequencies convergence rates},%
      % Layout
      columns=2,%
      rows=2,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=all,%
      % Y-Axes
      ylabels at=edge left,%
      yticklabels at=edge left,%
    },%
    %
    % General adjustments
    line plot,%
    cycle multiindex* list={%
      /phlpptmpl/linestyles*\nextlist%
      /phlpptmpl/mark list nth*%
    },%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % Title
    every axis title/.append style={%
      yshift=-0.50\baselineskip,%
    },%
    %
    % X-Axis
    % xmin=1,%
    % xmax=100,%
    xmode=log,%
    enlarge x limits=0.05,%
    xlabel={\# Segments $\numsegments / \parentheses{}$},%
    xtickten={0,1,2},%
    xmajorgrids=true,%
    % log x ticks with fixed point,%
    %
    % Y-Axis
    ymin=1e-7,%
    ymax=1,%
    ytickten={-6,-3,0},%
    ymode=log,%
    enlarge y limits=0.05,%
    ylabel={$\parentheses*{ \naturalfrequency_{k} - \bar{\naturalfrequency}_{k} } / \bar{\naturalfrequency}_{k}$},%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    title={$\naturalfrequency_{1} = \SI{0.373629516785}{\radian\per\second}$},
    % Legend style
    legend to name={natural frequencies convergence rates grouplegend},%
    legend style={%
      legend columns=-1,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
  ]
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
\addlegendentry{$\polydeg = 3$}
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-04_convergence.dat};
\addlegendentry{$\polydeg = 4$}
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
\addlegendentry{$\polydeg = 5$}
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};
\addlegendentry{$\polydeg = 6$}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    title={$\naturalfrequency_{2} = \SI{1.49451806714}{\radian\per\second}$},
  ]
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp2,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-04_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp1,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \nextgroupplot[%
%     title={$\naturalfrequency_{3} = \SI{3.362665651066}{\radian\per\second}$},
%   ]
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp3,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp3,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp3,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp3,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    title={$\naturalfrequency_{4} = \SI{5.978072268561}{\radian\per\second}$},
    % ymin=1e-7,%
  ]
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp4,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp4,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-04_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp4,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp4,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    title={$\naturalfrequency_{8} = \SI{23.912289074246}{\radian\per\second}$},
    % ymin=1e-7,%
  ]
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp8,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp8,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-04_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp8,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
\addplot+[%
  ]table[%
      col sep=comma,%
      x=ns,%
      y=wp8,%
    ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \nextgroupplot[%
%     title={$\naturalfrequency_{16} = \SI{95.649156296983}{\radian\per\second}$},
%     xmin=10,%
%     ymin=1e-5,%
%   ]
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp16,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-03_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp16,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-04_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp16,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-05_convergence.dat};
% \addplot+[%
%   ]table[%
%       col sep=comma,%
%       x=ns,%
%       y=wp16,%
%     ]{data/cosserat-rod/bsplines/natural-frequencies/tension-0N/degree-06_convergence.dat};



\end{groupplot}

\node[%
    yshift=-1.25\subplotvsep,%
  ]%
  at ($(natural frequencies convergence rates c1r2.south)!0.5!(natural frequencies convergence rates c2r2.south)$)%
  {%
    \ref{natural frequencies convergence rates grouplegend}%
  };

\end{tikzpicture}
