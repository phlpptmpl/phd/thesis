%!TEX root=../../../../../thesis.tex
%!TEX file=figures/cosserat-rod/bsplines/statics/0-deg/varying-segments-with-error.tikz

%\setlength{\subplothsep}{0.05\linewidth}
%\setlength{\subplotvsep}{3.00\baselineskip}
\setlength{\subplotwidth}{0.75\linewidth/2 - \subplothsep}
\setlength{\subplotheight}{1.00\subplotwidth} % make it square

\begin{tikzpicture}[%
  ]
  
\begin{groupplot}[%
    group style={
      % General stuff
      group name=cable model statics comparison catenary varying segments 0deg radius1,%
      % Layout
      columns=2,%
      rows=1,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=all,%
      yticklabels at=all,%
    },%
    %
    % 
    width=\subplotwidth,%
    height=\subplotheight,%
  ]
  

\pgfmathsetmacro{\cabledegree}{4}
\pgfmathsetmacro{\cablenumsegment}{11}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cartesian shape

\nextgroupplot[
    % General adjustments
    catenary planar,%
    %
    % Axis
    % mark repeat=19,%
    %
    % Legend style
    legend to name={varying-segments:0deg:radius-1.00:catenary:legend},%
    legend style={%
      at={(0.98,0.02)},%
      anchor=south east,%
      legend columns=-1,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
    %
    % X-Axis
    % xmin=0.00,%
    % xmax=1.00,%
    %
    % Y-Axis
    % ymin=-1.00,%
    % ymax=0.00,%
  ]

% Catenary
\addplot+[%
    % mark phase=0,%
  ]%
  table[%
      x=cx,%
      y=cy,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-1/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};
  \label{varying-segments:0deg:radius-1.00:catenary}
  \addlegendentry{catenary}

% Segments = 1
\addplot+[%
    % mark phase=1,%
  ]%
  table[%
      x=sx,%
      y=sy,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-1/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};
  \label{varying-segments:0deg:radius-1.00:1}
  \addlegendentry{$\numsegments = 1$}

% Segments = 5
\addplot+[%
    % mark phase=2,%
  ]%
  table[%
      x=sx,%
      y=sy,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-5/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};
  \label{varying-segments:0deg:radius-1.00:5}
  \addlegendentry{$\numsegments = 5$}

% Segments = 10
\addplot+[%
    % mark phase=3,%
  ]%
  table[%
      x=sx,%
      y=sy,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-10/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};
  \label{varying-segments:0deg:radius-1.00:10}
  \addlegendentry{$\numsegments = 10$}

% Segments = 20
\addplot+[%
    % mark phase=4,%
  ]%
  table[%
      x=sx,%
      y=sy,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-20/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};
  \label{varying-segments:0deg:radius-1.00:20}
  \addlegendentry{$\numsegments = 20$}

%% END cartesian shape
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% error

\nextgroupplot[
    % General adjustments
    catenary error residual vector norm,%
    %
    % Axis
    % mark repeat=19,%
    %
    % X-Axis
    %
    % Y-Axis
    ymode=log,%
]

% Shift to account for missing catenary shape
\pgfplotsset{cycle list shift=1}

% Segments = 1
\addplot+[%
    % mark phase=1,%
    % refstyle={varying-segments:0deg:radius-1.00:1},%
  ]%
  table[%
      x=xi,%
      y=ne,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-1/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};

% Segments = 5
\addplot+[%
    % mark phase=2,%
    % refstyle={varying-segments:0deg:radius-1.00:5},%
  ]%
  table[%
      x=xi,%
      y=ne,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-5/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};

% Segments = 10
\addplot+[%
    % mark phase=3,%
    % refstyle={varying-segments:0deg:radius-1.00:10},%
  ]%
  table[%
      x=xi,%
      y=ne,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-10/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};

% Segments = 20
\addplot+[%
    % mark phase=4,%
    % refstyle={varying-segments:0deg:radius-1.00:20},%
  ]%
  table[%
      x=xi,%
      y=ne,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/material-rubbersteel/degree-\cabledegree/segment-20/unstrained_1_00/angle-0deg/radius-1.00/catenary_comparison.dat};


%% END error
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{groupplot}

\node[%
    yshift=-1.25\subplotvsep,%
  ]%
  at ($(cable model statics comparison catenary varying segments 0deg radius1 c1r1.south)!0.5!(cable model statics comparison catenary varying segments 0deg radius1 c2r1.south)$)%
  {%
    \ref{varying-segments:0deg:radius-1.00:catenary:legend}%
  };

\end{tikzpicture}
