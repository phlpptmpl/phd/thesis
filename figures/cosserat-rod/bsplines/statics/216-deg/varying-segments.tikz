%!TEX root=../../../../../thesis.tex
%!TEX file=figures/cosserat-rod/bsplines/statics/216-deg/varying-segments.tikz

\begin{tikzpicture}[%
  ]

% Natural frequencies
\begin{axis}[%
    % General adjustments
    catenary comparison,%
    %
    % Legend style
    legend style={%
      at={(0.02,0.98)},%
      anchor=north west,%
    },%
    %
    % X-Axis
    xmin=-0.80,%
    xmax=0.00,%
    %
    % Y-Axis
    ymin=-0.60,%
    ymax=0.00,%
  ]


\pgfmathsetmacro{\polydegsample}{4}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% radius 1.00

% \pgfplotsset{cycle list shift=1}

% Catenary
\addplot+[%
    % mark phase=0,%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-1.00/shape_catenary.dat};
  \label{varying-segments:216deg:radius-1.00:radius-1.00:catenary}
  \addlegendentry{catenary}

\addplot+[%
    % mark phase=1,%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-1.00/shape.dat};
  \label{varying-segments:216deg:radius-1.00:radius-1.00:1}
  \addlegendentry{$\numsegments = 1$}

\addplot+[%
    % mark phase=2,%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-5/angle-216deg/radius-1.00/shape.dat};
  \label{varying-segments:216deg:radius-1.00:radius-1.00:5}
  \addlegendentry{$\numsegments = 5$}

\addplot+[%
    % mark phase=3,%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-10/angle-216deg/radius-1.00/shape.dat};
  \label{varying-segments:216deg:radius-1.00:radius-1.00:10}
  \addlegendentry{$\numsegments = 10$}

\addplot+[%
    % mark phase=4,%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-20/angle-216deg/radius-1.00/shape.dat};
  \label{varying-segments:216deg:radius-1.00:radius-1.00:20}
  \addlegendentry{$\numsegments = 20$}

%% END radius 1.00
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% radius 0.80

% Catenary
\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:catenary},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-0.80/shape_catenary.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:1},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-0.80/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:5},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-5/angle-216deg/radius-0.80/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:10},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-10/angle-216deg/radius-0.80/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:20},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-20/angle-216deg/radius-0.80/shape.dat};

%% END radius 0.80
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% radius 0.60

% Catenary
\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:catenary},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-0.60/shape_catenary.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:1},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-1/angle-216deg/radius-0.60/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:5},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-5/angle-216deg/radius-0.60/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:10},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-10/angle-216deg/radius-0.60/shape.dat};

\addplot[%
    % refstyle={varying-segments:216deg:radius-1.00:radius-1.00:20},%
  ]%
  table[%
      x=s1,%
      y=s2,%
      col sep=comma,%
    ]%
    {data/cosserat-rod/bsplines/statics/degree-\polydegsample/segment-20/angle-216deg/radius-0.60/shape.dat};

%% END radius 0.60
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\end{axis}

\end{tikzpicture}
