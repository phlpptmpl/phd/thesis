%!TEX root=../../../../thesis.tex
%!TEX file=figures/cosserat-rod/lagrange/bases-functions/bases.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{0.975\linewidth/3 - 3\subplothsep}
\setlength{\subplotheight}{0.5625\subplotwidth} % make it 16:9

\begin{tikzpicture}

% Natural frequencies
\begin{groupplot}[%
    % Group plot styles
    group style={%
      % General stuff
      group name=natural-frequencies,%
      % Layout
      columns=3,%
      rows=1,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=edge left,%
      yticklabels at=edge left,%
    },%
    % General adjustments
    line plot,%
    % scale only axis,
    width=\subplotwidth,%
    height=\subplotheight,%
    no markers,%
    % Legend
    legend style={%
      at={(0.98,0.02)},%
      anchor=south east,%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
    },%
    % X-Axis
    xmin=0,%
    xmax=1,%
    xlabel={Abscissae $\pathcoord / \parentheses{}$},%
    % Y-Axis
    ymin=-1.25,%
    ymax=1.25,%
    ylabel={$l_{\funcindex, \polydeg} \of{\xi}$},%
]


\nextgroupplot[%
  title={polynomial degree $\polydeg = 1$},%
  % legend to name=grouplegend,%
]
% Degree of Lagrange polynomial to plot
\pgfmathsetmacro\pda{1}
% Plot bases polynomials k = 0,1,...,n
\foreach \ord in {0,1,...,\pda}{
  \addplot+[] table[x=x,y=b\ord,col sep=comma]{data/shapefuns/lagrange/degree-\pda/bases_d0.dat};
  % \addlegendentryexpanded{$b_{\ord,\pda}$};
}


\nextgroupplot[%
  title={polynomial degree $\polydeg = 2$},%
]
% Degree of Lagrange polynomial to plot
\pgfmathsetmacro\pdb{2}
% Plot bases polynomials k = 0,1,...,n
\foreach \ord in {0,1,...,\pdb}{
  \addplot+[] table[x=x,y=b\ord,col sep=comma]{data/shapefuns/lagrange/degree-\pdb/bases_d0.dat};
  % \addlegendentryexpanded{$b_{\ord,\pdb}$};
}


\nextgroupplot[%
  title={polynomial degree $\polydeg = 4$},%
]
% Degree of Lagrange polynomial to plot
\pgfmathsetmacro\pdc{4}
% Plot bases polynomials k = 0,1,...,n
\foreach \ord in {0,1,...,\pdc}{
  \addplot+[] table[x=x,y=b\ord,col sep=comma]{data/shapefuns/lagrange/degree-\pdc/bases_d0.dat};
  % \addlegendentryexpanded{$b_{\ord,\pdc}$};
}

\end{groupplot}

\end{tikzpicture}
