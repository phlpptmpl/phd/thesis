%!TEX root=../../../thesis.tex
%!TEX file=figures/kinematics/wave-equation/global-displacement.tikz

\begin{tikzpicture}[%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% coordinates
\coordinate (xi-start) at (0.00,0.00);
\coordinate (xi-end) at (10.00cm,0.00);

\coordinate (cable-start) at (0.00,2.00cm);
\coordinate (cable-end) at (10.00cm,-1.00cm);
%% END coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% axis
% Axis of reference
\draw[%
    Bracket-Bracket,%
  ]%
  (xi-start)%
  -- (xi-end)%
    node[%
        pos=0.00,%
        anchor=north,%
      ]%
      {$\pathcoord = 0$}%
    node[%
        pos=1.00,%
        anchor=south,%
      ]%
      {$\pathcoord = 1$};%
% Extension of axis
\draw[%
    dashed,%
  ]%
  (xi-start)%
  -- ++(-1.00cm,0.00);
\draw[%
    dashed,%
  ]%
  (xi-end)%
  -- ++(1.00cm,0.00);
%% END axis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% proximal and distal points

% Proximal displacement
\draw[%
    ->,%
  ]%
  (cable-start |- 1.00cm,0.00)%
  -- (cable-start)%
    node[%
        pos=0.50,%
        anchor=east,%
      ]%
      {$\waveboundaryfunc_{\subproximal} \of{t}$};

% Distal displacement
\draw[%
    ->,%
  ]%
  (cable-end |- 1.00cm,0.00)%
  -- (cable-end)%
    node[%
        pos=0.50,%
        anchor=west,%
      ]%
      {$\waveboundaryfunc_{\subdistal} \of{t}$};

% Direct connection between proximal and distal displacement
\draw[%
    -,%
    name path=local base,%
  ]%
  (cable-start)%
  -- (cable-end)%
    node[%
        coordinate,%
        pos=0.60,%
      ]%
      (global displacement function)%
      {};%

\draw[%
  ]%
  (global displacement function |- cable-start)%
    node[%
        anchor=north,%
        /phlpptmpl/tight padding,%
        /phlpptmpl/no margin,%
      ]%
      (global displacement function label)%
      {$r \of{\pathcoord, t}$};%
\draw[%
    -,%
    shorten >=0.50ex,%
  ]%
  (global displacement function label)%
  -- (global displacement function);%
  


%% END proximal and distal points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% cable

\draw[%
    /cdpr/cable,%
    draw=White,%
    double=red,%
    double distance=\pgflinewidth,%
    name path=cable,%
  ]%
  (cable-start)%
  .. controls (3.33cm,2.50cm) and (6.66cm,-1.50)%
  .. (cable-end);%

% A random point on the X-axis
\coordinate (xi star) at ($(xi-start)!0.28!(xi-end)$);
\draw[%
  ]%
  (xi star)%
    node[%
        anchor=north,%
      ]%
      {$\pathcoord^{\ast}$};

% Line that goes vertically up from (xi star)
\path[%
    name path=xi star up,%
  ]%
  (xi star)%
  -- ++(0.00, 2.00cm);%

% Find intersection of xi star and the cable
\path[%
    name intersections={%
      of=cable and xi star up,%
      by=pertubation xi star world,%
    },%
  ];
% Label the world deformation
\draw[%
    ->,%
    dashed,%
  ]%
  (xi star)%
  -- (pertubation xi star world)
    node[%
        pos=1.00,%
        anchor=south west,%
        /phlpptmpl/tight padding,%
        /phlpptmpl/no margin,%
      ]%
      {$\wavepertubation \of{\pathcoord^{\ast}, t}$};

% Find intersection of xi star and the connection between proximal and distal end
\path[%
    name intersections={%
      of=local base and xi star up,%
      by=pertubation xi star local,%
    },%
  ];
% Label the local deformation
\draw[%
    -,%
    arrows={%
      -Stealth[harpoon]%
    },%
  ]%
  (pertubation xi star local)%
  -- (pertubation xi star world)%
    node[%
        pos= 0.50,%
        coordinate,%
      ]%
      (wave pertubation local line)%
      {};%
\draw[%
  ]%
  ($(xi-start)!0.5!(wave pertubation local line)$)%
    node[%
        anchor=south,%
        /phlpptmpl/tight padding,%
        /phlpptmpl/no margin,%
      ]%
      (wave pertubation local label)%
      {$\wavepertubationlocal \of{\pathcoord^{\ast}, t}$};
\draw[%
    shorten >=0.50ex,%
  ]%
  (wave pertubation local label)%
  -- (wave pertubation local line);%
    
%% END cable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{tikzpicture}
