%!TEX root=../../../thesis.tex
%!TEX file=figures/material/zener/step-responses.tikz

\begin{tikzpicture}

% Left axis: Strain => Stress
\begin{axis}[%
    % General adjustments
    material step strain2stress,%
    each nth point=10,%
    %
    % X-axis
    %
    % Y-Axis
  ]

\addplot+[%
  ]%
  table[%
      x=t,%
      y=y,%
      col sep=comma,%
    ]{data/material/zener/step/input_strain/step.dat};
  \label{zener:strain2stress:step}

\end{axis}

% Right axis: Stress => Strain
\begin{axis}[%
    % General adjustments
    material step stress2strain,%
    each nth point=10,%
    %
    % X-axis
    %
    % Y-Axis
  ]

% Add other axis' data to legend
\addlegendimage{refstyle={zener:strain2stress:step}};
\addlegendentry{$\stress* \of{t}$}

\pgfplotsset{cycle list shift=1}
\addplot+[%
  ]%
  table[%
      x=t,%
      y=y,%
      col sep=comma,%
    ]{data/material/zener/step/input_stress/step.dat};
\addlegendentry{$\strain* \of{t}$}

\end{axis}

\end{tikzpicture}
