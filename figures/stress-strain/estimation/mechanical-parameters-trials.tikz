%!TEX root=../../../thesis.tex
%!TEX file=figures/stress-strain/estimation/mechanical-parameters-trials.tikz

%\setlength{\subplothsep}{1.00\baselineskip}
%\setlength{\subplotvsep}{2.50\baselineskip}
\setlength{\subplotwidth}{0.70\linewidth/2}% - \subplothsep}
\setlength{\subplotheight}{1.00\subplotwidth} % make it square

\pgfmathsetmacro{\myboxplotboxextend}{0.20}

\begin{tikzpicture}[%
    boxplot median node/.style={%
      rotate=90,%
      anchor=west,%
      /phlpptmpl/no padding,%
    },%
    this fixed legend image/.style={%
      % /pgfplots/legend image code/.code={%
      %   \draw[%
      %       ##1,%
      %       /tikz/.cd,%
      %       bar width=3pt,%
      %       yshift=-0.2em,%
      %       bar shift=0pt,%
      %     ]%
      %     plot coordinates {(0cm,0.8em) (2*\pgfplotbarwidth,0.6em)};
      % },%
    },%
  ]

\begin{groupplot}[%
    group style={
      % General stuff
      group name=stress strain estimation mechanical parameters trials,%
      % Layout
      columns=2,%
      rows=1,%
      horizontal sep=\subplothsep,%
      vertical sep=\subplotvsep,%
      % X-Axes
      xlabels at=edge bottom,%
      xticklabels at=edge bottom,%
      % Y-Axes
      ylabels at=edge left,%
      yticklabels at=all,%
    },%
    % General adjustments
    boxplot plot,%
    width=\subplotwidth,%
    height=\subplotheight,%
    %
    % Legend
    legend style={%
      /tikz/every even column/.append style={%
        column sep=1.00ex,%
      },%
      /tikz/every odd column/.append style={%
        column sep=0.25ex,%
      },%
      legend columns=-1,%
    },%
    %
    % Boxplot adjustments
    boxplot={
      draw direction=y,%
      box extend=\myboxplotboxextend,%
      every median/.style={%
        solid,%
        /phlpptmpl/thin,%
      },
    },%
    %
    % X-Axis
    xmin=1,%
    xmax=4,%
    xtick={1,2,3,4},%
    enlarge x limits=0.15,%
    %
    % Y-Axis
    ymin=1e-1,%
    ymax=1e2,%
    ymode=log,%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    % General adjustments
    title={Elasticities},%
    %
    % Legend
    legend to name={stress strain estimation mechanical parameters trials grouplegend},%
    %
    % X-Axis
    xticklabels={$\youngsmodulus_{1}$,$\youngsmodulus_{2}$,$\youngsmodulus_{3}$,$\youngsmodulus_{4}$},%
    %
    % Y-Axis
    ylabel={Elasticity $\youngsmodulus_{\iterindex} / \si{\giga\pascal}$},%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 2
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=1-1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=2-1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=EG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
\addlegendentry{$n_{\msmaxwell} = 2$}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 3
% E_1
\addplot+[%
    ybar,%
    boxplot,%
    boxplot/draw position=1,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_2
\addplot+[%
    ybar,%
    boxplot,%
    boxplot/draw position=2,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_3
\addplot+[%
    ybar,%
    boxplot,%
    boxplot/draw position=3,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=EG3,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
\addlegendentry{$n_{\msmaxwell} = 3$}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 4
% E_1
\addplot+[%
    boxplot,%
    boxplot/draw position=1+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG1,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_3
\addplot+[%
    boxplot,%
    boxplot/draw position=3+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=EG3,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% E_4
\addplot+[%
    boxplot,%
    boxplot/draw position=4+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=EG4,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
\addlegendentry{$n_{\msmaxwell} = 4$}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\nextgroupplot[%
    % General adjustments
    title={Viscosities},%
    %
    % X-Axis
    xtick={2,3,4},%
    xticklabels={$\viscousmodulus_{2}$,$\viscousmodulus_{3}$,$\viscousmodulus_{4}$},%
    %
    % Y-Axis
    ylabel={Viscosity $\viscousmodulus_{\iterindex} / \si{\giga\pascal\second}$},%
    axis y line*=right,%
  ]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 2
% N_1
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=1-1.20*\myboxplotboxextend,%
%     area legend,%
%     ybar interval legend,%
%   ] table[%
%       y=NG1,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
%     % node[%
%     %     boxplot median node,%
%     %   ]%
%     %   at (boxplot box cs: \boxplotvalue{median},0.5)
%     %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2-1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=NG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n2_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 3
% N_1
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=1,%
%     area legend,%
%     forget plot,%
%   ] table[%
%       y=NG1,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
%     % node[%
%     %     boxplot median node,%
%     %   ]%
%     %   at (boxplot box cs: \boxplotvalue{median},0.5)
%     %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=NG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_3
\addplot+[%
    boxplot,%
    boxplot/draw position=3,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=NG3,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n3_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N = 4
% N_1
% \addplot+[%
%     boxplot,%
%     boxplot/draw position=1+1.20*\myboxplotboxextend,%
%     area legend,%
%     forget plot,%
%   ] table[%
%       y=NG1,%
%       col sep=comma,%
%     ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
%     % node[%
%     %     boxplot median node,%
%     %   ]%
%     %   at (boxplot box cs: \boxplotvalue{median},0.5)
%     %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_2
\addplot+[%
    boxplot,%
    boxplot/draw position=2+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=NG2,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_3
\addplot+[%
    boxplot,%
    boxplot/draw position=3+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
    forget plot,%
  ] table[%
      y=NG3,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};
% N_4
\addplot+[%
    boxplot,%
    boxplot/draw position=4+1.20*\myboxplotboxextend,%
    area legend,%
    this fixed legend image,%
  ] table[%
      y=NG4,%
      col sep=comma,%
    ] {data/stress-strain/trapezoidal-tf-00150/mechpars_n4_trials_en.dat};
    % node[%
    %     boxplot median node,%
    %   ]%
    %   at (boxplot box cs: \boxplotvalue{median},0.5)
    %     {\pgfmathprintnumber{\boxplotvalue{median}}};


\end{groupplot}

\node[%
    yshift=-\subplotvsep,%
  ]%
  at ($(stress strain estimation mechanical parameters trials c1r1.south)!0.5!(stress strain estimation mechanical parameters trials c2r1.south)$)%
  {%
    \ref{stress strain estimation mechanical parameters trials grouplegend}%
  };

\end{tikzpicture}
