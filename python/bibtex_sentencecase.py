#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def bibtex_sentencecase(p):
    ThesisTool(p).bibtex_sentencecase()


if __name__ == "__main__":
    bibtex_sentencecase(theroot())
