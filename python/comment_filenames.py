#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def comment_filenames(p):
    ThesisTool(p).comment_filenames()


if __name__ == "__main__":
    comment_filenames(theroot())
