#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def clean_media(p):
    ThesisTool(p).clean_media()


if __name__ == "__main__":
    clean_media(theroot())
