#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def clean_csv(p):
    ThesisTool(p).clean_csv()


if __name__ == "__main__":
    clean_csv(theroot())
