#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def clean_dat(p):
    ThesisTool(p).clean_dat()


if __name__ == "__main__":
    clean_dat(theroot())
