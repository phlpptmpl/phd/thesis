#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def clean_mat(p):
    ThesisTool(p).clean_mat()


if __name__ == "__main__":
    clean_mat(theroot())
