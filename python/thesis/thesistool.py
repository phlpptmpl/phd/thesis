import os
import pathlib as pl
import re
import shutil
import titlecase
import urllib.request

import bibtexparser
import prettyprinter
from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter


class ThesisTool(object):

    def __init__(self, bd):
        # Directory where "thesis.tex" is located
        self.basedir = pl.Path(bd).resolve()

        # Directory where all data files are located
        self.datadir = self.basedir / 'data'

    def bibtex_sentencecase(self):
        # get the main bib file
        bibfiles = self.basedir.glob('thesis.bib')
        bibfile = list(bibfiles)[0]

        parser = BibTexParser(common_strings=False)
        parser.ignore_nonstandard_types = False
        parser.homogenise_fields = False

        def abbreviations(word, **kwargs):
            if word.upper() in ('HTML', 'CSS', 'HTTP', 'URL', 'GUI', 'UI', 'ID', 'FAST', 'CDPR', 'IEEE', 'ASME', 'USA', 'ICRA', 'IROS', 'ARK', 'NRAO', 'NAIC', 'ASP', 'UHMWPE', 'ECCOMAS', 'CIRP', 'MARIONET', 'ACC', 'CSME', 'TOR', 'ISSN', 'WDPR', 'ICAE', 'MSC', 'HMPE', 'OCEANS', 'HNMIPE', 'HN1VPE', 'SPIE', 'ICCIA', 'STRING-MAN', 'DLCC', 'NIST', 'ROBOCRANE', 'NURBS', 'INCA', 'PD', 'PID', 'PI', 'LQR', 'LGQ', 'MIMO', 'SISO', 'ROMANSY'):
                return word.upper()
            elif word.lower() in ('und', 'der', 'des', 'die', 'bei'):
                return word.lower()
            elif word.lower() == 'iftomm':
                return 'IFToMM'
            elif word.lower() == 'dof':
                return 'DoF'
            elif word.lower() == 'ipanema':
                return 'IPAnema'
            elif word.lower() == 'cogiro':
                return 'CoGiRo'
            elif word.lower() == 'caro':
                return 'CaRo'
            elif word.lower() == 'cablar':
                return 'CABLAR'
            elif word.lower() == '\\mathcal':
                return 'mathcal'

        with bibfile.open('r') as bibtex_file:
            bib_database = bibtexparser.load(bibtex_file, parser)
        
        keys_to_change = [
            'title',
            'subtitle',
            'booktitle',
            'titleaddon',
            'journaltitle',
            'series',
            'eventtitle',
        ]
      
        for entry in bib_database.entries:
            entry.pop('abstract', None)
            entry.pop('file', None)
            entry.pop('note', None)
            entry.pop('shorthand', None)
            try:
                entry['doi'] = entry['doi'].upper()
            except KeyError:
                pass
            try:
                entry['url'] = entry['url'].lower()
            except KeyError:
                pass
            
            for key_to_change in keys_to_change:
                try:
                    entry[key_to_change] = titlecase.titlecase(entry[key_to_change], callback=abbreviations).replace('Mathcal', 'mathcal').replace('~', '')
                    prettyprinter.cpprint(entry[key_to_change])
                except KeyError:
                    pass

        writer = BibTexWriter()
        writer.indent = '  '  # indent entries with 4 spaces instead of one
        with bibfile.open('w') as bibtex_file:
            bibtex_file.write(writer.write(bib_database))

    def circle_packing(self):
        # Data directory
        ddir = self.datadir / 'material' / 'circle-in-circle'

        # Regular expression to find whitespaces
        r = re.compile('\s+')

        # Download data
        for i in range(1, 30):
            # URL where to find file
            url = 'http://hydra.nat.uni-magdeburg.de/packing/cci/txt/cci{}.txt'.format(i)

            # Filename to save data to
            fn = ddir / 'cci{}.dat'.format(i)

            # Download file and write
            with urllib.request.urlopen(url) as resp, open(fn, 'w') as f:
                # Read response as byte object
                data = resp.read()

                # Build lines of data
                t = ["idx,x,y"] + [r.sub(',', l.strip()) for l in data.decode('utf-8').split("\n")]

                # Write lines to file
                f.write("\n".join(t))

        # File of radii
        rfile = ddir / 'radii.dat'
        # Read the radii
        radii = rfile.read_text().split("\n")

        def write_circle_at_coord(f, x: float, y: float, r: float, cls: str = None):
            f.write('\\draw[%\n')
            if cls is not None:
                f.write('    {},%\n'.format(cls))
            f.write('  ]%\n')
            f.write('  ({x}cm,{y}cm)%\n'.format(x=x, y=y))
            f.write('    circle({r}cm);\n'.format(r=r))

        # Now, find all DAT files and loop over them, creating the corresponding TikZ file
        for i in range(1, 30):
            # Source file
            fs = ddir / 'cci{}.dat'.format(i)
            # Target file
            ft = ddir / 'cci{}.tikz'.format(i)
            # Read all coordinates of the file
            with open(fs, 'r') as fs_:
                coords = [l.strip().split(",") for l in fs_.readlines()]

            # Write TikZ picture file
            with open(ft, 'w') as ft_:
                ft_.write('\\begin{tikzpicture}[%\n')
                ft_.write('    yarn/.style={%\n')
                ft_.write('      /cdpr/cable,%\n')
                ft_.write('      draw=Black,%\n')
                ft_.write('      draw opacity=1.00,%\n')
                ft_.write('      fill=Red,%\n')
                ft_.write('      fill opacity=0.80,%\n')
                ft_.write('    },%\n')
                ft_.write('    assembled yarn/.style={%\n')
                ft_.write('      /cdpr/cable,%\n')
                ft_.write('      draw=Black,%\n')
                ft_.write('      draw opacity=1.00,%\n')
                ft_.write('      fill=Red,%\n')
                ft_.write('      fill opacity=0.60,%\n')
                ft_.write('    },%\n')
                ft_.write('    strand/.style={%\n')
                ft_.write('      /cdpr/cable,%\n')
                ft_.write('      draw=Black,%\n')
                ft_.write('      draw opacity=1.00,%\n')
                ft_.write('      fill=Red,%\n')
                ft_.write('      fill opacity=0.40,%\n')
                ft_.write('    },%\n')
                ft_.write('    rope/.style={%\n')
                ft_.write('      /cdpr/cable,%\n')
                ft_.write('      draw=Black,%\n')
                ft_.write('      draw opacity=1.00,%\n')
                ft_.write('      fill=Red,%\n')
                ft_.write('      fill opacity=0.20,%\n')
                ft_.write('    },%\n')
                ft_.write('  ]%\n')
                ft_.write('  \n')
                ft_.write('\n\n')

                r = float(radii[i - 1])

                write_circle_at_coord(ft_, x=0, y=0, r=1, cls="rope")
                ft_.write('\n')

                for coord in coords[1:]:
                    x = float(coord[1])
                    y = float(coord[2])

                    rr = float(radii[i - 1]) * r

                    write_circle_at_coord(ft_, x=x, y=y, r=r, cls="strand")
                    ft_.write('\n')

                    for coord_ in coords[1:]:
                        xx = x + float(coord_[1]) * r
                        yy = y + float(coord_[2]) * r

                        rrr = float(radii[i - 1]) * rr

                        write_circle_at_coord(ft_, x=xx, y=yy, r=rr, cls="assembled yarn")
                        ft_.write('\n')

                        for coord__ in coords[1:]:
                            xxx = xx + float(coord__[1]) * rr
                            yyy = yy + float(coord__[2]) * rr
                            write_circle_at_coord(ft_, x=xxx, y=yyy, r=rrr, cls="yarn")
                            ft_.write('\n')

                ft_.write('\\end{tikzpicture}\n')

    def clean_file(self, f):
        # Find all matching files
        files = self.datadir.glob('**/*.{}'.format(f))

        # Loop over each MAT file
        for file in files:
            # Delete file
            if file.exists() and file.is_file():
                file.unlink()

    def clean_dir(self, d):
        # Find all directories
        dirs = self.datadir.glob('**/{}'.format(d))

        # Loop over each directory
        for dir in dirs:
            # Delete directory
            if dir.exists() and dir.is_dir():
                shutil.rmtree(str(dir))

    def clean_mat(self):
        self.clean_file('mat')

    def clean_csv(self):
        self.clean_file('csv')

    def clean_tcsv(self):
        self.clean_file('tcsv')

    def clean_media(self):
        self.clean_dir('media')

    def clean_dat(self):
        # Get all "*.DAT" files
        files = self.datadir.glob('**/*.dat')

        # Our regexp pattern to match directory names "YYYYMMDDTHHMMSSmmmmm" or alike
        ptrn = re.compile('.*/\d{4}\d{2}\d{2}.*')

        # Process each file
        for file in files:
            if not ptrn.match(str(file.resolve().parents[0])) is None:
                # Source path
                s = file.resolve();
                # Target path
                t = (file.parent / '..' / file.name).resolve()
                # Move file
                file.rename(t)

        # Get all directories underneath the data directory
        dirs = [x for x in self.datadir.glob('**/') if x.is_dir()]
        for dir in dirs:
            # Check if directory matches name
            if ptrn.match(str(dir.resolve())):
                # Delete
                shutil.rmtree(dir)

    def comment_filenames(self):
        # Extensions to process
        exts = ['tex', 'tikz']

        # Directories to check
        dirs = ['config', 'content', 'copyrights', 'figures', 'listings', 'tables']

        # Loop over all directories to process
        for dir in dirs:
            # Resolve current working directory
            contentdir = (self.basedir / dir).resolve()

            # Get all files
            files = []
            for ext in exts:
                files.extend(contentdir.glob('**/*.{}'.format(ext)))

            # Loop over all `.tex` files
            for file in files:
                # Read file
                content = file.read_text().split('\n')

                # Path from base directory ("thesis.tex") to file
                pathtofile = file.relative_to(self.basedir)

                # Path from file to base directory ("thesis.tex")
                pathtothesis = os.path.relpath(str(contentdir), str(file))

                # "%!TEX root" directive exists, we'll update it
                if content[0].startswith('%!TEX root'):
                    content[0] = "%!TEX root={}/thesis.tex".format(pathtothesis)
                # No "%!TEX root" directive exists, we'll add it
                else:
                    content[0:0] = ["%!TEX root={}/thesis.tex".format(pathtothesis)]

                # "%!TEX file" directive exists, we'll update it
                if content[1].startswith('%!TEX file'):
                    content[1] = "%!TEX file={}".format(pathtofile)
                # "%!TEX file" directive does not exist, we'll add it
                else:
                    content[1:1] = ["%!TEX file={}".format(pathtofile)]

                # And add an empty line between the directives and the content if there is no empty line
                if len(content[2]):
                    content[2:2] = ['']

                # Write file content
                file.write_text('\n'.join(content))
