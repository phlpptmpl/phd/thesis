import subprocess as sp

def theroot():
    proc = sp.Popen(['git rev-parse --show-toplevel'], shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    outs, err = proc.communicate()
    return outs.decode('UTF-8').split('\n')[0]
