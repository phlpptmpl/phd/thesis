#!/usr/bin/env python3

from clean_csv import clean_csv
from clean_dat import clean_dat
from clean_mat import clean_mat
from clean_media import clean_media
from clean_tcsv import clean_tcsv
from comment_filenames import comment_filenames
from thesis.funs import theroot

if __name__ == "__main__":
    r = theroot()
    clean_media(r)
    clean_mat(r)
    clean_tcsv(r)
    clean_csv(r)
    clean_dat(r)
    comment_filenames(r)
    bibtex_sentencecase(r)
