#!/usr/bin/env python3

from thesis.funs import theroot
from thesis.thesistool import ThesisTool


def clean_tcsv(p):
    ThesisTool(p).clean_tcsv()


if __name__ == "__main__":
    clean_tcsv(theroot())
