# Prüfungskomitee

Zum Promotionsverfahren von Herrn Philipp TEMPEL

## Hauptberichter

Andreas POTT  
Schalbruch 67  
D-40721 Hilden  
Germany

Mail: andreas.pott@isw.uni-stuttgart.de  
Fon: +49 (174) 3158229


## Mitberichter


### Nummer 1

Prof. Dr. Bernard HAASDONK  
Pfaffenwaldring 57  
D-70569 Stuttgart  
Germany

Mail: bernard.haasdonk@ians.uni-stuttgart.de  
Fon: +49 (711) 685 x 65542  
Fax: +49 (711) 685 x 65507


### Nummer 2

Marc GOUTTEFARDE, Ph.D.  
Université Montpellier  
LIRMM UMR 5506  
CC477  
161 rue Ada  
34095 Montpellier Cedex 5  
FRANCE

Mail: marc.gouttefarde@lirmm.fr  
Fon: +33 (4) 67 41 85 x 59  
Fax: +33 (4) 67 41 85 x 00


<!-- ### Nummer 3

Prof. Dr.-Ing. Alexander VERL  
Institut für Steuerungstechnik der Werkzeugmaschinen und Fertigungseinrichtungen, ISW  
Seidenstrasse 36  
D-70174 Stuttgart

alexander.verl@isw.uni-stuttgart.de  
Fon: +49 (711) 685 x 82422 -->
