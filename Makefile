LATEX=latexmk
DEPS=thesis.tex $(shell find "./content" -type f -name "*.tex") $(shell find "./figures" -type f -name "*.tikz")
ROOT=thesis
MACROS=config/commands/cdprs.tex config/commands/electrical.tex config/commands/material-models.tex config/commands/math.tex config/commands/mechanics.tex config/commands/quadrature.tex config/commands/spatial-dynamics.tex config/commands/text.tex config/commands/wave-equation.tex
TIKZ=tikz_section1 tikz_section2 tikz_section3 tikz_section4 tikz_section5_1 tikz_section5_2 tikz_section5_3 tikz_section5_4 tikz_section5_5 tikz_section5_6 tikz_section5_7 tikz_section5_8 tikz_section5_10 tikz_section5_11 tikz_section5_12 tikz_section5_13 tikz_section6 tikz_appendix

.PHONY: list
list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: all
all: tikz draft final

.PHONY: tikz
tikz: $(TIKZ)

.PHONY: tikz_section5
tikz_section5: tikz_section5_1 tikz_section5_2 tikz_section5_3 tikz_section5_4 tikz_section5_5 tikz_section5_6 tikz_section5_7 tikz_section5_8 tikz_section5_10 tikz_section5_11 tikz_section5_12 tikz_section5_13

.PHONY: $(TIKZ)
$(TIKZ): tikz_%:
	latexmk $@
	[ ! -f "$@.makefile" ] || make -j 1 -f $@.makefile allimages
	[ ! -f "$@.makefile" ] || latexmk $@
	[ ! -f "$@.makefile" ] || make -j 1 -f $@.makefile allimages
	[ ! -f "$@.makefile" ] || latexmk $@

draft: $(DEPS)
	latexmk thesis

.PHONY: final
final: final_tex
	latexmk final.tex

.PHONY: publisher
publisher: publisher_tex
	latexmk publisher.tex

final_tex: $(DEPS)
	git show $(git branch | grep \* | cut -d ' ' -f2):$(ROOT).tex > final.tex
	sed -i.bak s/draft,/final,/g final.tex

publisher_tex: $(DEPS)
	git show $(git branch | grep \* | cut -d ' ' -f2):$(ROOT).tex > publisher.tex
	sed -i.bak s/draft,/final,/g publisher.tex
	sed -i.bak s/accepted,/accepted,published,/g publisher.tex

clean:
	latexmk -CA *.tex
	[ ! -f "*.makefile" ] || rm *.makefile
	[ ! -f "final.tex" ] || rm final.tex
	[ ! -f "publisher.tex" ] || rm publisher.tex
	rm -f ./tikz/*
