# Default list of files to be proccessed.
@default_files = ('thesis.tex');

# Use lualatex
$pdflatex = 'lualatex --shell-escape --synctex=1 %O %S';

# Always create PDFs
$pdf_mode = 1;

# Try 7 times at maximum then give up
$max_repeat = 7;

# Move all auxiliary files to another folder
# $aux_dir = '.latex';

# Use Preview.app to preview generated PDFs
$pdf_previewer = 'open -a Skim';

# File extensions to remove when cleaning
$clean_ext = 'bbl fdb_latexmk fls nav pdfsync pyg pytxcode run.xml ' .
             'snm synctex.gz synctex(busy) thm upa vrb _minted-%R pythontex-files-%R ' .
             'tdo **/*-eps-converted-to.pdf %R.figlist %R.makefile';

# Git version parsing into document
sub run_gitinfo {
    system("git rev-parse --short=8 HEAD > gitrev.txt");
    system("git log -1 --format=%cd > gitdate.txt");
}
run_gitinfo();

# Glossaries support
push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
push @generated_exts, 'slo', 'sls', 'slg';
push @generated_exts, 'nlo', 'nls', 'nlg';
sub run_makeglossaries {
  if ( $silent ) {
    system("makeglossaries -q $_[0]");
  }
  else {
    system("makeglossaries $_[0]");
  };
}
$clean_ext .= ' %R.ist %R.xdy';
add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');
add_cus_dep('slo', 'sls', 0, 'run_makeglossaries');
add_cus_dep('nlo', 'nls', 0, 'run_makeglossaries');

# PythonTeX support
add_cus_dep('pytxcode', 'tex', 0, 'pythontex');
sub pythontex { return system("pythontex", $_[0]); }
# R (knitr) support
if(grep(/\.(rnw|rtex)$/i, @ARGV)) {
    $pdflatex = 'internal knitrlatex ' . $pdflatex;
    my $knitr_compiled = {};
    sub knitrlatex {
        for (@_) {
            next unless -e $_;
            my $input = $_;
            next unless $_ =~ s/\.(rnw|rtex)$/.tex/i;
            my $tex = $_;
            my $checksum = (fdb_get($input))[-1];
            if (!$knitr_compiled{$input} || $knitr_compiled{$input} ne $checksum) {
                my $ret = system("Rscript", "-e", "knitr::knit('$input')");
                if($ret) { return $ret; }
                rdb_ensure_file($rule, $tex);
                $knitr_compiled{$input} = $checksum;
            }
        }
        return system(@_);
    }
    $clean_ext .= ' %R.tex';
}

no warnings 'redefine';

# Overwrite `unlink_or_move` to support clean directory.
use File::Path 'rmtree';
sub unlink_or_move {
    if ( $del_dir eq '' ) {
        foreach (@_) {
            if (-d $_) {
                rmtree $_;
            } else {
                unlink $_;
            }
        }
    }
    else {
        foreach (@_) {
            if (-e $_ && ! rename $_, "$del_dir/$_" ) {
                warn "$My_name:Cannot move '$_' to '$del_dir/$_'\n";
            }
        }
    }
}
