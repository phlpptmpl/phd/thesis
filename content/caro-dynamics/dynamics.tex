%!TEX root=../../thesis.tex
%!TEX file=content/caro-dynamics/dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamics}\label{sec:caro-dynamics:dynamics}

With the results presented in~\cref{sec:caro-dynamics:statics} showing straightforward transfer of the cable model and framework to simulation statics of \caros, we primarily want to perform forward dynamics simulation of \caros including platform-cable interaction.
As such, we shall assess forward dynamics simulation of \IPAnemaThree within the multibody simulation framework presented in~\cref{sec:caro-dynamics:multibody-cdpr:dynamics} with robot configuration given in~\cref{app:cahp:robot-configurations:ipanema-3}.
With \IPAnemaThree's frame being of rectangular shape with height small compared to width and length, we move the platform within the vertical direction upwards \textIe along the axis of least stiffness.

We initialize the system according to the kinetostatic result presented in~\cref{sec:caro-dynamics:statics:ipanema-3} \textIe with a given cable pre-tension such that slack cables can be best avoided but not ruled out a priori.
Cable set lengths are calculated using the standard inverse kinematics algorithm from~\cref{sec:fundamentals:inverse-kinematics:standard} and adjusted for the additional strain to obtain the initial cable tension.
As cable stiffness is one of the most challenging parameters to obtain from both literature and data sheets or from experimental tests, choosing~${\youngsmodulus = \SI{12.2}{\giga\pascal}}$ is simply for the sake of convenience and making perceivable examples, yet not baseless~(\textCf~\cref{sec:stress-strain:results:estimation:constancy}).

Let the input to our simulation be a vertical upwards motion from home pose~$\platpos_{0} = \platpos^{\ast}$ toward final pose~$\platpos_{T}$~with~$\platpos_{0, \ms{z}} = \SI{0}{\meter}$ and~$\platpos_{T, \ms{z}} = \SI{1.3305}{\meter}$ and~$\platpos_{0, \ms{x}} = \platpos_{0, \ms{y}} = \platpos_{T, \ms{x}} = \platpos_{T, \ms{y}} = 0$.
Transition time~$T$ is set to be~$T = \SI{1}{\second}$ yielding an average vertical velocity of~${\linvel_{\ms{z}} = \SI{1.3305}{\meter\per\second}}$.
To prevent jerky platform motion during start and end of the transition, we make use of the non-analytic smooth function $g : \interval{0}{1} \ni x \mapsto g \of{x}$ reading
%
\begin{align*}
  g \of{x}
    & = \frac{
          f \of{x}
        }{
          f \of{x}
          +
          f \of{1 - x}
        } \,
        ,
    \\
  \shortintertext{with}
  f \of{x}
    & = \begin{cases}
          \pexp*{-\frac{1}{x}}
            & x > 0 \,
            ,
            \\
          0
            & x \leq 0 \,
            ,
        \end{cases}
\end{align*}
%
or equivalently, with transition on interval~$\interval{a}{b}$, by~$x \mapsto g \of{\frac{x - a}{b - a}}$, yielding vertical transition
%
\begin{align*}
  \platpos_{\ms{z}} \of{t}
    & = \platpos_{0, \ms{z}}
        +
        \parentheses*{
          \platpos_{T, \ms{z}}
          -
          \platpos_{0, \ms{z}}
        } \,
        g \of*{
          \frac{
            t - a
          }{
            b - a
          }
        } \,
        ,
\end{align*}
%
with chosen~$a = 1, b = 2$
Further configuration parameters of the framework and integrator can be found in~\cref{tbl:caro-dynamics:dynamics:solver-parameters}.

\begin{figure}[btp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--motion_up--residuals}
  \includegraphics[%
      width=\linewidth,%
      axisratio=3.25,%
    ]{caro-dynamics/named/ipanema-3/motion_up/residuals}
  \tikzexternaldisable
  \caption[%
      Forward dynamics simulation of \IPAnemaThree: Newton's iterations.
    ]{%
      Progression of residual norm and Newton's step size solving forward dynamics given a vertical upwards motion of \IPAnemaThree.
      Maximum iteration limit was set to~${\iterindex_{\ms{max}} = \num{100}}$, while maximum halving of Newton's steps was set to~$\num{10}$ bisections \textIe~$\newtondamping_{\ms{min}} = 1 / \pow[10]{2} = 1 / 1024$.
    }
  \label{fig:caro-dynamics:dynamics:residuals}
\end{figure}

Observing convergence of the energy and momentum preserving integrator during solving the forward dynamics problem~(see~\cref{fig:caro-dynamics:dynamics:residuals}) shows efficient convergence below the threshold of $\threshold_{\residual*} = \num{5e-9}$~(\textCf~\cref{tbl:caro-dynamics:dynamics:solver-parameters}).
As we expect, during the simulation's steady state \textIe for $t < \SI{1}{\second}$, Newton's method converges within one step, though the damped Newton's method requires reduction of the step size by up to four halving during the first~$\iterindex = 6$ steps.
Once motion starts, solving the residual equation converges below the residual threshold with a maximum of $\iterindex_{\ms{max}} = 5$ iterations, despite the low residual threshold.
In all cases, Newton's method terminated due to too low residual error~$\threshold_{\residual*}$ rather than too small step size~$\threshold_{\iterstep}$.
The system integrator shows good performance for solving the discrete nonlinear root-finding problem in the system's next state~(\textCf~\cref{eqn:caro-dynamics:multibody-cdpr:dynamics:discrete-form}) as it keeps well below the maximum number of iterations and provides convergence within a low count of iterations.

\begin{figure}[btp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--motion_up--platform}
  \includegraphics[%
      width=\linewidth,%
      axisratio=3.00,%
    ]{caro-dynamics/named/ipanema-3/motion_up/platform}
  \tikzexternaldisable
  \caption[%
      Forward dynamics simulation of \IPAnemaThree: platform motion.
    ]{%
      Vertical position of platform center during forward dynamics showing oscillating motion as the transition ends.
      Since the system is neither physics-based nor numerically damped, vertical oscillation of the platform results around mean of~$\SI{0.599084014868586}{\meter}$.
      Deviation from the desired position of~$\SI{1.3305}{\meter}$ results from incorrect selection of cable elasticity, one of the biggest issues.
    }
  \label{fig:caro-dynamics:dynamics:platform}
\end{figure}

Time evolution of the platform's point of reference over time results in an oscillating response of the platform~(\textCf~\cref{fig:caro-dynamics:dynamics:platform}) which is to be expected due to lack of physics-based or numerical damping.
Positional error and tracking error are not negligible as it averages at~$\Delta z = \SI{0.731415985}{\meter}$, however, improper selection of cable elasticities is to blame for that.
While the cable model itself is capable of representing linear cable elasticity\footnote{%
  Whether linear elasticity is the correct model for describing \Dyneema cables is disregarded for this chapter.
  Refer to~\cref{chap:stress-strain-dynamics} for more insight into this topic.
} under the assumption of linearity for different cable lengths, it may simply be too elastic for the given \caro use case.
Despite difficulty of selecting cable elasticity, the effect of interaction between cables and platform can be seen from the oscillating response of the platform's point of reference, much like also the cables' midpoints show oscillation~(see~\cref{fig:caro-dynamics:dynamics:cable-midpoints}).

\begin{figure}[btp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--motion_up--cable-centerpositions}
  \includegraphics[%
      width=\linewidth,%
      axisratio=3.50,%
    ]{caro-dynamics/named/ipanema-3/motion_up/cable-centerpositions}
  \tikzexternaldisable
  \caption[%
      Forward dynamics simulation of \IPAnemaThree: cable elevations.
    ]{%
      Elevation of cables' mid-points~$\curve_{\cabindex} \of{\pathcoord = 0.5}$ during forward dynamics showing likewise oscillating motion after transition stops.
      Magnitude of vibration is largest at the cable's mid-point, despite cable length changes.
    }
  \label{fig:caro-dynamics:dynamics:cable-midpoints}
\end{figure}

Lastly, despite the iterative integration schemed derived by~\citeauthor{Betsch.2006} being energy conservative, we also investigate the conservation of energy of the simulation itself.
Sum of potential and kinetic energies remains zero when the \caro is steady before motion starts~(see~\cref{fig:caro-dynamics:dynamics:cable-energies}).
Once motion starts, we can see potential energies of the lower cables $i = \parentheses*{5, \dotsc, 8}$ reducing resulting from decrease in tension as the cables must elongate in order to allow for the platform to move upwards.
Likewise, potential energies of the upper four cables $i = \parentheses*{1, \dotsc, 4}$ increases as their length must reduce to lift the platform upwards resulting in increased tension.
After motion input ends at $t = \SI{2}{\second}$, the system is left to oscillate freely under only external, gravitational forces which is when constant transfer from potential energies to kinetic energies and back is triggered.
Interestingly, kinetic energies oscillate two times faster than potential energies, which can be led back to the motion reversal points of the platform at highest and lowest peak of oscillation.

\begin{figure}[btp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--motion_up--energies}
  \includegraphics[%
    ]{caro-dynamics/named/ipanema-3/motion_up/energies}
  \tikzexternaldisable
  \caption[%
      Forward dynamics simulation of \IPAnemaThree: energies.
    ]{%
      Kinetic and potential energies of cables and platform during forward dynamics simulation of \IPAnemaThree.
      Potential energy comprises cable strain and bending energies, as well as gravitational energies of cables and platform.
      Kinetic energies minimize at maximum and minimum potential energy \textIe whenever the platform is at its upper or lower point or motion reversal.
    }
  \label{fig:caro-dynamics:dynamics:cable-energies}
\end{figure}
