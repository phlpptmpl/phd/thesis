%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/multibody-framework/statics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Solving the Statics}\label{sec:caro-dynamics:multibody-cdpr:statics}

Determining the solution to the static case of~\cref{eqn:caro-dynamics:multibody-cdpr:full-model} at some time value~$t_{0}$ implies solving the static\footnote{We may alternatively also find the dynamic equilibrium solution in case the velocities and accelerations are unequal zero.
The methods to solving the static equilibrium apply similarly to solving the dynamic equilibrium.
However, the static equilibrium is of more interest to the analysis of \caros.} equilibrium state of the \caro \textIe velocities~$\dotgenq$ and accelerations~$\ddotgenq$ are equal zero, thus all dynamic forces vanish and we obtain the simplified set of nonlinear equations
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:statics:equilibrium}
\begin{align}
  \zeros
    & = -\forcevec_{\msexternal} \of{\genq, t_{0}}
        +
        \forcevec_{\msinternal} \of{\genq, t_{0}}
        +
        \transpose{
          \jacposconstraints
        } \of{\genq, t_{0}} \,
        \lagrangemultips \,
        ,
    \\
  \zeros
    & = \posconstraints \of{\genq, t_{0}} \,
        ,
    \\
  \intertext{or equivalently in full residual form}
  \zeros
    & = \residual \of{\genq, \lagrangemultips}
      = \begin{bmatrix}
          -\forcevec_{\msexternal} \of{\genq, t_{0}}
          +
          \forcevec_{\msinternal} \of{\genq, t_{0}}
          +
          \transpose{
            \jacposconstraints
          } \of{\genq, t_{0}} \,
          \lagrangemultips
          \\
          \posconstraints \of{\genq, t_{0}}
        \end{bmatrix} \,
        ,
        \label{eqn:caro-dynamics:multibody-cdpr:statics:equilibrium:residual}
\end{align}
\end{subequations}
%
to be solved concurrently for the unknown state~$\genq$ and unknown constraint forces~$\lagrangemultips$.
Denoting the vector of sought-for variables~$\itervar \coloneqq \transpose{ \left[ \transpose{\genq}, \transpose{\lagrangemultips} \right]}$, we are looking for the solution~$\actual{\itervar} = \transpose{ \left[ \transpose{{\actual{\genq}}}, \transpose{{\actual{\lagrangemultips}}} \right] }$ to the root-finding problem of~\cref{eqn:caro-dynamics:multibody-cdpr:statics:equilibrium} satisfying~$\residual \of{ \actual{\itervar} } \equiv \zeros$.

Literature provides a wide variety of different methods for solving root-finding problems of underdetermined, overdetermined, or constrained systems~(see~\cref{app:chap:root-finding} for an overview of root-finding algorithms), with the most common and simple method being \emph{Newton's method}, or its extension \emph{Damped Newton's method}, sometimes referred to as \emph{Softened Newton's method}.
Since Newton's method is an iterative solution finding algorithm, it requires calculating the~$\iterindex$-th iteration step~$\iterstep_{\iterindex}$ such that~$\itervar_{\iterindex + 1}$ is closer to the analytical residual~$\actual{\itervar}$ than~$\itervar_{\iterindex}$ is.
In Newton's method, the~$\iterindex$-th iteration step~$\iterstep_{\iterindex}$ is calculated from
%
\begin{align*}
  \iterstep_{\iterindex}
    & = - \inv*{ \jacobian[\itervar]{\residual} } \of{\itervar_{\iterindex}} \,
          \residual \of{\itervar_{\iterindex}} \,
          ,
    \\
  \shortintertext{where}
  \jacobian[\itervar]{\residual} \of{\itervar_{\iterindex}}
    & = \left.
          \pd{
            \residual \of{\itervar}
          }{
            \itervar
          }
        \right|_{\itervar = \itervar_{\iterindex}} \,
        \\
  \intertext{such that the iteration rule reads}
  \itervar_{\iterindex + 1}
    & = \itervar_{\iterindex} + \iterstep_{\iterindex} \,
        .
\end{align*}

In particular, the iteration matrix~$\jacobian[\itervar]{\residual} \of{\itervar_{\iterindex}}$ reads
%
\begin{align*}
  \jacobian[\itervar]{\residual} \of{\itervar_{\iterindex}}
    & = \begin{bmatrix}
          -\pd{
            \forcevec_{\msexternal}
          }{
            \genq
          }
          +
          \pd{
            \forcevec_{\msinternal}
          }{
            \genq
          }
          +
          \sum\limits_{\funcindex = 1}^{\numposconstraints}{
            \lagrangemultips_{\funcindex} \,
            \pd[2]{
              \posconstraints_{\funcindex}
            }{
              \genq
            }
          }
            & \transpose{\jacposconstraints}
          \\
          \jacposconstraints
            & \zeros
        \end{bmatrix} \,
        .
\end{align*}

Due to numerical rounding precision, the value of the analytical residual value~$\actual{\itervar}$ of~\cref{eqn:caro-dynamics:multibody-cdpr:statics:equilibrium:residual} cannot be calculated, thus a measure of ``being close to~$\actual{\itervar}$'' must be introduced.
According to Newton's method, being close to the residual occurs if either
%
\begin{inparaenum}[1)]
  \item the residual~$\residual \of{\itervar_{\iterindex}}$ is sufficiently small, or
  \item the step size~$\iterstep_{\iterindex}$ is sufficiently small.
\end{inparaenum}
%
Sufficiently small translates to the residual or step size norm being smaller than a user-defined threshold such that the termination conditions of Newton's method read
%
\begin{align*}
  \norm{ \residual \of{\itervar_{\iterindex}} }
    & \leq \threshold_{\residual*} \,
      ,
    \\
  \norm{ \iterstep_{\iterindex} }
    & \leq \threshold_{\iterstep} \,
      .
\end{align*}
%
The value of residual and step-size threshold~$\threshold_{\residual*}$ and~$\threshold_{\iterstep}$, respectively, must be determined manually and should also account for any numerical round off issues~(values we chose can be found in~\cref{tbl:caro-dynamics:statics:solver-parameters}).

\begin{table}[tbp]
  \centering
  \smaller[1]
  \caption[%
    Solver parameters for solving kinematic and kinetostatic equilibria.
  ]{%
    Solver parameters for solving kinematic and kinetostatic equilibria.
  }
  \label{tbl:caro-dynamics:statics:solver-parameters}
  \input{tables/caro-dynamics-solver-parameters-statics}
\end{table}
