%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/multibody-framework/dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Solving the Dynamics}\label{sec:caro-dynamics:multibody-cdpr:dynamics}

The dynamic solution to~\cref{eqn:caro-dynamics:multibody-cdpr:full-model} may be obtained similarly to the static solution taking into account to solve for the unknown accelerations~$\ddotgenq$ and constraint forces~$\lagrangemultips$.
However, since the formulation of the dynamics equation reads
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:dynamics:system}
\begin{align}
  \zeros
    & = \Mass \of{\genq} \,
        \ddotgenq
        -
        \forcevec_{\msexternal} \of{\genq, \dotgenq}
        +
        \forcevec_{\msinternal} \of{\genq, \dotgenq}
        +
        \transpose{
          \jacposconstraints
        } \of{\genq, t} \,
        \lagrangemultips \,
      ,
      \label{eqn:caro-dynamics:multibody-cdpr:dynamics:system:dynamics}
    \\
  \zeros
    & = \posconstraints \of{\genq, t} \,
      ,
      \label{eqn:caro-dynamics:multibody-cdpr:dynamics:system:constraints}
\end{align}
\end{subequations}
%
we have to deal with a system of \acrfullpl{DAE} that are \acrfullpl{ODE} in the state of generalized coordinates~$\vectset{ \genq ; \dotgenq ; \ddotgenq }$ and algebraic equations in the generalized positions~$\genq$.

Obtaining the solution to~\cref{eqn:caro-dynamics:multibody-cdpr:dynamics:system} is an involved task, since simple forward-in-time integration is inapplicable as the transient state may violate constraints.
Especially in the case of multibody simulations with rigid and flexible bodies, this is a challenge as the coupled nature of dynamics results in differential equations with a high ratio of stiffness\footnote{The stiffness ratio is defined as the ratio between the real parts of the largest and smallest eigenvalue of a linear constant coefficient inhomogeneous system.
In other words, it describes the ratio between the fastest transient and the slowest transient.
The ratio is mathematically expressed as~${ \abs{ \Re \of{\overline{\eigenvalue}}} } / { \abs{ \Re \of{ \underline{\eigenvalue}}} }$~\autocite{Lambert.1991}.} requiring explicit \gls{ODE}-solvers to proceed with small integration time steps~\autocite{Arnold.2004}.
This is apparent in explicit forward-in-time integration schemes such as Euler's method or higher-order extensions such as Runge-Kutta or Adams-Bashforth-Moulton.
Commercially available solvers for \glspl{ODE} out of the box integrate a first-order differential equation of nonlinear form~$\dotstate = f \of{\state, t}$ or linear form~$\dotstate = \matr{A} \, \state + \matr{B} \, \vect{u}$ by means of a Taylor series of the transient state at the next time step~\autocite{Stoer.2002}.
Since this Taylor series is based on the integration step size, a rapid transient state leads to large errors in the \gls{ODE}'s Taylor approximation, thus requiring the solver to reduce the step size, which results in either slow time integration or in failure of meeting integration tolerances.

Besides this numerical burden, commercially available \gls{ODE} solvers are unaware of the underlying structure of nature of the system-to-integrate.
While any higher order system can be easily transformed into a system of first-order \glspl{ODE}, it removes the physical meaning of states and their inherent connection.
Looking at a dynamic system from Hamiltonian mechanics, a system's velocities are the cotangents to its positions, as such they are inherently linked through their conjugate momentums~\autocite{Ardema.2005}.
When using a first-order \gls{ODE} solver on such systems, which treats the transient changes of positions and velocities independent of each other thus ignoring their physical meaning and connection, the transient states are merely integrated over time, and constraint violation is unavoidable\footnote{This is but one reason for the introduction of constraint violation handling methods such as commonly used Baumgarte stabilization~\autocite{Baumgarte.1972}.}.

To the rescue in finding numerical solutions to the \gls{FDP} of the presented \caro multibody simulation comes a different type of integration schemes.
The integration schemes of interest are mechanical-based integrators or Hamiltonian\hyp{}based integrators~(see~\cref{app:chap:solving-daes}).
In particular, we implement~\citeauthor{Betsch.2005} energy-consistent mechanical integrator for mechanical systems with mixed holonomic and and nonholonomic constraints~\autocite{Betsch.2005,Betsch.2006}.
We briefly present the iterative integration scheme for better understanding of the implied simplicity of the multibody formulation, its derivation and properties can be found in~\cref{sec:appendix:solving-daes:integrators:betsch}.

Let us assume our mechanical system to be subject to holonomic and nonholonomic constraints given by the \gls{DAE}
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:dynamics:full-system}
\begin{align}
  \Mass \of{\genq} \,
  \ddotgenq
  +
  \transpose{
    \jacobian{\energypotential} \of{\genq}
  }
    & = \transpose{\jacposconstraints} \of{\genq} \,
        \posconstraintforces \,
        ,
  \\
  \zeros
    & = \posconstraints \of{\genq} \,
        ,
\end{align}
\end{subequations}
%
where~$\jacobian{\energypotential} = \pd{\energypotential}{\genq}$ are forces derived from their potential energy formulation.
For the equation of motion given in~\cref{eqn:caro-dynamics:multibody-cdpr:dynamics:system} follows
%
\begin{align*}
  \jacobian{\energypotential}
    & = \forcevec_{\msinternal}
        -
        \forcevec_{\msexternal} \,
        .
\end{align*}

The conserving scheme over a time interval~$T_{\iterindex} = \interval{t_{\iterindex}}{t_{\iterindex + 1}}$ at time~$t_{\iterindex}$ with states~$\vectset{\genq_{\iterindex}; \genv_{\iterindex}}$ given and unknowns~$\vectset{\genq_{\iterindex + 1} ; \genv_{\iterindex + 1}}$ reads
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:dynamics:discrete-form}
\begin{align}
  \genq_{\iterindex + 1}
  -
  \genq_{\iterindex}
    & = \frac{
          \stepsize
        }{
          2
        } \,
        \parentheses*{
          \genv_{\iterindex}
          +
          \genv_{\iterindex + 1}
        } \,
    \\
  \Mass \,
  \parentheses*{
    \genv_{\iterindex + 1}
    -
    \genv_{\iterindex}
  }
    & = -
        \stepsize \,
        \discretenabla \energypotential \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        +
        \stepsize \,
        \transpose{
          \discretenabla \posconstraints \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        } \,
        \bar{\posconstraintforces} \,
        ,
    \\
  \posconstraints \of{ \genq_{\iterindex + 1} }
    & = \zeros \,
      ,
\end{align}
\end{subequations}
%
for which the generally time-dependent constraint forces~$\posconstraintforces \of{t}$,~$\velconstraintforces \of{t}$ are substituted by constant approximations~$\bar{\posconstraintforces}$ and~$\bar{\velconstraintforces}$, respectively.
\Cref{eqn:caro-dynamics:multibody-cdpr:dynamics:discrete-form} can be rewritten in vectorial form yielding the set of nonlinear equations in~$\genq_{\iterindex + 1}$ and~$\posconstraintforces_{\iterindex + 1}$ which can again be solved using a root-finding algorithm like Newton's method with appropriate termination thresholds on the residual and step size~(values found suitable for the \caro simulation framework presented in this thesis can be found in~\cref{tbl:caro-dynamics:dynamics:solver-parameters}).

\begin{table}[tbp]
  \centering
  \smaller[1]
  \caption[%
    Solver parameters for solving forward dynamics equilibria.
  ]{%
    Solver parameters for solving forward dynamics equilibria.
  }
  \label{tbl:caro-dynamics:dynamics:solver-parameters}
  \input{tables/caro-dynamics-solver-parameters-dynamics}
\end{table}

Despite~\cref{eqn:caro-dynamics:multibody-cdpr:dynamics:discrete-form} being a nonlinear, implicit equation in~$\vectset{ \genq_{\iterindex + 1} ; \lagrangemultips_{\iterindex + 1} }$, its beauty lies in it being a single equation that can be iteratively solved for the solution of the dynamics in the next time step by also ensuring satisfying the constraints.
The iterative scheme does not suffer from numerical instabilities as explicit Newton-Euler methods do, and additionally satisfies energy conservation \textIe neither the discrete equations of motion nor the iteration scheme add or remove energy from the system.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Platform Kinematics}\label{sec:caro-dynamics:multibody-cdpr:dynamics:platform}

Let the mobile platform be mechanically parametrized by linear inertia matrix~${\Mass_{\subplatform} = \mass_{\subplatform} \, \eye[3]}$ and rotational inertia matrix~${\inertiatensor_{\subplatform} = \diag \of{\inertia_{xx}, \inertia_{yy}, \inertia_{zz}}}$\footnote{Explicit parameters for the test case \caros are given in~\cref{app:chap:robot-configurations}.}.
The platform's state is defined by the generalized coordinates~${ \genq_{\subplatform} = \transpose{ \left[ x, y, z, \phi, \theta, \psi \right] } }$ following the equations of motion
%
\begin{align*}
  \begin{bmatrix}
    \Mass_{\subplatform}
      & \zeros
      \\
    \zeros
      & \platrot \,
        \inertiatensor_{\subplatform} \,
        \transpose{\platrot}
  \end{bmatrix} \,
  \ddotgenq_{\subplatform}
    & = \forcevec_{\subplatform, \subgravity}
        +
        \transpose{\jacposconstraints_{\subplatform}} \of{\genq, t} \,
        ,
    \\
  \zeros
    & = \posconstraints_{\subplatform} \of{\genq, t} \,
        ,
\end{align*}
%
with gravitational force vector~$\forcevec_{\subplatform, \subgravity}$, the constraints~$\posconstraints_{\subplatform} \of{\genq, t}$ as given in~\cref{eqn:caro-dynamics:multibody-cdpr:platform:constraints}, and the rotation matrix~$\platrot$ given through Tait-Bryan angles~$\vectset{ \phi ; \theta ; \psi }$.
We choose~$\numcable = 8$ cables.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Cable Kinematics}\label{sec:caro-dynamics:multibody-cdpr:dynamics:cable}

In~\cref{sec:spatial-dynamics:discretization:varying-length}, we derived the equation of motion for a single cable under the consideration of time-varying length.
By applying the integral transformation from time-varying interval~$\interval{0}{\cablen}$ to~$\interval{0}{1}$, we obtain the first and second derivative of cable length~$\cablen$ with respect to time~$t$.
For numerical evaluation of the dynamic response of the \caro, we thus require calculating not only desired cable lengths along a spatial trajectory, but also their derivatives.
To simplify calculations but also to highlight the effect of using standard inverse kinematics for control with elastic and flexible cables of time-varying length, we use the solution to the standard \gls{IKP}.
The~$\cabindex$-th cable's length is calculated from%
%
\begin{align*}
  \cablen_{\cabindex}
    & = \norm{ \cabdirn_{\cabindex} }
      = \sqrt{
          \dotp{
            \cabdirn_{\cabindex}
          }{
            \cabdirn_{\cabindex}
          }
        } \,
        ,
    \\
\intertext{resulting in}
  \dotcablen_{\cabindex}
  = \od{
      \cablen_{\cabindex}
    }{
      t
    }
    & = \frac{
          1
        }{
          \sqrt{
            \dotp{
              \cabdirn_{\cabindex}
            }{
              \cabdirn_{\cabindex}
            }
          }
        } \,
        \dotp*{
          \cabdirn_{\cabindex}
        }{
          \dotcabdirn_{\cabindex}
        } \,
        ,
    \\
  \ddotcablen_{\cabindex}
  = \od[2]{
      \cablen_{\cabindex}
    }{
      t
    }
    & = \frac{
          \dotp{
            \cabdirn_{\cabindex}
          }{
            \cabdirn_{\cabindex}
          }
        }{
          \pow[3]{
            \sqrt{
              \dotp{
                \cabdirn_{\cabindex}
              }{
                \cabdirn_{\cabindex}
              }
            }
          }
        } \,
        \dotp*{
          \cabdirn_{\cabindex}
        }{
          \dotcabdirn_{\cabindex}
        }
        +
        \frac{
          1
        }{
          \sqrt{
            \dotp{
              \cabdirn_{\cabindex}
            }{
              \cabdirn_{\cabindex}
            }
          }
        } \,
        \parentheses*{
          \dotp{
            \dotcabdirn_{\cabindex}
          }{
            \dotcabdirn_{\cabindex}
          }
          +
          \dotp{
            \cabdirn_{\cabindex}
          }{
            \ddotcabdirn_{\cabindex}
          }
        } \,
        ,
  \\
  \shortintertext{where}
  \cabdirn_{\cabindex}
    & = \frameanchor_{\cabindex}
        -
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
        } \,
        ,
    \\
  \dotcabdirn_{\cabindex}
    & = -\parentheses*{
          \dotplatpos
          +
          \dotplatrot \,
          \platanchor_{\cabindex}
        } \,
        ,
    \\
  \ddotcabdirn_{\cabindex}
    & = -\parentheses*{
          \ddotplatpos
          +
          \ddotplatrot \,
          \platanchor_{\cabindex}
        } \,
    \\
  \shortintertext{and}
  \dotplatrot
    & = \skewm{\angvel} \,
        \platrot \,
        ,
    \\
  \ddotplatrot
    & = \skewm{\angacc} \,
        \platrot
        +
        \skewm{\angvel} \,
        \skewm{\angvel} \,
        \platrot \,
        .
\end{align*}
