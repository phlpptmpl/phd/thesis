%!TEX root=../../thesis.tex
%!TEX file=content/caro-dynamics/statics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Statics}\label{sec:caro-dynamics:statics}

Before presenting results of forward dynamics simulation, we shift our interest to the solution of statics and kinetostatics.
The main difference between statics and kinetostatics lies in the latter not neglecting the force equilibrium of the platform and the cables, but also respecting the strain induced cable elongation.
In case of the static solution, we determine the unstrained cable lengths~$\cablens_{\subunstrained}$ given the \caro home pose~$\platpose^{\ast} = \vectset{ \platpos^{\ast} ; \platrot^{\ast} }$ defined through
%
\begin{align*}
  \platpos^{\ast}
    & = \begin{bmatrix}
          0
          \\
          0
          \\
          0
        \end{bmatrix} \,
        ,
    & \platrot^{\ast}
    & = \begin{bmatrix}
          1
            & 0
            & 0
            \\
          0
            & 1
            & 0
            \\
          0
            & 0
            & 1
        \end{bmatrix} \,
        ,
\end{align*}
%
using standard inverse kinematics formulation from~\cref{sec:fundamentals:inverse-kinematics:standard}.
This procedure will inevitably lead to platform deflection due to cable elongation and bending, which is what we want to provoke with this analysis.

For the kinetostatic analysis, we determine the unstrained cable length for the same \caro home pose, yet in ways that the platform may not deflect from its home pose.
Internally, we solve the kinetostatic problem from~\cref{sec:fundamentals:statics} with the goal of minimizing unstrained cable lengths~$\cablen_{\subunstrained}$, while respecting lower cable force limits~$\cabforcesmin$ and upper cable force limits~$\cabforcesmax$, in order to yield values comparable to other methods of determining cable force distributions.
We define our kinetostatic problem over the optimization vector of cable lengths~$\vect{x} \coloneqq \cablens$ as
%
\begin{subequations}\label{eqn:caro-dynamics:statics:kinetostatic-optimization-problem}
\begin{align}
    &
    & 
    & \min_{\vect{x}} \norm{ \vect{x} }
    \\
  \text{subject to}
    & 
    & \zeros
    & = -
        \forcevec_{\msexternal} \of{\genq \of{\vect{x}}, t_{0}}
        +
        \forcevec_{\msinternal} \of{\genq \of{\vect{x}}, t_{0}}
        +
        \dotsb
    \\
    &
    &
    & \phantom{=}
        +
          \transpose{
            \jacposconstraints
          } \of{\genq \of{\vect{x}}, t_{0}} \,
          \lagrangemultips \,
          ,
    \\
    &
    & \zeros
    & = \posconstraints \of{\genq \of{\vect{x}}, t_{0}} \,
        ,
    \\
    &
    & \zeros
    & = \platpos^{\ast}
        -
        \platpos \of{\vect{x}} \,
        ,
    \\
    &
    & \zeros
    & = \platrot^{\ast}
        -
        \platrot \of{\vect{x}} \,
        ,
    \\
    &
    & \cabforcesmin
    & \leq \cabforces \of{\vect{x}} \,
        ,
    \\
    &
    & \cabforces \of{\vect{x}}
    & \leq \cabforcesmax \,
        ,
\end{align}
\end{subequations}
%
where~$\genq \of{\vect{x}}$ are the generalized coordinates of the \caro confining with the static equilibrium for the given cable lengths and~$\cabforces \of{\vect{x}}$ are the cable forces obtained from the respective constraint forces of the static equilibrium.
In the end, we obtain unstrained cable lengths that satisfy a kinetostatic equilibrium for the desired home pose under the given cable force limits.
We may introduce additional constraints in or different constraints on the minimization problem like minimizing the $L^{p}$ norm of total cable tension or relative cable tensions~\autocite{Gosselin.2011,Borgstrom2009c}.

Examples of kinematic and kinetostatic solving will comprise \caros
%
\begin{inparaenum}[a)]
  \item \CoGiRo in~\cref{sec:caro-dynamics:statics:cogiro},
  \item \Expo in~\cref{sec:caro-dynamics:statics:expo},
  \item \IPAnemaMini in~\cref{sec:caro-dynamics:statics:ipanema-mini}, and
  \item \IPAnemaThree in~\cref{sec:caro-dynamics:statics:ipanema-3}.
\end{inparaenum}
%
Results of each \caro will be presented in the respective section, while a summary of applicability of the framework will be drawn at the very end of this section.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/statics/cogiro}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/statics/expo}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/statics/ipanema-mini}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/statics/ipanema-3}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/statics/conclusions}
