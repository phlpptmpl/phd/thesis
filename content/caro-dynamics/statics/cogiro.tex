%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/statics/cogiro.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[CoGiRo]{\CoGiRo}\label{sec:caro-dynamics:statics:cogiro}

Robot configuration for \CoGiRo can be found in~\cref{tbl:chap:robot-configurations:cogiro:geometry,tbl:chap:robot-configurations:cogiro:cable,tbl:chap:robot-configurations:cogiro:platform}.
Cable force limits are defined to be~$\cabforces \in \SIinterval{0}{5000}{\newton}$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinematic Case}

In the kinematic case, \CoGiRo's mobile platform deflects from its desired home pose by~${ \Delta x = \SI{7.86022257}{\milli\meter} }$, ${ \Delta y = \SI{8.389717314}{\milli\meter} }$, and ${ \Delta z = \SI{272.079606}{\milli\meter} }$ which is a noticeable deviation from the zero-pose.
This deviation is to be expected due to steel cables being employed on \CoGiRo with high stiffness.
Further, despite steel cables being comparatively heavy, no further vertical deflection of the mobile platform is implied as there are no cables dragging down on the platform as \CoGiRo is in a suspended configuration.
As such, the mobile platform's weight is the only mass producing a downward drag.
Lastly, fairly no cable sag can be concluded from the numerical results, which confines partially with observations made on \CoGiRo.
As pointed out by~\citeauthor{Gagliardini.2015}, visible cable sag emerges on the robot which cannot be compensated for by tightening the cables due to the suspended nature of \CoGiRo~\autocite{Collard.2011,ditSandretto.2012,Gagliardini.2015}.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--cogiro--statics--residuals}
  \includegraphics[%
      width=1.00\linewidth,%
      axisratio=3.00,%
    ]{caro-dynamics/named/cogiro/statics/residuals}
  \tikzexternaldisable%
  \caption[%
    Progression of residual solving kinematic equilibrium for \CoGiRo.
  ]{%
    Progression of residual norm~$\norm{\residual_{\iterindex}}$ over iteration solving the kinematic equilibrium case for \CoGiRo.%
  }
  \label{fig:caro-dynamics:statics:cogiro:residuals}
\end{figure}

From a numerical perspective, with a well pre-conditioned simulation, the solution is obtained within~$\iterindex = 11$ steps~(see~\cref{fig:caro-dynamics:statics:cogiro:residuals}) with initially slow rate of convergence\textemdash resulting from the constraint forces being initialized with zero and only slowly converging toward the final value\textemdash yet later, once constraint forces are obtained, termination occurs after four more steps.
Even with a numerically ill-conditioned system such as \CoGiRo due to its wide range of numerical stiffnesses stemming from the high mass of cables and their high modulus of elasticity versus the low platform mass, the \caro framework is well capable of handling even suspended \caros inherently, without requiring specific treatment of their special configuration.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinetostatic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--cogiro--kinetostatics--shape}
  \includegraphics[%
      width=\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/cogiro/kinetostatics/shape}
  \tikzexternaldisable%
  \caption[%
    Force-constrained, kinetostatic equilibrium of \CoGiRo.
  ]{%
    Static solution to the kinetostatically solved \caro \CoGiRo.
    Cable sag is negligible for the robot is suspended and thus all cables are strained no matter the platform pose.
  }
  \label{fig:caro-dynamics:kinetostatics:cogiro:shape}
\end{figure}

Kinetostatically solving \CoGiRo, the mobile platform deflects from its desired home pose by only~${ \Delta x = \SI{-0.032184}{\milli\meter} }$, ${ \Delta y = \SI{0.019163}{\milli\meter} }$, and ${ \Delta z = \SI{-1.425073}{\milli\meter} }$, which is a smaller deviation than a purely kinematically solved \CoGiRo~(see also~\cref{fig:caro-dynamics:kinetostatics:cogiro:shape}).
Horizontal deviations~$\Delta x$ and~$\Delta y$ result from marginally asymmetric frame design~(see~\cref{tbl:chap:robot-configurations:cogiro:geometry}).
Since \CoGiRo is a suspended \caro, there exists only one valid force distribution for a given pose since the cable forces must balance the external wrench at the pose.
Any increase or decrease of the inner level of pre-tension\textemdash or likewise an increase or decrease in stiffness\textemdash is not possible without deflecting from the desired position.
For \CoGiRo, we can however observe the effect of hefty cables not only by the amount of sagging of each cable, but also by the induced deflection from the desired position and the actual position when determining cable lengths purely based on the standard kinematics model~(see~\cref{sec:fundamentals:inverse-kinematics:standard}).
Large discrepancy of force distribution from~\citeauthor{Pott2013}'s advanced closed form and the results given in~\cref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-forces} are due to the fact that the former algorithm is suitable for only kinematically solving the force distribution but not kinetostatically.
\Caro configurations like \CoGiRo, however, can only be balanced kinetostatically.

\begin{table}[tbp]
  \centering
  \caption[%
      Kinematic and kinetostatic cable lengths and forces of~\CoGiRo.
    ]{%
      Comparison of cable lengths~\subref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-lengths} and cable forces~\subref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-forces} of kinematic~$\parentheses{}_{\mskinematic}$ and kinetostatic~$\parentheses{}_{\mskinetostatic}$ solution for \CoGiRo's home pose.
    }
  \label{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable}
  \begin{subtable}[t]{0.40\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/cogiro/cablelengths}%
    \caption{%
      Cable lengths~$\cablen_{\cabindex, \parentheses{}}$ and differences~${\delcablen_{\cabindex} = \cablen_{\cabindex, \mskinetostatic} - \cablen_{\cabindex, \mskinematic}}$.
    }
    \label{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-lengths}
  \end{subtable}%
  \hfill%
  \begin{subtable}[t]{0.58\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/cogiro/forces}%
    \caption{%
      Cable forces~$\cabforce_{\cabindex, \parentheses{}}$ and differences~${\Delta \cabforce_{\cabindex} = \cabforce_{\cabindex, \mskinetostatic} - \cabforce_{\cabindex, \mskinematic}}$
    }
    \label{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-forces}
  \end{subtable}%
\end{table}

\Cref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable} shows comparison of cable lengths obtained for the kinematic and kinetostatic problem of \CoGiRo~(\cref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-lengths}) and of cable forces at home pose~(\cref{tbl:caro-dynamics:statics:cogiro:kinematic-kinetostatics:cable-forces}).
As we expect, negative cable length changes of all cables result from the kinetostatic problem, in order to attain the desired pose and reduce position deviation to a minimum.
However, cable length change is not equal over all cables due to the asymmetric design of \CoGiRo.
Cable forces on the other hand change in both positive and negative direction, despite all cables being extended.
Most notably is the tendency of all cable forces being close to their mean value as the platform design in itself is nearly symmetric~(see~\cref{tbl:chap:robot-configurations:cogiro:geometry}).
Previous large differences in cable forces apparently result in the observed kinematic platform deviation.
We can further see the cable forces determined through the kinetostatic problem solving lie within reason with cable forces determined through the advanced closed form force distribution algorithm introduced in~\cref{sec:fundamentals:cable-force-distribution}.
Obvious discrepancy results from the force distribution algorithm calculating only cable forces rather than also minimizing unstrained cable length as the case study does~(see~\cref{eqn:caro-dynamics:statics:kinetostatic-optimization-problem}).
