%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/statics/ipanema-mini.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[IPAnema Mini]{\IPAnemaMini}\label{sec:caro-dynamics:statics:ipanema-mini}

Robot configuration for \IPAnemaMini can be found in~\cref{tbl:chap:robot-configurations:ipanema-mini:geometry,tbl:chap:robot-configurations:ipanema-mini:cable,tbl:chap:robot-configurations:ipanema-mini:platform}.
Cable force limits are defined to be~$\cabforces \in \SIinterval{0}{40}{\newton}$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinematic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-mini--statics--shape}
  \includegraphics[%
      width=0.80\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/ipanema-mini/statics/shape}
  \tikzexternaldisable%
  \caption[%
    Kinematic equilibrium of \IPAnemaMini.
  ]{%
    Static solution to the purely kinematically solved \caro \IPAnemaMini.
    Despite low platform inertia, lower four cables begin to sage and deviate from the straight line between both ends.
  }
  \label{fig:caro-dynamics:statics:ipanema-mini:shape}
\end{figure}

The smallest case study of \caros is given by \IPAnemaMini, with a workspace of close to~$\SI{1x1x1}{\meter}$~(see~\cref{tbl:chap:robot-configurations:ipanema-mini:geometry}).
Despite small magnitude of its geometrical and mechanical parameters, and thus relatively short unstrained cable lengths, kinematic solution to the static equilibrium of \IPAnemaMini show likewise sagging cables and a linear platform deflections of~${ \Delta x = \SI{-1.085582}{\milli\meter} }$, ${ \Delta y = \SI{1.586598}{\milli\meter} }$, and ${ \Delta z = \SI{-3.930322}{\milli\meter} }$~(see~\cref{fig:caro-dynamics:statics:ipanema-mini:shape}).
In fact, asymmetric deflection along~$\evecx$ and~$\evecy$ result from minor frame asymmetry.
Comparing kinematic perturbation of \IPAnemaMini with \textEg \IPAnemaThree or \Expo yields comprehensible understanding of the dimensionality of effects on \caros.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-mini--statics--residuals}
  \includegraphics[%
      width=1.0\linewidth,%
      axisratio=3.00,%
    ]{caro-dynamics/named/ipanema-mini/statics/residuals}
  \tikzexternaldisable%
  \caption[%
    Decay of residual solving kinematic equilibrium for \IPAnemaMini.
  ]{%
    Decay of residual norm~$\norm{\residual_{\iterindex}}$ during iteration solving the kinematic equilibrium case for \IPAnemaMini.
    Small iteration step sizes initially slow down the iteration process due to large Jacobian.
  }
  \label{fig:caro-dynamics:statics:ipanema-mini:residuals}
\end{figure}

Much like \IPAnemaThree~(see~\cref{sec:caro-dynamics:statics:ipanema-3}), \IPAnemaMini also features large iteration numbers for the purely kinematic static equilibrium with~${\iterindex \leq 47}$, due to the small step size of Newton's method over a wide range of iterations~(see~\cref{fig:caro-dynamics:statics:ipanema-mini:residuals}).
This numerical limitation can only be partially overcome by \textEg initializing the constraint forces by means of some force distribution algorithm~(see~\cref{sec:fundamentals:cable-force-distribution}), however, care needs to be taken as the resulting force values and directions may differ largely from the such obtained values\footnote{Not for \IPAnemaMini, as its geometry is rather confined, however, for \textEg \IPAnemaThree this may well emerge.}.
Another way to overcome this limitation were by increasing cable stiffness, yet this is mostly impracticable since the physical cable's stiffness ought be approximated.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinetostatic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-mini--kinetostatics--shape}
  \includegraphics[%
      width=0.80\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/ipanema-mini/kinetostatics/shape}
  \tikzexternaldisable%
  \caption[%
    Force-constrained, kinetostatic equilibrium of \IPAnemaMini.
  ]{%
    Static solution to the kinetostatically solved \caro \IPAnemaMini.
  }
  \label{fig:caro-dynamics:kinetostatics:ipanema-mini:shape}
\end{figure}

Kinetostatically solving the equilibrium at \IPAnemaMini's home pose yields, again, zero deflection of the platform, stemming from the twofold level of redundancy allowing for increasing \caro pre-tension without deflecting the platform~(see~\cref{fig:caro-dynamics:kinetostatics:ipanema-mini:shape}).
However, we observe that for \caros of dimensions similar to \IPAnemaMini, the resulting cable tension lies very close to the upper force limits, when defining the cost functional as minimizing cable length~(see~\cref{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-forces}).
This is, though, a mere numerical effect as the residual stays well above the residual threshold for low cable tensions, thus the iterative solver yields minimal cable lengths with nearly maximum cable tensions.
Nevertheless, initially slack cables \numrange{5}{8} are tensed through the kinetostatic procedure with the resulting cable tensions being nearly equal amongst all cables~(\textCf~\cref{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-lengths}).
Minor deviations result from marginally asymmetric frame and platform dimensions.

\begin{table}[tbp]
  \centering
  \caption[%
    Kinematic and kinetostatic cable lengths and forces of \IPAnemaMini.%
  ]{%
    Comparison of cable lengths~\subref{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-lengths} and cable forces~\subref{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-forces} of kinematic~$\parentheses{}_{\mskinematic}$ and kinetostatic~$\parentheses{}_{\mskinetostatic}$ solution for \IPAnemaMini's home pose.
  }
  \label{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable}
  \begin{subtable}[t]{0.40\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/ipanema_mini/cablelengths}
    \caption{%
      Cable lengths~$\cablen_{\cabindex, \parentheses{}}$ and differences~${\delcablen_{\cabindex} = \cablen_{\cabindex, \mskinetostatic} - \cablen_{\cabindex, \mskinematic}}$.
    }
    \label{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-lengths}
  \end{subtable}%
  \hfill%
  \begin{subtable}[t]{0.58\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/ipanema_mini/forces}
    \caption{%
      Cable forces~$\cabforce_{\cabindex, \parentheses{}}$ and differences~${\Delta \cabforce_{\cabindex} = \cabforce_{\cabindex, \mskinetostatic} - \cabforce_{\cabindex, \mskinematic}}$
    }
    \label{tbl:caro-dynamics:statics:ipanema-mini:kinematic-kinetostatics:cable-forces}
  \end{subtable}%
\end{table}
