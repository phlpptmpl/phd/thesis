%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/statics/ipanema-3.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[IPAnema 3]{\IPAnemaThree}\label{sec:caro-dynamics:statics:ipanema-3}

Robot configuration for \IPAnemaThree can be found in~\cref{tbl:chap:robot-configurations:ipanema-3:geometry,tbl:chap:robot-configurations:ipanema-3:cable,tbl:chap:robot-configurations:ipanema-3:platform}.
Cable force limits are defined to be~$\cabforces \in \SIinterval{100}{3000}{\newton}$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinematic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--statics--shape}
  \includegraphics[%
      width=\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/ipanema-3/statics/shape}
  \tikzexternaldisable%
  \caption[%
    Kinematic equilibrium of \IPAnemaThree.
  ]{%
    Static solution to the purely kinematically solved \caro \IPAnemaThree.
    Visible cable sag emerges for lower for cables as their geometric distance becomes shorter than their kinematic length.
  }
  \label{fig:caro-dynamics:statics:ipanema-3:shape}
\end{figure}

\IPAnemaThree uses cables with lower elastic modulus than the previously presented \caros, as such cable elongation is a more prevailing effect, as well as cable vibration.
Solving \IPAnemaThree kinematically, we obtain linear deflections of the platform pose as large as~${ \Delta x = \SI{10.386328}{\milli\meter} }$, ${ \Delta y = \SI{9.363453}{\milli\meter} }$, and ${ \Delta z = \SI{-307.754271}{\milli\meter} }$, highlighting the effect of reduced cable stiffness.
We can further observe apparent cable sag on cables \numrange{5}{7} ultimately causing platform deflection~(see~\cref{fig:caro-dynamics:statics:ipanema-3:shape}).
In particular, the platform's vertical deflection is largely due to the fact of cable elasticity, with the upper four cables straining so much, that the lower four cables are no longer supporting the platform.
Asymmetric platform drift, again, results from asymmetric frame dimensions~(see~\cref{tbl:chap:robot-configurations:ipanema-3:geometry}).

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--statics--residuals}
  \includegraphics[%
      width=1.0\linewidth,%
      axisratio=3.00,%
    ]{caro-dynamics/named/ipanema-3/statics/residuals}
  \tikzexternaldisable%
  \caption[%
    Decay of residual solving kinematic equilibrium for \IPAnemaThree.
  ]{%
    Decay of residual norm~$\norm{\residual_{\iterindex}}$ during iteration solving the kinematic equilibrium case for \IPAnemaThree.
    Due to small steps of the iterative Newton's method, convergence is initially slow until~$\iterindex \geq 40$.
  }
  \label{fig:caro-dynamics:statics:ipanema-3:residuals}
\end{figure}

Solving the kinematic equilibrium of \IPAnemaThree yields, despite the simulation framework being initialization initialized equally as in the previous two cases, lower iterations emerge with~${\iterindex \leq 43}$.
Since cable elasticity is larger for the \IPAnemaThree cables, the initially decoupled multibody simulation requires fewer iterations for solving since the iteration steps are of larger magnitude.
In addition, with reduced system stiffness, the inverse Jacobian used in Newton's iteration results in smaller step sizes thus causing longer iterations till the static residual norm is breached.
Even in this case of a large-scale \caro with slow iterations, the iteration count is rather deterministic and does not breach through the maximum iteration count.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinetostatic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--ipanema-3--kinetostatics--shape}
  \includegraphics[%
      width=\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/ipanema-3/kinetostatics/shape}
  \tikzexternaldisable%
  \caption[%
    Force-constrained, kinetostatic equilibrium of \IPAnemaThree.
  ]{%
    Static solution to the kinetostatically solved \caro \IPAnemaThree.
    Cable sag of the lower four cables has mostly vanished to a nearly negligible value due to the strain on the cables.
  }
  \label{fig:caro-dynamics:kinetostatics:ipanema-3:shape}
\end{figure}

Since \IPAnemaThree is an \gls{RRPM}, we can make use of the \caro's twofold redundancy allowing for introducing an internal stiffness or level or pre-tension into it.
As such, in the kinetostatic case, the platform deflection can be completely eliminated\footnote{Up to double precision of floating point numbers.} while keeping the cable tensions within their limits~(see~\cref{fig:caro-dynamics:kinetostatics:ipanema-3:shape,tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-forces}.).
We can, however, observe non-negligible elongation of all cables of up to~$\maxnorm{\delcablen_{\cabindex}} = \SI{0.970}{\meter}$ in order to attain both the platform position as well as to minimize cable length and satisfying cable force constraints~(see~\cref{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-lengths}).
Similarly, winding cables of \IPAnemaThree results in noticeable increase of cable tension of the lower four cables \numrange{5}{8}, which were initially near slack.
Since the home pose of \IPAnemaThree is near the lower workspace boundary~(see~\cref{tbl:chap:robot-configurations:ipanema-3:geometry} and \autocite{Pott2012b,Kraus2015d}), the same four cables require larger tension to attain the low platform position.

\begin{table}[tbp]
  \centering
  \caption[%
      Kinematic and kinetostatic cable lengths and forces of~\IPAnemaThree.
    ]{%
      Comparison of cable lengths~\subref{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-lengths} and cable forces~\subref{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-forces} of kinematic~$\parentheses{}_{\mskinematic}$ and kinetostatic~$\parentheses{}_{\mskinetostatic}$ solution for \IPAnemaThree's home pose.
    }
  \label{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable}
  \begin{subtable}[t]{0.40\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/ipanema_3/cablelengths}
    \caption{%
      Cable lengths~$\cablen_{\cabindex, \parentheses{}}$ and differences~${\delcablen_{\cabindex} = \cablen_{\cabindex, \mskinetostatic} - \cablen_{\cabindex, \mskinematic}}$.
    }
    \label{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-lengths}
  \end{subtable}%
  \hfill%
  \begin{subtable}[t]{0.58\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/ipanema_3/forces}
    \caption{%
      Cable forces~$\cabforce_{\cabindex, \parentheses{}}$ and differences~${\Delta \cabforce_{\cabindex} = \cabforce_{\cabindex, \mskinetostatic} - \cabforce_{\cabindex, \mskinematic}}$
    }
    \label{tbl:caro-dynamics:statics:ipanema-3:kinematic-kinetostatics:cable-forces}
  \end{subtable}%
\end{table}
