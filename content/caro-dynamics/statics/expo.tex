%!TEX root=../../../thesis.tex
%!TEX file=content/caro-dynamics/statics/expo.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[Expo]{\Expo}\label{sec:caro-dynamics:statics:expo}

Robot configuration for \Expo can be found in~\cref{tbl:chap:robot-configurations:expo:geometry,tbl:chap:robot-configurations:expo:cable,tbl:chap:robot-configurations:expo:platform}.
Cable force limits are defined to be~$\cabforces \in \SIinterval{200}{2400}{\newton}$.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinematic Case}

\Expo \caros are very similar to \CoGiRo in that they both are suspended \caros, however, \CoGiRo has a particular layout of connecting cables to the platform\footnote{Surprisingly, this so-called \CoGiRo-design makes it the only \caro with a range of rotation about the vertical axis of approximately~$\psi \in \SIinterval{-50}{50}{\degree}$~\autocite{Gouttefarde2007}.}.
Still, the deflection of the kinematically solved problem for \Expo \caro is noticeable with~${ \Delta x = \SI{0.363933}{\milli\meter} }$, ${ \Delta y = \SI{-18.890222}{\milli\meter} }$, and ${ \Delta z = \SI{-12.785254}{\milli\meter} }$, and no cable sag emerges.
Since \Expo \caros also use steel cables~(with a copper core to transmit electrical power to the platform), drag of the cables on the platform is inevitable.
This cable drag results in the positive displacement of the platform in the kinematic case\textemdash \Expo's frame design is asymmetric about the~$\evecy$-axis, yet is symmetrical and trapezoidal about the~$\evecx$-axis~(see~\cref{tbl:chap:robot-configurations:expo:geometry}).

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--expo--statics--residuals}
  \includegraphics[%
      width=1.0\linewidth,%
      axisratio=3.00,%
    ]{caro-dynamics/named/expo/statics/residuals}
  \tikzexternaldisable%
  \caption[%
    Decay of residual solving kinematic equilibrium for \Expo.
  ]{%
    Decay of residual norm~$\norm{\residual_{\iterindex}}$ during iteration solving the kinematic equilibrium case for \Expo.%
  }
  \label{fig:caro-dynamics:statics:expo:residuals}
\end{figure}

Numerically solving the kinematics problem of \Expo \caros is similarly quick as for \CoGiRo since both robots share similar frame and platform dimensions, much like inertia parameters of the platform and mechanical cable properties~(see~\cref{fig:caro-dynamics:statics:expo:residuals,tbl:chap:robot-configurations:expo:platform,tbl:chap:robot-configurations:expo:cable}).
Further speed improvements may only be achieved by initializing the constraint forces on the platform and cable proximal and distal ends by means of compensating for the gravitational forces acting on the platform.


 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Kinetostatic Case}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{caro-dynamics--named--expo--kinetostatics--shape}
  \includegraphics[%
      width=\linewidth,%
      axisratio=1.00,%
    ]{caro-dynamics/named/expo/kinetostatics/shape}
  \tikzexternaldisable%
  \caption[%
    Force-constrained, kinetostatic equilibrium of \Expo.
  ]{%
    Static solution to the kinetostatically solved \caro \Expo.
    Cable sag is negligible for the robot is suspended and thus all cables are strained no matter the platform pose.
  }
  \label{fig:caro-dynamics:kinetostatics:expo:shape}
\end{figure}

Kinetostatically solving the \Expo \caro yields ultimate platform position deflections of~${ \Delta x = \SI{-0.001851}{\milli\meter} }$, ${ \Delta y = \SI{-9.2e-05}{\milli\meter} }$, and ${ \Delta z = \SI{0.002608}{\milli\meter} }$, which renders the platform at precisely the desired position~(\cref{fig:caro-dynamics:kinetostatics:expo:shape}).
Compensating for the kinematic deviation results in cables~$1, 4, 5, 8$ being shortened and cables~$2, 3, 6, 7$ being lengthened~(see~\cref{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-lengths}), which results in the previously horizontal deflection in direction of~$+\evecy$ to be reduced.
In addition, the initially, vertically deeper placed platform is lifted upwards and positioned closer to the desired position.
Due to the asymmetric design of \Expo's frame and platform design, cables~$2, 3$ require the largest cable tension in order to compensate for platform roll~(rotation about~$\evecx$)~(see~\cref{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-forces}).
Kinematically, the same two cables' forces were the lowest contributing most to the platform's deflection.
Cable forces of cables 1 and \numrange{4}{8} are all within the range of~$\SI{200}{\newton}$ with cables~$5$ and~$8$ being under higher tension due to the downward drag of the platform.
Similar observations during commissioning were reported, too~\autocite{Tempel.2015g}.

\begin{table}[tbp]
  \centering
  \caption[%
    Kinematic and kinetostatic cable lengths and forces of \Expo.
  ]{%
    Comparison of cable lengths~\subref{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-lengths} and cable forces~\subref{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-forces} of kinematic~$\parentheses{}_{\mskinematic}$ and kinetostatic~$\parentheses{}_{\mskinetostatic}$ solution for \Expo's home pose.
  }
  \label{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable}
  \begin{subtable}[t]{0.40\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/expo2015/cablelengths}
    \caption{%
      Cable lengths~$\cablen_{\cabindex, \parentheses{}}$ and differences~${\delcablen_{\cabindex} = \cablen_{\cabindex, \mskinetostatic} - \cablen_{\cabindex, \mskinematic}}$.
    }
    \label{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-lengths}
  \end{subtable}%
  \hfill%
  \begin{subtable}[t]{0.58\linewidth}
    \centering
    \smaller[1]
    \input{tables/cdpr/named/kinematic-kinetostatics/expo2015/forces}
    \caption{%
      Cable forces~$\cabforce_{\cabindex, \parentheses{}}$ and differences~${\Delta \cabforce_{\cabindex} = \cabforce_{\cabindex, \mskinetostatic} - \cabforce_{\cabindex, \mskinematic}}$
    }
    \label{tbl:caro-dynamics:statics:expo:kinematic-kinetostatics:cable-forces}
  \end{subtable}%
\end{table}
