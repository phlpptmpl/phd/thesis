%!TEX root=../../thesis.tex
%!TEX file=content/caro-dynamics/multibody-framework.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Multibody Cable Robot Formulation}\label{sec:caro-dynamics:multibody-cdpr}

Simulations carried out in this section require numerically solving equations of statics or numerically integrating equations of motion to obtain results.
In order to accomplish this task, we propose a multibody simulation framework for solving of the statics and dynamics of \caros.
The challenging task in multibody simulation, in particular when dealing with kinematically redundant systems like \caros with more cables than \gls{DOF} of the mobile platform \textIe~$\numcable > \dof$, \textCf~\cref{sec:fundamentals:redundancy}, the kinematics and dynamics may not be simplified in terms of a small set of independent coordinates.
While splitting coordinates describing a multibody system into independent and dependent coordinates is a common task, it is not applicable to the \caro framework presented in this thesis.

Let us assume a \caro with a single platform of~$\dof$ \gls{DOF}~($\dof \equiv 3$ in the planar case,~$\dof \equiv 6$ in the spatial case) with its dynamics described in residual form\footnote{Explicit derivation of the equations of motion and the set of generalized coordinates follows in~\cref{sec:caro-dynamics:multibody-cdpr:dynamics:platform}.}
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:platform:eqom}
\begin{align}
  \zeros
    & = \Mass_{\subplatform} \of{\genq_{\subplatform}} \,
        \ddotgenq_{\subplatform}
        -
        \forcevec_{\msexternal, \subplatform}
        +
        \forcevec_{\msinternal, \subplatform}
        +
        \transpose{
          \jacposconstraints_{\subplatform}
        } \of{\genq_{\subplatform}, t} \,
        \lagrangemultips_{\subplatform} \,
        ,
    \\
  \zeros
    & = \posconstraints_{\subplatform} \of{\genq_{\subplatform}, t} \,
      ,
\end{align}
\end{subequations}
%
much like presented in~\cref{sec:fundamentals:dynamics} except free of any off-center point of reference \textIe the mass is concentrated at the platform's point of reference.
In addition, the platform is subject to geometric constraints~$\posconstraints_{\subplatform} \of{\genq_{\subplatform}, t}$ following from it being linked to the cables at each of its~$l$ anchors such that
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:platform:constraints}
\begin{align}
  \posconstraints_{\subplatform}
    & = \transpose{
          \begin{bmatrix}
            \transpose{ \posconstraints_{\subplatform, 1} } ,
              & \transpose{ \posconstraints_{\subplatform, 2} } ,
              & \dotso
              & \transpose{ \posconstraints_{\subplatform, l} } ,
          \end{bmatrix}
        } \,
        ,
    \\
  \posconstraints_{\subplatform, \cabindex}
    & = \platpos \of{\genq_{\subplatform}}
        +
        \platrot \of{\genq_{\subplatform}} \,
        \platanchor_{\cabindex}
        -
        \linpos_{\cabindex} \of{t} \,
        .
\end{align}
\end{subequations}
%
Each position constraint is of dimension~$\posconstraints_{\subplatform, \cabindex} \in \mathds{R}^{\dof_{\ms{dim}}}$ with~$\dof_{\ms{dim}} \equiv 2$ in the planar, and~$\dof_{\ms{dim}} \equiv 3$ in the spatial case.
The total amount of nonlinear geometric constraints sums to~$\numposconstraints = \dof_{\ms{dim}} \, \numcable$.
It can generally be assumed that the number of platform anchors matches the number of cables \textIe no two cables are attached to one point.
While this is a mere assumption out of convenience, it does not limit applicability of the simulation framework presented.

Each of the~$\numcable$ cables can similarly be described through its equations of motion following~\cref{eqn:spatial-dynamics:discretization:forces} reading
%
\begin{subequations}%\label{sec:caro-dynamics:multibody-cdpr:cable:eqom}
\begin{align*}
  \zeros
    & = \Mass_{\subcable_{\cabindex}} \of{\genq_{\subcable_{\cabindex}}} \,
        \ddotgenq_{\subcable_{\cabindex}}
        -
        \forcevec_{\msexternal, \subcable_{\cabindex}}
        +
        \forcevec_{\msinternal, \subcable_{\cabindex}}
        +
        \transpose{
          \jacposconstraints_{\subcable_{\cabindex}}
        } \of{\genq_{\subcable_{\cabindex}}, t} \,
        \lagrangemultips_{\subcable_{\cabindex}} \,
        ,
    \\
  \zeros
    & = \posconstraints_{\subcable_{\cabindex}} \of{\genq_{\subcable_{\cabindex}}, t} \,
      .
\end{align*}
\end{subequations}
%
Cable constraints~$\posconstraints_{\subcable_{\cabindex}} \of{\genq_{\subcable_{\cabindex}}, t}$ result from requiring the proximal cable point be located at the frame anchor and the distal cable point be located at the platform anchor yielding
%
\begin{align*}%\label{sec:caro-dynamics:multibody-cdpr:cable:constraints}
  \posconstraints_{\subcable_{\cabindex}}
    & = \transpose{
          \begin{bmatrix}
            \transpose{ \posconstraints_{\subcable_{\cabindex}, \subproximal} } ,
              & \transpose{ \posconstraints_{\subcable_{\cabindex}, \subdistal} }
          \end{bmatrix}
        } \,
        ,
    \\
  \posconstraints_{\subcable_{\cabindex}, \subproximal}
    & = \shapefuns_{\subcable_{\cabindex}} \of{0} \,
        \genq_{\subcable_{\cabindex}}
        -
        \linpos_{\subcable_{\cabindex}, \subproximal} \of{t}
        \,
        ,
    \\
  \posconstraints_{\subcable_{\cabindex}, \subdistal}
    & = \shapefuns_{\subcable_{\cabindex}} \of{1} \,
        \genq_{\subcable_{\cabindex}} \,
        -
        \linpos_{\subcable_{\cabindex}, \subdistal} \of{t} \,
        .
\end{align*}

The number of positions constraints for each cable sums up to~$\numposconstraints = 2 \, \dof_{\ms{dim}}$.
In order to respect linking cables to winch and platform, we introduce the geometric constraints~$\posconstraints_{\frameanchor_{\cabindex}}$ and~$\posconstraints_{\platanchor_{\cabindex}}$ given through
%
\begin{subequations}\label{eqn:sec:caro-dynamics:multibody-cdpr:constraints:cable}
\begin{align}
  \posconstraints_{\frameanchor_{\cabindex}}
    & = \shapefuns_{\subcable_{\cabindex}} \of{0} \,
        \genq_{\subcable_{\cabindex}}
        -
        \frameanchor_{\cabindex} \,
        ,
        \label{eqn:sec:caro-dynamics:multibody-cdpr:constraints:cable:frame}
    \\
  \posconstraints_{\platanchor_{\cabindex}}
    & = \shapefuns_{\subcable_{\cabindex}} \of{1} \,
        \genq_{\subcable_{\cabindex}}
        -
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
        } \,
        .
        \label{eqn:sec:caro-dynamics:multibody-cdpr:constraints:cable:platform}
\end{align}
\end{subequations}

Positions constraints~\cref{eqn:sec:caro-dynamics:multibody-cdpr:constraints:cable} define spatial constraints \textIe constraints on the~$\evecx$, $\evecy$, and~$\evecz$ component of the cable's proximal and distal point.
While this may at first seem counterintuitive since cables physically constraint only in one direction\textemdash along the direction of its axis\textemdash this formulation does not change the physicality of the constraint as such.
It merely provides separate constraint forces per spatial axis of~$\mathds{E}^{3}$ rather than one constraint force of the cable.
Since constraints are incorporated into the multibody formulation by means of their Jacobian, both expressions are equivalent as the Jacobian\textemdash and thus directions of constraint forces\textemdash are equivalent.

Let the \caro state vector~$\genq$ be obtained from concatenation of the platform state vector~$\genq_{\subplatform}$ and all~$\numcable$ cable state vectors~$\genq_{\subcable_{\cabindex}}$ holding\footnote{We shall omit any subscript index to our variables.
If the reference is unambiguous, then we refer to the \caro, otherwise~$\parentheses{}_{\subcable}$ refers to cable/cables and~$\parentheses{}_{\subplatform}$ refers to the platform.}
%
\begin{align*}
  \genq
    & = \transpose{
          \begin{bmatrix}
            \transpose{ \genq_{\subplatform} } ,
              & \transpose{ \genq_{\subcable_{1}} } ,
              & \transpose{ \genq_{\subcable_{2}} } ,
              & \dotso
              & \transpose{ \genq_{\subcable_{\numcable}} }
          \end{bmatrix}
        }
      = \transpose{
          \begin{bmatrix}
            \transpose{ \genq_{\subplatform} } ,
              & \transpose{ \genq_{\subcable} }
          \end{bmatrix}
        } \,
        .
\end{align*}

Noting that the constraints between platform and cable appear in pairs yielding the same constraint forces only in opposite directions, the Lagrange multipliers for these constraints must be the same.
As such, we introduce the vector of Lagrange multipliers~$\lagrangemultips$ reading
%
\begin{align*}
  \lagrangemultips
    & = \transpose{
          \begin{bmatrix}
            \lagrangemultip_{\frameanchor_{1}} ,
              & \lagrangemultip_{\frameanchor_{2}} ,
              & \dotso ,
              & \lagrangemultip_{\frameanchor_{\numcable}} ,
              & \lagrangemultip_{\platanchor_{1}} ,
              & \lagrangemultip_{\platanchor_{2}} ,
              & \dotso ,
              & \lagrangemultip_{\platanchor_{\numcable}} ,
          \end{bmatrix}
        }
      = \transpose{
          \begin{bmatrix}
            \transpose{ \lagrangemultips_{\frameanchor} } ,
              & \transpose{ \lagrangemultips_{\platanchor} }
          \end{bmatrix}
        } \,
        ,
\end{align*}
%
where~$\lagrangemultip_{\frameanchor_{\cabindex}}$,~$\parentheses*{\irange{\numcable \, \dof_{\ms{dim}}}}$, are constraint forces enforcing constraints at the cable proximal end,~$\lagrangemultip_{\platanchor_{\cabindex}}$,~$\parentheses*{\irange{\numcable \, \dof_{\ms{dim}}}}$, are constraint forces enforcing constraints at the cable distal end \textIe link it to the platform.
We can write the full \caro system to read similarly
%
\begin{subequations}\label{eqn:caro-dynamics:multibody-cdpr:full-model}
\begin{align}
  \begin{split}
  \zeros
    & = \Mass \of{\genq} \,
        \ddotgenq
        -
        \forcevec_{\msexternal} \of{\genq, \dotgenq}
        +
        \forcevec_{\msinternal} \of{\genq, \dotgenq}
        +
        \transpose{
          \jacposconstraints
        } \of{\genq, t} \,
        \lagrangemultips \,
        ,
    \\
  \zeros
    & = \posconstraints \of{\genq, t} \,
      ,
  \end{split}
    \\
  \shortintertext{where}
  \Mass \of{\genq}
    & = \blkdiag \of{
          \Mass_{\subplatform} ,
          \Mass_{\subcable_{1}} ,
          \Mass_{\subcable_{2}} ,
          \dotsc ,
          \Mass_{\subcable_{\numcable}}
        } \,
        ,
    \\
  \forcevec_{\msexternal} \of{\genq}
    & = \transpose{
          \left[
            \transpose{
              \forcevec_{\msexternal, \subplatform}
            } \,
            ,
            \transpose{
              \forcevec_{\msexternal, \subcable_{1}}
            } \,
            ,
            \transpose{
              \forcevec_{\msexternal, \subcable_{2}}
            } \,
            ,
            \dotsc ,
            \transpose{
              \forcevec_{\msexternal, \subcable_{\numcable}}
            }
          \right]
        } \,
        ,
    \\
  \forcevec_{\msinternal} \of{\genq}
    & = \transpose{
          \left[
            \zeros, 
            \transpose{
              \forcevec_{\msinternal, \subcable_{1}}
            } \,
            ,
            \transpose{
              \forcevec_{\msinternal, \subcable_{2}}
            } \,
            ,
            \dotsc ,
            \transpose{
              \forcevec_{\msinternal, \subcable_{\numcable}}
            }
          \right]
        } \,
        ,
    \\
  \posconstraints \of{\genq, t}
    & = \transpose{
          \left[
            \transpose{
              \posconstraints_{\frameanchor_{\cabindex}}
            } \,
            ,
            \transpose{
              \posconstraints_{\platanchor_{\cabindex}}
            }
          \right]
        } \,
        ,
    \\
  \posconstraints_{\frameanchor_{\cabindex}} \of{\genq, t}
    & = \transpose{
          \left[
            \transpose{
              \posconstraints_{\subcable_{1}, \subproximal}
            } \,
            ,
            \transpose{
              \posconstraints_{\subcable_{2}, \subproximal}
            } \,
            ,
            \dotsc ,
            \transpose{
              \posconstraints_{\subcable_{\numcable}, \subproximal}
            }
          \right]
        } \,
        ,
    \\
  \posconstraints_{\platanchor_{\cabindex}} \of{\genq, t}
    & = \transpose{
          \left[
            \transpose{
              \posconstraints_{\platanchor_{1}}
            } \,
            ,
            \transpose{
              \posconstraints_{\platanchor_{2}}
            } \,
            ,
            \dotsc ,
            \transpose{
              \posconstraints_{\platanchor_{\numcable}}
            }
          \right]
        } \,
        ,
    \\
  \jacposconstraints
    & = \begin{bmatrix}
          \zeros
            & \pd{
                \posconstraints_{\frameanchor_{\cabindex}}
              }{
                \genq_{\subcable}
              }
            \\
          \pd{
              \posconstraints_{\platanchor_{\cabindex}}
            }{
              \genq_{\subplatform}
            }
            & \pd{
                \posconstraints_{\platanchor_{\cabindex}}
              }{
                \genq_{\subcable}
              }
        \end{bmatrix} \,
        ,
    \\
  \pd{
    \posconstraints_{\frameanchor_{\cabindex}}
  }{
    \genq_{\subcable_{\cabindex}}
  }
    & = \begin{cases}
          \jacposconstraints_{\subcable_{\funcindex}}
            & \cabindex = \funcindex \, ,
            \\
          0
            & \cabindex \neq \funcindex \, ,
        \end{cases}
    \\
  \pd{
    \posconstraints_{\platanchor_{\cabindex}}
  }{
    \genq_{\subplatform}
  }
    & = \jacposconstraints_{\subplatform} \,
        ,
    \\
  \pd{
    \posconstraints_{\platanchor_{\cabindex}}
  }{
    \genq_{\subcable_{\cabindex}}
  }
    & = \begin{cases}
          -\jacposconstraints_{\subcable_{\funcindex}}
            & \cabindex = \funcindex \, ,
            \\
          0
            & \cabindex \neq \funcindex \, .
        \end{cases}
\end{align}
\end{subequations}

Closely inspecting~\cref{eqn:caro-dynamics:multibody-cdpr:platform:eqom,eqn:caro-dynamics:multibody-cdpr:platform:constraints} and comparing with~\cref{eqn:fundamentals:dynamics:forward-dynamics:platform:full-eqom-vector}, we observe the well-known structure matrix~$\structmat$ is expressed through the transposed constraint Jacobian~$\transpose{\jacposconstraints_{\subplatform}} \of{\genq, t}$, whereas the cable forces are now expressed in terms of the constraint forces~$\lagrangemultips_{\platanchor}$.
In particular, we can express the~$\cabindex$-th cable force direction~$\cabudirn_{\cabindex}$ in terms of the~$\cabindex$-th cable shape~$\curve_{\cabindex} \of{\pathcoord, t}$, respectively its tangent, such that it reads
%
\begin{align*}
  \cabudirn_{\cabindex}
    & = - \frac{
            \p{\curve}_{\cabindex} \of{\pathcoord = 1, t}
          }{
            \norm{
              \p{\curve}_{\cabindex} \of{\pathcoord = 1, t}
            }
          }
      = - \p{\shapefuns}_{\cabindex} \of{\pathcoord = 1} \,
          \genq_{\subcable, \cabindex} \of{t} \,
          ,
\end{align*}
%
yielding the structure matrix expressed through the Cosserat rod cable model
%
\begin{align*}
  \structmat
    & = \begin{bmatrix}
          -\p{\shapefuns}_{1} \,
            \genq_{\subcable, 1}
            & \dotso
            & -\p{\shapefuns}_{\numcable} \,
              \genq_{\subcable, \numcable}
            \\
          - \crossp{
            \platanchor_{1}
          }{
            \parentheses*{
              \p{\shapefuns}_{1} \,
              \genq_{\subcable, 1}
            }
          }
            & \dotso
            & - \crossp{
                \platanchor_{\numcable}
              }{
                \parentheses*{
                  \p{\shapefuns}_{\numcable} \,
                  \genq_{\subcable, \numcable}
                }
              }
        \end{bmatrix} \,
        ,
\end{align*}
%
allowing us to analyze the platform motion in the static case.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/multibody-framework/statics}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/caro-dynamics/multibody-framework/dynamics}
