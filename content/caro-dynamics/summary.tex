%!TEX root=../../thesis.tex
%!TEX file=content/caro-dynamics/summary.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}\label{sec:caro-dynamics:summary}

The \caro simulation framework presented in this section provides a mechanically formulated, energy and momentum-preserving formulation for \caros of different types.
Solving the kinematics or dynamics of multibody systems, in particular those with flexible and rigid bodies, requires special care when it comes to handling the constraints between bodies.
Conventionally, this is achieved by introducing constraint violation handling methods such as \gls{BSM} or using a different formulation of the \gls{DAE} such as \gls{acr:GGL}.
However, this affects the system dynamics as the constraints now feature their own dynamics, which, depending on the parametrization of its constraint violation handling, can make the forward dynamics simulation yield different results than anticipated.
With the formulation given in this section, there is no recasting of the system dynamics into a different form and constraint violation is handled implicitly through the iterative scheme.
Furthermore,~\citeauthor{Betsch.2006}'s iterative scheme can be solved purely on the positional level and implies the energy and momentum preserving velocities\textemdash satisfying them at intermediate points\textemdash remedying the need for solving for the accelerations and then subsequent twofold integration with respect to time.

Besides the numerical advantages of the integration scheme presented in this section, the multibody formulation of \caros within the framework given allows for arbitrary configurations of \caros, in fact we may even add more than one platform to the framework, which we may connect through cables or by means of other bodies.
We may also think of adding a serial manipulator onto the platform allowing for simulation of \caros in different tasks than before.
In the end, all that is required are the appropriate dynamics of each body and the constraints that bind it to another body; with the framework presented, the possibilities for analysis and simulation of \caros are endless.
Lastly, the framework enables its user to fully understand how the equations of motion are implemented and how exactly the forward-in-time integration scheme is laid out\textemdash in contrast to commercially available simulation tools based on \textEg FEM, the algorithmic implementation is not corporate secret but can be easily traced and understood.

Looking at the applied use case lying in \caro analysis, both statically/kinetostatically and dynamically, the framework is based on aforementioned facts, and shows itself capable of handling many different kinds and dimensions of \caros.
Since not all algorithms existing in literature are applicable to every kind or dimension of \caros\textemdash for example, the closed-form force distribution presented in~\cref{sec:fundamentals:cable-force-distribution} does not yield valid results for suspended \caros and is not even applicable to \caros other than those of \gls{RRPM} type\textemdash the use range of the framework is rather wide.
We have shown applicability to obtaining both kinematic and kinetostatic solutions to \caros of different type\textemdash \CoGiRo/\Expo and \textsc{IPAnema}\textemdash as well as of different sizes\textemdash \CoGiRo and \textsc{IPAnema~Mini}\textemdash while keeping the algorithm's speed of convergence within reasonable bounds.
With further improvements, such as using an existing cable force distribution algorithm on the initial guess of the kinematic and kinetostatic problem, the number of iterations may further be reduced.

In all fair-mindedness, one implementation limitation of the proposed multibody simulation framework has to be mentioned which is its general speed of execution.
While the static equilibrium solutions~(\cref{sec:caro-dynamics:statics}) can be obtained in less than approximately~$\SI{0.9}{\second}$ in total\footnote{Counting only the execution time of the iterative Newton's method; not including any simulation setup times or data pre-processing or post-processing.}, even in case of high iteration counts of \IPAnemaThree and \IPAnemaMini, it is still relatively slow when it comes to solving the dynamics solution.
With a time step size of~$\stepsize = \SI{1}{\milli\second} = \SI{0.001}{\second}$, a simulation over~$T = \SI{10}{\second}$ takes at least\footnote{We say ``\textit{at least}'' as the dynamic case will increase the iteration count per time step during large motion and as such,~$t_{\ms{step}} \approxeq \SI{0.9}{\second}$ can no longer be sustained.
Worst case scenarios have revealed integration step times of up to~$t_{\ms{step}} \approxeq \SI{5}{\second}$ with iteration count reaching the self-defined limit of~$\max \of{\iterindex} = 100$.}~$t_{\ms{sim}} = \SI{9000}{\second} = \SI{150}{\minute}$.
However, the algorithmic implementation of the multibody framework can be well improved by pre-simulation code generation resolving loops over all bodies and constraints, much like the calculation of the constraint forces can be streamlined.
Lastly, the iterative Newton's scheme can be improved by better conditioning of the Jacobian using a less-sparse formulation improving inversion of the high-dimensional matrix~\autocite{Baraff.1996}.
