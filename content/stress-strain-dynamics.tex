%!TEX root=../thesis.tex
%!TEX file=content/stress-strain-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Stress-Strain Dynamics}\label{chap:stress-strain-dynamics}

\input{content/stress-strain-dynamics/intro}

\addref[inline]{Somewhere \autocite{Budday.2017}}

\iffalse
How exactly does the cable tension behave, when the cable goes from slack to tensed \textIe when it is just a little bit longer than the geometric length up to when its just a bit shorter than the geometric length between two points?
What we know from literature are graphs have the~$x$-axis defined as~$\Delta l$ and then the cable force~$F$ is shown as a function of this change of length.
However, the change of length always is with respect to a given length.
To my knowledge, no other researchers have yet looked at the cable force/tension as a function of the stretch~$\epsilon = \frac{ l }{ l_{0} }$~(at least with focus on \caros or with respect to \UHMWPE.
Tackling this topic will be done mostly through experiments as there can be models set for the behavior~(nonlinear, I presume something like a PT$_2$ behavior), but it is mandatory to actually determine this behavior through experiments.
\fi

\iffalse
How does the tension in the cable change when we add pulleys to the system?
I, and Andreas Pott, assume~(actually we can infer this from literature) the cable to act as a low-pass system to the tension change frequency.
What this means is that rapid changes in the length on the winch do not propagate that quickly to the other end of the cable thus we could change the actuation such that it will not even be faster than any given bandwidth frequency.
\fi

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--stress-strain-real}
  \includegraphics[%
      width=\linewidth,%
      axisratio=2.50,%
    ]{stress-strain/stress-strain-real}
  \tikzexternaldisable%
  \caption[%
    Hysteretic stress-strain behavior of \Dyneema.
  ]{%
    Experimentally obtained hysteretic stress-strain dynamics of \Dyneema~SK78 for an arbitrary tensing-relaxing trajectory visualizing the material's hysteretic behavior.
    Stress during tensing is largely linear though with varying slopes depending on the previous resting stress.
    Stress during relaxation decays exponentially with varying slopes.
  }
  \label{fig:stress-strain:key-characteristics:hysteretic-stress-strain}
\end{figure}

\amend[inline]{Somewhere we should point out that strength of these cables is always given in \emph{tenacity of yarn}~(of units \si{\newton\per\dtex} or \si{\centi\newton\per\dtex}).
You can convert from Tenacity to GPa by dividing by density in g/cc.
for a multifilament fiber like a carbon nanotube, the density is hard to measure, so reporting data in Tenacity is more accurate.}

Conventional parallel kinematics use rigid joints composed of prismatic or rotational actuators for which the governing equations of force/torque transmission can be obtained straightforward under the assumption of neither static nor dynamic friction or backlash.
Incorporating effects such as static and dynamic friction can be done by choice of an appropriate friction model such as~\textcite{Dahl1968} or~\textcite{Stribeck1903}.
These factors similarly affect cable robots since their main actuating system is usually composed of a rotational actuator that is in one way~(gearbox \textEg \IPAnema system family~\autocite{Pott2012b}) or another~(timing belt; \textEg \CoGiRo~\autocite{Riehl.2010}) attached to the winch.
However, \caros differ from rigid link robots in that sense that their links are flexible and elastic components, with their own dynamics.
As shown in~\cref{chap:spatial-dynamics}, flexibility of the cables introduces spatial motion, as such deviation from the rigid link's straight-line connection.
However, it is the links' higher elasticity when compared to rigid links that entails more involved stress-strain dynamics of the material.
\Cref{fig:stress-strain:key-characteristics:hysteretic-stress-strain} shows two key response factors of polyethylene fibers in comparison to a Hookean cable.
The first factor is the material's nonlinearity in the stress response given a strain input apparent by various force plateaus during relaxing or tensing.
The other key factor is the material's hysteretic behavior during tensing and relaxing clearly visible through a nearly linear tensing slope yet a exponential decay during relaxing.
A third property of viscoelastic polyethylene fibers is the so-called stress-relaxation \textIe a change in stress while strain remains unchanged~(shown in~\cref{fig:stress-strain:key-characteristics:strain-relaxation}).

Keeping these three factors in mind, consequently, the distally measured tension may not be the same as the proximally applied tension\textemdash for the sake of simplicity, we shall not consider loss of tension on pulleys or other guiding elements as has been investigated by~\textcite{Kraus.2015b}.
The material's dynamics to stress and strain need special consideration when it comes to better understanding \caros, in particular during non-quasistatic motion.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--stress-relaxation}
  \includegraphics[%
      width=\linewidth,%
      axisratio=2.00,%
    ]{stress-strain/stress-relaxation}
  \tikzexternaldisable%
  \caption[%
    Stress relaxation of \Dyneema~SK78 after instantaneous strain-step.
  ]{%
    Stress relaxation of \Dyneema~SK78 cable showing a decrease in the cable stress~$\stress*$ while its strain~$\strain*$ is kept constant.
    In addition, initial force~$\cabforce \of{0}$ is larger by~${\Delta \cabforce = \SI{5}{\newton}}$ than final force~$\cabforce \of{\infty}$ after~${t_{\ms{relax}} = \SI{45}{\second}}$ of relaxation.
  }
  \label{fig:stress-strain:key-characteristics:strain-relaxation}
\end{figure}

Further implications of non-negligible stress-strain dynamics of fiber cables, resulting from their composition of several strains of hundreds of thin fibers~(a schematic depiction of a \Dyneema cable is given in~\cref{fig:cable-strain:crosssection-actuator-cable:fiber-cable}) affect cable force control much like force control in operational space.
Moreover, fiber cables show inherently different behavior to tensing and relaxing as a result of their mechanical property.
While linear actuators are generally built from elements with dense cross-section, fiber cables are woven or laid from hundreds of strains.
Thus, in the sense of a prismatic joint, a fiber cable consists of a myriad of small-diameter linear prismatic joints\textemdash that may only pull\textemdash which are axially loaded, are in radial contact with its neighbors, and are additionally torsionally pre-stressed.
While a single cable fiber may be assumed assimilable to a linear prismatic joint, the sum of strains of fibers results in a complex nonlinear stress-strain behavior of the cable.
This makes modeling fiber cables through tribology \textIe on a microscopic level, rather challenging, last because material properties such as elastic, viscous, or friction coefficients must be obtained for a single strand.
Consequently, a homologous model of the material dynamics is favored over a tribologically correct one, last because we are only interested in the overall stress-strain dynamics and not the inner workings of every single fiber.


\begin{figure}
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{crosssection-cable}
  \includegraphics[%
      % width=0.75\linewidth,%
      height=0.30\textheight,%
    ]{crosssection-cable}
  \tikzexternaldisable%
  \caption[%
      Schematic cross-section of \Dyneema fiber cable.
    ]{%
      Schematic cross-section of \Dyneema fiber cable composed of multiple strands, in turn composed of assembled yarns, in turn composed of single yarns, comprising many fibers.
      The total area of contact is small compared to the overall surface area due to the strands having only punctiform contact to its neighboring yarns.
    }
  \label{fig:cable-strain:crosssection-actuator-cable:fiber-cable}
\end{figure}

Commonly, cables in \caros are assumed linear elastic and modeled by a linear spring element or are assumed linear viscoelastic modeled by a serial combination of a linear spring and a linear damper.
In either case, elastic or viscoelastic parameters, respectively, are assumed time-independent and vary only with respect to the nominal cable length.
Most, if not all of the algorithms for design and analysis of \caros, make use of these simplified models as they drastically reduce computational cost and approximate the static behavior of fiber cables well.
However, applicability of these models to dynamics and even quasi-static simulation of \caros is debatably limited due to lack of four key characteristics of fiber cables:
%
\begin{inparaenum}[1)]
  \item comparing tensing and relaxing, fiber cables show distinctive hysteretic behavior;
  \item when under constant stress, strain relaxation emerges \textIe cable elongation increases; or likewise
  \item when under constant strain, stress relaxation occurs \textIe cable tension decreases; and
  \item material properties change over time due to internal mechanical stress but also due to environmental influence.
\end{inparaenum}
%
Knowing fiber cables show nonlinear mechanical characteristics but also short-term and long-term dynamical responses to applied stress, it becomes apparent that more sophisticated models need to be derived.
These models may later be used in any step from design over analysis to control of \caros, however, in fields such as design and static workspace analysis, it still suffices to use a purely elastic cable model composed of only a Hookean material.

The remainder of the chapter is structured as follows:
\Cref{sec:stress-strain:modeling} introduces the basic techniques for modeling of viscoelastic materials as well as two general material models.
Three particular implementations of these general material models are evaluated with regards to their applicability resulting in a selection of the material model to use for the following experimental estimation
In \cref{sec:stress-strain:procedure}, the procedure of experimental model and model parameter estimation is given introducing the experimental setup as well as the techniques used during numerical analysis.
Following that, \cref{sec:stress-strain:results} shows results from estimation as well as stability of the estimation and goodness of fit.
Lastly, \cref{sec:stress-strain:conclusions} concludes the chapter with a recommended selection of a material model, also commenting on challenges faced during the estimation step.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/modeling}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/procedure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/results}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/conclusions}
