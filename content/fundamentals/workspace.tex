%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/workspace.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Workspace}\label{sec:fundamentals:workspace}

Determining the workspace of a \caro is a task crucial to evaluating applicability of a design to a certain task as the term \textit{workspace} describes the area\footnote{Area, in this case, does not refer to what generally an area is understood to mean, but merely a geometric object of some geometric extent.
Depending on the \caro structure, it may also include rotational \gls{sym:degrees-of-freedom} that cannot generally be measured like a geometric area of linear units.} a \caro can be safely operated in given the requirements of its task.
While this definition seems rather intuitive, the technical implications may not be as straightforward.

A \caro's workspace is usually defined by means of some geometric description that determines merely the workspace boundary of the six-dimensional installation space subject to one or more of certain criteria.
The most commonly used subsets of workspaces are
%
\begin{description}
  \item[Translation Workspace] All {positions~$\platpos \in \mathds{R}^{3}$} that can be reached with a fixed {orientation~$\platrot \in \SO[3]$} of the mobile platform;
  \item[Orientation Workspace] All {orientations~$\platrot \in \SO[3]$} that can be attained at a fixed {position~$\platpos \in \mathds{R}^{3}$} of the mobile platform;
  \item[Inclusion Orientation Workspace] All positions for which at least one mobile platform {orientation~$\platrot \in \mathcal{R}_{0}$} of a set of {orientations~${\mathcal{R} \subset \SO[3]}$} can be attained;
  \item[Maximum Workspace] All positions for which any mobile platform {orientation~${\platrot \in \SO[3]}$} can be attained;
  \item[Total Orientation Workspace] All positions for which all mobile platform {orientations~$\platrot \in \mathcal{R}_{0}$} of a set of orientations~$\mathcal{R}_{0}$ can be reached;
  \item[Dexterous Workspace] All positions for which all mobile platform {orientations~$\platrot \in \mathcal{R}_{0} \equiv \SO[3]$} can be attained.
\end{description}

\correct[inline]{This is not entirely consistent but I don't want to provide any equations here, as these are anyway not very informative~(it's just a description of a mathematical set).}

Besides the geometric description of a workspace, criteria for determination of validity must be employed, too, yielding a flag whether a given pose belongs to the chosen type of geometric workspace.
The most common criteria are
%
\begin{description}
  \item[Reachability] A pose that can be reached within physical limits of the cable lengths;
  \item[Collision] No collision between cables, between cable and platform or environment, or between platform and environment occur;
  \item[Wrench-Closure Workspace] The pose provides a valid force distribution for every {wrench~$\wrench \in \mathds{R}^{\dof}$};
  \item[Wrench-Feasible Workspace] The pose provides at least one valid force distribution for any external {wrench~$\wrench \in \mathds{R}^{\dof}$};
  \item[Singularity] A pose is singular if the rank of structure matrix degrades below the number of \gls{DOF} of the system {\textIe~$\rank \structmat < \dof$}.
\end{description}

While the field of workspace determination for \caros is subject to ongoing research, all existing workspace sets and criteria for determination depend again on the kinematic structure of the \caro.
That is, both the \gls{IKP} as well as the structure matrix~$\structmat$ play a vital role in the determination of the workspace boundary.
Last because most of the workspace algorithms make use of finding a valid force distribution, which in itself already depends on the structure matrix.
That said, workspace algorithms must not be altered when the underlying kinematic \caro model is changed, however, substituting one kinematic model for another the size of workspaces must not remain the same.
