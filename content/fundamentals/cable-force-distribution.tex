%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/cable-force-distribution.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cable Force Distribution}\label{sec:fundamentals:cable-force-distribution}

Even though \caros are generally very similar to their rigid link counterparts, not all theory transfers directly.
The most dominant difference are the cables, which can only transmit tensile forces, making it ineluctable to ensure positive tension in the cables at all times.
At first, this problem may sound like a trivial task, but depending on the \caro's level of redundancy~(see~\cref{sec:fundamentals:redundancy}), it is in fact a nontrivial task.
Only in case of \gls{CRPM}, with number of cables~$\numcable$ equaling the number of \gls{DOF}~$\dof$, \textIe~$\numcable = \dof$, it is possible to uniquely solve the kinetostatic\footnote{Without loss of generality, we introduce calculation of valid cable force distributions only on the kinetostatic level.
It can be easily shown that the same methods also apply for the dynamic case where a cable force distribution valid to balancing the dynamic forces is sought by simply replacing the static wrench~$\wrench$ with the dynamic wrench from the left-hand side of~\cref{eqn:fundamentals:dynamics:forward-dynamics:platform:2nd-order-ode}.} equation~\cref{eqn:fundamentals:statics:static-equilibrium} for the cable forces~$\cabforces$ given an external wrench~$\wrench$.
The straightforward calculation of the solution follows directly from mathematics as the structure matrix~${ \structmat \in \mathds{R}^{\numcable \times \numcable} }$ and the external wrench~$\wrench \in \mathds{R}^{\numcable}$ form a system of linear equations of equal dimension.
Thus, the cable force distribution solution follows directly to read~$\cabforcesdist = \inv*{\structmat} \, \wrench$ as the structure matrix's invers exists.
Of course, the solution still needs to satisfy~$\cabforces > \zeros$ in order to be able to employ cables since positive cable forces represent tensile cable forces.
If this requirement is not satisfied \textIe~$\cabforces \ngtr \zeros$, then there is no valid force distribution.




For \caros of type \gls{RRPM}, the problem of finding valid force distributions becomes more involved, since~\cref{eqn:fundamentals:statics:static-equilibrium} no longer is well-posed but underconstrained for there are less equations than unknowns to solve for.
We can then solve the static equilibrium for the cable forces by calculating the nonnegative least squares solution such that
%
\begin{align*}
    &
    & \cabforces
    & = \arg
          \min\limits_{\cabforces}
          \norm{
            \structmat \,
            \cabforces
            +
            \wrench
          }
    \\
  \text{subject to}
    &
    & \cabforces
    & \geq \zeros \,
  ,
  \\
\intertext{which can be rewritten as a quadratic programming problem}
    & 
    & \cabforcesdist
    & = \arg \min\limits_{\cabforces \geq 0} \of*{
          \frac{
            1
          }{
            2
          } \,
          \transpose{
            \cabforces
          } \,
          \matr{Q} \,
          \cabforces
          +
          \transpose{
            \vect{c}
          } \,
          \cabforces
        } \,
      ,
    \\
  \text{subject to}
    & 
    & \matr{Q}
    & = \matr{A} \,
        \structmat \,
        ,
    \\
    &
    & \vect{c}
    & = -
        \matr{A} \,
        \wrench \,
        .
\end{align*}
%
Inspecting~\cref{eqn:fundamentals:statics:static-equilibrium} with at least~$\numcable > \dof$, we see now~${\structmat \in \mathds{R}^{\dof \times \numcable}}$ and~${\wrench \in \mathds{R}^{\dof}}$.
As such, we may still obtain the solution by simply pre-multiplying the equation with the inverse structure matrix yielding~${\cabforcesdist = \inv*{\structmat} \, \wrench}$, however, the inverse of~$\structmat$ is not unique since the matrix is rectangular \textIe it has more columns than rows.


Literature overview states many different approaches the solution of calculating valid force distributions for overconstrained systems with different characteristics on the resulting force distribution.
Several researchers have developed algorithms to determine a minimal or maximal force distribution with respect to some norm, yielding either systems with low stored energy or high stiffness~\autocite{Gouttefarde.2015b,Caverly2015}.
While many algorithms are iterative by nature, there also exist several closed-form solutions that also satisfy minimum norm and other characteristics such as continuity along a trajectory.
The algorithm called \textit{closed-form cable force distribution} by~\citeauthor{Pott2013} and briefly derived here, is valid for \caros of arbitrary redundancy~$\redundancy = \numcable - \dof > 0$~\autocite{Pott2013}.
It makes use of a limit on the minimum and maximum cable forces~$\cabforcesmin$ and~$\cabforcesmax$, respectively.
Such limits arise on the lower end since a minimum tension to keep is desirable in order to avoid slack cables, and on the upper end since the driving motors, without loss of generality, cannot produce an infinitely large torque/force on the cable but are physically limited to providing a maximum torque.
If the drives were to create infinitely large torques, one may choose the yield strength of the cable as maximum force limit.

We begin by splitting the sought-for cable force distribution~$\cabforcesdist$ into a mean value~$\bar{\cabforces} = \parentheses*{\cabforcesmax + \cabforcesmin} / 2$ and an arbitrarily scaled force vector~$\cabforces_{\ms{arb}}$ such that~$\cabforcesdist = \bar{\cabforces} + \cabforces_{\ms{arb}}$.
After some mathematical manipulation of~~\cref{eqn:fundamentals:statics:static-equilibrium}, this gives us the solution
%
\begin{align*}
  \cabforcesdist
    & = \bar{\cabforces}
        -
        \pinv*{\structmat} \,
        \parentheses*{
          \wrench
          +
          \structmat \,
          \bar{\cabforces}
        } \,
        ,
\end{align*}
%
where~$\pinv*{\structmat}$ is the Moore-Penrose inverse of~$\structmat$ computed as
%
\begin{align*}
  \pinv{\structmat}
    & = \inv*{%
          \hermconj{\structmat} \,
          \structmat%
        } \,
        \hermconj{\structmat} \,
        ,
\end{align*}
%
with ${\hermconj{\structmat} = \conj{\transpose*{\structmat}}}$ the Hermitian transpose~(conjugate transpose) matrix.
The obtained result then needs to be checked for consistency \textIe~$\cabforcesdist > \zeros$ as negative cable forces may still result.
It is favorable to increase numerical stability by not calculating the Moore-Penrose inverse explicitly, but by solving the system of linear equations for~$\cabforces_{\ms{arb}}$
%
\begin{align*}
  \matr{A} \,
  \structmat \,
  \cabforces_{\ms{arb}}
    & = -\matr{A} \,
          \parentheses*{
          \wrench
          +
          \structmat \,
          \bar{\cabforces}
        } \,
        ,
\end{align*}
%
for which efficient algorithms like Gaussian elimination exist~\autocite{Pott.2018}.

\Cref{eqn:fundamentals:statics:static-equilibrium} is fundamental to all force distribution algorithms, with the only variable capturing the geometric structure and model of the \caro being the structure matrix~$\structmat$.
Since the cable forces~$\cabforces$ and wrench~$\wrench$ are independent of the cable model in use, they are invariant under substitution of cable models.
One can also read the structure matrix as a linear mapping from a higher-dimensional space~(cable forces) into a lower-dimensional space~(platform wrench).
Clearly, this transformation depends on the coordinate system used in the higher dimensional case, which can be thought of as being spanned by the directional vectors of cable forces.
