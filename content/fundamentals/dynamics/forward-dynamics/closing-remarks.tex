%!TEX root=../../../../thesis.tex
%!TEX file=content/fundamentals/dynamics/forward-dynamics/closing-remarks.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Closing Remarks}

Numerically solving the forward dynamics of \caros with the dynamics equation introduced in this chapter is a comparatively straightforward task.
Since cable dynamics are only considered on a kinematic force level and not on an inertial level, as would be the case for simulation of conventional parallel robots with non-negligible link mass, all simulation merely comprise a single rigid body.
If we were also interested in drive torques needed to perform certain motion, a simplified or full servo drive model may be implemented that allows for simulating dynamics of the drive trains~\autocite{Miermeister.2010b,Khosravi.2014,Tempel.2015e}.
In either case, from change of position and change of velocity of wound cable results a strain on the cable which ultimately leads to a corresponding stress moving the platform.

With the \caro dynamics model defined in this thesis, a \caro simulation framework is given that is applicable to a wide range of \caros, as will be shown in~\cref{chap:caro-dynamics}.
While the model derived in this chapter comprises only very basic kinematic and dynamic formulations of cable robots\textemdash in the kinetostatic realm, cable mass can be considered through~\citeauthor{Irvine.1974}'s cable model, in the dynamic realm no common form of consideration of cable dynamics has been established\textemdash the model is accepted as being generally valid.
However, integration of these models into a more detailed dynamics simulation is not possible due to two shortcomings in the model derivation:
\begin{inparaenum}[1)]
  \item the nonlinear model is mathematically underconstrained, and
  \item it is based on purely kinematic \textIe force considerations.
\end{inparaenum}
The latter model shortcoming renders it not applicable to simulation of compound dynamics simulation where spatial cable motion \textIe vibration is of interest, as this is not covered by the model equations.
The former shortcoming becomes apparent when observing the equations for the cable shape of Irvine's cable model from~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}: given an unstrained cable length~$\cablen_{\subunstrained}$ and the horizontal and vertical distal point forces~$\force_{x}$ and~$\force_{z}$, respectively, we can directly solve for the horizontal and vertical displacement of the cable distal point.
However, when given the distal cable point displacement and either the unstrained cable length or the distal point forces, the nonlinear equations become implicit in either of these values.
In addition, while previously having uncoupled equations for the distal cable point displacement, the equations for distal cable force or unstrained cable length are now coupled and thus cannot be solved for independently.
We can, however, rewrite the equations in residual form and solve the resulting root-finding problem using algorithm's like Newton's method or Levenberg-Marquardt~(see~\cref{app:chap:root-finding}).
