%!TEX root=../../../../thesis.tex
%!TEX file=content/fundamentals/dynamics/forward-dynamics/platform.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Platform}\label{sec:fundamentals:dynamics:forward-dynamics:platform}

Following well-known mechanics \textIa~\autocite{Featherstone.2008}, for a free-floating rigid body, with its position and orientation of interest given in a world coordinate frame as shown in~\cref{eqn:fundamentals:dynamics:forward-dynamics:free-floating-rigid-body}, we can state the combined translational and rotational dynamics for an arbitrary point~$\symplatform$ indexed~$\parentheses{\cdot}_{\subplatform}$ on the rigid body located at~$\cogvec$ from the center of mass using Newton-Euler equations as
%
\begin{align}
  \underbrace{
    \begin{bmatrix}
      \mass \,
      \eye[3]
        & -
          \mass \,
          \skewm{\cogvec} \\
      \mass \,
      \skewm{\cogvec}
        & \platrot \,
          \inertiatensor[\subplatform]_{\subplatform} \,
          \transpose{
            \platrot
          }
    \end{bmatrix}
  }_{\Mass \of{\genq}}
  \underbrace{
    \begin{bmatrix}
      \linacc_{\subplatform} \\
      \angacc_{\subplatform}
    \end{bmatrix}
  }_{\ddotgenq}
  +
  \underbrace{
    \begin{bmatrix}
      \mass \,
      \skewm{\angvel} \,
      \skewm{\angvel} \,
      \cogvec
      \\
      \skewm{\angvel} \,
      \platrot \,
      \inertiatensor[\subplatform]_{\subplatform} \,
      \transpose{
        \platrot
      } \,
      \angvel
    \end{bmatrix}
  }_{\fictitiousforces \of{\genq, \dotgenq}}
  & = 
    \underbrace{
      \begin{bmatrix}
        \forcevec_{\subplatform} \\
        \torquevec_{\subplatform}
      \end{bmatrix}
    }_{\wrench_{\subplatform}} \,
    ,
  \label{eqn:fundamentals:dynamics:forward-dynamics:platform:newton-euler:any-reference-frame}
\end{align}
%
where~$\forcevec_{\subplatform}$ and~$\torquevec_{\subplatform}$ are, respectively, the forces and torques about point~$\symplatform$;~$\linacc_{\subplatform}$ and~$\angacc_{\subplatform}$ are the linear and angular acceleration, respectively, of~$\symplatform$ \textWrt the world frame~$\coordsys{O}$;~$\inertiatensor[\subplatform]_{\subplatform}$ is the inertia tensor \textWrt point~$\symplatform$ given in world coordinates \textIe
%
\begin{align*}
  \inertiatensor[\subplatform]_{\subplatform}
    & = \inertiatensor[\subplatform]_{\subcenterofmass}
        -
        \mass \,
        \skewm{\cogvec} \,
        \skewm{\cogvec} \,
        ,
\end{align*}
%
and~$\skewm{\cdot}$ denotes the skew-symmetric cross product matrix given as
%
\begin{align}
  \skewm{\vect{a}}
    & =
      \begin{bmatrix}
        0
          & -a_{3}
          & a_{2} \\
        a_{3}
          & 0
          & -a_{1} \\
        -a_{2}
          & a_{1}
          & 0
      \end{bmatrix} ,%
      \label{eqn:fundamentals:dynamics:forward-dynamics:platform:cross-product-matrix}
\end{align}
%
for any vector~$\vect{a} = \transpose{ \left[ a_{1}, a_{2}, a_{3} \right] }$ that holds~$\skewm{ \vect{a} } \vect{b} = \crossp{ \vect{a} }{ \vect{b} }$ for~$\vect{b} = \transpose{ \left[ b_{1}, b_{2}, b_{3} \right] }$.

The left-hand side of~\cref{eqn:fundamentals:dynamics:forward-dynamics:platform:newton-euler:any-reference-frame} defines an algebraic equation for the external wrench as a function of the states~$\state = \vectset{ \genq ; \dotgenq ; \ddotgenq }$.
That is, given a trajectory of the rigid body, the external wrench enforcing said motion can be calculated.
However, for forward dynamics, we interpret the same equation as an ordinary differential equation yielding the acceleration~$\ddotgenq$ given the current system state~$\state = \vectset{ \genq ; \dotgenq }$ and an external wrench~$\wrench_{\subplatform}$.
This equation is fully qualified for dynamics simulation, if we can determine the external wrench for a given system state.
Without loss of generality, we state the external wrench to be composed of three wrenches: %
\begin{inparaenum}[1)]
  \item cable tension-induced forces and torques~$\wrench_{\subcable}$,
  \item process-induced forces and torques~$\wrench_{\subprocess}$, and lastly
  \item gravity-induced forces and torques~$\wrench_{\subgravity}$,
\end{inparaenum}
such that the external wrench reads
%
\begin{align}
  \wrench_{\subplatform}
    & = \wrench_{\subcable}
        +
        \wrench_{\subprocess}
        +
        \wrench_{\subgravity} \,
        .
\end{align}
%
Cable tension-induced wrench~$\wrench_{\subcable}$ and process-induced wrench~$\wrench_{\subprocess}$ are subject to the \caro and process, while gravitational wrench~$\wrench_{\subgravity}$ can most generally be written as concatenation of gravitational forces~$\forcevec_{\subgravity} = \mass_{\subplatform} \, \evec{g}$ and gravitational torques~$\torquevec_{\subgravity} = \crossp{ \platrot \, \cogvec }{ \forcevec_{\subgravity} }$.
Given a generalized \caro, the action wrench~$\wrench_{\subprocess}$ may be chosen arbitrarily and we impose no physical limitation on it.
The cable tension-induced wrench, on the other hand, cannot be chosen arbitrarily as it depends on the current state~(and possibly previous states) of the \caro.

Plugging~\cref{eqn:fundamentals:statics:cable-wrench-structure-matrix} into~\cref{eqn:fundamentals:dynamics:forward-dynamics:platform:newton-euler:any-reference-frame}, we obtain the fully qualified second-order \gls{ODE} for the \caro's mobile platform as
%
\begin{align}
    \begin{bmatrix}
      \mass \,
      \eye[3]
        & -
          \mass \,
          \skewm{\cogvec} \\
      \mass \,
      \skewm{\cogvec}
        & \inertiatensor_{\subplatform}
    \end{bmatrix}
    \begin{bmatrix}
      \linacc_{\subplatform} \\
      \angacc_{\subplatform}
    \end{bmatrix}
  +
    \begin{bmatrix}
      \mass \,
      \skewm{\angvel} \,
      \skewm{\angvel} \,
      \cogvec
      \\
      \skewm{\angvel} \,
      \inertiatensor_{\subplatform} \,
      \angvel
    \end{bmatrix}
  & = 
    \structmat \,
    \cabforces
    +
    \wrench_{\subgravity}
    +
    \wrench_{\subprocess} \,
    ,%
    \label{eqn:fundamentals:dynamics:forward-dynamics:platform:full-eqom-vector}
\end{align}
%
which, given in matrix-vector notation for the generalized coordinates~$\genq$ and its time derivatives using the notation introduced in~\cref{eqn:fundamentals:dynamics:forward-dynamics:platform:newton-euler:any-reference-frame}, reads
%
\begin{align}
  %
  \Mass \of{\genq} \,
  \ddotgenq
  +
  \fictitiousforces \of{\genq, \dotgenq}
    & = \structmat \,
        \cabforces
        +
        \wrench_{ \subgravity}
        +
        \wrench_{\subprocess} \,
        .
        \label{eqn:fundamentals:dynamics:forward-dynamics:platform:2nd-order-ode}
\end{align}
