%!TEX root=../../../../thesis.tex
%!TEX file=content/fundamentals/dynamics/forward-dynamics/cables.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Cables}\label{sec:fundamentals:dynamics:forward-dynamics:cables}

To resolve the kinematic reduncany of having~$\numcable > \dof$ cables, the drive-commanded position values are recast into a force-based formulation.
This requires finding a model suitable for both numerical analysis and simulation as well as for closely capturing physical effects.
A kinematic formulation of the cable forces~$\cabforces$ impinging on the platform is defined such that every cable force~$\cabforce_{\cabindex}$ provides a unilateral constraint force that acts only if the cable is strained \textIe~$\delcablen_{\cabindex} > 0$, writing the equation of cable dynamics
%
\begin{align}
  \cabforce_{\cabindex}
    & = \begin{cases}
          \cabforce_{\cabindex_{\ms{mdl}}} \of{\delcablen_{\cabindex}, \dotdelcablen_{\cabindex}, t}
            & \delcablen_{\cabindex} > 0 \,
              ,
            \\
          0
            & \delcablen_{\cabindex} \leq 0 \,
              ,
        \end{cases}
        \label{eqn:fundamentals:dynamics:forward-dynamics:cables:formulation}
\end{align}
%
in which in most cases~\autocite{Behzadipour2006c,Bulin.2018,Dallej2012,Hajzman.2011,Lambert.2006,Nahon.1999,Miermeister2010,Palli.2012,Piao.2017,Tempel.2015e}, the model for cables forces~$\cabforce_{\cabindex_{\ms{mdl}}} \of{ \delcablen_{\cabindex}, \dotdelcablen_{\cabindex}}$ are calculated based on Hooke's law or a linear viscoelastic material model providing for the cable force model~$\cabforce_{\cabindex_{\ms{mdl}}}$
%
\begin{subequations}\label{eqn:fundamentals:dynamics:cable-force-models}%
\begin{align}
  \cabforce_{\cabindex_{\ms{elas}}} \of{\delcablen_{\cabindex}, \dotdelcablen_{\cabindex}, t}%
    & = \frac{
          \youngsmodulus \,
          \crosssection
        }{
          \cablen_{\cabindex, \subunstrained }
        } \,
        \delcablen_{\cabindex} \,
        ,
        \label{eqn:fundamentals:dynamics:elastic-force}
    \\
  \shortintertext{or respectively}
  \cabforce_{\cabindex_{\ms{visco}}} \of{\delcablen_{\cabindex}, \dotdelcablen_{\cabindex}, t}%
    & = \frac{
          \youngsmodulus \,
          \crosssection
        }{
          \cablen_{\cabindex, \subunstrained }
        } \,
        \delcablen_{\cabindex}
        +
        \frac{
          \viscousmodulus \,
          \crosssection
        }{
          \cablen_{\cabindex, \subunstrained }
        } \,
        \dotdelcablen_{\cabindex} \,
        .
        \label{eqn:fundamentals:dynamics:viscoelastic-force}
\end{align}
\end{subequations}
%
The mechanically consistent formulation of~\cref{eqn:fundamentals:dynamics:viscoelastic-force} in material coordinates strain~$\strain*$ and stress~$\stress*$ reads
%
\begin{align}
  \stress*_{\cabindex}
    & =
      \begin{cases}
        \youngsmodulus \,
        \crosssection \,
        \strain*
        +
        \viscousmodulus \,
        \crosssection \,
        \dotstrain *\,
        ,
          & \strain*_{ \cabindex } > 0 \,
            ,
          \\
        0 \,
        ,
          & \strain*_{\cabindex} \leq 0 \,
            .
      \end{cases}
      \label{eqn:fundamentals:dynamics:cables:kelvin-voigt}
\end{align}
%
Conditions different to the one given in~\cref{eqn:fundamentals:dynamics:elastic-force,eqn:fundamentals:dynamics:viscoelastic-force} on the zero-strain case may be enforced such as positive strain and positive strain rate \textIe~$\parentheses{ \strain*_{\cabindex} > 0 \land \dotstrain*_{\cabindex} > 0 }$.
Such conditions are rarely used, but may achieve better description of the modeled cable force as viscous forces may supersede elastic forces by magnitude yet opposite sign resulting in a negative cable tension.
