%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/kinematics.tex

\section{Kinematics}\label{sec:fundamentals:kinematics}

Despite its name being similar with the branch of classical mechanics, kinematics with respect to robotics focuses on applying geometric introspection to the motion of robotic systems~\autocite{McCarthy.1990b,Paul.1981}.
Before the two types of kinematic equations are mapped out, a brief introduction of nomenclature and variables used within this context are given.

In pursuance of solving kinematics of robotic systems, a clear notation of the robot configuration of interest must be given.
We call such a state a \emph{pose}\textemdash a set of position and orientation\textemdash given by the vector tuple~$\platpose$ defined as
%
\begin{align} 
  \platpose
    & = \vectset{ \platpos ; \platrot }, \label{eqn:pose-tuple}
\end{align}
%
in which~$\platpos$ is the position of the platform's point of reference\footnote{Usually, but not always, coinciding with its center of gravity, compare with \textEg \CoGiRo's dimensions given in~\cref{tbl:chap:robot-configurations:cogiro:geometry}.} and~$\platrot$ is the orientation of the platform with respect a body-fixed frame expressed in world-frame coordinates.
Parametrization for position and orientation may be chosen arbitrarily as long as position and orientation can be described through the following vector and matrix, respectively
%
\begin{align}
  \platpos
    & = %
      \begin{bmatrix}
        \posscal_{\mc{x}} \\
        \posscal_{\mc{y}} \\
        \posscal_{\mc{z}} \\
      \end{bmatrix} \, , 
    & \platrot
    & = %
      \begin{bmatrix}
        \rotscal_{11}
          & \rotscal_{12}
          & \rotscal_{13} \\
        \rotscal_{21}
          & \rotscal_{22}
          & \rotscal_{23} \\
        \rotscal_{31}
          & \rotscal_{32}
          & \rotscal_{33} \\
      \end{bmatrix} \,
    .
 \label{eqn:fundamentals:state-parametrization}
\end{align}
%
Rotation matrix~$\platrot$ transforms the platform local coordinate system~$\coordsys{\symplatform}$ into the global coordinate system~$\coordsys{\symworld}$.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics-problems}
  \includegraphics[%
      % width=0.75\linewidth,%
    ]{kinematics-problems}
  \tikzexternaldisable%
  \caption[%
    Relation of the two kinematic problems of robotic manipulators.
  ]{%
    Relation between the two kinematic problems of robotic manipulators: the \acrshort{FKP}~$\dirkin$ solves for the operational space coordinates~$\platpose$ while the \acrshort{IKP}~$\invkin$ solves for the joint space coordinates~$\jointcoords$.%
  }
  \label{fig:fundamentals:kinematics-problems}
\end{figure}

For the main, two kinematic problems exist~(see~\cref{fig:fundamentals:kinematics-problems}) complementing each other: inverse kinematics\textemdash also called \gls{IKP}\textemdash calculates joint space coordinates \textIe cable lengths, from operational space coordinates\textemdash world coordinates or Cartesian coordinates.
Reverse calculation\textemdash determining operational space coordinates from joint space coordinates\textemdash is called {\gls{FKP}}.
The reader is referred to literature showing the \gls{IKP} of parallel structures being easier to solve than the \gls{FKP} \textEg~\textcite{Merlet.2000b,Cubero.2006,Siciliano.2008,Merlet.2008b}.
Both problems can be solved on different levels of complexity ignoring or incorporating certain physical properties or effects.
As the contribution of this thesis gears mainly toward solving the \gls{IKP} and \gls{FDP}, only a brief overview of specifics to the \gls{FKP} will be given.

Motion of a \caro is achieved through changing cable lengths by means of coiling or uncoiling.
To determine the correct length needed along a spatial trajectory, the \gls{IKP} needs to be solved along that given trajectory.
The general formulation of the \gls{IKP} is given by the equation
%
\begin{align}
  \cablens
    & = \invkin{\platpose} \, , \label{eqn:fundamentals:general-ikp}  
\end{align}
%
in which~$\cablens = \transpose{ \left[ \cablen_{1} , \cablen_{2} , \dotsc \cablen_{\numcable} \right] }~$ is the solution to the \gls{IKP} as a function of pose~$\platpose$.
No matter the parametrization of the pose, the inverse kinematics function~$\invkin$ will always be a nonlinear function, though its complexity can be greatly reduced given some simplifications.
Vice versa, the pose~$\platpose = \vectset{ \platpos ; \platrot }$ is the solution to the \gls{FKP}
%
\begin{align}
  \platpose
    & = \dirkin{\cablens} \, , \label{eqn:fundamentals:general-fkp}  
\end{align}
%
as a function of~$\numcable$ cable lengths~$\cablens$.
It may be noted that calculated cable lengths denoted~$\cablens$ most generally differ from the geometric distances~$\vect{d}$ between the cable's proximal and distal point either due to cable strain or since cable sag is assumed.
We denote~$d_{\cabindex} = \norm{\linpos_{\cabindex, \subdistal} - \linpos_{\cabindex, \subproximal}}$ the geometric distance between the~{$\cabindex$-th} cable's proximal point~$\linpos_{\cabindex, \subproximal}$ and its distal point~$\linpos_{\cabindex, \subdistal}$, and~$\vect{d} = \transpose{ \left[ d_{1} , d_{2} , \dotsc d_{\numcable} \right] }$.

Solving~\cref{eqn:fundamentals:general-fkp} is only needed when the end effector position in space is needed in a control task like Cartesian position or force control.
Complexity of the \gls{FKP} highly depends on the parametrization of pose~$\platpose$ as well as the level of redundancy of the robotic system.
It is, additionally, not a priori known if solutions exist nor if they are unique~\autocite{Rolland.2005,Faugere.2006}.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/fundamentals/kinematics/inverse-kinematics}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/fundamentals/kinematics/forward-kinematics}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/fundamentals/kinematics/remark}
