%!TEX root=../../../thesis.tex
%!TEX file=content/fundamentals/kinematics/forward-kinematics.tex

\subsection{Forward Kinematics}\label{sec:fundamentals:forward-kinematics}


Due to its high nonlinearity, solving the \gls{FKP} provides for non-unique solutions~\autocite{Rolland.2005,Faugere.2006}.
\citeauthor{Dietmaier.1998,Raghavan1993} show existence of up to \num{40} real solutions to the \gls{FKP} of a \caro with~$\dof = \num{6}$ \gls{DOF} and likewise~$\numcable = \num{6}$ cables, not all of which must be physically feasible in the first place \autocite{Dietmaier.1998}~(see also~\autocite{Husty.1996,Pott.2018,Rolland.2005,Faugere.2006}).

With the cable robot pose being at least an~$\dof$-dimensional tuple and the control input being a tuple of dimension~$\numcable$, and under the constraint of~$\numcable \geq \dof$, we see that solving the forward kinematics is overdetermined in the cable lengths.
In addition, when referring back to the inverse kinematics problem, we observe the cable length being a scalar value obtained from a vector norm.
Mathematically, this involves solving for the square root of the radicand which is known to provide two solutions\textemdash the positive-signed or principal square root and the negative-signed square root.
To circumvent this shortcoming, it is possible to rewrite~\cref{eqn:fundamentals:inverse-kinematics:standard-cable-length} such that it contains no square roots and reads
%
\begin{align}
  \pow{
    \cablen_{\cabindex}
  }
    & = \pow{
          \norm{
            \frameanchor_{\cabindex}
            -
            \parentheses{
              \platpos
              +
              \platrot \,
              \platanchor_{\cabindex}
            }
          }
        }
      = \transpose{
          \cabdirn_{\cabindex}
        } \,
        \cabdirn_{\cabindex} \,
        .
        \label{eqn:fundamentals:forward-kinematics:cable-vector-loop-squared}
\end{align}
%
While above equation may still lead to two solutions, it is numerically more stable as it avoids calculating square roots~(see~\autocite{Pott.2015b} for an algorithmic point of view of the forward kinematics).
For solving the \gls{FKP}, we recast~\cref{eqn:fundamentals:forward-kinematics:cable-vector-loop-squared} into residual form in the to-be-estimated platform pose~$\platpose$ as there exists closed-form solutions only in certain robot configurations
%
\begin{align*}
  \zeros
    & = \pow{
          \cablens
        }
        -
        \invkin{\platpose}
      = \residual_{\ms{DK}} \of{\platpose} \,
        .
\end{align*}
%
Robot configurations with~$\dof = 2$ \glspl{DOF} and~$\numcable = 3$ cables provide for closed-form solutions, same as some robots with~$\dof = \numcable = 6$ of special geometry~\autocite{Hiller.2005b,Husty.2002,Merlet1990,Rolland.2005}

When it comes to calculating the solutions of the \gls{FKP}, there exist many different algorithms dealing with different motion patterns~(see~\cref{tbl:fundamentals:motion-patterns}) and different levels of redundancy~(see~\cref{sec:fundamentals:redundancy}) for which the numerical solutions need to be obtained differently.
In addition, different parametrization of the platform pose~$\platpose$ additionally require different handling of the \gls{FKP}~\autocite{Schmidt.2016}.
Lastly, it all comes down to solving the \gls{FKP} using either an iterative method like Newton's Method, Levenberg-Marquardt, or Trust-Region-Reflective~(a brief introduction into root-finding problems like these is given in~\cref{app:chap:root-finding}), or by employing interval analysis.
While methods based on interval analysis may provide all possible solutions, iterative methods may only find one of them and their region of convergence cannot be stated a priori.
Lastly, any solving method for the \gls{FKP} ultimately makes use of the inverse kinematics formulation.
In addition, some algorithms make use of the kinetostatic problem formulation that also takes into account cable elasticity by means of incorporating cable tensions into the problem formulation~(see \textEg~\textcite{Merlet.2015b,Merlet2015c}).
