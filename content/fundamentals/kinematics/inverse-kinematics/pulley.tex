%!TEX root=../../../../thesis.tex
%!TEX file=content/fundamentals/kinematics/inverse-kinematics/pulley.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Pulley-Based Kinematics}\label{sec:fundamentals:inverse-kinematics:pulley}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--pulley--vector-loop}
  \includegraphics[%
      width=0.35\textheight,%
    ]{kinematics/pulley/vector-loop}
  \tikzexternaldisable%
  \caption[%
    Kinematic loop of the pulley-based inverse kinematics problem.
  ]{%
    Kinematic loop of the pulley-based inverse kinematics problem with cable entry point~$\Ai$ at~$\ai$, corrected cable leave point~$\Ci$, platform pose~$\platpose = \vectset{ \platpos ; \platrot }$, and local platform anchor~$\Bi$ at~$\bi$ given in~$\coordsys{\symplatform}$.
  }%
  \label{fig:fundamentals:inverse-kinematics:pulley:kinematic-loop}
\end{figure}

While the standard inverse kinematics model is valid in many cases, it is\textemdash from a mechanical point of view\textemdash seldomly possible to achieve a perfect punctiform deflection unit.
For one reason being manufacturing tolerances yielding nonideal punctiform points\textemdash there always is some spatial extent to them\textemdash, for another reason being the sharpness of the punctiform unit causing the cables to grind on the surface ultimately causing premature cable rupture.
Pulley-based deflection units with the cable guided over a pivotally mounted pulley of circular shape reduce wear and tear on the cable increasing its lifespan drastically.
However, the cable now follows a longer path as it is now directed along the pulley circumference to the platform attachment point.
Extensions of the standard inverse kinematics model to include these effects were proposed by~\textcite{Bruckmann.2008,Pott.2012}.

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--pulley--topview}
    \includegraphics[%
        width=\linewidth,%
      ]{kinematics/pulley/topview}
    \tikzexternaldisable%
    \caption{%
      Pulley top view with winch coordinate system~$\coordsys{\symwinch}$ and pulley coordinate system~$\coordsys{\sympulley}$ rotated by swivel angle~$\swivelangle$ about~$\evecz[\subwinch]$.%
    }
    \label{fig:fundamentals:inverse-kinematics:pulley:top}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--pulley--sideview}
    \includegraphics[%
        width=\linewidth,%
      ]{kinematics/pulley/sideview}
    \tikzexternaldisable%
    \caption{%
        Side view of the pulley with angle of wrap~$\wrapangle[\cabindex]$ and opposite wrap angle~$\wrapangleopp[\cabindex]$.%
    }
    \label{fig:fundamentals:inverse-kinematics:pulley:side}
  \end{subfigure}%
  \caption[%
    Kinematic components of the pulley-based inverse kinematics.
  ]{%
    Kinematic components of the pulley-based inverse kinematics with side-view~\subref{fig:fundamentals:inverse-kinematics:pulley:side} in winch coordinates~$\coordsys{\symwinch}$ and top-view~\subref{fig:fundamentals:inverse-kinematics:pulley:top} in pulley coordinates~$\coordsys{\sympulley}$; cable drawn in red.%
  }
  \label{fig:fundamentals:inverse-kinematics:pulley:top-and-side}
\end{figure}

In short terms based on~\cref{fig:fundamentals:inverse-kinematics:pulley:kinematic-loop}, the pulley-based inverse kinematics model yields the equation for the~$\cabindex$-th cable as
%
\begin{align*}
  \cablen_{\cabindex}
    & = \radius_{\cabindex} \,
        \wrapangle[\cabindex]
        +
        \norm{
          \cabdirn_{\cabindex}
        }
\end{align*}
%
with the pulley radius~$\radius_{\cabindex}$, the angle of wrap of cable on the pulley~${\wrapangle[\cabindex]}$, and the opposite angle of wrap~$\wrapangleopp[\cabindex]$ defined as~${\wrapangleopp[\cabindex] = \pi - \wrapangle[\cabindex]}$,~(see~\cref{fig:fundamentals:inverse-kinematics:pulley:top-and-side}).
The cable direction vector~$\cabdirn_{\cabindex}$ corrects for the geometrical displacement of the cable leave point due to pulley swiveling and cable wrapping; it follows from
%
\begin{align}
  \cabdirn_{\cabindex}
    & = \frameanchor_{\cabindex}
        +
        \rotmatr[\subworld][\subwinch]_{\subpulley_{\cabindex}} \, % Rotation from winch initial to world
        \rotmatr[\subwinch][\subcable]_{\cabindex} \of{ \swivelangle_{\cabindex} } \, % Rotation from pulley to winch
        \parentheses*{ % Displacement along the surface of the pulley
          \eye
          +
          \rotmatr_{\cabindex} \of{ \wrapangle[\cabindex] }
        } \,
        \evecx \,
        \radius_{\cabindex}
        -
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
        } \,
        ,
        \label{eqn:fundamentals:inverse-kinematics:pulley-cable-length}
\end{align}
%
where~$\rotmatr[\subworld][\subwinch]_{\subpulley_{\cabindex}}$ is the winch rotation \textWrt world frame $\coordsys{\subworld}$,~$\rotmatr[\subwinch][\subcable]_{\cabindex} \of{ \swivelangle_{\cabindex} }$ is the rotation of the pulley \textWrt its winch with~$\swivelangle_{\cabindex}$ the angle of swivel of the~$\cabindex$-th pulley, and~$\rotmatr_{\cabindex} \of{\wrapangle[\cabindex]}$ is the rotation matrix of wrapping~(see~\cref{fig:fundamentals:inverse-kinematics:pulley:top-and-side}).

We can calculate~$\swivelangle_{\cabindex}$ by inspecting the position of~$\platanchor_{\cabindex}$ expressed in coordinates of frame~$\coordsys{\symwinch}$ obtained from
%
\begin{align*}
  \platanchor[\subwinch]_{\cabindex}
    & = \rotmatr[\subwinch] \,
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
          -
          \frameanchor_{\cabindex}
        } \,
        .
\end{align*}
%
Since the pulley rotates about~$\prescript{}{\subwinch}{\evecz}$ only, the angle of rotation is defined through the local~$x$ and~$y$ coordinates of~$\platanchor[\subwinch]$ thus
%
\begin{align*}
  \swivelangle_{\cabindex}
    & = \arctant{
          \platanchor[\subwinch]_{\cabindex, \ms{y}}
        }{
          \platanchor[\subwinch]_{\cabindex, \ms{x}}
        } \,
        .
\end{align*}

Given the platform anchor~$\platanchor[\subpulley]_{\cabindex}$ be expressed in pulley frame~$\coordsys{\sympulley}$, we can obtain the angle of wrap from
%
\begin{align*}
  \wrapangle
    & = \arctant*{%
          \lambda \,
          \parentheses*{
            \platanchor[\subpulley]_{\cabindex, \ms{x}}
            -
            \diameter_{\subpulley}
          }
          +
          \platanchor[\subpulley]_{\cabindex, \ms{z}} \,
          \diameter_{\subpulley}
        }{%
          -
          \diameter_{\subpulley} \,
            \parentheses*{
              \platanchor[\subpulley]_{\cabindex, \ms{x}}
              -
              \diameter_{\subpulley}
            }
            +
            \lambda \,
            \platanchor[\subpulley]_{\cabindex, \ms{z}}
        } \,
        ,
\end{align*}
%
where~$\lambda = \sqrt{ \platanchor[\ms{M}]_{\cabindex}^{2} - \diameter_{\subpulley}^{2} }$ is the geometric cable length in the workspace and~$\platanchor[\ms{M}]_{\cabindex}$ is the position of~$\platanchor_{\cabindex}$ with respect the pulley center such that~$\platanchor[\ms{M}]_{\cabindex} = \platanchor[\subpulley]_{\cabindex} - \diameter_{\subpulley} \, \evecx$.

With pulley based kinematics, the cable lengths~$\cablens$ differ from the geometric distances between its proximal point~$\frameanchor_{\cabindex}$ and its distal point~$\platanchor_{\cabindex}$ as the cable wound around the pulley has to be considered.
