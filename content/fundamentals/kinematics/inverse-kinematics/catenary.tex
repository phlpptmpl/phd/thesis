%!TEX root=../../../../thesis.tex
%!TEX file=content/fundamentals/kinematics/inverse-kinematics/catenary.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Mechanical Kinematics}\label{sec:fundamentals:inverse-kinematics:mechanical}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--catenary--catenary-vs-geometriclength}
    \includegraphics[%
      width=1.00\linewidth,%
      axisratio=1.00,%
      % height=0.35\textheight,%
    ]{kinematics/catenary/catenary-vs-geometric_length}
    \tikzexternaldisable%
    \caption{%
      Logarithmic display of the difference of cable lengths from standard kinematics and hefty kinematics when varying distal cable forces in their allowed range.
    }
    \label{fig:fundamentals:inverse-kinematics:mechanical:length-over-force}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--catenary--catenary-vs-geometricsag}
    \includegraphics[%
      width=1.00\linewidth,%
      axisratio=1.00,%
      % height=0.35\textheight,%
    ]{kinematics/catenary/catenary-vs-geometric_sag}
    \tikzexternaldisable%
    \caption{%
      Comparison of cable sag~(maximum sag marked with gray fill) for different cable forces reaching a maximum sag of \SI{0.6449}{\meter} at the minimum of allowed nominal distal cable forces.%
    }
    \label{fig:fundamentals:inverse-kinematics:mechanical:sagging-over-force}
  \end{subfigure}
  \caption[%
    Effect of nominal distal cable force on the resulting cable length.
  ]{%
    Effect of nominal distal cable force on the resulting cable length~\subref{fig:fundamentals:inverse-kinematics:mechanical:length-over-force} and on the resulting cable shape~\subref{fig:fundamentals:inverse-kinematics:mechanical:sagging-over-force}.
    Cable-elasticity induced elongation of the cable above the geometric distance affects unstrained length by up to~$\delcablen = \SI{0.133166}{\meter}$ and induces maximum sag of up to~$\SI{0.644875}{\meter}$.
  }
  \label{fig:fundamentals:inverse-kinematics:mechanical:effect-of-varying-force}
\end{figure}

While both the geometric and pulley-based inverse kinematics provide a good approximation under a wide range of assumptions, it has limitations due to the assumption  of totally massless cables.
In fact, for cable robots even of the \COPacabana size~(robot geometry given in~\cref{tbl:chap:robot-configurations:copacabana-handling:geometry,tbl:chap:robot-configurations:copacabana-inspection:geometry}, respectively), this assumption does not hold true.
Even though cable mass is relatively low at~$\unitdensity = \SI{970}{\kilo\gram\per\cubic\meter}$ compared to steel cables at~$\unitdensity = \SI{7800}{\kilo\gram\per\cubic\meter}$, cable sag occurs last in case of low cable tension.
\Cref{fig:fundamentals:inverse-kinematics:mechanical:length-over-force} shows numerically evaluated unstrained cable lengths given a range of distal cable forces of~$\cabforce \in \SIset{100;3000}{\newton}$, while~\cref{fig:fundamentals:inverse-kinematics:mechanical:sagging-over-force} shows the actual cable shape and apparent sagging.
It is obvious that low-tension cables do no longer form a straight line between two points but sag at a maximum of~$\SI{0.6449}{\meter}$.
This effect of cable sag is an apparent characteristic of \caros such as the \Expo \caros~\autocite{Tempel.2015g}, as well as on the \SI{500}{\meter}-aperture \Fast \caro~\autocite{Kozak.2006,Nan.2011}, where deflections in the pose of absolute up to~$\SI{4}{\meter}$ and in the cable lengths of absolute up to~$\SI{7}{\meter}$ occur due to cable sag.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--catenary--vector-loop}
  \includegraphics[%
      width=0.35\textheight,%
    ]{kinematics/catenary/vector-loop}
  \tikzexternaldisable%
  \caption[%
    Kinematic loop of inverse kinematics problem hefty cables.
  ]{%
    Kinematic loop of inverse kinematics problem for hefty cables with cable entry point~$\frameanchor_{\cabindex}$, platform pose~$\platpose = \vectset{ \platpos ; \platrot }$, and local platform anchor~$\platanchor_{\cabindex}$ given in~$\coordsys{\symplatform}$.
    Cable drawn in red highlights~(exaggeratedly) cable sagging where it does not form a straight line between~$\frameanchor_{\cabindex}$ and~$\platanchor_{\cabindex}$.
  }
  \label{fig:fundamentals:inverse-kinematics:mechanical:vector-loop}
\end{figure}

\Citeauthor{Irvine.1974} was first to derive the kinetostatics of cables under the influence of gravitation in~\citeyear{Irvine.1974}~\autocite{Irvine.1974}, based on which~\citeauthor{Kozak.2006} derived the kinetostatic problem of \caros with cables of non-negligible mass~\autocite{Kozak.2006}.
We briefly derive the essential equations according to~\autocite{Irvine.1974,Kozak.2006} and given~\cref{fig:fundamentals:inverse-kinematics:mechanical:standalone}.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--catenary--cable}
  \includegraphics[%
      height=0.35\textheight,%
    ]{kinematics/catenary/cable}
  \tikzexternaldisable%
  \caption[%
    Diagram of sagging cable acting under the influence of gravity.%
  ]{%
    Diagram of sagging cable acting under the influence of gravity fixed at cable drawing point~$\Ai$ with force~$\forcevec_{\Bi}$ applied at its distal attachment point~$\Bi$.% with unit density~$\unitdensity$, Young modulus~$\youngsmodulus$, and unstrained cross-section~$\crosssection$.
  }
  \label{fig:fundamentals:inverse-kinematics:mechanical:standalone}
\end{figure}

Considering a linear-elastic cable with Young's Modulus~$\youngsmodulus$, cross-section~$\crosssection$, and unit density~$\unitdensity$, satisfying Hooke's law, its tension at some point~$\arbpoint = \arbpoint \of{\lpathcoord}$ along the unstrained path length reads
%
\begin{align}
  \strain* \of{\lpathcoord}
    & = \youngsmodulus \,
        \crosssection \,
        \parentheses*{
          \od{
            \strainedlpathcoord
          }{
            \lpathcoord
          }
          -
          1
        } \,
        ,
        \label{eqn:fundamentals:inverse-kinematics:mechanical:hookes-law}
\end{align}
%
where~$\od{\strainedlpathcoord}{\lpathcoord}$ is the cable strain at~$\lpathcoord$.
At point~$\arbpoint$, with horizontal and vertical force component~$\force_{\Bi{\ms{x}}}$ and~$\force_{\Bi{\ms{z}}}$, respectively, the static equilibrium of forces must read
%
\begin{subequations}
\begin{align*}
  \strain* \of{\lpathcoord} \,
  \od{
    x
  }{
    \strainedlpathcoord
  }
    & = \force_{\Bi{\ms{x}}} \,
    ,
    \\
  \strain* \of{\lpathcoord} \,
  \od{
    z
  }{
    \strainedlpathcoord
  }
    & = \force_{\Bi{\ms{z}}}
        +
        \unitdensity \,
        \crosssection \,
        \parentheses*{
          \lpathcoord
          -
          \cablen_{\subunstrained}
        }
    .
\end{align*}
\end{subequations}

Solving for~$\td{x} / \td{\strainedlpathcoord}$ and~$\td{z} / \td{\strainedlpathcoord}$, respectively, under assumption of the geometric constraint%
\begin{align*}
  \pow*[2]{
    \od{
      x
    }{
      \lpathcoord
    }
  }
  +
  \pow*[2]{
    \od{
      z
    }{
      \lpathcoord
    }
  }
    & = 1 \,
        ,
\end{align*}
%
and identities
%
\begin{align*}
  \od{
    \parentheses{}
  }{
    \lpathcoord
  }
    & = \od{
          \parentheses{}
        }{
          \strainedlpathcoord
        } \,
        \od{
          \parentheses{}
        }{
          \lpathcoord
        } \,
        ,
\end{align*}
%
we find another expression for the tension at unstrained path coordinate~$\lpathcoord$, such that
%
\begin{align*}
  \strain* \of{\lpathcoord}
    & = \sqrt{
          \pow*[2]{
            \force_{\Bi{\ms{x}}}
          }
          +
          \pow*[2]{
            \force_{\Bi{\ms{z}}}
            +
            \unitdensity \,
            \crosssection \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }
        } \,
        ,
\end{align*}
%
which in turn must also satisfy~\cref{eqn:fundamentals:inverse-kinematics:mechanical:hookes-law}.
Requiring such equality and applying some mathematical simplifications, we obtain differential equations for the cable shape along~$x$ and~$z$ in~$\lpathcoord$
%
\begingroup
\allowdisplaybreaks[0]
\begin{subequations}\label{eqn:fundamentals:inverse-kinematics:mechanical:geometric-differential-equations}
\begin{align}
  \od{
    x
  }{
    \lpathcoord
  }
    & = \frac{
            \force_{\Bi{\ms{x}}}
          }{
            \youngsmodulus \,
            \crosssection
          }
          +
          \frac{
            \force_{\Bi{\ms{x}}}
          }{
            \sqrt{
              \pow{
                \force_{\Bi{\ms{x}}}
              }
              +
              \pow*[2]{
                \force_{\Bi{\ms{z}}}
                +
                \unitdensity \,
                \gravity* \,
                \parentheses*{
                  \lpathcoord
                  -
                  \cablen_{\subunstrained}
                }
              }
            }
          } \,
    \\
    \od{
      z
    }{
      \lpathcoord
    }
      & = \frac{
            \force_{\Bi{\ms{z}}}
          }{
            \youngsmodulus \,
            \crosssection
          }%
          +
          \frac{
            \unitdensity \,
            \gravity* \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }{
            \youngsmodulus \,
            \crosssection
          }
        +
        \frac{
          \force_{\Bi{\ms{z}}}
          +
          \unitdensity \,
          \gravity* \,
          \parentheses*{
            \lpathcoord
            -
            \cablen_{\subunstrained}
          }
        }{ 
          \sqrt{
            \pow{
              \force_{\Bi{\ms{x}}}
            }
            +
            \pow*[2]{
              \force_{\Bi{\ms{z}}}
              + 
              \unitdensity \,
              \gravity* \,
              \parentheses*{
                \lpathcoord
                -
                \cablen_{\subunstrained}
              }
            }
          }
        } \,
        .
\end{align}
\end{subequations}
\endgroup

Integrating~\cref{eqn:fundamentals:inverse-kinematics:mechanical:geometric-differential-equations} for~$\lpathcoord$ and applying the boundary conditions at~$\lpathcoord = 0$ taken from~\cref{fig:fundamentals:inverse-kinematics:mechanical:standalone} \textIe~$x \of{0} = 0$ and~$z \of{0} = 0$, yields the equation of the cable sagging under the influence of gravity
%
\begin{subequations}\label{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}
\begin{align}
\begin{split}
  x \of{\lpathcoord}
    & = \frac{
          \force_{\Bi{\ms{x}}}
        }{
          \youngsmodulus \,
          \crosssection
        } \,
        \lpathcoord
        +
        \frac{
          \abs{\force_{\Bi{\ms{x}}}}
        }{
          \unitdensity \,
          \gravity*
        } \,
        \biggl(
          \parcsinh*{
            \frac{
              \force_{\Bi{\ms{z}}}
              +
              \unitdensity \,
              \gravity* \,
              \parentheses*{
                \lpathcoord
                -
                \cablen_{\subunstrained}
              }
            }{
              \force_{\Bi{\ms{x}}}
            }
          }
        +
        \dotsb
    \\
    & \phantom{=}
      \phantom{
        \frac{
          \force_{\Bi{\ms{x}}}
        }{
          \youngsmodulus \,
          \crosssection
        } \,
        \lpathcoord
        +
        \frac{
          \abs{\force_{\Bi{\ms{x}}}}
        }{
          \unitdensity \,
          \gravity*
        } \,
        \biggl(
      }
      -
      \parcsinh*{
        \frac{
          \force_{\Bi{\ms{z}}}
          -
          \unitdensity \,
          \gravity* \,
          \parentheses*{
            \lpathcoord
            -
            \cablen_{\subunstrained}
          }
        }{
          \force_{\Bi{\ms{x}}}
        }
      }
      \biggr) \,
      ,
\end{split}
    \\
\begin{split}
  z \of{\lpathcoord}
    & = \frac{
          \lpathcoord
        }{
          \youngsmodulus \,
          \crosssection
        } \,
        \parentheses*{
          \force_{\Bi{\ms{z}}} %
          +
          \unitdensity \,
          \gravity* \,
          \parentheses*{
            \frac{
              \lpathcoord
            }{
              2
            }
            -
            \cablen_{\subunstrained}
          }
        }
        +
        \dotsb
        \\
    & \phantom{=}
      +
      \frac{
        1
      }{
        \unitdensity \,
        \gravity*
      } \,
      \biggl(
        \sqrt{
          \pow{
            \force_{\Bi{\ms{x}}}
          }
          +
          \pow*[2]{
            \force_{\Bi{\ms{z}}}
            +
            \unitdensity \,
            \gravity* \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }
        }
        +
        \dotsb
    \\
    & \phantom{=}
      \phantom{
        +
        \frac{
          1
        }{
          \unitdensity \,
          \gravity*
        } \,
        \biggl(
      }
      -
      \sqrt{
        \pow{
          \force_{\Bi{\ms{x}}}
        }
        +
        \pow*[2]{
          \force_{\Bi{\ms{z}}}
          -
          \unitdensity \,
          \gravity* \,
          \cablen_{\subunstrained}
        }
      }
      \biggr) \,
      .
\end{split}
\end{align}
\end{subequations}

To determine the strained length of the cable between proximal and distal point, we integrate~\cref{eqn:fundamentals:inverse-kinematics:mechanical:hookes-law} with respect to the~$\lpathcoord$ and evaluate for~$\lpathcoord = \cablen_{\subunstrained}$, yielding the strained length~$\cablen_{\substrained} = \cablen_{\subunstrained} + \delcablen$ with $\delcablen$ calculated from
%
\begin{align*}
  \delcablen
    & = \frac{
          1
        }{
          2 \,
          \unitdensity \,
          \youngsmodulus \,
          \crosssection
        } \,
        \Biggl(
          \force_{\Bi{\ms{z}}} \,
          \sqrt{
            \pow{
              \force_{\Bi{\ms{x}}}
            }
            +
            \pow{
              \force_{\Bi{\ms{z}}}
            }
          }
          +
          \pow{
            \force_{\Bi{\ms{x}}}
          } \,
          \parcsinh*{
            \frac{
              \force_{\Bi{\ms{z}}}
            }{
              \abs{\force_{\Bi{\ms{x}}}}
            }
          }
          +
          \dotsb
    \\
    & \phantom{=}
      \phantom{
        \frac{
          1
        }{
          2 \,
          \unitdensity \,
          \youngsmodulus \,
          \crosssection
        } \,
        \Biggl(
      }
      -
      \parentheses*{
        \force_{\Bi{\ms{z}}}
        -
        \unitdensity \,
        \gravity* \,
        \cablen_{\subunstrained}
      } \,
      \sqrt{
        \pow{
          \force_{\Bi{\ms{x}}}
        }
        +
        \pow*{
          \force_{\Bi{\ms{z}}}
          -
          \unitdensity \,
          \gravity* \,
          \cablen_{\subunstrained}
        }
      }
      +
      \dotsb
    \\
    & \phantom{=}
      \phantom{
        \frac{
          1
        }{
          2 \,
          \unitdensity \,
          \youngsmodulus \,
          \crosssection
        } \,
        \Biggl(
      }
      -
      \pow{
        \force_{\Bi{\ms{x}}}
      } \,
      \parcsinh*{
        \frac{
          \force_{\Bi{\ms{z}}}
          -
          \unitdensity \,
          \gravity* \,
          \cablen_{\subunstrained}
        }{
          \force_{\Bi{\ms{x}}}
        }
      }
      \Biggr) \,
      .
\end{align*}

Since cable shape as well as cable length are now directly linked to the force at the distal cable end, solving~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution} now requires two out of the three independent variables 
%
\begin{inparaenum}[1)]
  \item distal point coordinates,
  \item applied distal cable force, or
  \item unstrained cable length.
\end{inparaenum}
%
In general, the solution to~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution} must be found iteratively for the unknown variable\textemdash usually the unstrained cable length.
It may be pointed out when considering cable mass in the \gls{IKP}, the problems of finding a suitable cable force distribution and solving the \gls{IKP} are now no longer decoupled; the problem turns from a purely geometric formulation to a kinetostatic formulation.
As such, we need to solve the kinetostatic problem from~\cref{eqn:fundamentals:statics:static-equilibrium} while at the same time respecting~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}~(see~\cref{sec:fundamentals:statics} for the mathematical formulation).
This fact may also be visually apprehended from~\cref{fig:fundamentals:inverse-kinematics:mechanical:vector-loop} showing the vector loop of the inverse kinematics under consideration of cable mass.
The cable attached to the platform produces additional downwards drag in the vertical direction, which must be compensated for by opposing cables.
