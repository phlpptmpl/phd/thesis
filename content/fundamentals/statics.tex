%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/statics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Statics}\label{sec:fundamentals:statics}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{platform-standalone-forces}
  \includegraphics[%
      width=0.50\linewidth,%
    ]{platform-standalone-forces}
  \tikzexternaldisable%
  \caption[%
    Schematic of the static equilibrium of a \caro mobile platform.
  ]{%
    Schematic of the static equilibrium of a \caro mobile platform at pose~$\platpose = \vectset{ \platpos ; \platrot }$ with cable forces~$\cabforcedirn_{\cabindex}$ and external forces due to gravity~$\gravityvec$.
  }
  \label{eqn:fundamentals:dynamics:forward-dynamics:free-floating-rigid-body}
\end{figure}

Since cables used in \caros can only exert tensile forces, it is not a priori known if a pose can be statically attained.
Answering this question involves solving the static equilibrium at a given pose and under the influence of external forces reading
%
\begingroup
\allowdisplaybreaks[0]
\begin{align*}
  \zeros
    & = \forcevec_{\subplatform}
        +
        \sum\limits_{\cabindex = 1}^{\numcable}{
          \cabforcedirn_{\cabindex}
        } \,
        ,
    \\
  \zeros
    & = \torquevec_{\subplatform}
        +
        \sum\limits_{\cabindex = 1}^{\numcable}{
          \torquevec_{\cabindex}
        } \,
        ,
\end{align*}
\endgroup
%
where~$\forcevec_{\subplatform}$ and~$\torquevec_{\subplatform}$ are, respectively, the forces and torques applied onto the point of reference on the platform and~$\torquevec_{\cabindex} = \crossp{\bi}{\cabforcedirn_{\cabindex}}$ are the torques induced by~$\cabindex$-th cable force~$\cabforcedirn_{\cabindex}$.
By splitting each cable force~$\cabforcedirn_{\cabindex}$ into its nominal value~$\cabforce_{\cabindex}$ and direction~$\cabudirn_{\cabindex}$ for which holds~$\norm{ \cabudirn_{\cabindex} } \equiv 1$ such that~$\cabforcedirn_{\cabindex} = \cabforce_{\cabindex} \, \cabudirn_{\cabindex}$, we can recast the~$\cabindex$-th cable's external wrench to read
%
\begin{align}
  \wrench_{\subcable_{\cabindex}}
    & = \begin{bmatrix}
        \cabforce_{\cabindex} \,
        \cabudirn_{\cabindex}
          \\
        \cabforce_{\cabindex} \,
        \crossp*{ \platanchor_{\cabindex} }{ \cabudirn_{\cabindex} }
      \end{bmatrix}
      = \cabforce_{\cabindex} \,
        \begin{bmatrix}
          \cabudirn_{\cabindex} \\
          \crossp{ \platanchor_{\cabindex} }{ \cabudirn_{\cabindex} }
        \end{bmatrix} \,
        ,
        \label{eqn:fundamentals:statics:cable-wrench-definition}
\end{align}
%
given that cable force~$\cabforcedirn_{\cabindex}$ generates a torque about platform point of reference~$\symplatform$ at distance~$\platanchor_{\cabindex}$.

To determine the full cable tension-induced wrench, we add~\cref{eqn:fundamentals:statics:cable-wrench-definition} for all~$\numcable$ cables and obtain the total cable wrench~$\wrench_{\subcable}$ in matrix-vector form as
%
\begin{subequations}
\begin{align}
  \wrench_{\subcable}
        & =
          \underbrace{
            \begin{bmatrix}
              \cabudirn_{1}
                & \cabudirn_{2}
                & \dotso
                & \cabudirn_{\numcable} \\
              \crossp{ \platanchor_{1} }{ \cabudirn_{1} }
                & \crossp{ \platanchor_{2} }{ \cabudirn_{2} }
                & \dotso
                & \crossp{ \platanchor_{\numcable} }{ \cabudirn_{\numcable} }
            \end{bmatrix}
          }_{\structmat}
          \cdot
          \underbrace{
            \begin{bmatrix}
              \cabforce_{1} \\
              \cabforce_{2} \\
              \vdots \\
              \cabforce_{\numcable}
            \end{bmatrix}
          }_{\cabforces}
        = \structmat \,
          \cabforces \,
          ,
          \label{eqn:fundamentals:statics:cable-wrench-structure-matrix}
\end{align}
\end{subequations}
%
with~$\structmat$ being the so-called \textit{structure matrix} first named by~\textcite{Verhoeven.2004c}.
For solving the statics of \caros\footnote{A brief overview of solving~\cref{eqn:fundamentals:statics:static-equilibrium} for~$\cabforces$ will be given in~\cref{sec:fundamentals:cable-force-distribution}.}, we then solve the equation
%
\begin{align}
  \zeros
    & = \structmat \,
        \cabforces
        +
        \wrench \,
        ,
        \label{eqn:fundamentals:statics:static-equilibrium}
\end{align}
%
in which the wrench~$\wrench$ combines all external wrenches such as gravitational forces and torques or process forces and torques.
Finding one or all solutions to~\cref{eqn:fundamentals:statics:static-equilibrium} is a nontrivial task since the cable forces must always be positive\textemdash cables can only exert tensile forces\textemdash and generally, the number of cables~$\numcable$ outgrows the number of \gls{DOF}~$\dof$ resulting in~\cref{eqn:fundamentals:statics:static-equilibrium} being an underdetermined system linear of equations.
What is to be kept in mind is the source of entries of structure matrix~$\structmat$ which are time-independent platform anchor positions~$\platanchor_{\cabindex}$ and time and pose-dependent cable directions~$\cabudirn_{\cabindex}$.
We observe the structure matrix's numerical values to be dependent on the cable model put in use since generally the platform-sided cable force direction is a function of the cable model.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Kinetostatics}

The kinetostatic problem combines the static equilibrium from~\cref{eqn:fundamentals:statics:static-equilibrium} with a cable model of choice, such as a massless viscoelastic cable~(see~\cref{sec:fundamentals:dynamics:forward-dynamics:cables}) or a hefty elastic cable~(see~\cref{sec:fundamentals:inverse-kinematics:mechanical}).
In either case, the kinetostatic equations is rewritten to read
%
\begin{subequations}\label{eqn:fundamentals:statics:kinetostatic-equilibrium}
\begin{align}
  \zeros
    & = \structmat \,
        \cabforces
        +
        \wrench \,
        ,
        \label{eqn:fundamentals:statics:kinetostatic-equilibrium:force}
        \\
  \zeros
    & = \linpos_{\subdistal, \cabindex} \of{\cablens_{\cabindex, \subunstrained} , \dotsc}
        -
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
        } \,
        ,
      \quad \cabindex = \parentheses*{1 , \dotsc , \cabindex} \,
      ,
      \label{eqn:fundamentals:statics:kinetostatic-equilibrium:geometric}
\end{align}
\end{subequations}
%
in which~\cref{eqn:fundamentals:statics:kinetostatic-equilibrium:geometric} ensures that the~$\cabindex$-th cable's distal point~$\linpos_{\subdistal, \cabindex} \of{\cablens_{\cabindex, \subunstrained} , \dotsc}$ as a function of at least the unstrained cable length~$\cablens_{\cabindex, \subunstrained}$ equals the spatial position of the respective platform cable anchor.
The kinetostatic problem is then formulated to read
%
\begin{subequations}\label{eqn:fundamentals:statics:kinetostatic-problem}
\begin{align}
  \text{solve}
    & 
    & \cablens^{\ast}
    & = \arg \min\limits_{\cablens} \norm{ \cablens }
    \\
  \text{subject to}
    &
    & \zeros
    & = \structmat \,
        \cabforces
        +
        \wrench \,
        ,
        \label{eqn:fundamentals:statics:kinetostatic-problem:forces}
        \\
    &
    & \zeros
    & = \linpos_{\subdistal, \cabindex} \of{\cablens_{\cabindex, \subunstrained} , \dotsc}
        -
        \parentheses*{
          \platpos
          +
          \platrot \,
          \platanchor_{\cabindex}
        } \,
        ,
        \label{eqn:fundamentals:statics:kinetostatic-problem:geometric}
\end{align}
\end{subequations}
%
where our cost functional is determined by minimizing cable lengths~$\cablens$ under the nonlinear constraints of static equilibrium~(\cref{eqn:fundamentals:statics:kinetostatic-problem:forces}) and of all cable distal ends being located at the platform~(\cref{eqn:fundamentals:statics:kinetostatic-problem:geometric}).
At the end, we obtain the unstrained cable lengths~$\cablens^{\ast}$ satisfying static equilibrium.
