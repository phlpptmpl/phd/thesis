%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/mechatronic-system.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mechatronic System}\label{sec:fundamentals:mechatronic-system}

\begin{figure}[tbp]
  \centering
  \smaller[2]
  % \tikzexternalenable%
  % \tikzsetnextfilename{mechatronic-system-model}
  \includegraphics[%
      width=1.00\linewidth,%
    ]{mechatronic-system-model}
  % \tikzexternaldisable%
  \caption[%
    System-dynamics model of the mechatronic system of \caros.%
  ]{%
    System-dynamics model of the mechatronic system of \caros with the robot electrics in the left half and the robot mechanics in the right half.
  }
  \label{fig:fundamentals:mechatronic-system-representation}
\end{figure}

Since \caros are more than just their mobile platform or end effector, it is beneficial to consider a full mechatronic system for representation~(see~\cref{fig:fundamentals:mechatronic-system-representation}).
A \caro usually consists of one mobile platform with~$\dof$ \gls{DOF} and~$\numcable$ cables, each attached to one winch.
These winches are made up of a drum which coils and uncoils the cable, and a motor~(most often a synchronous servo motor).
Lastly, a control system is used to correctly drive all motors to synchronously control the cable coiling.
Each of these parts has its own dynamics influencing the overall system dynamics as has been previously investigated for the \IPAnema \caro family~\autocite{Miermeister.2010b,Kraus.2015c,Tempel.2015d}.
A short overview of these components will be given in the following list in order from platform to winch, while the mathematical models for dynamics will be further explained and quantified in~\cref{sec:fundamentals:dynamics}.

\begin{description}
  \item[Platform] Without loss of generality, it can be assumed that for any \caro class, the platform is rigid and of given dimensions and inertia.
    This allows the platform to be considered as rigid body for which the equations of motion can be derived using Newton-Euler equations, Lagrangian mechanics, or d'Alembert's principle.
    The forces causing platform motion come from
    \begin{compactenum}
      \item cables, due to their strain and their spatial motion;
      \item a process \textIe forces and torques from \textEg a milling process;
      \item the environment, where in most cases only forces and torques due to gravity are considered.
    \end{compactenum}
  \item[Cable] The connecting and motion-generating elements of a \caro.
  It connects to the platform on the distal end and to the winch on the proximal end.
    In comparison to other \caro components, cables show lower tensile rigidity and almost negligible flexural rigidity.
    That is to say, cables are elastic when it comes to longitudinal deformation \textIe stretching, and almost perfectly elastic when it comes to transversal deformation \textIe bending.
    This leads to more sophisticated behavior of the cables when considering it in modeling or control of \caros.
    In addition, cables are less resilient to resisting transversal motion causing them to vibrate when either end is moved not in direction of the neutral axis.
  \item[Pulley] In the simplest case, a pulley can be considered a roller of certain diameter and gauge.
    As with other bodies of mass, these systems undergo certain dynamics which in the case of a pulley reflect mostly as rotational dynamics as the cable runs on the circumference of the pulley.
    It has been observed during experiments, not only in \caros, that pulleys affect both the cable tension due to their rotational inertia when the cable abruptly changes direction of travel~\autocite{Kraus.2015b,Miyasaka.2015}, as well as affecting the effective cable direction due to bearing friction of the swiveling arm of the pulley.
  \item[Drum] Designed as a hollow cylinder, the drum itself has limited inertia affecting only winding dynamics and accuracy rather than platform dynamics.
    A larger impact on the dynamics however comes from friction in the bearings at either end of the drum and backlash in the gearbox.
    Especially for small torques transmitted from the motor to the drum, static friction prohibits the drum from moving right away and may cause backlash to be visible in the cable tension.
  \item[Motor] Each drum is driven by a motor receiving its command value from the control system.
    This commanded value can be either a motor torque, angular velocity, or angular position, depending on the control system's configuration.
    In case of the demonstrators used in this thesis, angular position is commanded to the motors which then control angular velocity and torque.
  \item[Control System] %The heart and brain of the \caro.
    All input to the system and output from the system is controlled and processed by the control system.
    With the system running in a dedicated process in real-time at a given sampling rate set to~$\SI{1}{\milli\second}$ for the \COPacabana and \IPAnema \caros, there are certain dynamics in the process itself.
    These dynamics come from how computers and especially control computers operate as they read and write the output only at certain instances to and from a shared memory or a field bus.
    This implies a certain delay between setting an output value such as the set-point of a drive and the target system actually obtaining this value.
    This delay has been identified to be a multiple of the control cycle time~\autocite{Kraus.2014}.
    If \caros were to reach even higher dynamics and still remain controllable\footnote{With maximum velocities of up to~$\maxnorm{\linacc} = \SI{30}{\meter\per\second}$ on some systems, a \caro will travel~$\SI{30}{\milli\meter}$ within one clock of~$\stepsize = \SI{1}{\milli\second}$ of the control system.
    This distance can be crucially important in case of high-speed and high-accuracy application requirements.}, the clock frequency of the control system also plays a vital role in process stability.
\end{description}
