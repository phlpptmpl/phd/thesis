%!TEX root=../../thesis.tex
%!TEX file=content/fundamentals/classification.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Classification}\label{sec:fundamentals:classification}

The concepts of classifying \caros is not without reason taken from their rigid-body counterparts, as both systems are dually equivalent.
Despite the use of cables as main driving elements, \caros can be classified in terms of their level of redundancy or their \glspl{DOF}.
Redundancy, in general terms, relates to the number of \glspl{DOF} of a robotic manipulator with respect to a given task.
If the robot has more \glspl{DOF} than required for a given motion, it is called kinematically redundant~\autocite{Cubero.2006,Merlet2015}.
%A \caro can be classified, as introduced by~\citeauthor{Ming.1994,Verhoeven.2004c}, depending on its actuator redundancy~$r = m - n$ which yields three different classes.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Redundancy}\label{sec:fundamentals:redundancy}

Originally introduced by~\citeauthor{Ming.1994}, \caros can be classified depending on their actuator redundancy~$\redundancy = \numcable - \dof$, yielding two fundamentally different classes~\autocite{Ming.1994}.
An extension derived by~\citeauthor{Verhoeven.2004c} splits the second class into two sub-classes that are concept-wise the same but differ in their respective level of redundancy~\autocite{Verhoeven.2004c}:
%
\begin{enumerate}
  \item \emph{\Acrlong{IRPM}}~(\acrshort{IRPM}):
    Number of cables $\numcable$ is less than or equal to the number of \glspl{DOF} $\dof$ \textIe~$\numcable \leq \dof$.
    If $\numcable = \dof - 1$, gravitational force stemming from a mobile platform with comparably high mass%
    \footnote{``Comparably'' in this sense means the weight of the platform~$\platformmass$ is much larger than the total weight of the cables \textIe~$\platformmass \gg 10 \, \mass_{\subcable}$.}
    acts as a virtual cable~(the $\parentheses*{\numcable + 1}$-th cable).
    In any other case, one or more \glspl{DOF} will remain uncontrollable\textemdash imagine a crane with $\numcable = 1$ cables but $\dof = 6$ \glspl{DOF} of the end-effector.
  \item \emph{\Acrlong{CRPM}}~(\acrshort{CRPM}):
    With~${\numcable = \dof + 1}$ cables, the system state is completely defined by the cable tensions and no external forces are needed.
  \item \emph{\Acrlong{RRPM}}~(\acrshort{RRPM}):
    Such mechanisms feature more cables than \glspl{DOF} \textIe~$\numcable > \dof + 1$, which makes them redundant in the number of constraints, not in their kinematics.
\end{enumerate}

As noted by~\citeauthor{Verhoeven.2004c}, the classes \gls{CRPM} and \gls{RRPM} can more generally be referred to as \gls{FRPM}~\autocite{Verhoeven.2004c}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Degrees of Freedom}\label{sec:fundamentals:degrees-of-freedom}

\begin{table}[tbp]
  \centering%
  \smaller[1]
  \caption[%
    Classification scheme of \caros.
  ]{%
    Classification scheme of \caros depending on the number of \acrshort{DOF} of the mobile platform where \emph{$\symtranslational$} stands for translational and \emph{$\symrotational$} stands for rotational \glspl{DOF}, respectively, after~\textcite{Verhoeven.2004c}.%
  }%
  \label{tbl:fundamentals:motion-patterns}%
  \input{tables/motion-patterns}
\end{table}

Another concept of classification of parallel robots makes use of the number of \glspl{DOF} and type of \glspl{DOF} of the mobile platform or end effector.
Going from the simplest case of end effector \glspl{DOF} with~$\dof = 1$ up to the spatial case with~$\dof = 6$~(here~$\dof_{\subtranslational} = 3$ translational and~$n_{\subrotational} = 3$ rotational) \glspl{DOF}, respectively, a scheme as shown in~\cref{tbl:fundamentals:motion-patterns} can be mapped out.
These cases cover all possible configurations of end effector motion and were previously introduced by~\citeauthor{Verhoeven.2004c}.
His exhaustive attempt to investigate all fully parallel \caros prove the list complete under the assumption of each cable being actuated independently of the others~\autocite{Verhoeven.2004c}.
He also showed that there exist no \caros with purely rotational motion patterns \textIe \textsc{1R}, \textsc{2R}, and \textsc{3R} are not possible, much like a \MP{1}{3} \caro~(Schoenflies motion) and \MP{2}{2} are not possible to build.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{Closing Remarks}

Despite being able to classify \caros using both of the above mentioned procedures, some \caros presented in~\cref{sec:introduction:applications-demonstrators} differ from others in ways that they are so-called \emph{suspended} \caros whereas others are \emph{redundantly restrained}.
The actual difference between these two classes lies in the routing of the cables from the platform to the winches.
In case of suspended \caros, the cables are all routed upwards from the platform, for redundantly restrained systems cables are routed likewise upwards and downwards from the platform~(see~\cref{fig:fundamentals:suspended-vs-restrained} for a visualization).
These two classes are fundamentally different as the former class does require gravitational force to keep all cables taut.
This not only affects the kinematics and dynamics of such systems, but also algorithms for determination of valid cable tension distributions or workspaces.

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=1.00\linewidth,%
      ]{schematic-caro/suspended}
    \caption{%
      Schematic drawing of a suspended \caro in \CoGiRo configuration with its cables routed only upwards away from the platform.%
    }
    \label{fig:fundamentals:cable-robot-scheme:suspended}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{schematic-caro/redundantly-restrained}
    \caption{%
      Schematic drawing of a restrained \caro in \IPAnema configuration with its cables routed upwards and likewise downwards away from the platform.%
    }
    \label{fig:fundamentals:cable-robot-scheme:restrained}
  \end{subfigure}
  \caption[%
    Drawing of a suspended and a redundantly restrained \caro.%
  ]{%
    Graphical representation of a suspended \caro~\subref{fig:fundamentals:cable-robot-scheme:suspended} and a redundantly restrained \caro~\subref{fig:fundamentals:cable-robot-scheme:restrained}.
  }
  \label{fig:fundamentals:suspended-vs-restrained}
\end{figure}

The primary application and target of algorithms presented in this thesis gears toward the class of \gls{FRPM} \caros, and results are exemplified for the motion patterns \MP{1}{2} and \MP{3}{3}.
Their applicability to other configurations is not always thoroughly shown, but the findings may easily be transferred to other \caro classes.
