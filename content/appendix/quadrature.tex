%!TEX root=../../thesis.tex
%!TEX file=content/appendix/quadrature.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Quadrature}\label{app:chap:quadrature}

The basic problem of numerical integration lies in finding a definite integral of a smooth function~$f \of{x}$ on the interval~$x \in \interval{a}{b}$ such that we obtain
%
\begin{align*}
  & \int\limits_{a}^{b}{
      f \of{x} \,
      \td{x}
    }
\end{align*}
%
to a given accuracy.
The basic principle of numerical integration lies in collecting a weighted sum of the integrand evaluated at discrete points within the interval.
Depending on the desired accuracy and the method in use for intergration, the integration points and their weights vary.
In its very basic form, a constant interpolating function is formed at the point~${a + b} / {2}$ giving~$f \of{{a + b} / {2}}$, usually referred to as the midpoint rule, which gives us
%
\begin{align*}
  \int\limits_{a}^{b}{
    f \of{x} \,
    \td{x}
  }
    & = \parentheses*{
          b - a
        } \,
        f \of*{
          \frac{
            a + b
          }{
            2
          }
        } \,
      .
\end{align*}
%
With the degree of the interpolating function increasing\textemdash the most common ones are linear or quadratic polynomials--, also the quality of approximation increasing yielding better results \textIe lower integration errors.
One may then improve approximation accuracy, by splitting the interval~$\interval{a}{b}$ into~$\nquadraturepoints$ subintervals for each of which the integral approximation is calculated.
This yields the composite rule of numerical integration of \textEg the interval~$\interval{a + \loopindex \, h}{a + \parentheses*{\loopindex + 1} \, h} \subseteq \interval{a}{b}$ with~$h = \parentheses*{b - a} / {\nquadraturepoints}$ and~$\loopindex \in \cparentheses*{0, \dotsc, \nquadraturepoints}$.
If the intervals are equally spaced \textIe there exist only one interval step size~$h$, then we obtain Newton-Cotes formulas or Simpson's rule.
On the other hand, if the subintervals are unevenly spaced we obtain the Gaussian quadrature formulae, which are more accurate than Newton-Cotes formulas with~$f$ being smooth \textIe sufficiently differentiable.

Due to higher accuracy than Newton-Cotes formulas for smooth functions, Gaussian quadrature rule is employed to approximate the integrals of forces and energies of the cable.
This formulation, named after Carl Friedrich Gauss, is stated for any function~$f \of{x}$ on the interval~$\interval{a}{b}$ with quadrature weights~$w_{\loopindex}$.
Gaussian quadrature rule is exact for polynomials of degree~$\polydeg \leq 2 \, \nquadraturepoints - 1$ if the nodes~$x_{\loopindex}$ and their corresponding weights~$w_{\loopindex}$ are chosen suitably.
The bases functions used in Gaussian quadrature are Legendre polynomials of degree~$\nquadraturepoints$ denoted~$P_{\nquadraturepoints} \of{x}$, with the~$\nquadraturepoints$-th polynomial normalized to give~$P_{\nquadraturepoints} \of{1} = 1$.
Then, the~$\loopindex$-th Gauss node~$x_{\loopindex}$ is the~$\loopindex$-th root of~$P_{\nquadraturepoints}$ and the weights are given by the formula~(see~\textcite{Abramowitz.1972})
%
\begin{align*}
  w_{\loopindex}
    & = \frac{
          2
        }{
          \pow{
            \parentheses*{
              1 - \pow{x_{\loopindex}}
            }
          }
          \pow{
            \left[
              P_{\nquadraturepoints}^{\prime} \of{x_{\loopindex}}
            \right]
          }
        } \,
        .
\end{align*}

The~$\loopindex$-th root of~$P_{\nquadraturepoints}$ can be determined in different ways depending on the desired accuracy on the value using \textEg Newton's method for solving~$P_{\nquadraturepoints} \of{x_{\loopindex}} = 0$ using an asymptotic formula~\autocite{Abramowitz.1972}, or by means of the Golub-Welsch algorithm~\autocite{Gil.2007}.
Values for lower-order quadrature rules up to~$\nquadraturepoints = 5$ over interval~$\interval{-1}{1}$ are given in~\cref{tbl:chap:quadrature:examples}.

\begin{table}[tbp]
  \centering
  \relsize{-1.25}
  \caption[%
      Abscissae and weights for Gaussian quadrature rule.
    ]{%
      Abscissae and weights for Gaussian quadrature rule on interval~${\interval{-1}{1}}$ for different number of quadrature points~$\nquadraturepoints$.
    }
  \label{tbl:chap:quadrature:examples}
  \input{tables/gaussian-quadrature}
\end{table}

For a more general formulation of functions defined on an interval~$\interval{a}{b}$, the integral must be recast to~$\interval{0}{1}$ before applying Gaussian quadrature rule.
With a linear function~$g \colon x \in \interval{a}{b} \to \interval{0}{1} \ni \tilde{x} \colon x \mapsto x \, \frac{b - a}{2} + \frac{a + b}{2}$ this can be expressed straightforward as
%
\begin{align*}
  \int\limits_{a}^{b}{
    f \of{x} \,
    \td{x}
  }
    & = \frac{
          b - a
        }{
          2
        } \,
        \sum\limits_{\loopindex = 1}^{\nquadraturepoints}{
          w_{\loopindex} \,
          f \of*{
            x_{\loopindex} \,
            \frac{
              b - a
            }{
              2
            }
            +
            \frac{
              a + b
            }{
              2
            }
          }
        } \,
        .
\end{align*}
