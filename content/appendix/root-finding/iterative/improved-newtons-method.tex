%!TEX root=../../../../thesis.tex
%!TEX file=content/appendix/root-finding/iterative/improved-newtons-method.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Improved Newton's Method}\label{app:sec:root-finding:improved-newton}

Improved versions of the conventional Newton's method have been introduced as the algorithm can get into trouble when the increment~$\iterstep$ gets too large.
One way to mitigate this problem is a softened or damped iteration method by means of which the increment~$\iterstep$ is scaled such that
%
\begin{align*}
  \itervar_{\iterindex + 1}
    & = \itervar_{\iterindex}
        -
        \newtondamping \,
        \iterstep
      =
        \itervar_{\iterindex}
        -
        \newtondamping \,
        \inv{
          \jacobian{\vect{f}} \of{ \itervar_{\iterindex} }
        } \,
        \vect{f} \of{ \itervar_\iterindex } \,
    ,
\end{align*}
%
where~$\newtondamping$ is a small number with~$\newtondamping \leq 1$.
Damping by a constant factor like~$\newtondamping = \nicefrac{1}{2}$ can improve initial iterations, but destroys the quadratic convergence.
Besides that, it is difficult to determine how to select the damping factor a priori.
However, we can adjust damping such that it vanishes as iteration converges by setting
%
\begin{align*}
  \newtondamping
    & = \frac{
          1
        }{
          1
          +
          \beta \,
          \norm{ \iterstep }
        } \,
      ,
\end{align*}
%
where a good practice is to set~$\beta \equiv 10$~\autocite{Tibshirani.2015q}.

Since finding a suitable value for~$\beta$ is still involved and depends on many different factors, another strategy is to choose~$\newtondamping$ adaptively, so that the objective function almost always decreases on each iteration.
This additional adaptive selection of~$\newtondamping$ introduces a second loop inside each Newton iteration loop.
Starting with a full Newton step, we check this step reduces the objective function.
If it does not, step size is halved and we repeat this procedure until we find a step size that reduces the residual, or until a maximum number of halving have been performed \textEg 10.
Newton's damped method is especially powerful when Newton's method would otherwise oscillate around the root due to too tight tolerances.
In this case, the reduced step size almost always allows for satisfying the residual or step size tolerances without drastically increasing the iteration count.
A pseudo algorithm of the adaptive damped Newton's method is given in~\cref{app:root-finding:adaptive-damped-newton}.

\begin{algorithm}[tb]
  \centering
  \caption{Pseudo-algorithm of the adaptive damped Newton's method.}
  \label{app:root-finding:adaptive-damped-newton}
  \smaller[1]
  \input{algorithm/root-finding/adaptive-damped-newton}
\end{algorithm}
