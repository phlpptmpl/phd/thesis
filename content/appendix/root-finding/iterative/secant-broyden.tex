%!TEX root=../../../../thesis.tex
%!TEX file=content/appendix/root-finding/iterative/secant-broyden.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Secant Method / Broyden's method}\label{app:sec:root-finding:broyden}

By replacing the derivative in Newton's method with a finite difference, one implements the Secant method for scalar-valued functions or Broyden's Method for higher dimensions.
This method is benefitial if the derivative cannot be determined analytically or if it can not be computed.
However, the speed of convergence is slower.

We start with the initial interval~$\interval{a_{0}}{b_{0}}$, the end points of which we use to construct a line of the form
%
\begin{align*}
  y
    & = \frac{
          f \of{b_{0}}
          -
          f \of{a_{0}}  
        }{
          b_{0}
          -
          a_{0}
        } \,
        \parentheses*{
          \itervar*
          -
          b_{0}
        }
        +
        f \of{ b_{0} } \,
      ,
\end{align*}
%
of which we find the root \textIe~$y = 0$ giving us
%
\begin{align*}
  \itervar*
    & = b_{0}
        -
        f \of{ b_{0} } \,
        \frac{
          b_{0}
          -
          a_{0}
        }{
          f \of{ b_{0} }
          -
          f \of{ a_{0} }
        } \,
      .
\end{align*}
%
We then set~$a_{1} = b_{1}$ and~$b_{1} = \itervar*$, and repeat above formula giving us the iterative formulation
%
\begin{align*}
  \itervar*_{\iterindex + 1}
    & = b_{\iterindex}
        -
        f \of{ b_{\iterindex} } \,
        \frac{
          b_{\iterindex}
          -
          a_{\iterindex}
        }{
          f \of{ b_{\iterindex} }
          -
          f \of{ a_{\iterindex} }
        } \,
      ,
    \\
  a_{\iterindex + 1}
    & = b_{\iterindex} \,
      ,
    \\
  b_{\iterindex + 1}
    & = \itervar*_{\iterindex + 1} \,
    ,
\end{align*}
%
until close convergence of the interval around the function's root is found.

One drawback of the secant method is possible failure to converge as it does not require the root to be within the initial interval~$\interval{a_{\iterindex}}{b_{\iterindex}}$ at every step which can be observed from the update procedure.
Conversely, the false position~(regula falsi) method iterates on the interval with changing its change-of-sign behavior.
A small change to the interval boundary updating procedure ensures that regula falsi method will always converge.
At iteration step~$\iterindex$, the signs of~$f \of{ a_{\iterindex} }$ and~$f \of{ \itervar*_{\iterindex + 1} }$ are compared and if they have same signs \textIe~$f \of{ a_{\iterindex} } \, f \of{ \itervar*_{\iterindex + 1} } \geq 0$, then interval boundaries are updated to
%
\begin{align*}
  \interval{
    a_{\iterindex + 1}
  }{
    b_{\iterindex + 1}
  }
    & = \interval{
          \itervar*_{\iterindex + 1}
        }{
          b_{\iterindex}
        } \,
        ,
    \\
\intertext{else to}
  \interval{
    a_{\iterindex + 1}
  }{
    b_{\iterindex + 1}
  }
    & = \interval{
          a_{\iterindex}
        }{
          \itervar*_{\iterindex + 1}
        } \,
        .
\end{align*}

Broyden's method is the multi-dimensional generalization of the Secant method where the Jacobian matrix~$\jacobian{\vect{f}}_{\iterindex}$ is determined iteratively at iteration step~$\iterindex$ using the finite-difference approximation
%
\begin{align*}
  \jacobian{\vect{f}}_{\iterindex} \,
  \parentheses*{
    \itervar_{\iterindex}
    -
    \itervar_{\iterindex - 1}
  }
    & \simeq \vect{f} \of{ \itervar_{\iterindex} }
        -
        \vect{f} \of{ \itervar_{\iterindex - 1} } \,
      \,
    \\
\intertext{or equivalently}
  \jacobian{\vect{f}}_{\iterindex} \,
    \iterstep_{\iterindex}
    & = \Delta \vect{f}_{\iterindex} \,
      \,
\end{align*}
%
if we use~$\iterstep_{\iterindex} = \itervar_{\iterindex} - \itervar_{\iterindex - 1}$ and likeweise~$\Delta \vect{f}_{\iterindex} = \vect{f} \of{ \itervar_{\iterindex} } - \vect{f} \of{ \itervar_{\iterindex - 1} }$.
This equation, however, is underdetermined in the multi-dimensional case, which is why Broyden's method uses the current estimate of the Jacobian matrix~$\jacobian{\vect{f}}_{\iterindex - 1}$ and update it by taking the solution to the secant equation
%
\begin{align*}
  \jacobian{\vect{f}}_{\iterindex}
    & = \jacobian{\vect{f}}_{\iterindex - 1}
        +
        \frac{
          \Delta \vect{f}_{\iterindex}
          -
          \jacobian{\vect{f}}_{\iterindex - 1} \,
          \iterstep_{\iterindex}
        }{
          \pow[2]{
            \norm{
              \iterstep_{\iterindex}
            }
          }
        } \,
        \transpose{
          \iterstep_{\iterindex}
        } \,
        ,
\end{align*}
%
which allows us to perform a typical Newton step
%
\begin{align*}
  \itervar_{\iterindex + 1}
    & = \itervar_{\iterindex}
        -
        \inv{
          \jacobian{\vect{f}}_{\iterindex}
        } \,
        \vect{f} \of{ \itervar_{\iterindex} } \,
        .
\end{align*}
%
To directly update the inverse Jacobian matrix, we may use Sherman–Morrison formula
%
\begin{align*}
  \inv{
    \jacobian{\vect{f}}_{\iterindex}
  }
    & = \inv{
          \jacobian{\vect{f}}_{\iterindex -1}
        }
        +
        \frac{
          \iterstep_{\iterindex}
          -
          \inv{
            \jacobian{\vect{f}}_{\iterindex - 1}
          } \,
          \Delta \vect{f}_{\iterindex}
        }{
          \transpose{
            \itervar_{\iterindex}
          } \,
          \inv{
            \jacobian{\vect{f}}_{\iterindex - 1}
          } \,
          \Delta \vect{f}_{\iterindex}
        } \,
        \transpose{
          \iterstep_{\iterindex}
        } \,
        \inv{
          \jacobian{\vect{f}}_{\iterindex - 1}
        }
        ,
\end{align*}
%
which may be favorable when computing the inverse Jacobian is computationally too expensive since only the initial inverse Jacobian~$\inv{\jacobian{\vect{f}}_{0}}$ has to be calculated.
