%!TEX root=../../../../thesis.tex
%!TEX file=content/appendix/solving-daes/numerical-integrators/leapfrog.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Leapfrog}\label{sec:appendix:solving-daes:integrators:leapfrog}

Leapfrog algorithm considers the dynamics in the Hamiltonian description such that we do not obtain a single, second-order differential equation, but merely two coupled, first-order differential equations~\autocite{Young.2014}.
The algorithm was first introduced by~\textcite{Yoshida.1990,Birdsall.1991} and later used as base for improved integrators of motion in mult-body systems by~\textcite{Omelyan.2002}.
We rewrite~$\ddot{x} = F \of{ x, t}$ to read\footnote{Without loss of generality we state only the scalar form of the Leapfrog integrator.
Transfer to the vectorial case is left to the reader.}
%
\begin{align*}
  \od{x \of{t}}{t}
    & = v \of{t} \,
      ,
      \\
  a \of{t}
    = \od{v \of{t}}{t}
    & = F \of{x \of{t}} = - \od{\energypotential \of{x \of{t}}}{x} \,
      .
\end{align*}
%
where we use the auxiliary state of velocities~$v \of{t}$ and the potential~$\energypotential \of{x \of{t}}$ of the forces~$ F \of{x \of{t}}$.
If we perform Taylor expansion of~$x \of{t}$ at time step~$t_{\iterindex}$ with step size~$\stepsize$, we obtain
%
\begin{align*}
  x \of{t_{\iterindex} + \stepsize}
    & = x \of{x_{\iterindex}}
        +
        \stepsize \,
        v \of{ t_{\iterindex} }
        +
        \frac{
          1
        }{
          2
        } \,
        \pow{\stepsize} \,
        a \of{ t_{\iterindex} }
        +
        O \of{ \pow[3]{\stepsize} } \,
      ,
\end{align*}
%
with~$v \of{ t_{\iterindex} } = \od{ x \of{t_{\iterindex}} }{ t }$ and~$a \of{ t_{\iterindex} } = \od[2]{ x \of{t_{\iterindex}} }{ t }$; while we obtain for the Taylor expansion of the velocity~$\od{ x \of{t_{\iterindex}} }{ t }$ at time~$t_{\iterindex} + \stepsize$
%
\begin{align*}
  v \of{t_{\iterindex} + \stepsize}
    & = v \of{ t_{\iterindex} }
        +
        \frac{
          1
        }{
          2
        } \,
        \stepsize \,
        a \of{ t_{\iterindex} }
      = v \of{
            t_{ \iterindex + \frac{\stepsize}{2} }
          } \,
      .
\end{align*}
%
Substituting this equation into the Taylor expansion of~$x \of{ t_{\iterindex} + \stepsize}$ gives
%
\begin{align*}
  x \of{ t_{\iterindex} + \stepsize}
    & = x \of{ t_{\iterindex} }
        +
        \stepsize \,
        v \of{ t_{\iterindex} + \frac{\stepsize}{2} }
        +
        O \of{ \pow[3]{\stepsize} } \,
      .
\end{align*}
%
This gives the standard Leapfrog algorithm for time step~$\iterindex$
%
\begin{subequations}
\begin{align*}
  v_{ \iterindex + \frac{1}{2} }
    & = v_{ \iterindex - \frac{1}{2} }
        +
        \stepsize \,
        a_{\iterindex} \,
      , \\
  x_{ \iterindex + 1 }
    & = x_{\iterindex}
        +
        \stepsize \,
        v_{ \iterindex + \frac{1}{2} } \,
      .
\end{align*}
\end{subequations}

Despite this algorithm numerically integrating mechanical systems with a step size error of~$O \of{ \pow[4]{\stepsize} }$, it is not often very practical as it does not synchronize the velocities\textemdash position values are available at full-integer time values~$\iterindex$, whilst velocity values are only available at half-integer time values either~$\iterindex - \frac{1}{2}$ or~$\iterindex + \frac{1}{2}$.
However, the equations can be expressed in a form that allows for obtaining velocities and positions at full-integer values giving the re-arranged and synchronized ``kick-drift-kick'' form
%
\begin{align*}
  v_{ \iterindex + \frac{1}{2} }
    & = v_{\iterindex}
        +
        \frac{
          \stepsize
        }{
          2
        } \,
        a_{\iterindex} \,
      ,
    \\
  x_{\iterindex + 1}
    & = x_{\iterindex}
        +
        \stepsize \,
        v_{\iterindex + 1 / 2} \,
      ,
    \\
  v_{ \iterindex + 1}
    & = v_{\iterindex + \frac{1}{2}}
        +
        \frac{
          \stepsize
        }{
          2
        } \,
        a_{\iterindex + 1} \,
      .
\end{align*}

Above formulation is favorable for variable step size integration as the separation of velocities and accelerations to the start and end of the algorithm requires only one extra\textemdash possibly expensive\textemdash calculation of accelerations when halving the {step size~$\stepsize \to \nicefrac{\stepsize}{2}$}.
In addition, the leapfrog algorithm also allows for considering dissipative forces during integration, even though the algorithm then becomes slightly more involved.
In this case, the accelerative forces are split into two parts such that the accelerations read
%
\begin{align*}
  a \of{x, v, t}
    & = F \of{x, t} - G \of{v} \,
      .
\end{align*}

In a first step, we consider the approximation of the accelerations to be given through
%
\begin{align*}
  a_{\iterindex}
    & = F_{\iterindex} - G_{\iterindex - \frac{1}{2}} \,
    \\
\intertext{giving us}
  \hat{v}_{\iterindex + \frac{1}{2}}
    & = v_{\iterindex - \frac{1}{2}}
        + 
        \stepsize \,
        F_{\iterindex}
        -
        \stepsize \,
        G_{\iterindex - \frac{1}{2}} \,
      ,
    \\
  \hat{x}_{\iterindex + 1}
    & = x_{\iterindex}
        +
        \stepsize \,
        \hat{v}_{\iterindex + \frac{1}{2}} \,
      .
\end{align*}

Then, a second step approximation yields the current velocities
%
\begin{align*}
  v_{\iterindex}
    & = \frac{
          \hat{x}_{\iterindex + 1} - x_{\iterindex - 1}
        }{
          2 \,
          \stepsize
        } \,
    ,
\end{align*}
%
which we can then use to continue with the conventional leapfrog algorithm.
In the end, the leapfrog algorithm for systems with velocity-dependent forces reads
%
\begin{align*}
  \hat{v}_{\iterindex + \frac{1}{2}}
    & = v_{\iterindex - \frac{1}{2}}
        + 
        \stepsize \,
        F_{\iterindex}
        -
        \stepsize \,
        G_{\iterindex - \frac{1}{2}} \,
      ,
    \\
  \hat{x}_{\iterindex + 1}
    & = x_{\iterindex}
        +
        \stepsize \,
        \hat{v}_{\iterindex + \frac{1}{2}} \,
      ,
    \\
  v_{\iterindex}
    & = \frac{
          \hat{x}_{\iterindex + 1} - x_{\iterindex - 1}
        }{
          2 \,
          \stepsize
        } \,
      ,
    \\
  v_{\iterindex + \frac{1}{2}}
    & = v_{\iterindex - \frac{1}{2}}
        +
        \stepsize \,
        a_{\iterindex} \,
      ,
    \\
  x_{\iterindex + 1}
    & = x_{\iterindex}
        +
        \stepsize \,
        v_{\iterindex + \frac{1}{2}} \,
      .
\end{align*}

Since the leapfrog algorithm provides an explicit solution for the next state, geometric constraints can be incorporated straightforward by solving the system of~(non)linear constraint equations of the next step simultaneously with the constraint forces~\autocite{Omelyan.1999b}.
