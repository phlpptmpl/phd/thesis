%!TEX root=../../../../thesis.tex
%!TEX file=content/appendix/solving-daes/numerical-integrators/betsch.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Energy-Momentum Conserving Integrator}\label{sec:appendix:solving-daes:integrators:betsch}

We again consider a mechanical system subject to holonomic and hon-holonomic constraints.
The motion of the system is governed by the set of \acrlongpl{DAE} reading
%
\begin{align*}
  \Mass \,
  \ddotgenq
  +
  \jacobian{\energypotential} \of{\genq}
    & = \transpose{\jacposconstraints} \of{\genq} \,
        \posconstraintforces
        +
        \transpose{\jacvelconstraints} \of{\genq} \,
        \velconstraintforces \,
        ,
      \\
  \posconstraints \of{\genq}
    & = \zeros \,
        ,
    \\
  \velconstraints \of{\genq, \dotgenq}
    & = \zeros \,
      .
\end{align*}

Holonomic constraints are given by~$\numposconstraints$ constraint functions~${\posconstraints \of{\genq} \in \mathds{R}^{\numposconstraints}}$ which restrict possible motions of the system through constraint forces with relative magnitude given through~$\posconstraintforces \in \mathds{R}^{\numposconstraints}$.
Nonholonomic constraints are given by~$\numvelconstraints$ constraint functions~$\velconstraints \of{\genq, \dotgenq} \in \mathds{R}^{\numvelconstraints}$ restricting motion through constraint forces with relative magnitude given through~$\velconstraintforces \in \mathds{R}^{\numvelconstraints}$.
The approach revisited here and originally presented by~\textcite{Betsch.2006} restricts to systems with potential function~${\energypotential \of{\genq} \in \mathds{R}}$ resulting in forces~$\jacobian{\energypotential} \of{\genq}$, and constant mass matrix~${\Mass \in \mathds{R}^{\dof \times \dof}}$.

\Citeauthor{Betsch.2006} proposes a new mechanical integrator for energy-consistent integration of mechanical systems subjected to kinematic and geometric constraints.
We consider a time interval~$T_{\iterindex} = \interval{t_{\iterindex}}{t_{\iterindex + 1}}$ with given positions~$\genq_{\iterindex}$ and velocities~$\genv_{\iterindex}$ at time~$t_{\iterindex}$.
By defining the mid-point value similar to the leapfrog algorithm as~$\parentheses{\cdot}_{\iterindex + \frac{1}{2}} = \parentheses*{ \parentheses{\cdot}_{\iterindex} + \parentheses{\cdot}_{\iterindex + 1} } / 2$, the one-step time-integration scheme with~$\stepsize = t_{\iterindex + 1} - t_{\iterindex}$ then reads
%
\begin{align}\label{eqn:app:solving-daes:integrators:betsch}
  \begin{split}
  \genq_{\iterindex + 1} - \genq_{\iterindex}
    & = \stepsize \,
        \genv_{\iterindex + \frac{1}{2}} \,
        ,
    \\
  \Mass \parentheses*{ \genv_{\iterindex + 1} - \genv_{\iterindex} }
    & = - \stepsize \,
        \discretenabla \energypotential \of{\genq_{\iterindex}, \genq_{\iterindex + 1}}
        +
        \dotsb
    \\
    & \phantom{=}
        +
        \stepsize \,
        \transpose{
          \discretenabla \posconstraints \of{\genq_{\iterindex}, \genq_{\iterindex + 1}}
        } \,
        \bar{\posconstraintforces}
        +
        \dotsb
    \\
    & \phantom{=}
        +
        \stepsize \,
        \transpose{
          \discretenabla \velconstraints \of{\genq_{\iterindex}, \genq_{\iterindex + 1}}
        } \,
        \bar{\velconstraintforces} \,
        ,
      \\
  \zeros
    & = \discretenabla \posconstraints \of{\genq_{\iterindex}, \genq_{\iterindex + 1}} \,
        \genv_{ \iterindex + \frac{1}{2}} \,
        ,
    \\
  \zeros
    & = \discretenabla \velconstraints \of{\genq_{\iterindex}, \genq_{\iterindex + 1}} \,
        \genv_{ \iterindex + \frac{1}{2}} \,
        .
  \end{split}
\end{align}
%
The nominal constraint forces~$\posconstraintforces \of{t}$,~$\velconstraintforces \of{t}$ are assumed constant within each time step \textIe~$\posconstraintforces \of{t_{\iterindex}} \approxeq \bar{\posconstraintforces}$,~$\velconstraintforces \of{t_{\iterindex}} \approxeq \bar{\velconstraintforces}$.
In~\cref{eqn:app:solving-daes:integrators:betsch},~$\discretenabla$ refers to the discrete derivative of its argument~\autocite{Gonzalez.1996}~(see the following section).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Discrete Derivative}

For a scalar-valued function~${ f\colon \mathds{R}^{n} \to \mathds{R} }$, ${ \vect{q} \mapsto f \of{\genq} }$ and two points~${ \vect{x}, \vect{y} \in \mathds{R}^{n} }$, \citeauthor{Gonzalez.1996} defines the discrete derivative similar to the continuous derivative as
%
\begin{align}\label{eqn:app:solving-daes:gonzalez:discrete-derivative}
\begin{split}
  \discretenabla f \of{ \vect{x}, \vect{y} }
    & = \pd{
          f \of{ \vect{z} }
        }{
          \genq
        }
        -
        \frac{
          f \of{ \vect{y} }
          -
          f \of{ \vect{x} }
          -
          \pd{
            f \of{ \vect{z} }
          }{
            \genq
          } \,
          \vect{v}
        }{
          \norm{
            \vect{v}
          }
        } \,
        \transpose{
          \vect{v}
        } \,
        ,
    \\
  \vect{v}
    & = \vect{y} - \vect{x} \,
      ,
    \\
  \vect{z}
    & = \frac{
          1
        }{
          2
        } \,
        \parentheses{
          \vect{x} + \vect{y}
        } \,
    ,
\end{split}
\end{align}
%
with its two properties of directionality
%
\begin{subequations}
\begin{align}
  \begin{split}
  \discretenabla f \of{ \vect{x}, \vect{y} } \,
    \vect{v}
    & = \parentheses*{
          \pd{
            f \of{ \vect{z} }
          }{
            \genq
          }
          -
          \frac{
            f \of{ \vect{y} }
            -
            f \of{ \vect{x} }
            -
            \pd{
              f \of{ \vect{z} }
            }{
              \genq
            } \,
            \vect{v}
          }{
            \norm{
              \vect{v}
            }
          } \,
          \transpose{
            \vect{v}
          }
        } \,
        \vect{v} \,
        ,
    \\
    & = f \of{ \vect{y} } - f \of{ \vect{x} } \,
    ,
    \label{eqn:app:solving-daes:gonzalez:directionality}
  \end{split}
  \\
%
\intertext{and of consistency}
%
  \discretenabla f \of{ \vect{x}, \vect{y} }
      & = \pd{
            f \of{ \vect{z} }
          }{
            \genq
          } \,
          ,
          \label{eqn:app:solving-daes:gonzalez:consistency}
\end{align}
\end{subequations}
%
for~$\norm{ \vect{y} - \vect{x} } \to 0$, which can be shown using the Taylor expansion of~$f \of{ \vect{x} }$ and~$f \of{ \vect{y} }$ up to order \num{4}.

\Cref{eqn:app:solving-daes:gonzalez:discrete-derivative} can be extended to vector-valued functions~${ \vect{f}\colon \mathds{R}^{n} \mapsto \mathds{R}^{m} }$ for which the partial derivative~$\pd{ \vect{f} \of{ \vect{z} } }{ \genq } = \jacobian{ \vect{f} }$ represents the Jacobian matrix.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{Constrained Scheme}

\Citeauthor{Betsch.2006} highlights some key properties of this constrained schemed as
%
\begin{enumerate}
  \item Conservation of energy.
    It can be shown for the energy function
    %
    \begin{align*}
      E \of{ \genq_{\iterindex}, \genv_{\iterindex} }
        & = \frac{
              1
            }{
              2
            } \,
              \genv_{\iterindex} \,
              \Mass \,
              \genv_{\iterindex}
            +
            \energypotential \of{\genq_{\iterindex}} \,
        ,
        \\
\intertext{the integrator algorithmically conserves energy within the integration interval \textIe}
      E \of{\genq_{\iterindex + 1}, \genv_{\iterindex + 1}} - E \of{\genq_{\iterindex}, \genv_{\iterindex}}
        & \equiv 0 \,
          .
    \end{align*}
  \item Satisfaction of geometric constraints at time~$t_{\iterindex + 1}$.
    This follows straight from~\cref{eqn:app:solving-daes:integrators:betsch}$_{3}$ taking into account~\cref{eqn:app:solving-daes:integrators:betsch}$_{1}$ and the directionality property of the discrete derivative~\cref{eqn:app:solving-daes:gonzalez:directionality}.
  \item Velocity constraints are satisfied only for mid-point velocities~$\genv_{\iterindex + \frac{1}{2}}$.
\end{enumerate}

System motion can now be solved iteratively for the unknowns~$\parentheses*{ \genq_{\iterindex + 1}, \genv_{\iterindex + 1} }$ and~$\bar{\posconstraintforces}$,~$\bar{\velconstraintforces}$ employing the scheme given in~\cref{eqn:app:solving-daes:integrators:betsch} such that we first eliminate the unknown velocities~$\genv_{\iterindex + 1}$
%
\begin{subequations}\label{eqn:app:solving-daes:integrators:betsch-iteration-scheme}
\begin{align}
  \genv_{\iterindex + 1}
    & = \frac{
          2
        }{
          \stepsize
        } \,
        \parentheses*{
          \genq_{\iterindex + 1}
          -
          \genq_{\iterindex}
        }
        -
        \genv_{\iterindex} \,
      ,
    \\
\intertext{which, substituted into~\cref{eqn:app:solving-daes:integrators:betsch}, gives}
  \begin{split}
  \zeros
    & = \frac{
          2
        }{
          \stepsize
        } \,
        \Mass \,
        \parentheses*{
          \genq_{\iterindex + 1}
          -
          \genq_{\iterindex}
        }
        -
        2 \,
        \Mass \,
        \genv_{\iterindex}
        +
        \discretenabla \energypotential \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        + \dotsb
        \\
    & \phantom{=}
        +
        \stepsize \,
        \transpose{
          \discretenabla \posconstraints \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        } \,
        \bar{\posconstraintforces}
        +
        \stepsize \,
        \transpose{
          \discretenabla \velconstraints \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        } \,
        \bar{\velconstraintforces} \,
        ,
  \end{split}
    \\
  \zeros
    & = \posconstraints \of{ \genq_{\iterindex + 1} } \,
        ,
    \\
  \zeros
    & = \velconstraints \of{\genq_{\iterindex + \frac{1}{2}}} \,
        \parentheses*{
          \genq_{\iterindex + 1}
          -
          \genq_{\iterindex}
        }
\end{align}
\end{subequations}
%
This system of nonlinear equations can be solved by applying Newton's method as presented in~\cref{app:sec:root-finding:newton}.
For this, we state the Jacobian matrix \textWrt~$\genq_{\iterindex + 1}$ at Newton's step~$\loopindex$ of the residual function as given in~\cref{eqn:app:solving-daes:integrators:betsch-iteration-scheme}
%
\begin{align*}
  \jacobian{\residual} \of{\genq_{\iterindex + 1}^{\parentheses{\loopindex}}}
  & =
    \begin{bmatrix}
      \matr{\iterindex}_{1}
        & \stepsize \,
          \discretenabla \posconstraints \of{ \genq_{\iterindex}, \genq_{\iterindex + 1}^{\parentheses{\loopindex}} }
        & \stepsize \,
          \discretenabla \velconstraints \of{ \genq_{\iterindex + \frac{1}{2}}^{\parentheses{\loopindex}} }
        \\
      \matr{\iterindex}_{2}
        & \zeros
        & \zeros
        \\
      \matr{\iterindex}_{3}
        & \zeros
        & \zeros
    \end{bmatrix} \,
    ,
  \\
  \intertext{with}
  \matr{\iterindex}_{1}
    & = \frac{
          2
        }{
          \stepsize
        } \,
        \Mass
        +
        \pd{
          \discretenabla \energypotential \of{ \genq_{\iterindex}, \genq_{\iterindex + 1} }
        }{
          \genq_{\iterindex + 1}
        }
        +
        \dotsb
    \\
    & \phantom{=}
        +
        \stepsize \,
        \sum\limits_{r = 1}^{\numconstraints}{
          \posconstraintforces_{r} \,
          \pd{
            \discretenabla \posconstraints_{r} \of{ \genq_{\iterindex}, \genq_{\iterindex + 1}^{\parentheses{\loopindex}} }
          }{
            \genq_{\iterindex + 1}
          }
        }
        +
        \dotsb
    \\
    & \phantom{=}
        +
        \stepsize \,
        \sum\limits_{r = 1}^{\numvelconstraints}{
          \velconstraintforces_{r} \,
          \pd{
            \discretenabla \velconstraints_{r} \of{ \genq_{\iterindex + \frac{1}{2}} }
          }{
            \genq_{\iterindex + 1}
          }
        } \,
    \\
  \matr{\iterindex}_{2}
    & = \jacposconstraints \of{ \genq_{\iterindex + 1}^{\parentheses{\loopindex}} } \,
      ,
    \\
  \matr{\iterindex}_{3}
    & = \jacvelconstraints \of{\genq_{\iterindex + \frac{1}{2}}}
        +
        \begin{bmatrix}
          \transpose*{
            \genq_{\iterindex + 1}
            -
            \genq_{\iterindex}
          } \,
          \pd{
            \velconstraints_{1} \of{\genq_{\iterindex + \frac{1}{2}}^{\parentheses{\loopindex}}}
          }{
            \genq_{\iterindex + 1}
          }
            \\
          \vdots
            \\
          \transpose*{
            \genq_{\iterindex + 1}
            -
            \genq_{\iterindex}
          } \,
          \pd{
            \velconstraints_{\numvelconstraints} \of{\genq_{\iterindex + \frac{1}{2}}^{\parentheses{\loopindex}}}
          }{
            \genq_{\iterindex + 1}
          }
        \end{bmatrix} \,
        ,
\end{align*}
%
in which the partial derivative \textWrt~$\vect{y}$ of the discrete derivative~$\pd{ \discretenabla \vect{f} \of{ \vect{x}, \vect{y} } }{ \vect{y} }$ of a vector-valued function~$\vect{f} \of{\genq}$ is given through
%
\begin{align*}
  \pd{
    \discretenabla f \of{ \vect{x}, \vect{y} }
  }{
    \vect{y}
  }
    & = \pd[2]{
          \vect{f} \of{ \vect{z} }
        }{
          \genq
        }
        + \dotsb
    \\
    & \phantom{=}
      + 
        \frac{
          \vect{v}
        }{
          \norm{
            \vect{v}
          }
      } \,
      \parentheses*{
        \pd{
          \vect{f} \of{ \vect{y} }
        }{
          \genq
        }
        -
        \transpose{
          \vect{v}
        } \,
        \pd[2]{
          \vect{f} \of{ \vect{z} }
        }{
          \genq
        }
        -
        \pd{
          \vect{f} \of{ \vect{z} }
        }{
          \genq
        }
      }
      +
      \dotsb
  \\
  & \phantom{=}
    + \parentheses*{
        \frac{
          \eye[\dof]
        }{
          \norm{
            \vect{v}
          }
        }
        +
        2 \,
        \frac{
          \vect{v} \,
          \transpose{
            \vect{v}
          }
        }{
          \pow{
            \norm{
              \vect{v}
            }
          }
        }
      } \,
      \parentheses*{
        \vect{f} \of{ \vect{y} }
        -
        \vect{f} \of{ \vect{x} }
        -
        \pd{
          \vect{f} \of{ \vect{z} }
        }{
          \genq
        }
        \,
        \vect{v}
      } \,
      ,
  \\
\intertext{again with}
  \vect{v}
    & = \vect{y} - \vect{x} \,
      ,
    \\
  \vect{z}
    & = \frac{
          1
        }{
          2
        } \,
        \parentheses{
          \vect{x} + \vect{y}
        } \,
    .
\end{align*}
