%!TEX root=../../../thesis.tex
%!TEX file=content/appendix/solving-daes/constraint-violation-handling.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Constraint Violation Handling}\label{app:chap:solving-daes:constraint-violation-handling}

\talkabout[inline]{Introduce the different kinds of solving \glspl{DAE} namely present results by~\textcite{Flores.2014} and~\textcite{Arnold.2004} showing different integration techniques, their validity, and their shortcomings.}

\correct[inline]{Refer to~\autocite{Flores.2014} which nicely states how the dynamics of a constrained multibody system are frequently formulated.}



Assume a constrained mechanical system with its second-order dynamics \gls{ODE} as given by
%
\begin{subequations}\label{eqn:app:solving-daes:dae-system}
  \begin{align}
    \Mass \of{\genq} \,
    \ddotgenq
      & = \dynforces \of{\genq, \dotgenq}
          +
          \jacposconstraints \of{\genq} \,
          \lagrangemultips \of{t} \,
          ,
          \label{eqn:app:solving-daes:dae-system:dynamics}
      \\
\shortintertext{under the constraints}
    \zeros
      & = \posconstraints \of{\genq, t} \,
          ,
          \label{eqn:app:solving-daes:dae-system:constraints}
  \end{align}
\end{subequations}
%
where~$\posconstraints \of{\genq, t}$ is the vector of~$\numconstraints$ position-level constraints on the mechanical system.
For the sake of brevity, we omit nonholonomic constraints of the form~$\zeros = \matr{A} \, \dotgenq$ and focus solely on holonomic constraints.
However, constraints~\cref{eqn:app:solving-daes:dae-system:constraints} imply additional restrictions on the state variables $\genq$, $\dotgenq$, and $\lagrangemultips$.
These implicit constraints can be inferred from twofold time-derivation of the positional constraints
%
\begin{subequations}\label{eqn:app:solving-daes:dae-system:hidden-constraints}
\begin{align}
  \od{
    \posconstraints
  }{
    t
  }
    & = \pd{
          \posconstraints
        }{
          \genq
        } \,
        \dotgenq
        +
        \pd{
          \posconstraints
        }{
          t
        }
      = \jacposconstraints \,
        \dotgenq
        +
        \jacobian[t]{\posconstraints}
      \equiv \zeros \,
        ,
        \label{eqn:app:solving-daes:dae-system:hidden-constraints:velocity}
    \\  
  \od[2]{
    \posconstraints
  }{
    t
  }
    & = \jacobian*[\genq]{
          \jacposconstraints \,
          \dotgenq
        } \,
        \dotgenq
        +
        2 \,
        \jacposconstraints \,
        \dotgenq
        +
        \jacposconstraints \,
        \ddotgenq
        +
        \jacobian[t][2]{
          \posconstraints
        }
      \equiv \zeros \,
        .
        \label{eqn:app:solving-daes:dae-system:hidden-constraints:acceleration}
\end{align}
\end{subequations}

Rearranging~\cref{eqn:app:solving-daes:dae-system:hidden-constraints:acceleration} for~$\jacposconstraints \, \ddotgenq$ such that
%
\begin{align*}
  \jacposconstraints \,
  \ddotgenq
    & = -\jacobian*[\genq]{
          \jacposconstraints \,
          \dotgenq
        } \,
        \dotgenq
        -
        2 \,
        \jacposconstraints \,
        \dotgenq
        -
        \jacobian[t][2]{
          \posconstraints
        }
      \eqqcolon \vect{\zeta}
\end{align*}
%
yields the hidden acceleration constraints which we can then place in parallel to~\cref{eqn:app:solving-daes:dae-system:dynamics} yielding the corrected~\gls{ODE}
%
\begin{subequations}\label{eqn:app:solving-daes:constrained-dae-equations}
  \begin{align}
    \Mass \of{\genq} \,
    \ddotgenq
      & = \dynforces \of{\genq, \dotgenq}
          +
          \transpose{
            \jacposconstraints
          } \,
          \posconstraintforces \,
          ,
          \label{eqn:app:solving-daes:constrained-dae-equations:accelerations}
      \\
    \jacposconstraints \,
    \ddotgenq
      & = \vect{\zeta} \of{\genq, \dotgenq, t}
          ,
          \label{eqn:app:solving-daes:constrained-dae-equations:constraints}
  \end{align}
\end{subequations}

Equivalently, we may state~\cref{eqn:app:solving-daes:constrained-dae-equations} in vector-matrix notation reading
%
\begin{align}
  \begin{bmatrix}
    \Mass \of{\genq, \dotgenq, t}
      & -\transpose{\jacposconstraints} \\
    \jacposconstraints
      & \zeros
  \end{bmatrix}
  \cdot
  \begin{bmatrix}
    \ddotgenq \\
    \posconstraintforces
  \end{bmatrix}
  & = 
    \begin{bmatrix}
      \dynforces \of{\genq, \dotgenq, t} \\
      \vect{\zeta} \of{\genq, \dotgenq, t}
    \end{bmatrix} \,
    .
    \label{eqn:app:solving-daes:constrained-dae-vector-matrix} 
\end{align}

Given in~\cref{eqn:app:solving-daes:constrained-dae-vector-matrix} is the full \gls{DAE} that can numerically be solved simultaneously for the accelerations~$\ddotgenq$ and Lagrange multipliers~$\posconstraintforces$ in order to obtain the constraint respecting system response.
This method is generally referred to as the \gls{SLM}~\autocite{Flores.2014}.
When using \gls{SLM}, indefinite growth of constraint violation occurs over time and, as such, will produce unacceptable results due to integration of numerical errors implying constraint violation where there physically would be none.
In fact, the constraints~\cref{eqn:app:solving-daes:dae-system:constraints} are no longer considered explicitly in conjunction with the system dynamics~\cref{eqn:app:solving-daes:dae-system:dynamics} but only implicitly through their first and second total derivative \textWrt time.
Ultimately, constraint violation handling methods are necessary for correcting the systems state ensuring both minimal to no constraint violation as well as correct result propagation\footnote{A ``correct result propagation'' implies energy conservative integration of the \gls{DAE} \textIe the constraint violation handling/compensation must not introduce energy into the system where it may physically be not possible.}.
Several methods for handling the constraint violation exist, some of which will be presented to show their complexity, procedural implementation, and their applicability to simulation of \caros.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Baumgarte Stabilization Method}\label{sec:solving-daes:baumgarte-stabilization}

\Gls{BSM} allows for minor violation of the constraints before introducing corrective forces~\autocite{Baumgarte.1972,Blajer.2011}
Its implementation for the control of constraint violation is one of the simplest making it the most commonly used constraint violation handling method and the first choice for simulation of any dynamical system.
However, its simplicity comes at the price of ambiguous selection of parameters.

\Gls{BSM} introduces a corrective feedback term on the constraints~\cref{eqn:app:solving-daes:constrained-dae-vector-matrix} such that they obey to
%
\begin{align}
  \ddotposconstraints
  +
  2 \,
  \matr{\alpha} \,
  \dotposconstraints
  +
  \pow{
    \matr{\beta}
  } \,
  \posconstraints
    & = \zeros \,
        ,
        \label{eqn:constraint-violation-handling:baumgarte-feedback-term}
\end{align}
%
where matrices~$\matr{\alpha} = \diag \of{\alpha_{1}, \alpha_{2}, \dotsc, \alpha_{\numconstraints}}$ and~$\matr{\beta} = \diag \of{\beta_{1}, \beta_{2}, \dotsc, \beta_{\numconstraints}}$ are $\parentheses{\numconstraints \times \numconstraints}$ matrices of stabilization parameters of the feedback term.
One can read these parameters as being the natural frequency~$\matr{\naturalfrequency}_{0} = \matr{\beta}$ and damping~$\matr{\damping} = \inv{\matr{\beta}} \, \matr{\alpha}$ of a normalized mass-spring-damper system.
Selection of the parameter values is arbitrary, however, the stability of the general solution for linear dynamical systems is guaranteed if they are chosen as positive definite matrices or positive definite constants in the scalar case.
However, since most mechanical multibody systems are nonlinear, their numerical stability cannot be guaranteed a priori.
One option is to set~$\matr{\alpha} = \matr{\beta}$ to achieve critical damping in the constraint violation, as pointed out by~\citeauthor{Baumgarte.1972} himself~\autocite{Baumgarte.1972}.
In any other case, numerical experiments are needed to find suitable choices for~$\matr{\alpha}$ and~$\matr{\beta}$.

Appending~\cref{eqn:constraint-violation-handling:baumgarte-feedback-term} to the \gls{DAE} from~\cref{eqn:app:solving-daes:constrained-dae-vector-matrix} yields the constraint corrected \gls{DAE} as
%
\begin{align}
  \begin{bmatrix}
    \Mass \of{\genq}
      & -\transpose{\jacposconstraints} \\
    \jacposconstraints
      & \zeros
  \end{bmatrix}
  \,
  \begin{bmatrix}
    \ddotgenq \\
    \posconstraintforces
  \end{bmatrix}
  & = 
    \begin{bmatrix}
      \dynforces \of{\genq, \dotgenq} \\
      \vect{\zeta} \of{\genq, \dotgenq}
        -
        2 \,
        \matr{\alpha} \,
        \dotposconstraints
        -
        \pow{
          \matr{\beta}
        } \,
        \posconstraints
    \end{bmatrix} \,
    .
    \label{eqn:constraint-violation-handling:baumgarte-system}
\end{align}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Penalty Method}\label{sec:solving-daes:penalty-method}

\gls{PM} handles constraint violation differently than \gls{BSM} as it introduces constraint violation handling into the equations of motion in a different manner.
The \gls{PM}-based \gls{DAE} is based on the unconstrained system of bodies
%
\begin{align}
  \Mass \of{\genq} \,
  \ddotgenq
    & = \dynforces \of{\genq, \dotgenq} \,
        ,
        \label{eqn:constraint-violation-handling:penalty-method-dynamical-base-system}
\end{align}
%
for which the constraint violation at all levels of position, velocity, and acceleration are considered a second-order \gls{ODE} of the form
%
\begin{align}
  \matr{m}_{\ms{PM}} \,
  \ddotposconstraints
  +
  \matr{d}_{\ms{PM}} \,
  \dotposconstraints
  +
  \matr{\iterindex}_{\ms{PM}} \,
  \posconstraints
    & = \zeros \,
      ,
      \label{eqn:constraint-violation-handling:penalty-method-feedback-term}
\end{align}
%
where~$\matr{m}_{\ms{PM}}$,~$\matr{d}_{\ms{PM}}$, and~$\matr{\iterindex}_{\ms{PM}}$ are some constant, positive diagonal matrices giving the equation physical meaning by turning it into the equivalent of a mass-spring-damper system.
After some mathematical manipulations of~\cref{eqn:constraint-violation-handling:penalty-method-feedback-term} and introducing it in~\cref{eqn:constraint-violation-handling:penalty-method-dynamical-base-system}, the new \gls{ODE} may be retrieved as
%
\begin{align}
  \parentheses{
    \Mass
    + \matr{\alpha} \,
      \transpose{\jacposconstraints} \,
      \jacposconstraints
  } \,
  \ddotgenq
    & = \dynforces
        - \matr{\alpha} \,
          \transpose{\jacposconstraints} \,
          \parentheses{
            \dotjacposconstraints \,
            \dotgenq
            +
            2 \,
            \matr{\mu} \,
            \matr{\omega} \,
            \dotposconstraints
            +
            \pow{
              \matr{\omega}
            } \,
            \posconstraints
          }
          \,
          ,
          \label{eqn:constraint-violation-handling:penalty-method-system}
\end{align}
%
where~$\matr{\omega} = \inv{\matr{m}}_{\ms{PM}} \, \matr{\iterindex}_{\ms{PM}}$,~$\matr{\mu} = \inv*{2 \, \matr{\omega} \, \matr{m}_{\ms{PM}}} \, \matr{d}_{\ms{PM}}$, and~$\matr{\alpha} = \matr{m}_{\ms{PM}}$.

With the \gls{PM} being very similar to the \gls{SLM}, it also suffers from problems arising from introducing new arbitrary parameters~$\matr{\alpha}$,~$\matr{\mu}$, and~$\matr{\omega}$ into the system dynamics.
The benefit of \gls{PM} comes from reducing the number of equations that need to be solved since~\cref{eqn:constraint-violation-handling:penalty-method-system} poses a system of only~$\dof$ equations.
\Gls{PM} provides good control over constraint violation with~$\matr{\alpha} \to \infty$, typical values are~${\matr{\alpha} = \num{1e17} \, \eye}$,~${\matr{\omega} = \num{10} \, \eye}$, and~${\matr{\mu} = 1 \, \eye}$~\autocite{Nikravesh.2008}.
However, the parameter choice still remains ambiguous and numerical stability can only be guaranteed for linear dynamical systems.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Augmented Lagrangian Formulation}\label{sec:solving-daes:augmented-lagrangian}

\gls{ALF} is a revised formulation of the \gls{PM} trying to remove ambiguity of choosing~$\matr{\alpha}$,~$\matr{\omega}$ and~$\matr{\mu}$ by iteratively solving the system of equations for the accelerations.
The iterative process at a given time step~$t \equiv t_{\ms{eval}}$ with iteration index~$\iterindex = 0$ is initialized for the unconstrained system
%
\begin{align}
  \Mass \of{\genq} \,
  \ddotgenq_{\iterindex}
    & = \dynforces \of{\genq, \dotgenq} \,
      .
\end{align}
%
The iterative process continues with evaluating
%
\begin{align}
  \parentheses{%
    \Mass
    + \matr{\alpha} \,
      \transpose{\jacposconstraints} \,
      \jacposconstraints
  } \,
  \ddotgenq_{\iterindex + 1}
    & = \Mass \ddotgenq_{\iterindex}
        +
        \dotsb
    \\
    & \phantom{=}
        -
        \matr{\alpha} \,
        \transpose{\jacposconstraints} \,
        \parentheses*{
          \dotjacposconstraints \,
          \dotgenq%
          +
          2 \,
          \matr{\mu} \,
          \matr{\omega} \,
          \dotposconstraints
          +
          \pow{
            \matr{\omega}
          } \,
          \posconstraints
        } \,
      ,
      \label{eqn:app:solving-daes:alf:iteration}
\end{align}
%
for~$\ddotgenq_{\iterindex + 1}$ until satisfying
%
\begin{align}
  \norm{ \ddotgenq_{\iterindex + 1} - \ddotgenq_{\iterindex} }
    & \leq \threshold \,
      ,
\end{align}
%
where~$\threshold$ is a specified level of error tolerance on the error of generalized accelerations.
Using \acrshort{ALF}, no iterative root-finding algorithm like Newton's method is needed as~\cref{eqn:app:solving-daes:alf:iteration} can be understood as finding the solution to the linear equation~$ \matr{A} \vect{x} = \vect{b}$ for which efficient numerical methods exist.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Coordinate Partitioning Method}\label{sec:solving-daes:coordinate-partitioning}

As its name implies, \gls{CPM} requires partitioning the generalized coordinates into two sets of %
\begin{inparaenum}[1)]
  \item independent coordinates~$\genq^{ \parentheses{ \ms{ind} } }$, and
  \item dependent coordinates~$\genq^{ \parentheses{ \ms{dep} } }$
\end{inparaenum}:
%
\begin{align*}
  \genq
    & \Rightarrow
      \begin{Bmatrix}
        \genq^{ \parentheses{ \ms{ind} } } \\
        \genq^{ \parentheses{ \ms{dep} } }
      \end{Bmatrix} \, , 
\end{align*}
%
for which the number of independent coordinates~$\genq^{ \parentheses{ \ms{ind} } }$ equals the number of \gls{DOF} of the system~\autocite{Wehage.1982}.
Similarly, the system velocities and accelerations are also partitioned.

Finally, the integration process is revised to consider only the independent variables such that
%
\begin{align*}
  \int\ddotgenq^{ \parentheses{ \ms{ind} } }
    & = \dotgenq^{ \parentheses{ \ms{ind} } } \,
        ,
    \\
  \int\dotgenq^{ \parentheses{ \ms{ind} } }
    & = \genq^{ \parentheses{ \ms{ind} } } \,
        .
\end{align*}
%
All dependent coordinates~$\genq^{ \parentheses{ \ms{dep} } }$ and independent velocities~$\dotgenq^{ \parentheses{ \ms{ind} } }$ are then calculated from the corresponding constraint equations.
One drawback of the \gls{CPM} is their poor numerical efficiency stemming from the iterative solution for dependent generalized coordinates.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Direct Correction Approach}\label{sec:solving-daes:direct-correction}

Constraint violation handling methods are set up to ensure no violation of the coordinate level constraints.
We may, as such, consider the final constraint satisfying coordinates as coordinates obtained through numerical integration corrected by a correction term as
%
\begin{align*}
  \genq_{\subcorrected}
    & = \genq_{\subuncorrected} + \delta \genq \, ,
\end{align*}
%
where subscript~$\parentheses{\cdot}_{\subcorrected}$ and~$\parentheses{\cdot}_{\subuncorrected}$ denote corrected and uncorrected coordinates, respectively, and~$\delta \genq$ is the vector of coordinate corrections to ensure constraint violation.
Similarly, we may rewrite the constraint equations as
%
\begin{align}
  \posconstraints \of{ \genq_{\subcorrected} }
    & = \posconstraints \of{ \genq_{\subuncorrected} }
        +
        \delta \posconstraints
        = \zeros \,
        ,
        \label{eqn:constraint-violation-handling:direct-correction-corrected-constraints}
\end{align}
%
where~$\delta \posconstraints$ is the variation of the constraint equations
%
\begin{align*}
  \delta \posconstraints
    & = \pd{ \posconstraints }{ \genq_{1} } \delta \genq_{1} + \pd{ \posconstraints }{ \genq_{2} } \delta \genq_{2} + \dotsb + \pd{ \posconstraints }{ \genq_{n} } \delta \genq_{n} 
      = \jacposconstraints \,
        \delta \genq \,
        ,
\end{align*}
%
which we can use to rewrite~\cref{eqn:constraint-violation-handling:direct-correction-corrected-constraints} to yield
%
\begin{align}
  \delta \genq
    & = - \inv{ \jacposconstraints } \,
          \posconstraints \of{ \genq_{\subuncorrected} } \,
          .
          \label{eqn:constraint-violation-handling:direct-correction-variation-inverse}
\end{align}

In general, the constraint Jacobian~$\jacposconstraints$ is rectangular, thus the inverse~$\inv{\jacposconstraints}$ does not exist.
We may, however, apply the concept of the Moore-Penrose inverse matrix, yielding
%
\begin{align*}
  \pinv*{\jacposconstraints}
    & = \hermconj{\jacposconstraints} \,
      \inv*{
        \jacposconstraints \,
        \hermconj{\jacposconstraints}%
      } \,
      , 
\end{align*}
%
in which~$\hermconj{\jacposconstraints}$ is the conjugate transpose of matrix~$\jacposconstraints$.
Using this, we can rewrite~\cref{eqn:constraint-violation-handling:direct-correction-variation-inverse} to read
%
\begin{align}
  \delta \genq
    & = - \hermconj{\jacposconstraints} \,
        \inv*{%
            \jacposconstraints \,
            \hermconj{ \jacposconstraints }
        } \,
        \posconstraints \of{ \genq_{\subuncorrected} } \,
        .
\end{align}

Finally, we obtain the equation that represents the corrected generalized coordinates~$\genq_{\subcorrected}$ in each time step as
%
\begin{align}
  \genq_{\subcorrected}
    & = \genq_{\subuncorrected}
        - \hermconj{\jacposconstraints}%
          \inv*{
            \jacposconstraints \,
            \hermconj{ \jacposconstraints }
          } \,
        \posconstraints \of{ \genq_{\subuncorrected} } \,
        .
\end{align}

Similarly, the generalized velocities are corrected by
%
\begin{align*}
  \dotgenq_{\subcorrected}
    & = \dotgenq_{\subuncorrected}
        +
        \delta \dotgenq \,
      ,
\end{align*}
%
which must satisfy the velocity constraints
%
\begin{align*}
  \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subcorrected} }
    & = \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subuncorrected} }
        + \delta \dotposconstraints
        = \zeros \,
        ,
\end{align*}
%
for which~$\delta \dotposconstraints$ represents the variation of the velocity constraints reading
%
\begin{align*}
  \delta \dotposconstraints
    & = \pd{ \dotposconstraints }{ \genq } \,
        \delta \genq
        + \pd{ \dotposconstraints }{ \dotgenq } \,
        \delta \dotgenq \,
        .
\end{align*}

With the generalized coordinates already being corrected \textIe~${\delta \genq \equiv \zeros}$, and introducing~$\jacdotposconstraints$ as the Jacobian of the velocity constraints, we obtain
%
\begin{align*}
  \delta \dotposconstraints
    & = \jacdotposconstraints \,
        \delta \dotgenq \,
        ,
    \\
\intertext{resulting in}
  \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subuncorrected} }
    + \jacdotposconstraints \,
        \delta \dotgenq
    & = \zeros \,
      ,
    \\
\intertext{which may be rewritten in terms of~$\delta \dotgenq$ to read}
  \delta \dotgenq
    & = - \pinv*{\jacdotposconstraints} \,
        \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subuncorrected} } \\
    & = - \hermconj*{ \jacdotposconstraints } \,
      \inv*{
        \jacdotposconstraints \,
        \transpose{\jacdotposconstraints}
      } \,
      \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subuncorrected} } \,
      .
\end{align*}

Ultimately, we obtain the equation for the corrected velocities at every integration time step as
%
\begin{align*}
  \dotgenq_{\subcorrected}
    & = \dotgenq_{\subuncorrected}%
        - \hermconj{ \jacdotposconstraints } \,
          \inv{%
            \parentheses{%
              \jacdotposconstraints \,
              \transpose{ \jacdotposconstraints}%
            }%
          } \,
          \dotposconstraints \of{ \genq_{\subcorrected}, \dotgenq_{\subuncorrected} } .
\end{align*}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gear-Gupta-Leimkuhler}\label{sec:solving-daes:gear-gupta-leimkuhler}

From a numerical point of view, coordinate partitioning methods like the one presented in~\cref{sec:solving-daes:coordinate-partitioning} are less favorable because they select only a sub-set of linearly independent generalized coordinates computing the remaining dependent generalized coordinates by solving the system of nonlinear equations~\cref{eqn:app:solving-daes:dae-system:constraints}.
It is more favorable to keep \emph{all} coordinates in the system dynamics described through~\cref{eqn:app:solving-daes:dae-system}.
This can be achieved by either index reduction or projection, where projection techniques are state-of-the-art to avoid any drift effects in the numerical solving procedure~\autocite{Eich.1993,Lubich.1995}.
The constraint residual~$\norm{ \posconstraints \of{ \genq_{\iterindex}, t_{\iterindex} } }$ at time step~$t = t_{\iterindex}$ is monitored, and, when breaching through a user-defined error threshold~$\threshold > 0$, projected onto the {manifold~$\set{ \vect{\eta} \colon \posconstraints \of{ \vect{\eta}, t_{\iterindex} } \equiv \zeros }$}.
Mathematically, this can be defined as a minimization problem of the form
%
\begin{align*}
  \hat{\genq}_{\iterindex}
    & = \min_{
          \set{ \vect{\eta} \colon \posconstraints \of{ \vect{\eta}, t_{\iterindex} } \equiv \zeros }
        }{
          \norm{
            \vect{\eta} - \genq_{\iterindex}
          }
        }
\end{align*}
%
which may be solved efficiently by Newton iterations to find the projected new state~$\hat{\genq}_{n}$.
The implementation of explicit projection methods for both~$\genq_{n}$ and~$\dotgenq_{n}$ can help avoid drift in the constraints, however, using such methods is restricted to Runga-Kutta and other one-step solvers as the implementation in advanced \gls{acr:BDF} solvers is nontrivial.
Knowing that, a reformulation of~\cref{eqn:app:solving-daes:dae-system} in ways that it implicitly includes the projection onto the constraint manifold may be found as described by \gls{acr:GGL}.
By introducing a new auxiliary state~$\auxstate = \dotgenq$ with~$\dotauxstate = \od{\auxstate}{t}$ and consideration of~\cref{eqn:app:solving-daes:dae-system:constraints,eqn:app:solving-daes:dae-system:hidden-constraints:velocity} simultaneously, \gls{acr:GGL} defines the extended dynamical \gls{DAE}-system as
%
\begin{align}
  \begin{split}
    \dotgenq \of{t}
      & = \auxstate
          -
          \transpose{
            \jacposconstraints
          } \of{\genq, t} \,
          \velconstraintforces \,
          ,
      \\
    \Mass \of{\genq} \,
      \dotauxstate \of{t}
      & = \dynforces \of{\genq, \auxstate, t}
          -
          \transpose{
            \jacposconstraints
          } \,
          \posconstraintforces \,
          ,
      \\
    \zeros
      & = \posconstraints \of{\genq, t} \,
          ,
      \\
    \zeros
      & = \jacposconstraints \of{\genq, t} \,
          \auxstate
          +
          \dotposconstraints \of{\genq, t} \,
          .
  \end{split}
\end{align}

\amend[inline]{Maybe now talk about how this can be solved. %
Either using Newton-Raphson or fsolve/fmincon}
