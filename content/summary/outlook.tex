%!TEX root=../../thesis.tex
%!TEX file=content/summary/outlook.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Outlook}\label{sec:summary:outlook}

On the side of simulation, we should desire faster numerical integration of the dynamics of a \caro as formulated in this thesis.
This may either be very easily obtained by converting the MATLAB-based simulation framework into a C/C++-based framework, while keeping its modularity in mind.
It may, however, also be obtained by using numerically more efficient root-finding problems compared to the rather simple Newton's method used in finding the results in presented in this thesis.
While the number of iterations has never outreached the self-set limit of~$\num{100}$ iterations, Newton's method's constant step size hinders the root-finding from making larger steps along the gradient\textemdash this may easily be remedied by using a trust-region-reflective algorithm or Levenberg-Marquardt with adaptive step size.
While the iterative integration schemes has several advantages over conventional \gls{ODE}-solver, it inherently results in a nonlinear residual function needing evaluation.
That is, other than conventional \gls{ODE}-solver solving for the state's derivative and integrating over time, this formulation will inherently always remain slower.
It remains to be seen how much speed improvement can be gained.

Our multibody \caro simulation framework was initially implemented using  constraint-violation handling based on \gls{acr:GGL}, which allows for solving for the accelerations, yet introduces an additional level of constraints on the velocity level.
Due to numerical issues of stability and small time-step sizes, we transitioned to the mechanical integrator by~\citeauthor{Betsch.2006} which does not feature these shortcomings, yet requires iteratively solving a system of nonlinear equations in the next-state estimates.
There are other numerical integration schemes specifically designed for multibody systems of largely varying stiffness or incorporating beams or cables like the projective dynamics methods used by~\textcite{Soler.2018}.
These integrators also provide force and momentum-consistent integration while not requiring solving nonlinear root-finding problems at every time-step.
From the numerical point of view, there is room for improvement, requiring however good knowledge of numerical integration schemes and the mechanics behind the system.

The cable model based on the well-established Cosserat rod theory can be transferred to describing a full \caro system, despite it primarily addressing beams and rods.
However, it currently lacks viscous dynamics and as such any oscillation remains undamped.
It is possible to incorporate viscous components into the Cosserat rod by means of conventional \gls{acr:KV} elements~\autocite{Lacarbonara.2008,Lang.2013}.
This will render the cable model more realistic, however, also more challenging to parametrize as further mechanical properties needs to be identified.

% \autocite{Lacarbonara.2008,Lang.2013} on a kelvin-voigt beam model

Results obtained from cable axial stress-strain dynamics identification can now be used in order to better evaluate cable force control or to implement model-based control on the force level.
Since the dynamics formulation comprise a linear \gls{ODE} rather than nonlinear compensation methods, it is possible to use the resulting transfer function in feed-forward control or model-based creep compensation.
This remedies having to tune cable force controller with large safety margins due to stability concerns.

Experimental observations give the delusive impression that a purely spring-damper based kinematic cable is insufficient with regards to short-term dynamics.
This impression stems from cables showing some memory effect when being continuously excited and then kept at constant strain.
Such behavior cannot be modeled through springs or dampers, but by means of fractional derivatives~\autocite{Mainardi.2011}.
Using fractional derivatives, external excitation on the cable like contact with pulleys or the winch can be modeled too, as these affect the stress-strain propagation in dependence of the direction of excitation~\autocite{Sun.2015}.
Following this direction requires though more deeper knowledge and understanding of material rheology, which may be out of the scope of any roboticist.

The most challenging open task resulting from the two findings of this thesis\textemdash a flexible multibody simulation framework and the stress-strain dynamics of fiber cables\textemdash is the incorporation of the latter in the former.
We propose a spatial cable model based on purely linear elasticity, while on the other hand showing that physical fiber cables behave anything but that.
However, while the stress-strain dynamics of fiber cables remains largely uncharted territory, its incorporation into the Cosserat rod-based cable remains to-be-completed.
Extensions of the Cosserat rod with viscoelastic elements can be done, however, the stress-strain cable model we presented is more complex than a simple spring-damper element requiring the fourth-order derivative of strain and stress.
To attain continuity of the cable shape in itself, the polynomial degree must be raised.
Another major challenge arising from this task lies in a mechanically consistent formulation of internal energies and forces using a \nth{4}-order \gls{ODE} on stress and strain.
Without profound knowledge in continuum mechanics and possibly even viscoelastic materials, this task must remain open for the time being.

Lastly, as is the case with any modeling and simulation, obtaining mechanical parameters in order to parametrize the model is as vital as is a sufficient model.
Most of the parameters for the cable model presented in this thesis can be obtained rather straightforward like cross-section area, diameter, unit weight, or length.
However, obtaining values for the parameters specifically impacting dynamics like elastic modulus, shear modulus, or viscous modulus, is not straightforward.
With the experimental identification of the stress-strain dynamics, we presented a methodology for obtaining numerical values of elasticity and viscosity, yet other values remain unknown.
If we are interested in physically meaningful parameter values of our \caro model, then experimental parameter identification of unknown parameters have to be made.
If all we are interested in is a realistically behaving \caro model, then physically meaningful values may no be of interest and we can choose to use optimization methods matching simulation data with experimental data to find the right parameter set.
Consequently, experimental validation of the spatial cable model presented in this thesis is necessary and ought be the next step.
