%!TEX root=../../../thesis.tex
%!TEX file=content/introduction/applications-demonstrators/applications.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Applications}\label{sec:introduction:applications-demonstrators:applications}

In this section, we will focus on applications of \caros in different fields such as virtual reality, manipulation tasks, entertainment, and other applications.
This selection of some of the most noticeable \caros shall highlight their wide range of applicability as well as the range of scale\textemdash both geometrically as well as dynamically\textemdash in which \caros are developed.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Virtual Reality}\label{sec:introduction:applications-demonstrators:applications:virtual-reality}

Virtual reality may be more generalized to manipulation and handling tasks as the \caro performs manipulation of its end effector, which in turn is connected or controlled directly by a human operator.
However, in scenarios of virtual reality, the \caro and human must either cooperatively manipulate the end effector or the robot is designed to provide a certain feedback to the human thus actively working against the human input.



%%%%%%%%%%%%%%%%%%%%
\paragraph{\INCA} by \citeauthor{Chellal.2012} uses thin cables since these are very unobtrusive to the human and hardly harmful~\autocite{Chellal.2012,Chellal.2015}.
These two characteristics make \caros a very useful haptic interface and force display system, which is already commercially available as product of Haption SA.
\INCA is a force feedback haptic interface developed to investigate manipulation of objects in an environment of virtual reality.
It uses passive pretensioning of the cables by means of a spring-based balancing mechanism and infrared cameras for vision-based control of the end effector.




%%%%%%%%%%%%%%%%%%%%
\paragraph{Virtual Tennis} is presented by~\citeauthor{Kawamura.1995b} using a \caro by means of which the reactive forces during playing tennis are generated~\autocite{Kawamura.1995b}.
The system itself is in a sideways-laid suspended configuration, as such the player's force input is used to keep all cables taut.
In combination with a helmet-mounted display, the user can experience the mass of the tennis racket, the moment of inertia of the same, and reaction forces when the ball is hit.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Manipulation}\label{sec:introduction:applications-demonstrators:applications:manipulation}

Previously presented virtual reality applications differ from manipulation tasks with respect to how a human operator interacts with the system.
In manipulation tasks, we assume the operator to not directly be interacting with the end effector but controlling its position by means of some control input \textEg control joysticks.
Manipulation tasks may also be programmed and automated, whereas virtual reality tasks in general are immersive and dynamically changing tasks.


%%%%%%%%%%%%%%%%%%%%
\paragraph{Wind Tunnels} of various sizes can be realized with \caros as the cables can be made with comparatively small diameter making them very unobtrusive to causing wind turbulences~\autocite{Kawamura1995}.
Any other suspension system would either lack structural integrity within large wind tunnels or would cause the wind flow to be disturbed.
However, care needs to be taken as tensed cables are subject to wind-induced vibration which can propagate onto the supported structure.


%%%%%%%%%%%%%%%%%%%%
\paragraph{\RoboCrane} by~\citeauthor{Dagalakis.1989} of the National Institute of Standards and Technology was one of the first projects to investigate possible applications for cable robots~\autocite{Dagalakis.1989,Bostelman.1994}.
As the name suggests, the primary direction of research were automated cranes, but~\citeauthor{Bostelman.1994} also presented aircraft painting and stripping with hybrid cable-driven robot arms.
The \RoboCrane was the first \caro system that was operated with a conventional, industry-like control system.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Entertainment}\label{sec:introduction:applications-demonstrators:applications:entertainment}


%%%%%%%%%%%%%%%%%%%%
\paragraph{SkyCam} as well as Spidercam and CableCam are computer-controlled, stabilized, cable-suspended camera system that are more and more often deployed in both film and movie studios as well as during big scale sport events like soccer, football, or Formula~1 races~\autocite{Cone.1985}.
They provide for a bird's-eye view of an event allowing for better visually capturing the extent.
While this technology may generally not be called a robot, it still employs \caro technology to move the end-effector\textemdash its camera\textemdash in a large workspace with high dynamics.
Further, cable-suspended camera systems are prime examples of the ease of reconfiguration of \caros as there are no systems permanently installed in stadiums, but they are set up for each event.


%%%%%%%%%%%%%%%%%%%%
\paragraph{\Expo CableRobots} shown in \cref{fig:introduction:applications-demonstrators:applications:expo}, are twin \caros developed by~\citeauthor{Tempel.2015g} of the Institute for Control Engineering of Machine Tools and Manufacturing Units ISW and the Institute of Engineering and Computational Mechanics ITM and benefited from some research findings also available in this thesis~\autocite{Tempel.2015g,Tempel.2015f}.
The \Expo CableRobots again nicely show benefits of \caros over their rigid link counterparts as the systems were operated continuously above audiences' heads over a period of \num{6} months on a workspace of approximately~$\SI{7x6x14}{\meter}$ per robot.
With the intention of making the perfect illusion of two freely floating eyes in space with~$\num{6}$ \gls{DOF}, it was improbable to use an actuating system other than cable robots.
One of the key challenges was the increased level of safety as the \caros must not have failed during operation \textIe cable rupture or tipping over of the suspended \caros was to be avoided.



\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/arecibo-02}
    \caption{%
        Arecibo radio telescope, as seen from the observatory deck.
        \copyrights{arecibo}%
      }
    \label{fig:introduction:applications-demonstrators:applications:arecibo}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/expo2015-05}
    \caption{%
        One of the two \Expo CableRobots during their show in the German Pavillon at the universal exposition Expo 2015 in Milan, Italy.
        \copyrights{expo2015}%
      }
    \label{fig:introduction:applications-demonstrators:applications:expo}
  \end{subfigure}
  \caption[%
    \Caro applications Arecibo and \Expo.
  ]{%
    \Caro applications Arecibo~\subref{fig:introduction:applications-demonstrators:applications:arecibo} and \Expo~\subref{fig:introduction:applications-demonstrators:applications:expo}.
  }%
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Other Applications}\label{sec:introduction:applications-demonstrators:applications:other}


%%%%%%%%%%%%%%%%%%%%
\paragraph{Telescopes and Aerostats} are large structures that can span hundreds of meters in diameter like the~$\SI{500}{\meter}$-aperture telescope in China~\autocite{Nan2003}.
Telescopes generally use a primary and secondary reflector, of which the latter one is designed to be movable when the telescopic structure as a whole cannot be moved~\autocite{Tang2009}.
However, with telescopes becoming even larger to see farther into the universe, their secondary reflectors become larger, too, requiring new means of support and moving.
One of the first telescopes using the \caro technology is the Arecibo observatory in Puerto Rico~(\cref{fig:introduction:applications-demonstrators:applications:arecibo}).
Its secondary reflector is supported by cables wound onto three towers over a~$\SI{305}{\meter}$-aperture reflector~\autocite{Altschuler.2002,Altschuler.1998}.
Arecibo was superseded as largest telescope by the~$\SI{500}{\meter}$-aperture telescope \Fast in 2016, which also set new standards for \caro research such as design~\autocite{Luo.2001,Qian.2007}, kinematic enhancements~\autocite{Kozak.2006,Li2011b}, vibration analysis~\autocite{Liu.2013,Tang.2014b}, force distribution calculation and control~\autocite{Li.2013b}, workspace analysis~\autocite{Li.2008c}, or simulation and accuracy analysis~\autocite{Sun2008}.


%%%%%%%%%%%%%%%%%%%%
\paragraph{Search and Rescue Operations} are usually scenarios with dangerous and subpar environment of uneven or unstable ground surface in possibly even remote locations.
As such, not all robotic devices can be put to use in these environments and research for all-terrain robots, whether humanoid or not, is still an active field.
To the rescue come \caros like \Marionet by~\citeauthor{Merlet.2014b}, which are by design lightweight robots and allow for easy transportation as well as quick installation as their requirements to the environment are rather few.
\Marionet is intended for lifting objects of up to~$\SI{2.5}{\tonne}$, scalable to workspaces of~$\SI{100x100x50}{\meter}$, while still being very portable at just~$\SI{200}{\kilo\gram}$ total weight~\autocite{Merlet.2014b}.



%%%%%%%%%%%%%%%%%%%%
\paragraph{Contour Crafting} is a layered fabrication technology for automated construction of structures where~\citeauthor{Bosscher.2007} propose using a \caro to construct such large objects~\autocite{Bosscher.2007}.
Since robotic systems based on the gantry concept will require huge elements to keep their own structural integrity, they drop out of competition.
\Caros, on the other hand, can span large workspaces and, by means of an adjusted design, still will not suffer from collision with the printed structure~\autocite{Fabritius.2019,Pott.2019d}.
This concept is also transferable to printing of smaller scaled objects in modern day additive manufacturing.


%%%%%%%%%%%%%%%%%%%%
\paragraph{Exoskeletons} are strictly speaking no real cable-driven parallel robots, but merely hybrid structures for they have a serial chain of links actuated by parallel pairs of cables\textemdash due to the need for antagonistic pairs of cables.
However, exoskeletons using cables are a very promising technique to reducing the inertia of the device while still allowing for high supportive forces.
In fact, \citeauthor{Mao.2015} present a cable-based exoskeleton used for rehabilitation treatment of upper-limb impairments with the added benefit of reducing the risk of harming the patient~\autocite{Mao.2015}.
A proper \cdpr* was developed by~\citeauthor{Rosati2005} in several publications where they focus on treatment of patients with stroke-related paralyzed or paretic upper limb~\autocite{Rosati2005,Rosati.2007,Rosati.2008}.
\Citeauthor{Surdilovic.2007} developed a \caro-based full body harness that can be used for treatment of paretic lower limbs or as a training device for athletes.
In all these applications, the technology of cable-driven systems makes the robots intrinsically safe and is used to its full potential.

