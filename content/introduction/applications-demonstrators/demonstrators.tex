%!TEX root=../../../thesis.tex
%!TEX file=content/introduction/applications-demonstrators/demonstrators.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Demonstrators}\label{sec:introduction:applications-demonstrators:demonstrators}

We classify \caros as demonstrator when their main purpose is demonstrating \caro technology and using it as test carrier for various applications.


\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/copacabana-06}
    \caption{%
        \COPacabana at Institute for Control Engineering of Machine Tools and Manufacturing Units ISW of University of Stuttgart, Stuttgart, Germany.
      }
    \label{fig:introduction:applications-demonstrators:demonstrators:copacabana}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/ipanema3-01}
    \caption{%
        \IPAnemaThree at Fraunhofer Institute for Manufacturing Engineering and Automation IPA, Stuttgart, Germany.
        \copyrights{ipanema}%
      }
    \label{fig:introduction:applications-demonstrators:demonstrators:ipanema}
  \end{subfigure}
  \caption[%
    \Caros: \COPacabana, \IPAnema.
  ]{%
    \Caro demonstrators \COPacabana~\subref{fig:introduction:applications-demonstrators:demonstrators:copacabana} and \IPAnema~\subref{fig:introduction:applications-demonstrators:demonstrators:ipanema}.
  }%
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection[COPacabana]{\COPacabana}\label{sec:introduction:applications-demonstrators:demonstrators:copacabana}


Designed as pair of cooperating \caros, \COPacabana comprises two medium-scale \caros~(see~\cref{fig:introduction:applications-demonstrators:demonstrators:copacabana}), one designated for handling \textIe loading and unloading of pieces into a CNC drilling machine, the other for inspection of the same pieces.
While their workspaces are only \SI{3x4x4}{\meter} each, both systems can attain maximum accelerations of~$\maxnorm{\linacc} = \SI{1.4}{\meter\per\square\second}$ linear and~$\maxnorm{\angacc} = \SI{45}{\degree\per\square\second}$ angular, respectively, with a maximum payload of~$\SI{40}{\kilo\gram}$ each.
Their design is based on the \IPAnema winches and designed as trade-off between lower dynamics yet higher payload.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection[IPAnema]{\IPAnema}\label{sec:introduction:applications-demonstrators:demonstrators:ipanema}


The \IPAnema system by~\citeauthor{Pott2012b} is a family of \caros ranging from the smallest \caro \IPAnemaMini, barely larger than an elevator cab, up to the large-scale configuration \IPAnemaThree, with a workspace of~$\SI{16x12x3}{\meter}$~\autocite{Pott2012b}~(see~\cref{fig:introduction:applications-demonstrators:demonstrators:ipanema}).
In their research,~\citeauthor{Pott2012b} created the first \caro with industrial grade servo drives and control aiming at introducing a modular concept for different \caro applications of either high dynamics or high payloads.
With these demonstrators, \citeauthor{Pott2012b} address issues such as cable force control~\autocite{Kraus2015b,Kraus.2015b}, forward kinematics~\autocite{Pott.2015b,Pott.2010,Schmidt2014}, and system identification~\autocite{Kraus.2014}, but also application driven research like haptic interfaces~\autocite{Kraus.2015c}.
\IPAnemaThree is the world's second largest \caro in any research facility\textemdash only outdone by the \MPICRS, while it is the world's largest \caro for research on \caro technology.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection[Marionet]{\Marionet}\label{sec:introduction:applications-demonstrators:demonstrators:marionet}

Like \IPAnema, \Marionet is a family of modular \caros developed primarily by~\citeauthor{Merlet.2010} with four different sizes application purposes.
The author's designs comprise
\begin{inparaenum}[1)]
  \item \MarionetRehab~($\num{7}$ cables), intended for tasks in patient rehabilitation and fast pick-and-place applications,
  \item \MarionetCrane~($\num{6}$ cables), designed for search and rescue operations and as manipulator for high payloads over large workspaces,
  \item \MarionetAssist~($\num{6}$ cables), envisioned as personal assistance robot for elderly and handicapped people,
  \item \MarionetVR~($\num{6}$ cables), to be used as haptic device in virtual reality environments.
\end{inparaenum}
With these differently purposed \caros, \citeauthor{Merlet.2010} wants to discover new theoretical and practical problems, in particular of suspended \caros.
Their work focuses primarily on kinematics~\autocite{Berti.2013,Berti.2016,Berti2016,Merlet2015b,Merlet2016b}, workspace analysis~\autocite{Blanchet.2014,Carricato2010}, and design methodologies~\autocite{Hao2005} in order to improve accuracy, but also on trajectory generation~\autocite{Ramadour2014b}.

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/cogiro-03}
    \caption{%
        \CoGiRo used at Laboratoire d'Informatique, de Robotique et de Micro{\'e}lectronique de Montpellier~(LIRMM), Montpellier, France.
        \copyrights{cogiro}%
      }
    \label{fig:introduction:applications-demonstrators:demonstrators:cogiro}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=\linewidth,%
      ]{cable-robots/mpi-crs-01}
    \caption{%
        \MPICRS by \textProf B\"ulthoff's group at Max Planck Institute for Biological Cybernetics, T\"ubingen, Germany.
        \copyrights{mpicrs}%
      }
    \label{fig:introduction:applications-demonstrators:demonstrators:mpicrs}
  \end{subfigure}
  \caption[%
    \Caros: \CoGiRo, \MPICRS.
  ]{%
    \Caro demonstrators \CoGiRo~\subref{fig:introduction:applications-demonstrators:demonstrators:cogiro} and \MPICRS~\subref{fig:introduction:applications-demonstrators:demonstrators:mpicrs}.
  }%
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection[CoGiRo]{\CoGiRo}\label{sec:introduction:applications-demonstrators:demonstrators:cogiro}

\CoGiRo shown in~\cref{fig:introduction:applications-demonstrators:demonstrators:cogiro} is a large-scale industrial, suspended \caro designed by~\citeauthor{Gouttefarde2008} at LIRMM in Montpellier, France.
The aimed-at tasks are handling of large scale structures~\autocite{Izard.2018} and manipulation tasks like surface painting or assembly~\autocite{Izard.2013b}.
In addition, it is being used for better understanding of tension distribution of suspended \caros~\autocite{Lamaury2013}, kinetostatic analysis~\autocite{Gouttefarde2012}, workspace optimization~\autocite{Hussein.2018}, and reconfiguration~\autocite{Izard.2013b}.
With its workspace size of \SI{15x10x6}{\meter} and a payload of \SI{400}{\kilo\gram}, driven by steel cables, it is the only \caro with large rotational workspaces allowing for rotations of up to~\SI{+-105}{\degree} about the vertical axis~\autocite{Gouttefarde2007} due to its unique cable routing.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection[MPI Cable Robot Simulator]{\MPICRS}\label{sec:introduction:applications-demonstrators:demonstrators:mpi-cablerobot}

Designed as large-scale motion simulator, the MPI CableRobot Simulator at the Max Planck Institute for Biological Cybernetics, is the first \caro certified for carrying passengers aboard its icosahedron shaped platform.
With stunning specifications, the system is a prime example of the newly opened possibilities deploying \caros in real-world examples.
According to~\textcite{Miermeister.2016}, the acceleration limits at~${ \maxnorm{ \linacc } = \SI{14}{\meter\per\square\second} }$ and~${ \maxnorm{ \angacc } = \SI{100}{\degree\per\square\second} }$ linear and angular, respectively, at maximum velocities of~${ \maxnorm{ \linvel } = \SI{5}{\meter\per\second} }$,~${ \maxnorm{ \angvel } = \SI{100}{\degree\per\second} }$ linear and angular, respectively, while allowing for safe translation in a \SI{4x5x5}{\meter} sized workspace.
Allowing for up to~$\SI{500}{\kilo\gram}$ of payload, the system can not only carry passengers, but also a physical chassis to render the motion experience even more immersive.
Research on \MPICRS focuses on cable vibration analysis and compensation~\autocite{Schenk.2016}, position control~\autocite{Schenk2015}.
