%!TEX root=../../thesis.tex
%!TEX file=content/introduction/cable-driven-parallel-robots.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cable-Driven Parallel Robots}\label{sec:introduction:cable-driven-parallel-robots}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \includegraphics[%
      width=\linewidth,%
      height=0.30\textheight,%
      keepaspectratio,%
    ]{ipanema-frame}
  \caption[%
    Illustration of a \caro with its core components shown.
  ]{%
    Illustration of a \caro with its core components: a frame~(surrounding), attached winches, cables~(red), and the mobile platform~(center).
  }
  \label{fig:introduction:cable-driven-parallel-robots:diagram}
\end{figure}

\Acrlongpl{CDPR}\textemdash we will use the shorter form \textit{\caro} in the remainder\textemdash combine the advantages of serial mechanisms with those of parallel mechanisms and of cable-based actuation aiming at overcoming the mechanisms' shortcomings~(a schematic sketch is shown in~\cref{fig:introduction:cable-driven-parallel-robots:diagram}).
The concept of \caros is as simple as it is ingenious: by replacing prismatic joints with elastic cables, the stroke of each actuating chain can be drastically increased while at the same time reducing its inertia.
Since cables are almost infinitely flexible\footnote{%
To some extent this holds true as cables can be bend and twisted without providing any significant resistance.
Of course, at some point, the mechanical structure of the cable or its strands will yield and the cable breaks.
}, it is possible to store the superfluous cable in a limited space \textEg on a winch or simply inside a container.
These cable storage and retrieval units can then be stored outside of the workspace and fixed to the environment \textIe the ground, the wall, or a separate frame.
This implicates a reduction in moving inertia as this now reduces to the end effector inertia, the inertia of the cables, and the inertia of the coiling mechanism \textIe drum itself.
In general one assumes that, in contrast to the platform inertia, the cable inertia can be neglected as it does not primarily affect the dynamics.
However, this largely depends on the scale and configuration of the cable robot, the mechanical properties of the cables in use, the designated application, as well as the maneuver performed.
Experimental observations have shown that cable dynamics cannot be neglected completely, but a common approach to understanding and modeling the cables is yet to be found and is thus the main focus of this thesis.

A \caro is generally composed of four components:
%
\begin{inparaenum}[1)]
\item an end effector or mobile platform with $\dof$ degrees of freedom, positioned within the workspace to perform a certain task,
\item $\numcable$ cables to control the pose \textIe position and orientation of the platform,
\item winches that change the length of the cables, and
\item a frame that is used to attach the winches to.
\end{inparaenum}
%
One may not always see a closed frame, or even any frame, on a \caro as simple towers can be used in their place, or the environment itself may be used as frame.
The simplest form of a \caro would be a building crane, however, to be precise, a crane is not a robot as per definition.
Following the definition of the Robotics Industries Association RIA, a robot ``is a reprogrammable, multifunctional manipulator designed to move material, parts, tools or specialized devices through variable programmed motions for the performance of a variety of tasks''~\autocite{RoboticIndustriesAssociation.06211999}.
In this case, building cranes are no robots as they are no reprogrammable, multifunctional manipulators.

Compared to building cranes, \caros used in industrial or entertainment applications are more complex and make use of more than one cable, however, challenges are very much alike in both systems.
At the core of all challenges of \caros are the cables as they are the driving element of such systems.
When comparing \caros to their rigid body counterparts, with which they share very much every other benefit and demerit, the only apparent difference is the use of cables instead of rigid joints.
It is remarkable that this at first sight very small structural change has major impact on positioning accuracy, control, workspace, dynamics, and more related to \caros.
Any sort of cable, be it~$\diameter = \SI[round-mode=off]{0.5}{\milli\meter}$ nylon fibers,~$\diameter = \SI{6}{\milli\meter}$ polyethylene fibers, or even~$\diameter = \SI{200}{\milli\meter}$ steel cables, suffers from noticeably low flexural rigidity with respect to its tensile rigidity.
This fact becomes apparent in cables tending to sag under their own weight due to gravity when spanned between two points, with the magnitude of sag dependent on the applied tension and mechanical properties~(a mechanical derivation can be found in~\cref{sec:fundamentals:inverse-kinematics:mechanical}).
Three side-effects emerge from cable sag being
\begin{inparaenum}[1)]
  \item the cable length being larger than the geometric distance between the cable's two ends,
  \item the direction of cable force at the distal end changing from the direct-line connection to also comprise vertical drag, and
  \item the inability to transmit large forces through the cable \textIe only a tensed and nearly straight cable can transmit any force wit marginal loss from one end to the other.
\end{inparaenum}
Sagging occurs in even very lightweight cables, however, it may be neglected as the magnitude of sag is small compared to the cable length~\autocite{Irvine.1974}.

Going from statics to dynamics, any excitation perpendicular to the cable's neutral axis will not be hindered by restoring shear forces, as is the case for rigid bodies.
Ultimately, this results in longitudinal motion or vibration of the cable which are subject to gravity and the cable's internal dynamics\textemdash neglecting effects like air friction or contact with the environment.
In any event, vibration will decline, yet magnitude and decay time vary largely depending on cable tension and magnitude of excitation.
What is even worse than a freely vibrating cable, is a vibrating cable interacting with other bodies onto which the vibration may propagate.
This is the case in \caros where the vibration of a single cable propagates in both directions, toward the winch and the platform where the wave continues traveling farther.
While the winch can robustly compensate for the waves due to its high inertia and being rigidly attached to the ground, the mobile platform is attached to only the cables thus it will also start vibrating and propagating its motion onto the other cables.
This affects not only dynamic trajectory tracking, but also reflects itself in the overall system stiffness being degraded due to vibrating cables.

While international standards exist for structurally designing and selecting cables in engineering technologies like cable-stayed bridges, elevators, aerial tramways, or cranes, choosing mechanical cable properties is done based on empirical values and expert know-how~\autocite{Jung.2018,Ji.2018,Collings.2016}.
In many cases, the dynamics of such systems are neither considered nor investigated when designing the application, but are based on finite element static analysis\textemdash and considerably high safety factors.
Even so, to this date, no comprehensive and agreed upon modeling of \caros with elastic and flexible cables of time-varying length has been made which is why understanding of \caro dynamics remains in a state of uncertainty.
For small scale \caros, a simplified dynamics model based on pure kinematic properties suffices, however, there is discrepancy between simulation and experiment which cannot solely be lead back to non-modeled effects like friction.
Particularly, cable motion as the platform sways along a trajectory can be observed in experiments, but it is unclear as to how a suitable model for a \caro together with its cables can be found~\autocite{Tang.2014}.
In the following section, we will give an overview of literature focusing on modeling of \caros, as well as cables to use in \caros to point out the improvements already made and shortcomings, as well as the direction research is headed.
