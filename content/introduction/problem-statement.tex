%!TEX root=../../thesis.tex
%!TEX file=content/introduction/problem-statement.tex

\section{Problem Statement and Goal}\label{sec:introduction:problem-statement}

This thesis aims at finding a model formulation suitable for forward dynamics simulation of \caros under consideration of spatial cable dynamics as current modeling approaches describe the system only partially mechanically consistent.
To advance \caro research and applications, we want to coherently research and evaluate modeling approaches of \caro dynamics allowing for more holistic formulations of said systems under consideration of the main components\textemdash platform and cables.
Let us state the goal of this thesis in the following way:

\begin{thesisgoal}
  We desire a low-complexity and low-dimensional \caro model describing the overall dynamics of the mobile platform in conjunction with the cables.
  Particular emphasis shall be put on spatial dynamics of the cables, while we also want to address the stress-strain dynamics of cables used in \caros of the \IPAnema family type.
\end{thesisgoal}

We shall further impose the following assumptions or simplifications on the modeling under investigation in this thesis:
%
\begin{description}
  \item[Fiber Cables Only] Cables in \caros are usually made from either steel or polyethylene fibers, which makes them behave very differently.
    Not only are steel cables heavier thus tend to sag more drastically, they also feature higher stiffness, resulting in less strain with the same amount of tensile force, and showcase very linear stress-strain relationships.
    However, fiber cables are advantageous due to their reduced inertia allowing for higher dynamics and larger scale \caros, yet come at the price of more unknown intra-cable kinematics and nonlinear stress-strain dynamics.
    With most \caros being operated with fiber cables, we limit our focus to these materials only.
  \item[Deformation-free Torsion] The cable does not undergo any spreading deformation during torsion.
    Since torsional displacement of the cable would\textemdash depending on the direction of torsion and the cable's direction of weave\textemdash stretch or relax the fibers, their length would decrease or increase, respectively.
    From this follows a nonlinear change in cable strain and as such a difference in force transmission behavior.
    In case of severe twisting of the cable, phenomena like birdcaging~\autocite{Costello.1990} or twisting of the wire will occur causing damage to the cable and largely different stress-strain or spatial dynamics.
  \item[No Dependence on Environmental Changes] Mechanical properties of any material characteristically change with varying environmental properties.
    For example, the elasticity of a beam made of steel becomes larger with increasing temperature.
    Such effects can also be observed for polyethylene fibers such as \Dyneema~\autocite{Schmidt.2016} but may not be explicitly considered in this work.
  \item[Solid Cross-Section] We assume the cable to be of solid cross-section \textIe not composed of single strands or multiple strands.
    While physically any cable or wire rope is made from laying or weaving single strands of wires into larger strands which then in turn are laid or woven into the resulting wire rope, we assume the cable given with solid cross-section area.
    This assumption goes hand in hand with the next two.
  \item[Unaltered Cross-Section] While the cable's cross-section does in fact change during stress-induced elongation~\autocite{Howell.1992,Schmidt.2016}, we shall assume the cross-section to always be of circular shape.
    Not only does this provide for cleaner equations of motion and computationally less expensive numerical integration, but it is also not of major concern to determine the change of cable shape.
    With regards to cable collision detection, this may be of interest, however, as shown by \textEg~\textcite{Schmidt.2016}, the ovalization characteristics of \Dyneema cables do not change the cross-section shape too drastically.
    As such, the worst-case bounding box of a cable under high tension may be given by a foursquare of edge length set to the cable's diameter.
  \item[No Relative Intra-Cable Kinematics] With assuming our cables being made from one strand only, this also implies that there is no relative intra-cable kinematics affecting the whole cable kinematics and dynamics.
    As a cable is made from many strands, these will inevitably be stressed or strained differently depending on the overall bending of the cable.
    Concluding, there is relative motion between single adjacent strands which implies intra-cable friction.
    In fact, this causes elastic strain relaxation due to low speed friction, as will be shown in a later chapter.
\end{description}
