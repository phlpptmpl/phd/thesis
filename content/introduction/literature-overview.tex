%!TEX root=../../thesis.tex
%!TEX file=content/introduction/literature-overview.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Literature Overview}\label{sec:introduction:literature-overview}

Despite cables being a mechanical structure that has been known to mankind for many millennia, comparatively only very little is known about these structures.
It is generally accepted that cables can exert only tensile forces, and that their elastic behavior can, in a first-order approximation, be considered linear with a certain factor of stiffness.
However, exact cable models, in particular for use in \caros, are still scarce with a lot of the state of modeling being academic research findings rather than versatile formulations.
This is last due to the complex nature of how cables are manufactured and the vast variety of different structures making modeling very involved.
In this literature overview, we will focus on this thesis' primary direction \textIe modeling of spatial cable dynamics explicitly considering transversal cable motion.
An overview of research on longitudinal cables waves \textIe stress-strain dynamics will comprise this section.

Different approaches to modeling cables in various fields of engineering can be found which can be categorized into
%
\begin{inparaenum}[1)]
\item detailed modeling as a composition of multiple strands of wires,
\item approximate models of only the large signal dynamics, or
\item fake dynamics models.
\end{inparaenum}
%
While each of these approaches has its merits and demerits, every single one has its right to exist.
Coming from material sciences, the detailed modeling of cables and wires is done on a very small-scale level \textIe while microscopic effects may not be considered after all, the interaction between single cable strands is at the root of these models.
If the intra-cable dynamics are of only little interest, we can assume the cable to be composed of one solid body exhibiting respective elastic and flexural rigidity.
In this case, only the overall shape or motion of the cable and its response to excitation\textemdash either motion or force\textemdash are to be of interest.
Such models will reduce the complexity and provide only for the essential data, but may lack in covering effects such as rupture or disintegration of single fiber strands.
Lastly, especially in the field of animation, fake dynamics of cables or cable-like objects, are employed as one is merely interested in having an object that behaves physically plausible.
However, its physically correct behavior \textIe allowing for correct interpretation of forces and moments is not of interest thus requiring little to no physical foundation for the models.



One of the first publications on \caros is the aforementioned Skycam by~\citeauthor{Cone.1985} in which the basic kinematic and dynamics formulations for \caros were given incorporating nonlinear elastic cables~\autocite{Cone.1985} and Newtonian mechanics of a free-floating rigid body.
It was not until~\citeyear{Albus.1992}, when~\citeauthor{Albus.1992} published a more in-depth analysis of \caros and possible fields of applications.
Already then, a viscoelastic cable model composed of a parallel combination of linear spring and linear damper was considered in modeling and simulation of the \RoboCrane~\autocite{Albus.1992}.
Further consideration of the kinematics and dynamics of the cables were not performed until much later and cables were considered unilateral constraints\textemdash providing tensile forces when elongated and no compressive forces when shortened.
This resulted in the cables being assumed massless slender rods of only viscoelastic behavior with unilateral stress-strain dynamics, which over time ultimately evolved into the so-called \textit{standard \caro model}.
After almost one decade of no further contributions, \citeauthor{Nahon.1999} presents the dynamics modeling of a tethered aerostat \caro based on finite element discretization of the cables by a lumped-mass approach with absolute nodal coordinate formulation~\autocite{Nahon.1999}.
Changes in cable length are accounted for by varying the length of the very first segment.

The lumped mass approach was later followed by further researchers~\autocite{Hajzman.2011,Caverly.2014,Caverly.2015,Bedoustani.2008,Duan.2010,Duan.2011} and proven not suitable to the winding and guiding dynamics of cables nor to covering well the flexural rigidity due to the type of discretization used.
In \caros, these models can be used for static, kinetostatic, and vibration analysis of the system \textIe for any static scenario.
Spatially discretized cables can be successfully employed in cases where cable length does not change and where the cable must not be guided like in underwater riser applications~\autocite{Du.2012,Quisenberry.2006}.
However, finding numerically efficient formulations with high accuracy is a contradictory task due to the inherent kinematic constraining of each segment to its preceding and succeeding element.
Simulation times of several hours [\textSic.] are not unlikely, even with just one cable freely swinging~\autocite{AdamiecWojcik.2014}.

Bestowed on discretized models is the ability to allow for kinetostatic and vibration analysis of the cable robot's mobile platform at any given pose within the workspace.
Further, \autocite{Nahon.1999,Duan.2011} show the theoretical workspace of \caros with lumped mass cables differs from the one obtained using the standard model.
In fact, in-depth research by~\citeauthor{Bedoustani.2008} points out neither cable mass nor cable elasticity can be neglected with both having non-negligible effect on terminal accuracy of the platform~\autocite{Bedoustani.2008,Bedoustani.2011}.


While cable mass was willfully neglected in early years in both statics and dynamics, \citeauthor{Kozak.2006} first consider cable mass and elasticity in the workspace analysis of \caros by employing~\citeauthor{Irvine.1974}'s infamous hefty cable model, turning \caro both the kinematics problem and workspace calculations into a kinetostatic problem~\autocite{Kozak.2006}.
Reaching from the~\citeyear{Irvine.1974} work by~\citeauthor{Irvine.1974}, further models for vibration and stiffness analysis of \caros emerged, both in two dimensions~\autocite{Nan.2011}, as well as three dimensions~\autocite{Diao.2009}.
In all these contributions, the static solution given the cable model by~\citeauthor{Irvine.1974} is superimposed by the linear wave equation allowing for additional transversal deflection of the cable.
The results signify the importance of considering not only longitudinal cable elasticity, but also their transversal flexibility, already in the statics case.
Unfortunately, the results are valid in only a very limited range around the taut string due to the linearization employed during derivation of the linear wave equation.

Other approaches to modeling and simulation of \caros where proposed using either Robotran software~\autocite{Collard.2011} or \gls{CAD}-based simulation~\autocite{Michelin.2015}.
With Robotran being a multibody simulation framework designed for robotic applications, \citeauthor{Collard.2011} built a \caro using a segmentized cable of fixed length, where the number of segments is chosen as a trade-off between model complexity, accuracy, and simulation speed~\autocite{Collard.2011}.
\citeauthor{Michelin.2015}, on the other hand, built a complete \gls{CAD} environment of a \caro containing every component to simulate \CoGiRo, a suspended \caro.
Despite simplifying the complexity of the used \gls{CAD} model, simulations still run very inefficiently as high number of segments per cable cause the collision detection algorithm of XDE to have to consider many collisions.
Even though there are also other software solutions to simulating cables or ropes, it is the high stiffness and kinematic redundancy that causes most cable models to be ill-suited for simulation of \caros~\autocite{GarciaFernandez.2008}.

Only recently, more sophisticated models for \caros with consideration of cable dynamics have been proposed for both the transversal motion~\autocite{AyalaCuevas.2018} and the longitudinal motion~\autocite{Godbole.2018}.
In both contributions, the cable deflection is described by means of linear combinations of shape functions; the dynamics are thoroughly derived using Lagrange's equation of the first kind.
While~\citeauthor{AyalaCuevas.2018} describes only the transversal motion of an inelastic string using a low-degree polynomial, their model allows for showing interaction between cable and platform especially during jerky motion.
In addition, change of cable length is incorporated into the model by considering the change of inertia during cable coiling~\autocite{AyalaCuevas.2018}.
\citeauthor{Godbole.2018} on the other hand describe only the longitudinal motion using a set of harmonic shape functions, which highlights the high-frequency elastic deformation vibrations inside the cable.
Using simple normalization of the shape functions, variable cable length is incorporated into the dynamics, and for energetic consistency incorporated into the inertia of the winding drum~\autocite{Godbole.2018}.


Besides addressing transversal motion of cables in the context of \caros, their longitudinal dynamics are also of interest \textIe their dynamics of stress and strain or likewise of elongation and force.
Since cables are not allowed to go slack during operation of \caros, cable force control algorithms are heavily being developed ranging from linear controllers like \gls{acr:PI-controller} or \gls{acr:PID-controller}~\autocite{Kraus.2014,Mikelsons2008,Bruckmann2006b,Kawamura1995,Khosravi.2013b,Reichert.2015} over optimal controllers like \gls{acr:LQR-controller} or \gls{acr:LQG-controller}~\autocite{Lambert.2007,Korayem2011,Abdolshah2015} or sliding mode~\autocite{Liu2009b,Oh2004c,Schenk2015,Alikhani.2011,ElGhazaly.2015} to methods like $H_{\infty}$~\autocite{Chellal2014,Laroche.2013} or passivity based control~\autocite{Zarebidoki.2011a,Caverly.2014}.
In most, if not all of these contributions, cable length or elongation is the control input whereas cable force on the platform is the output, where the tracking errors are calculated based on deviation of nominal cable forces from a calculated force distribution.
While the latter topic of calculation of cable force distributions in itself is yet another involved task, in particular for \caros with more cables than \gls{DOF}, we may simply assume that cable force control will reduce error between a desired cable force and the currently prevalent force.
Any control law used in this scenario must apparently provide stability of the closed-loop in order to not cause harm to or destroy the system~\autocite{Lunze2010}.
Stability can be proven either analytically or through simulation where in either case some knowledge of the to-be-controlled plant\footnote{In this case, plant simply means cable.} must be available.

Since some knowledge of the underlying cable dynamics must be know, most cable force controller are based on linear elastic models\textemdash cable is assumed to behave like a spring\textemdash or linear viscoelastic model\textemdash cable is assumed to behave like a spring-damper.
While it may still be difficult to obtain quantitative parameters of elasticity and particularly of viscosity for a cable, these models have a wide range of validity for both steel cables as well as polyethylene fiber-based cables like \Dyneema.
However, since cables are no single elastic body but merely composed of several hundreds or thousands of strands, interstrand friction emerges during any cable tensing and relaxing.
This happens for both steel cables in cable-stayed bridges~\autocite{Sauter.2003} or surgical devices~\autocite{Miyasaka.2016} but also polyethylene cables typically used in \caros of small size~\autocite{Miermeister.2015}, medium size~\autocite{Palli.2012}, and large scale~\autocite{Lalo2013b}.
Since polyethylene fiber cables are more promising to use in \caros due to their small inertia-to-tensile-strength ratio, a deeper understanding of such fiber cables' stress-strain dynamics has been of interest to several research groups.

\citeauthor{Miermeister.2015} present an elastic cable model with hysteresis effect that allows for good approximation of the cable's actual stress-strain dynamics, based on linear elastic Hookean material superimposed with a polynomial function of elongation, tension, and excitation frequency for compensation.
For use in a surgical robotic system, \citeauthor{Miyasaka.2016} identify cable force models based on Buoc-Wen hysteresis and also incorporate cable-pulley interaction modeling, increasing approximation quality for steel wires of diameter $\diameter = \SI{0.61}{\milli\meter}$ and $\diameter = \SI{1.19}{\milli\meter}$.
\citeauthor{Palli.2012} use a general viscoelastic material model composed of serial springs and dampers combined in parallel to model the stress-strain dynamics for their use case of tension-prediction on a tendon-based hand.
In the field of \caros, \textcite{Piao.2018,Tempel.2019} also make use of a general viscoelastic material model to describe the dynamics and then later use experimental data to identify material parameters.
\citeauthor{Piao.2018} measure cable elongation as a response of instantaneous stress increase, whereas~\citeauthor{Tempel.2019} measure cable stress given strain inputs.
Either contribution show an improvement in the quality of approximation of both creep phenomena and hysteretic behavior while providing a purely mechanics-based model with physically meaningful parameters.

Findings by \citeauthor{Piao.2018} and \citeauthor{Tempel.2019} can be used for improving cable force control laws by providing either a more comprehensive cable strain model for simulative analysis, or by providing a model that can be used in feed-forward control laws to anticipate the cable's stress dynamics.
However, some questions remain unanswered like the linearity or nonlinearity of mechanical parameters with respect to the unstrained cable length, or the influence of pulleys.
