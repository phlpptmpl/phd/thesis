%!TEX root=../../../thesis.tex
%!TEX file=content/introduction/literature-overview/cable-modeling.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[Cable Modeling]{Cable Modeling for use in Cable Robots and Other Applications}

\Citeauthor{AdamiecWojcik.2013} present the Modified Rigid Finite Element method to describe a cable model based on slender rod objects with linear springs-damper elements for longitudinal flexibility and angular spring-dampers for bending flexibility~\autocite{AdamiecWojcik.2013}.
The model formulation is derived based on energy formulations and then using two sets of generalized coordinates:
%
\begin{inparaenum}[1)]
\item absolute \textIe Cartesian position and rotation, and elongation of each segment, or
\item independent coordinates \textIe rotation and elongation of each segment only.
\end{inparaenum}
%
The numerical efficiency of the first approach is highlighted for a very heavy and stiff cable~(unit density~$\unitdensity = \SI{2000}{\kilo\gram\per\cubic\meter}$, Young's modulus~$\youngsmodulus = \SI{10e11}{\newton\per\square\meter}$, Length~$\cablen = \SI{100}{\meter}$).
They also add hydrodynamic forces~(uplift pressure, drag forces, inertial) to the model for a hollow cable riser.
Numerical simulations are run for a large-scale steel cable~(length~$\cablen = \SI{500}{\meter}$, Diameter~$2 \, \radius = \SI{0.5}{\meter}$, Young's modulus~$\youngsmodulus = \SI{2.07e11}{\newton\per\square\meter}$, unit density~$\unitdensity = \SI{7850}{\kilo\gram\per\meter\cubed}$).
This modeling approach seems promising for modeling of cables in \caros, however, its numerical efficiency must be proved since the material parameters of \UHMWPE cables are largely different~(\textIe smaller) to the parameters used here.

Static cable robot stiffness analysis is carried out by~\citeauthor{Amare.2017} applying Irvine's cable model~(after deriving it) in the three-dimensional case~\autocite{Amare.2017}.
Lastly, they present a kinetostatic model used for solving the inverse kinematics~(using optimization and \texttt{fminmax}).
Some long derivation of the static stiffness matrix follows deriving each component of~$\stiffness$ rigorously.
In the end, the authors end up with differential equations for the dynamic stiffness of the cable which they assess the impact of platform mass as well as the natural frequencies of dynamic stiffness along all axes.
Ultimately, this paper is based on the Irvine cable model in order to derive all necessary equations.
It remains unclear whether a cable with actually dynamically behave as is given by the Irvine model \textIe just a superposition of Irvine and some harmonic shape functions~(on which this work seems to be based upon).

\Citeauthor{Bamdad.2013} presents analytical dynamical solution of suspended cable robots~\autocite{Bamdad.2013}.
He first derives the kinematics and dynamics model based on a flexible cable through Newton-Euler which gives the well known wave equation of the cable.
After separation of variables, the solution of the cable robot motion can be given in a closed form.
Since the cable length changes during motion, the model is solved iteratively at every time step keeping boundary conditions consistent.
This approach seems promising at first, however, interpolation of the data at each time step seems unintuitive when a correct coordinate transformation provides for a closed form solution under consideration of a change in length of the cable.
\Citeauthor{Bamdad.2013} even introduces dynamics of the drives~(drums) considering viscous damping.
Two different cable classes are evaluated, one being nylon fiber, one being a 6x7 wire rope.
Both cables show different actuating torques throughout the winches implying some winches are contributing more to the actual motion than others.
This modeling approach is very similar to the assumed-modes approach~(in fact, \citeauthor{Bamdad.2013} performs the same separation of variables as assumed-modes approach suggests), however, solving the dynamics equation iteratively for every time step with a linear interpolation of the boundary conditions seems unintuitive and may be, energetically speaking, non-conservative.

\Citeauthor{Barzel.1997} presents the idea of faking rope dynamics but simply performing a modal decomposition and letting the animator choose how the gross- and wave-shape deformation of the cable are desired to look like~\autocite{Barzel.1997}.
The resulting shape is then built as superposition of the basic shapes.
Four different ``cable models'' are presented: 1) the suspended rope which is the basic two-ends-fixed cable which may swing from side to side or sway from front to back 2) the loose rope which is a straight line at rest and can have one or more bends while the wave deformation is the same as for the suspended rope 3) the coiled cord extending on the suspended cord as its backbone where the coiled cord is automatically wrapped around and 4) a spring cord which is similar to the coiled cord except the shape of the coiling can be adjusted in terms of its magnitude, frequency, and phase.
While there are some kinematics fundamentals included in the model, there can be absolutely no dynamical foundation found as the dynamics are desired to be generated by the animator \textIe chosen freely and exaggerated to feel more caricature like.
This approach of describing ropes and springs using basic principles of kinematics and animation techniques is interesting to the point where only the shape is of interest.
As soon as other components of the cable are of interest \textIe its proximal and distal tension, this model fails as it does not provide for these properties.
Thus, it is rendered useless unless one wants to only visualize a cable robot given some data from simulation.

\Citeauthor{Benedettini.1995} present a continuum model based approach to oscillation of a cable in plane~$\parentheses{x,y}$ and out of plane - thus having three vibratory axes~$\parentheses{u, v, w}$ which is induced by external forces working on the cable or by support motion~\autocite{Benedettini.1995}.
As desired by their model derivation, consideration of arbitrary support motion is given as well as a Galerkin approach separating time and space of the solution.
The authors perform rigorous analytical perturbation analysis of the resulted model as well as steady state motions and stability analysis.
This model is very advanced however does not allow for easy incorporation of time-varying cable length.
Its complexity may be too big for a full \caro system, however, though possible, a simplification of the equations is not that trivial.

A very interesting approach to modeling a discrete elastic beam is given by~\citeauthor{Bergou.2008}~\autocite{Bergou.2008}.
The model is based on a largely involved approach to describe the overall cable/beam shape by assigning a frame\textemdash the Bishop frame\textemdash to the centerline of the cable.
While this is too advanced for cable robots~(we do not expect any torsion or twisting) it gives the most accurate results as twisting and bending can be most easily described using this frame.
However, the model lacks in support linear elasticity \textIe stretching of the cable/beam but only considers bending and twisting energy of the cable/beam.
Solving the equations of motion is performed using the Fast Projection algorithm which leads to intrinsic energy dissipation and as such is not suitable for energy consistent/preserving integration.
This work seems quite an overkill for cable robots as it models the cables very precisely~(though neglects any material characteristics ranging from the interaction of fibers\textemdash in fact, a solid body is assumed) but neglects elasticity which clearly can be shown to be non-negligible for the application of \caros.

\Citeauthor{Bulin.2018} present different modeling approaches to cables:
%
\begin{inparaenum}
  \item force representation, as we know it from~\citeauthor{Miermeister.2010b},
  \item point-mass interconnected by springs where the resulting force is as given in above point, and
  \item an absolute nodal coordinate formulation~(ANCF) which is much like the method presented in this thesis.
\end{inparaenum}
%
~\autocite{Bulin.2018}
The authors nicely present the energy formulations of the cable and investigate the behavior of a fiber cable~(in a mass-fiber-cable-pulley setup).
ANCF lies closest to measurements.
\Citeauthor{Bulin.2018} nicely show a very good summary of my thesis...what the heck? However, they are only considering fixed-length cables \textIe without coiling/uncoiling and also do not consider redundantly restrained robots.

\Citeauthor{Choo.1973} present an overview of analytical approaches to modeling cable-based systems~\autocite{Choo.1973}.
They state conventional approaches to describing the tension~(linear, Hooke's law) and how to get finite elements of the cables for turning the PDEs into manageable ODEs.
Merits an demerits of the numerical approaches are presented such as e.g, the stability condition.
Based on ultimate values of the properties of cables, maximum values of the wave speeds have been evaluated even earlier, from which one can conclude that the longitudinal wave speed is more than ten times the transversal wave speed and that these speeds are independent of the cable diameter.
\Citeauthor{Choo.1973} present really a good overview of different approaches to modeling cables~(lumped mass, lumped elastic mass, rigid bodies, linearization) with their merits and demerits.
Even though this paper is from 1973 and computational power was limited then it still is valid today as most of the challenges have not changed much despite increased computational power.

The application covered by~\citeauthor{Crellin.1997} is the change of length of tethering cables between two or more satellites on space orbit~\autocite{Crellin.1997}.
This is delicate because the change of mass due to deploying the cable causes so-called rocket terms\textemdash a continuous sequence of plastic impacts.
The authors present a new approach to modeling this behavior as a time-independent description for the motion of the cable of the satellites is used.
Momentum balance equations are used to derive the equations of motion of a single body deploying/retrieving a single *inextensible* cable.
To account for the change of the system's energy~(the retrieval/deployment makes the system non-conservative) Carnot energies are introduced.
This paper is quite an interesting read, but maybe not even that applicable to \caros.

Starting off of Irvine's catenary model, \citeauthor{Dan.2014} derive transverse vibration of a cable~(in 2D) due to transverse forces~\autocite{Dan.2014}.
The cable is divided into various segments each with a unique transverse force for which the dynamics stiffness method is used to establish equations of motion.
Transverse forces on the cable are modeled as dampers.
Free vibration of the two segments is given through beam theory~(I guess Euler-Bernoulli beam).
Since the cable is acted upon with a transverse force anywhere but the proximal or distal point, this model may be useful for simulation of the changing dynamics of cables under contact with the environment or collision with other cables of the \caro.
However, for the case of my thesis, this model is over the top yet provides useful insight and basics into the dimensionless modeling of cables.

\Citeauthor{Duan.2010} assume an ideal extensible and uniformly distributed mass cable without torsional deformation, elongation is approximated through linear stress-strain relationship~\autocite{Duan.2010}.
Similar to~\citeauthor{Du.2012} and~\citeauthor{Du.2012b}, the authors derive the equations of motion from a steady state applying Newton's laws of motion - for a 3D cable~\autocite{Du.2012,Du.2012b}.
There is no consideration of bending or torsional moments [probably also in~\autocite{Du.2012} and~\citeauthor{Du.2012b}, but not explicitly stated therein], yet~\citeauthor{Duan.2010} consider aerodynamic drag forces \textIe wind velocity and air density.
Equation 3.5 presents nicely the relation between the actual cable length and the winding velocity of the cable.
This model contains a few good components suitable to the modeling of \caro cables, however, it still is based on a spatial discretization of the cable model.
It can be seen as a good start that the cable length is determined through the winding velocity of the pulley~(considering the current strain), however, this may be very difficult to consider perfectly fine in the modeling as there also is a change in length of each segment introducing mass/energy at the wrong points.

\Citeauthor{Du.2015} use an energy-based formulation for the spatial cable dynamics explicitly considering strain and winding in their formulation~\autocite{Du.2015}.
Their spatial cable model is derived using the principle of variation of action functionals and then later discretized using Rayleigh-Ritz approach with scaled polynomial functions.
Numerical results are obtained using the conventional \gls{ODE} integrator \texttt{ode45}, with handling of the multibody system's constraint violation through \gls{BSM}, which in itself may affect the system dynamics.
Despite numerical issues of instability arising with an integration step size of $\stepsize = \SI{2e-4}{\second}$, their results well depict spatial cable motion, though it remains undamped due to consideration of only elastic deformations.

\Citeauthor{GarciaFernandez.2008} present a great overview of modeling of cables for different applications~\autocite{GarciaFernandez.2008}.
Worth mentioning is the observation that many cable models are not suited for high tension applications~(as in the case of lifting cranes or \caros).
Conventional approaches usually lead to highly stiff ODEs/DAEs which are difficult to handle, even in an implicit formulation.
\Citeauthor{GarciaFernandez.2008} cable model consists of a cable guided along multiple pulleys for which even slackness~(\textIe no tension under compression) is modeled.
The guiding pulleys, interestingly, are considered ideal \textIe massless and frictionless such that tension on either side is the same.
The Euclidean distance between two of these pulleys is used to determine the strained cable length of the system while the initially given cable length is used as base [this approach does not account for cable length on the pulley, and it does not really account for different tensions along the cable\textemdash which would not appear as the pulleys are assumed frictionless.
These two facts actually limit direct application to \caros - especially the fact of considering friction less pulleys, \textCf~\citeauthor{Kraus.2015}.
This model covers the main needs of real-time computer graphics simulations; low computational cost and the capability of performing interactive simulations in complex simulation environments
Cable oscillation is incorporated by empowering the linear wave equation of constant tension with variable boundary conditions on the left and right side.
The equation is then transformed into an the space of~$\interval{0}{1}$ and superimposed to the horizontal line between two pulleys.
For simulation, cable tension is not accounted for explicitly but at every simulation time step such that before the deflection are determined, the tension is determined according to a one-sided Kelvin--Voigt model.
Since~\citeauthor{GarciaFernandez.2008} use the continuous wave equation, they need to perform numerical discretization of the cable segments between pulleys.
While this model can be used for 3D simulations as well, it suffers from an explicit consideration of cable stress induced by strain due to bending or motion of the endpoints.
Still an interesting approach but with probably quite some simplifications that cannot be assumed for \caros right away \textEg frictionless pulleys or simple determination of the cable tension~(see Burgers material).
This limits its applicability to physical simulation of cable robots especially since the cable tension is of explicit interest and controlled by means of cable length changes.

\Citeauthor{Hadap.2001} focus on modeling dynamics of hair as a continuum.
While they do model a hair strand itself as a 3-DOF mRFEM kinematic chain, they then consider the whole system dynamics based on a fluid hair model and an Eulerian viewpoint for the fluid dynamics~\autocite{Hadap.2001}.
This may be applicable to hair as the authors also state that a single hair strand may behave like a wire/rope/string, however, hair in its totality does behave more like a fluid which is flowing through the air.
Their numerical model is based on smoothed particle hydrodynamics~(SPH).
\Citeauthor{Hadap.2001} present three different results where the hair strands and the ``hair fluid'' behave realistically and provide for numerically efficient simulation as the ``hair fluid'''s dynamics can be calculated much more efficiently than calculating the dynamics of all the hair strains inside individually.
An interesting approach to modeling hair showing that even small scale hair with high tensile strength compared to its bending and torsion rigidity.
However, then the modeling approach becomes inapplicable to \caros as the authors use a formulation of smoothed particle hydrodynamics and consider all hair together as a fluid describing the overall dynamics of a large set of hair strands.

\Citeauthor{Harsch.2016} presents another approach to modeling of flexible beams and string-wires~\autocite{Harsch.2016}.
Based on a continuum mechanics and a spatial discretization of the cable~(needed for proper numerical integration for which they use B-splines as Ansatzfunctions for segments of the beam/cable) the equations of motion are derived from the principle of virtual work and under consideration of mechanical material equations~(for stretch and bend).
Using B-splines as Ansatzfunction provides advantages in the limited complexity or number of \glspl{DOF} of the system needed for proper~(\textIe physically correct) numerical simulation.
It only needs cubic B-splines \textIe of degree~$\polydeg = 3$ to properly approximate the shape of the beam/cable.
The beam/cable is discretized into a set of finite elements for which the chosen B-spline Ansatzfunctions are then used.
As the principle of virtual work is providing for unsolvable integrals, Gaussian-Quadrature is employed to find a closed-form formulation for the EqOM.
Further beam characteristics are then introduced with use of the nonlinear Bernoulli-Euler beam.
\Citeauthor{Harsch.2016} presents promising results on the modeling of beams and cables using a very low-dimensional approach based on Bubnov-Galerkin discretization and B-spline shape function.
The model formulation provides for easy integration of uniform and nonuniform payloads on the cable which would render it applicable to simulating coiling/uncoiling of the cable itself.
While the numerical complexity is not too bad, the cable model still needs validation and its interaction with \textEg a \caro platform is not investigated~(\textIe as a stiff cable in a DAE system).

\Citeauthor{Howell.1992}'s thesis is concerned with modeling dynamics of cables of low-tension~\autocite{Howell.1992}.
This class of cables is extremely interesting and useful as the material characteristics of low-tension profiles are largely nonlinear.
\Citeauthor{Howell.1992}'s model is starting off with a incremental cable segment with stretching and bending forces and moments, respectively, upon which the dynamics are then derived and subsequently simplified~(as such they are e.g., neglecting rotational inertia terms because the ratio of these to the bending stiffness).
Since the author is dealing with applications of cables in water, he's also introducing hydrodynamic forces.
For evaluation of the model, numerical schemes are employed and explicit and implicit formulations are compared~(for ones with and without bending stiffness).
\Citeauthor{Howell.1992} wants to assess low-tension cables which will be subject to a velocity at one end.
As such, he is interested in finding out the dynamics of a tension step response~(for the case of a line-shape cable, a circular-shape cable, and other shapes).
For experimental assessment, \citeauthor{Howell.1992} performs analysis response of a hanging chain.
In fact, the chain is hanging down, the distal end is tension free, and the proximal end is then moved a certain distance.
Resulting motion of the cable is investigated and the ``whip effect'' is captured.
Lastly, \citeauthor{Howell.1992} extends to incorporate cable elasticity which he analyzes analytically and numerically.
The author presents a great thesis on modeling dynamics of low-tension cables for underwater applications.
The derivation of the model is done in 3D and under consideration of most of the important mechanical properties and forces/torques.
However, again, the cable length is not variable nor is the model well applicable to \caros as the distal end is assumed to be at low-tension.

\Citeauthor{Humer.2011} present a very advanced beam model built with kinematics of the Bernoulli-Euler beam theory~\autocite{Humer.2011}.
Then, using Reissner, the virtual work of the internal beam forces are derived~(again the beam is assumed of solid cross-section).
St.\@ Venant-Kirchhoff law is applied despite allowing large compressive strain by finite stresses, however, it is more suitable in cases where the Green strain tensor is small but deformation gradients are large.
Virtual work by internal forces and by external forces~(of uniform loads) is balanced.
Lastly, \citeauthor{Humer.2011} introduce an unknown length of the beam of the reference configuration.
To numerically solve the problem, a finite element formulation~(absolute nodal coordinate formulation) is used and 6 shape functions~(as fifth-order polynomials) are given.
Finally, the results of the geometrically exact relations presented above are compared to those obtained from a beam theory of lower order.
This paper presents a solution to purely static problem.
While the modeling approach is very interesting and shows the range of validity of the Bernoulli-Euler beam and Reissner beam, it may not be applicable to \caros as such because the boundary conditions are quite different~(for \caros, the beam~(in our case cable) is not clamped at either end\textemdash while it can slide on the distal end\textemdash and as such the virtual work by external forces and the shape functions need to be selected accordingly.
Then, the validity of the model and its assumptions remains to be answered).

\Citeauthor{Ibrahimbegovic.1995} aims to present a 3-dimensional beam element whose reference axes can be arbitrary space-curved lines based on Reissner's three-dimensional finite-strain beam theory~\autocite{Ibrahimbegovic.1995}.
The equations of motion are derived based on the model formulation assumptions and provide a detailed continuum mechanics model.
Which is then discretized using Lagrange-type polynomials; virtual rotations are adopted for by isoparametric interpolation.
Numerical examples are presented 1) unrolling of a cantilever beam, 2) post-buckling analysis of a circular arch, 3) three-dimensional analysis of curved cantilever under free-end forces.
All results present reasonable improvement of the proposed model to existing models, and show improved accuracy for predicting the final displacement values.
This model, again, is an overkill for \caros~(also, it again assumes fixed length cables and transfer for variable length isn't trivial) as it is also geared toward another use case.
However, it shows how continuum mechanics cables can be modeled and how shape functions~(here, based on Lagrange-type polynomials, is introduced.

\Citeauthor{Irvine.1974}'s infamous book in which he derives the statics and dynamics of simple cable systems~\autocite{Irvine.1974}.
This is where the infamous Irvine catenary equation emerges from for both a free cable as well as a cable with point load(s) on it.
The static elastic and inelastic catenary are derived as well as the linear theory of free vibrations of a parabolic cable.
Ultimately, \citeauthor{Irvine.1974} arrives at a \gls{PDE} for the motion.
As far as I can tell, the dynamics of simple cable systems is merely based on the vibrational case of the cable \textIe for changing loads or external impact on the load.
If, however, there is any proximal or distal point motion, then these results are only partially valid \textIe it is assumed that gravity acts perpendicular to the spanning line of proximal and distal cable point.

The paper by~\citeauthor{Ju.2005} derives a so-called ``super element'' to model a continuous cable guided through multiple pulleys~\autocite{Ju.2005}.
Such a super element consists of many sub-elements~(one cable between two points \textIe either between the winch/the payload and a pulley or between two pulleys).
Using some trigonometric properties of the system, the stiffness matrix of one sub-element is derived which yields information on the cable elongation and the cable passage~(the length of cable that ``slips'' over the pulley to equilibrate tension over the whole cable) given the forces on either side of the cable~(or the displacement of each pulley, if this were a \gls{DOF}).
Collecting the stiffness matrix of each sub-element renders the global super element stiffness matrix.
Using this, the tension distribution between pulleys can be determined as well as the cable passages equilibrating the tension over the continuous cable.
Three verification studies are shown.
The first, very simple study uses one super element composed of two pulleys and thus three sub-elements.
A force p pulls on the right end of the cable, the left end is fixed to a nonmoving object.
All cable passages and tensions can be calculated from solving the stiffness equations.
The other two results focus more on real-world scenarios: one luffing crane and one pulley spreader beam.
Showing the importance of incorporating friction between pulley and cable with this result it can be seen that tension does not distribute uniformly over the cable.
The interesting approach in this paper, leading to a super element for a cable passing through multiple pulleys~(see also Garcia-Fernandez.2007), makes the whole cable model independent of any spatial discretization between the pulleys.
This is nice to have because the whole statics can be solved without consideration of any discretization techniques.
Numerical results for real applications show that the effect of friction of pulleys on the cable tensions is significant and the friction-free and fixed models both give unrealistic and incorrect results in cable tensions in some cases.

\Citeauthor{Lacarbonara.2008} propose a geometrically exact formulation of nonlinear cables with flexural stiffness \textIe undergoing elongation and bending~\autocite{Lacarbonara.2008}.
Their model is based on a geometrically exact formulation of the beam based on a undeformed system that is then stressed into a certain configuration.
Then, the local form of balance of the linear and angular momentum can be written and extended to cover for the whole cable.
Then, the static configuration is deformed to the current configuration which introduces the flexural curvature of the system.
Even though their use of the constitutive law of the material does not require a linear viscoelastic behavior, Lacarbonara introduce a linear stress-strain field.
Their complete model is analyzed with respect to the linearization and vibration eigenvalue problems for which a dimension-less model is first derived \textIe normalized to its mechanical properties like length or diameter.
\Citeauthor{Lacarbonara.2008} use finite-difference to path-follow the nonlinear static solutions, namely a five-point scheme on an equidistant grid.
Their numerical simulations were compared against an implementation in COMSOL with \num{7680} quadratic Lagrangian finite elements and a tolerance of \num{1e-6}.
\Citeauthor{Lacarbonara.2008}'s model shows reasonable results and a close match to the COMSOL solution such that it seems applicable to the \caros at first sight.
However, problems arise from the numerical complexity and size of system with respect to quick real-time simulations.
The model is after all a finite-difference model and as such has a large number of states.

\Citeauthor{Ni.2002} present a dynamics formulation for a 3D cable under consideration of only elasticity and perfect flexibility~\autocite{Ni.2002}.
Their strain measure is the Lagrangian strain measure in 3D which they plug in to Hamilton's principle of virtual work to obtain the equations of motion.
Then, shape functions in natural path coordinate~$\xi$ are plugged in.
A formulation for a cable with flexural rigidity is also given providing only for another stiffness matrix.
The cable model then undergoes parametric studies and is finally used for simulation of two stay-cable bridges.
The results show that the cable bending stiffness contributes a considerable influence on the natural frequencies when the tension force is relatively small, and affects the higher-mode frequencies more significantly than the lower-mode frequencies

\Citeauthor{Ozdemir.1979} presents a segmentized cable model derived via virtual work principles employing the~(second) Piola-Kirchhoff stress and Green-Lagrange strain~\autocite{Ozdemir.1979}.
This nonlinear model is then linearized with respect to time, but it is necessary to resort to an iterative scheme due to nonlinear displacements.
For obtaining the spatial dynamics, the Lagrangian interpolation functions are chosen to define the absolute nodal coordinate of each node.
The model presented is based on the assumption of being governed by one-dimensional normal stress.
This is an approach similar to a combination of the general lumped-mass or modified rigid finite element method and the Euler-Bernoulli-beam-based cable model\textemdash except for the use of Lagrangian interpolation as assumed mode.

\Citeauthor{RamanNair.2003} investigate the 3-dimensional dynamics of a cable modeled as a lumped-mass cable with linear viscoelastic elements between nodes to model elasticity, and angular viscoelastic elements at the nodes to model bending stiffness~(no consideration of torsional or shear deformations)~\autocite{RamanNair.2003}.
\Citeauthor{RamanNair.2003} also introduce buoyancy, touchdown, viscous drag, hydrodynamic pressure forces, and vortex-induced lift forces  into the model.
Since the riser is hollow, it can have an internal flow which must also be modeled.
Lastly, the equations of motion are obtained for~$2 \, m$ nonlinear DAEs which is solved with a Runge-Kutta based integrator.
