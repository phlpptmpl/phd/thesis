%!TEX root=../../../thesis.tex
%!TEX file=content/introduction/literature-overview/stress-strain-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection[Stress-Strain Dynamics]{Stress-Strain Dynamics of Polyethylene Fibers}

\Citeauthor{Damerau.2008} presents fractional derivatives applied to model the stress-strain response of mechanical material laws~\autocite{Damerau.2008}.
With this formulation, memory effects can be better described as this is inherent to the fractional derivative.
On the base of Timoshenko and Bernoulli beam theory, Damerau shows that easily manageable material models relate well to continuum mechanics material laws as shown through their relaxation, memory and loss moduli, hysteresis and phase velocity.
However, the thesis does only consider fractional material laws for beams \textIe objects with relatively high linear stiffness.
It remains unclear if a formulation using fractional derivatives is applicable to the more nonlinear materials used in polyethylene cables such as Dyneema.

\Citeauthor{Kim.1999} apply the same techniques used to describe dynamics of cables in \caros and other cable-driven systems\textemdash that is the lumped-mass approach\textemdash to determine creepage of a wire~(\textIe an electrical ducting) under the influence of cars driving over it~\autocite{Kim.1999}.
Creepage is occurring because the cable conduit behaves differently under transversal displacement than does the inner cable layer.
It may be concluded that cable creepage can be explained exactly by their newly proposed kinematic mechanism and the force generation mechanism of cable creepage is finally clarified.

\Citeauthor{Miermeister.2015} focus on tackling the material dynamics of \caro cables~\autocite{Miermeister.2015}.
They state that experimental data shows hysteretic behavior of the cables dependent on the frequency and amplitude of actuation, the tension level, as well as the pulley wrapping angle.
As such, \citeauthor{Miermeister.2015} investigate the stress-strain response of a cable and postulate a numerical formulation based on a polynomial with an added velocity and frequency dependent term.
Parameter estimation is performed yielding the four parameters of the model.
Further, \citeauthor{Miermeister.2015} show that the such parametrized \textIe, constantly parameterized model, is only valid for a small range of deflection as well as for only some points in the workspace.
The latter is assumed due to the change in wrapping angle of the cable around its guiding pulley introducing further friction.
This cable elasticity model is purely based on experimental observation and data fitting which I find hard to postulate as a ``good model''.
If the model can be stated as a parametric model of the geometric parameters, then it's useful.
Otherwise, more mechanically-based models should be used and validated without other factors such as pulleys~(or the direction of cable guiding, throwing in gravity effects).

\Citeauthor{Miyasaka.2015} deal with thin steel cables which are guided over pulleys in a surgical robot system~\autocite{Miyasaka.2015}.
Of course, pulley friction appears which is matter of identification in order to compensate for it.
The authors generally model their friction with Coulomb, viscous, stiction, and Stribeck terms.
Consequently, a model for~$n$ pulleys, which are differently wrapped with the cable, is obtained.
Two experimental setups were performed:
%
\begin{inparaenum}[1)]
  \item gravity driven, and
  \item motor driven.
\end{inparaenum}
%
These experiments show velocity independence of the friction, however, friction does depend on temperature T, angle of wrap theta, and number of pulleys.
A model for that was given.
This is a nice paper showing how pulley friction can be estimated using different, physically meaningful friction models.

Later, \citeauthor{Miyasaka.2016} present a Bouc-Wen hysLteresis model for two different diameter cables~(\SI{0.61}{\milli\meter}, and \SI{1.19}{\milli\meter}) for use in surgical robot RAVEN2~\autocite{Miyasaka.2016}.
The cyclic loading test of the cable is used to precondition the cables such that their stress-strain dynamics are dynamically converged to their physical correctness.
Since the new model is derived from the Bouc-Wen model, it must be derivable from the Duhem operator possessing rate independence and memory effect.
\Citeauthor{Miyasaka.2016} perform parameter identification using a genetic algorithm.
Since the hysteretic model does not directly match the experimental data, a linear damper is introduced which visibly reduces the error between measurement and simulation.
Even though the linear damping coefficients were only proved to work for the length of the cable tested in the experiments, the coefficients can be generalizable by performing further experiments and finding relationships between the coefficient and the cable geometry.
A unique feature of the hysteresis model is that it captures energy dissipation that is independent of the cable stretch rate.
The hysteresis model, together with a linear damper demonstrated high correlation to experimentally obtained data in terms of vibration frequency, steady state stretch, and logarithmic decrement.
The attenuation of amplitude with time was captured better compared to non-hysteretic model.

\Citeauthor{Piao.2018} determine a creep model for a polymer cable based on Burgers model which, according to the authors, does not express long term creep behavior~\autocite{Piao.2018}.
To capture for that, the authors propose adding another Kelvin--Voigt model to the Burgers material model.
Then, the transient response of the system is used to determine the strain as a function of time and stress step.
Following the same way, the authors also provide an equation for the strain recovery.
The model parameters~(elasticity and viscosity) are then expressed as function of mass and cable length for which~\citeauthor{Piao.2018} then perform fitting of a polynomial of degree~$\polydeg = 2$ in mass and length.
This work is nice, has nice figures, but it is only concerned with the stress-step response of a cable, not with its dynamics response.
According to~\citeauthor{Tempel.2018}, a Burger material model already well captures the dynamics if parameters are estimated well~\autocite{Tempel.2018}.
\Citeauthor{Piao.2017} paper takes~\autocite{Piao.2018} a step further and presents the promised open loop cable tension position controller for cable length compensation, which is just a feedforward control~\autocite{Piao.2017}.
The feedforward term is determined based on the time-based function for the cable creep at a certain point in time of stopping.
The experimental results show that the payload-induced cable elongation can be compensated for by the controller and trajectory deviation becomes smaller.
No control theoretical system analysis of the controller is given limiting trust in it because its bandwidth is unknown.

\Citeauthor{Schmidt.2015b} are concerned with the effects of cable force on winch winding accuracy~\autocite{Schmidt.2015b}.
As cable tension changes the cable cross-section, the effective winch radius changes resulting in altered torque-force transmission.
Other effects are uneven helix, rope elongation during winding and drive train accuracy.
While many of these phenomena affect the cable tension, ovalization is one of the more dominant effects.
However, with all of the phenomena of elastic cables it is difficult to obtain accurate winding dynamics and to improve terminal accuracy.
