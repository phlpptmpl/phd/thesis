%!TEX root=../../../thesis.tex
%!TEX file=content/introduction/literature-overview/cdpr-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dynamics Modeling of Cable Robots}

\Citeauthor{AyalaCuevas.2018} present a cable robot cable model based on the Rayleigh-Ritz Assumed-Mode formulation~\autocite{AyalaCuevas.2018}.
The model is derived without any mechanical properties of the cable \textIe without elasticity or flexibility as such, however, the cable may bend \textIe it is considered an inextensible ideal beam.
The selection of assumed modes is very limited as it only considers polynomials~(in the example up to degree~$\polydeg = 2$) which is known to be a very limited approximation of the cable shape itself~(see also~\textcite{Gouttefarde.2012}).
The results are shown for a planar 2T 3C cable robot where vibration of the cables can be seen to clearly affect the dynamics.
However, since no damping is introduced into the cable model, the transversal vibrations are rather large.
\Citeauthor{AyalaCuevas.2018}'s model is formulated as a DAE with constraints on the position and velocity level where the velocity level is given through the Jacobian, and, conversely to well-known practice, the DAE is solved symbolically for the mass matrix such that the accelerations and Lagrange multipliers are obtained directly.
It is unclear why no constraint violation handling method is employed as is know that such systems tend to drift over time.
Simulation of the system is performed with a PD control law for the platform position on the kinetic model for the velocities and compared for different cable densities showing more vibration for higher densities.
\Citeauthor{AyalaCuevas.2018} are planning to account for cable elongation and incorporate everything into a 3D setup.
This approach is generally interesting, however, very basic as there is no consideration of elasticity or of explicit flexibility.
The flexibility is converted geometrically to a horizontal deflection which makes the system very stiff~(a stiff flexible slender rod is assumed).

\Citeauthor{Bedoustani.2008} intends to study kinematics and dynamics analysis of \caros in detail while cables are assumed lumped-masses \textIe one lumped-mass per cable and cable elasticity is a single spring-dashpot on one side of the cable~\autocite{Bedoustani.2008}.
Apparently, due to the implicit nature of the dynamics, Runge-Kutta methods cannot be used to solve this problem.
Two models are derived by~\citeauthor{Bedoustani.2008}:
%
\begin{inparaenum}
  \item being a inelastic cable modeling approach where there is a consideration of mass flow into/out of the system due to coiling/uncoiling, and
  \item being the cable under consideration of elasticity by means of a spring-damper element.
\end{inparaenum}
%
The authors show that time-varying mass should be considered, but also that cable elasticity has a non-negligible effect on the terminal accuracy of the platform.
The robot in use for simulative assessment was a \MP{1}{2} cable robot with a circular platform shape and~$2 \times 2$ cables attached over cross; the cables have elasticity of~$\youngsmodulus = \SI{38}{\giga\pascal}$.
The effect of large delays due to elasticity in the model can be explained as a fast pole with high gain added to the system dynamics which is a high force in high frequencies that makes control of the system difficult.
This paper is of interest as it describes the \caro dynamics using preservation of momentum and describes how a~(planar) cable robot reacts under the consideration of changing cable mass which is important to consider for larger scale or larger unit density \caros.

Later, \citeauthor{Bedoustani.2011} point out that the consideration of change of mass in \caros has long been neglected due to small changes in effective mass~\autocite{Bedoustani.2011}.
However, as they correctly state, for larger scale systems such as the FAST this assumption no longer holds true.
As such, their \caro model explicitly incorporates the variation of mass.
\Citeauthor{Bedoustani.2011} rigorously derive the equations of motion of a \caro with time-varying mass due to coiling/uncoiling of the cable.
The derivation is given clearly, however, uses notation slightly different to today's notation convention on \caros.
For numerical evaluation of the procedure, \citeauthor{Bedoustani.2011} present a force distribution optimization procedure for the extended cable robot model.
The authors can visibly state that the variation of mass for the given cable robot configuration is apparent especially with regards to the vertical forces applied on the platform: it is nonlinear and strongly dependent on the geometric and mass parameters of the robot, and on the maneuvering trajectory.
However, the sagging effect of cables is not considered in this modeling which may introduce additional challenges.

\Citeauthor{Caverly.2014} and~\citeauthor{Caverly.2015} model a \caro as a lumped mass system with a point-mass platform~\autocite{Caverly.2014,Caverly.2015}.
The change in cable mass and winch inertia is considered through integrating it in the Lagrangian formulation of energies.
This contribution is very detailed in the description of the cable model and considering its time-varying material dynamics~(change in mass, inertia, ...).
This time-dependency is integrated into the very first segment of the cable - it remains unclear what happens if the wound length of the cable is just as much as the segment's length is, because then the mass becomes negative).
To assemble the full system, the ``nullspace method'' is employed which rearranges the generalized coordinates into a set of rigid coordinates and a set of elastic coordinates.
Then, they are constraining the two cables on the payload velocity level.
A simplification using massive payload assumption is introduced which states that during motion the kinetic energy consists of only the payload moving while at rest it is the kinetic energy of the flexible coordinates.
For the full system~(4 cables + 1 payload mass), a passivity theorem L2 control formulation is given based on the~$\mu$-tip rate respectively the~$\mu$-tip rate tracking.
All in all this is a very amazing paper and a good base to start off of stating the shortcomings of this work \textIe lumping cables isn't the best choice in terms of approximating real cable's natural frequencies \textVs the number of nodes/lumped masses needed.

\Citeauthor{Collard.2011} present dynamics modeling of a large \caro using Robotran software providing the equation of motion in a compact symbolic form using recursive algorithms~\autocite{Collard.2011}.
The authors model the mass transfer from the drum to the first cable segment as a supporting ball joint.
Three different verification experiments are run:
%
\begin{inparaenum}
  \item static analysis of the vertical uplift of the platform due to cable weight,
  \item lifting simulation over~$\SI{1}{\meter}$ of the platform, and
  \item modal analysis for determination of natural frequencies.
\end{inparaenum}
%
The number of segments has been chosen as a trade-off between these three results and the model complexity.
Finally, as application, the chosen model has been simulated to design a controller based on the aforementioned quasi-static model.
It has shown the importance of the model to highlight the disturbances due to the oscillations of the cables.
This paper is also a good starting point for my argumentation of the cable model since they point out that it is important to model the cable oscillation~(can also be seen from the data).
However, with the rigid finite element method this becomes challenging as the change of mass needs to be respected in a re-segmentation in case the first segment is completely wound.

\Citeauthor{Diao.2009} perform vibration analysis of a fully-constrained \caro with using the standard kinematics of a 6DOF \MP{3}{3} 7C \caro of carbon steel wires of diameter~$\SI{1}{\milli\meter}$~\autocite{Diao.2009}.
First, vibration analysis is only done for the conventional cable model where the cable forces are along the straight line of the platform-to-base connecting cable.
Then, transverse vibration of cables is introduced by assuming it is deflected within its linear elastic range, the conventional wave equation is imbedded.
The authors state ``that the effect of the transversal vibration of the cables on the vibration of the effector is so small that it can be ignored.''
In this paper, the finding is made that the transversal vibration of cables only contributes to~$\SI{2}{\percent}$ of vibration of the EE while~$\SI{98}{\percent}$ go to the longitudinal vibration).
This finding should be compared to findings for suspended cable robots.
The approach of adding the linear wave equation to the whole system for modeling its dynamic stiffness is not new but probably arises some limitations as the deflection may be small only for strongly taut cables.
Especially in the case of redundantly restrained cable robots, this may hold true, but for suspended ones there are no antagonistic pairs of cables as such transversal vibrations may impose much higher end-effector vibration.

\Citeauthor{Du.2012} first present the conventional Irvine catenary model for describing the static shape of the cable~\autocite{Du.2012}.
They state that the control output is chosen to be the cable length as a cable tension output is too difficult~(and expensive) to implement due to low amplitude vibration in the cable constantly changing the tension.
The authors derive a linear PD controller for the resultant cable wrench and show its Lyapunov stability.
This linear PD controller is then ``nonlinearized'' using the quasi-static state as reference signal and adding its controller output atop.
To perform numerical investigation, an FEM model is presented that is used in the static sense, too, \textIe the change in cable length is added to the whole cable and not just the first segment like others do.
Simulation and experimental results show effectiveness of the methods proposed.
Small vibrations of the EE remain throughout the experiments yet they decrease with decreased tracking velocity of the EE.
Vibration mainly results in orientation changes when it moves slowly.
This paper is useful as it shows how the FEM model is applicable~(and necessary) to simulation of large span \caros.
Also, it can be used for controller design and verification.
However, it isn't clear to me why they are deriving the static model~(Irvine catenary) when they are actually proposing using the FEM model for use...

In an extension to that, \citeauthor{Du.2012b} present a 3D catenary model based on Irvine's equations, however, they derive it slightly more mechanically consistent introducing the axial strain given through a position vector of a point on the cable~\autocite{Du.2012b}.
The derived PDE is then spatially discretized with into~$n$ elements.
Following this discretization, the spatial system dynamics of the discretized cable model are given~(typical mass spring damper approach) with absolute nodal coordinates of the cable nodes.
Simulative assessment is performed for a \MP{2} \num{2}C \caro with their proposed method and an ADAMS model~(in ADAMS, the distance between adjacent nodes must be less than the radius of the fixed pulley to enable smooth contacts between the cable and the pulley [still wonder how the modeled the pulley for their own model since they are not talking about any sort of contact mechanics modeling]).
\Citeauthor{Du.2012b} point out that for slowly changing systems~(like the actual physical FAST) the cable velocity is negligible and one obtains a reduced structural vibration problem with moving equilibrium position.
As such, only the first two or three vibration modes are needed to be taken into account.

An early contribution to modeling of cables for \caros is presented by~\citeauthor{Hajzman.2011}.
The different approaches include
%
\begin{inparaenum}
  \item massless cable,
  \item point-mass cable~(lumped mass, SDE between masses),
  \item finite segment cable,
  \item nonlinear finite 3D elements, or
  \item absolute nodal coordinate formulation.
\end{inparaenum}
%
A simple single DoF pendulum was used for numerical implementation and comparison of the different models~\autocite{Hajzman.2011}.
The main drawback of more complex models~(finite segment, nonlinear, ANCF) is a difficult simulation of wiring.
I generally am thinking that the lumped mass model is generally too bad for exact simulation as the cable's flexural rigidity is not considered.
Yes, this is quite a small value, but still such cable models may experience knots or alike while the real cable would not do that~(at least not that easily).

Based on Irvine's catenary model, \citeauthor{Kozak.2006} present the catenary model for both elastic and inelastic cables converted into \caro notation~\autocite{Kozak.2006}.
With these equations derived, the authors introduce the problem of solving the inverse kinematics problem which becomes a kinetostatic problem then.
The results are shown for the three different kinds of cable robots: underconstrained, minimally constrained, and over constrained \caros; highlighting different problems in solving the kinetostatic problem for inextensible and extensible cables.
Lastly, \citeauthor{Kozak.2006} present the cable-sag induced flexibility for the inextensible and extensible cable case, and solve the IK problem for two \caros~(planar and 6DOF).
This is a very fundamental research with respect to the Irvine's catenary model in application to \caros.
\Citeauthor{Kozak.2006} not only derive the equations in a \caro-suitable formulation, but also tackle stiffness and inverse kinematics analysis in this work.

\Citeauthor{Michelin.2015} present a different approach to modeling and simulating \caros~\autocite{Michelin.2015}.
Their contribution gives a CAD-based simulation environment for a \caro which considers the winches~(with winding dynamics of the cable), the pulleys, the cables, and the platform; all while accounting for collision detection as well.
What makes this whole simulation worthwhile is its high level of accuracy as every component can be described through a CAD model.
However, this merit is an inherent demerit of the simulation environment at the same time: the number of generalized coordinates of the system easily reaches tens of thousands~(despite a CAD-model simplification performed by~\citeauthor{Michelin.2015}).
For typical simulations with a \SI{100}{\hertz} oscillating cable, simulation has to be run at \SI{1000}{\hertz} sampling rate which leads to long computation times.
With physically realistic parameters for the cable and other components of a half-sized CoGiRo, the nodes count of each cable is around \num{500} and, with a sampling frequency of \SI{200}{\hertz}, the simulation of \SI{10}{\second} took about \SI{10}{\hour}.
This is by no means suitable for any sort of real-time simulation let alone for prototyping or design of \caros or for closed-loop controller design.

\Citeauthor{Miermeister.2010b} present a real-time capable simulation model of a \caro of arbitrary configuration~(as long as it is a \MP{3}{3} \caro)~\autocite{Miermeister.2010b}.
The mechatronic model of the \caro is derived with all components in mind and a very detailed modeling of the winches and drives.
It even includes a simplified model where there complex mechatronic system ``drum+winch'' is substituted by a PT2 transfer function.
The mobile platform dynamics is a free-floating rigid body on which forces are imposed.
These forces are calculated by considering the cables as linear viscoelastic Kelvin--Voigt materials.
Simulative analysis and experimental validation of the simulation model is performed for IPAnema 1 showing good matching of the platform pose, the drive states, and the generated torques and forces.

\Citeauthor{Nahon.1999} presents the dynamics modeling of a tethered aerostat based on a finite element discretization of the cables by a lumped-mass approach with absolute nodal coordinate formulation~\autocite{Nahon.1999}.
The tension in the cable is composed of an element of linear viscoelasticity.
Also, due to their application, air drag forces are introduced on the nodes.
The overall equation of motion are assembled for the set of cables and the payload.
\Citeauthor{Nahon.1999} incorporates a winch controller which adjusts the effective length of the first segment to control the overall length of the cable.
The authors approach is the basic one to simulate cable-driven manipulators with varying cable length.
It still feels a bit weird to me that one can just change the length of the first node as, at some point, all subsequent nodes should be penetrated through the ground, no?

\Citeauthor{Schenk.2016} compare two cable linearization approaches with regards to their accuracy of prediction of vibration characteristics~\autocite{Schenk.2016} .
The two models under investigation are a linearized PDE of the motion, and a finite element model.
For both models, the stiffness matrices are derived in terms of the approximating basis.
Experimental assessment is performed for a single fixed to the ground at the distal end and actuated by the winch on the other end.
A Vicon system is used to track the cable vibration.
Two experiments are run:
%
\begin{inparaenum}[1)]
  \item to investigate the decay of vibration, and
  \item to analyze the vibrations and how well the two models predict the natural frequencies.
\end{inparaenum}
%
\Citeauthor{Schenk.2016} provide phase plots of the cables for different tension levels that are then assessed.
The authors find out that some behavior might suggest the classic linear decoupled models might be inadequate to fully describe the behavior of cables in \caro.
The results predicted from the models seem to become more unreliable when the cable starts to sag, perhaps because the linear approximation of the system does not hold anymore.

\Citeauthor{Servin.2008} present a rigid body approach for real-time cable simulation~(at \SI{60}{\hertz}) with other requirements similar to those of \caros~\autocite{Servin.2008}.
The authors base their model on the ANCF of the RFEM model.
With the constraints, they obtain a DAE which they impose under a variable constraint enforcement equation\textemdash the constraint violation can be chosen to be between very stiff and very lose.
Since they want to run the model in real-time, they perform linearization of the equation of motion \textIe discretization in time using symplectic Euler discretization and Taylor expansion.
\Citeauthor{Servin.2008} extend their model to account for elastic cables using Cosserat theory and the Kirchhoff relation as constitutive model.
As such, they obtain elongation, bending, and torsion for which the potential energies can be formulated.
Constraints enforce all cable segments to be between their respective nodes.
As solver for the dynamics system, the authors choose a preconditioned Gauss-Seidel algorithm implemented by themselves.
\Citeauthor{Servin.2008} model employs an important angular constraint that is natural for handling deformation energies based on elastic theories.
As such, the model can handle large deformations such as twists larger than~$2 \, \pi$.
The developed methods meet the requirements for simulation of hoisting cables in VR.
Using MATLAB, the authors were able to achieve even higher ratios of load mass to cable mass at the same timestep using a direct solver.

\Citeauthor{Sohrabi.2016} present trajectory path planning for cable robots with hefty cables~\autocite{Sohrabi.2016}.
These are first derived with a continuum mechanics approach but then spatially discretized into nodes using the finite-difference method.
A change in cable length is achieved by allowing the total cable length to be a function of time~(this does introduce impulses along the cable and adds/removes mass/energy along the cable not at the proximal end).
The such derived numerical model is compared to an ADAMS model of a \MP{2} 2C planar cable robot with cable winding.
While their paper states they do trajectory path planning, no path planning algorithm is shown and only the dynamics modeling is justified by means of a \MP{2} 2C and a \MP{3}{3} 6C \caro.
