%!TEX root=../../thesis.tex
%!TEX file=content/introduction/parallel-mechanisms.tex

\section{Parallel Mechanisms}\label{sec:introduction:parallel-mechanisms}

Two distinct mechanism designs have to be considered when observing any kind of linked actuating manipulator:
%
\begin{inparaenum}[1)]
\item serial mechanism, and
\item parallel mechanisms\footnote{%
  One may include hybrid mechanisms in this list, as well, however, hybrid mechanisms make use of serial and parallel structures by \textEg mounting a serial chain on top of the end effector of a parallel mechanism.
  As such, this kind of configuration does not create a completely new and distinct mechanism as the two sub-mechanisms may as well be considered separately.
}.
\end{inparaenum}
%
Given their naming, it may seem obvious that serial mechanisms contain a series of actuating elements attached to one another.
These actuating elements may be active \textIe actuated, or passive \textIe unactuated, and be composed of basic mechanical elements such as prismatic joints, cylindrical joints, or revolute joints~(see~\cref{fig:introduction:parallel-mechanisms:different-joints}).
Ultimately, at the end of such a serial chain is the end effector or manipulator, which is used to grasp, move, or in other ways manipulate an object.
We define the configuration of the end effector\textemdash its position and, where applicable, its orientation\textemdash as pose.
A typical example of such a serial mechanism can be found fourfold on the human body: our arms and legs form serial chains connected to a common base\textemdash our torso\textemdash by means of which we can manipulate our surrounding.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \begin{subfigure}{0.30\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.25\textheight,%
      ]{}%Cutaway view of a revolute joint.}
    \caption{%
      Cutaway view of a revolute joint.
    }
    \label{fig:introduction:parallel-mechanisms:different-joints:revolute}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}{0.30\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.25\textheight,%
      ]{}%Cutaway view of a cylindrical joint.}
    \caption{%
      Cutaway view of a cylindrical joint.
    }
    \label{fig:introduction:parallel-mechanisms:different-joints:cylindrical}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}{0.30\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.25\textheight,%
      ]{}%Cutaway view of a prismatic joint.}
    \caption{%
      Cutaway view of a prismatic joint.
    }
    \label{fig:introduction:parallel-mechanisms:different-joints:prismatic}
  \end{subfigure}%
  \caption{
    Cutaway of different types of joints:~\subref{fig:introduction:parallel-mechanisms:different-joints:revolute} revolute joint,~\subref{fig:introduction:parallel-mechanisms:different-joints:cylindrical}, and~\subref{fig:introduction:parallel-mechanisms:different-joints:prismatic} joint.
  }
  \label{fig:introduction:parallel-mechanisms:different-joints}
\end{figure}

On the contrary are parallel mechanisms, composed of actuating elements that are connecting to the end effector in parallel.
To stick with the example from serial manipulators, if we crawl on all fours, our arms and legs are the actuating elements that are connected to our torso in parallel.
This also leads to our torso no longer being the base of the actuating elements but rather it turning into the end effector that we manipulate.
Given this example, one can already see one further characteristic of parallel mechanisms: the actuating chains are, themselves, serial chains of active and passive joints, yet only form a parallel mechanism through connecting with other serial chains on a common end effector.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \begin{subfigure}{0.49\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.20\textheight,%
      ]{Structural view of a serial manipulator.}
    \caption{%
      Structural view of a serial manipulator.
    }
    \label{fig:introduction:parallel-mechanisms:serial-vs-parallel:serial}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}{0.49\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.20\textheight,%
      ]{Structural view of a parallel manipulator.}
    \caption{%
      Structural view of a parallel manipulator.
    }
    \label{fig:introduction:parallel-mechanisms:serial-vs-parallel:parallel}
  \end{subfigure}%
  \caption{
    Structural views of a serial mechanism~\subref{fig:introduction:parallel-mechanisms:serial-vs-parallel:serial} and a parallel mechanism~\subref{fig:introduction:parallel-mechanisms:serial-vs-parallel:parallel}.
  }
  \label{fig:introduction:parallel-mechanisms:serial-vs-parallel}
\end{figure}

The reason two different types of mechanical mechanisms exist stems from their inherent characteristics.
Serial mechanisms boast with large workspaces as these are only limited by the number of chain elements and the length of each of these\textemdash at least in theory, due to singularities and other physically limiting factors.
In addition, due to their serial structure, serial mechanisms can reach poses in their workspace with different inputs on the axes\textemdash they may, for example, attain a position likewise from the left or right.
Such characteristics cannot be implemented with parallel robots.
The disadvantages of serial mechanisms, such as reduced accuracy and stiffness due to additive error propagation along the serial chain of joints, as well as a very low mass-to-payload ratio\textemdash for spherical manipulators with 6 \glspl{DOF} this ratio is less than~$\num{0.15}$ \textIe a \SI{750}{\kilo\gram} payload requires a \SI{5000}{\kilo\gram} or \SI{5}{\tonne} base~\autocite{Merlet.2006}--lead to development of parallel mechanisms.

With the introduction of parallel mechanisms~(see~\cref{fig:introduction:parallel-mechanisms:serial-vs-parallel:parallel} for a structural view), a mechanical device was found that drastically increased the payload-to-mass ratio since the payload is distributed over more than one actuating chain.
Further, parallel mechanisms show better accuracy when compared to serial manipulators\textemdash even though the quantitative measure thereof is still a topic of discern~\autocite{Briot.2007}.
However, due to their parallel nature, parallel mechanisms introduced a characteristic unknown to serial chains: collision between the actuating chains.
This architectural flaw can only be downsized by appropriate design, but it can never be fully remedied.
Stemming from this design characteristic is another demerit: a comparatively small workspace size.
Since all actuating chains are attached to the same end effector, it is impossible to enlarge the workspace of a parallel robot beyond its structural size\textemdash only serial robots can span farther than their footprint.
Reduced workspace size affects both the translational and rotational workspace\textemdash even more so to the disadvantage of the rotational workspace.
We can infer parallel actuated mechanisms have advantages over their serial counterparts, but also some drawbacks, the pros and cons of which have to be weighed up for the specific application.

A further distinction of robotic devices and manipulators may be made dividing these into fixed base and mobile base robots.
Fixed base robotic manipulators are, as the name implies, fixed in the spatial position of their base, implying that they can only reach so far away from the base as their actuators can extend.
Conversely, mobile base robots do not exhibit this limiting factor as their base is not fixed and thus can move around space\textemdash as a matter of fact for the most part only on surfaces\textemdash to reposition itself.
The other side of the coin for mobile robots is their reduced stability against slipping or tipping over.
It is an undeniable fact of any non ground-fixed mechanical system that its center of mass must not exceed its footprint area, otherwise tipping over occurs.
However, for the matter of this thesis, this distinction is not of concern as we are only interested in the actuating chain of \cdprs and as such do not distinguish between these systems being on a fixed base or a mobile base.
