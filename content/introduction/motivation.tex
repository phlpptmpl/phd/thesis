%!TEX root=../../thesis.tex
%!TEX file=content/introduction/motivation.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Cable-driven mechanisms\footnote{With synonyms \textit{wire}- or \textit{tendon}-driven or based mechanisms.} have been known to mankind for many millennia since cables were one of the first things men used in mooring and fastening objects.
There is evidence dating cables back to \num{17000} BC, while the Egyptians were probably the first to use cables as tools for erecting buildings like the Great Pyramid of Giza.
While the materials used in manufacturing cables have changed from water reeds, grass, leather, and animal hair to more modern materials like steel or even synthetic fibers like Aramid/Kevlar or Dyneema~(similar to Aramid, yet made of so-called \gls{UHMWPE} fiber), their basic usage principle has not changed much: On the one end, a cable is attached to a spooling device, most usually a rotating drum, on the other end, the cable is attached to an object that is to be manipulated.
By changing the cable's length, the object can be moved; however, care needs to be taken as cables can only exert tensile forces \textIe they can only pull on whatever they are connected to, resulting in a unilateral motion of the manipulated object.
Adding a second cable to the opposite side moving antagonistic to the first cable, we are now able to move the object in one dimension.
Adding more and more cables, ideally always in antagonistic pairs of two, we can increase the number of controllable \glspl{DOF} from one to up to six.
Then, we are able to position an object in space in virtually arbitrary orientation and position since we control all six \glspl{DOF}\textemdash three translational and three rotational.
By adding a control system to the equation and automating synchronized motion of the winches, we obtain a cable-driven mechanism.

\begin{table}[tbp]
  \centering
  \smaller[1]
  \caption[%
    Yield strengths and unit densities of different materials.
  ]{%
    Yield strengths and unit densities of different materials.
    Yield strength refer to a single fiber or strand of the material.
  }
  \label{tbl:introduction:cablematerial-comparisons}
  \input{tables/material-properties-comparison}
\end{table}

The most well-known representatives of cable-driven mechanisms are probably cranes and the so-called Skycam~\autocite{Cone.1985} and its competing products CableCam and Spidercam.
All systems use four cables guided from a winch over a series of pulleys toward the camera cage attached to the distal end.
By changing cable lengths, the camera moves over the field of soccer, football, or baseball stadia giving the audience a bird's-eye view of the happenings on the field.
Such perspectives cannot be achieved using conventional boom lift camera cranes as their operational space is very limited due to limitations arising from structural instabilities.
Cable-driven systems like Skycam, on the other hand, make great use of the advantages of cable-driven systems: reduced inertia stemming from having to move comparatively lightweight cables\footnote{Even though cables may have densities of several thousand kilogram per cubic meter~(\textCf \cref{tbl:introduction:cablematerial-comparisons}), comparing their weight for a given length, it is much lower than an equivalent beam structure.} allows for high dynamics with cables wound and unwound quickly on rigidly attached winches.
In addition, the yield strength of cables \textIe the maximum tension before breakage, is very high compared to their density allowing for huge workspaces~(see~\cref{tbl:introduction:cablematerial-comparisons} for a comparison of different material yield strengths and unit densities).

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.32\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=1.00\linewidth,%
        height=1.00\linewidth,%
        keepaspectratio,%
      ]{robot-industrial}
    \caption{%
      Industrial robot, the most common representative of serial mechanisms.%
    }
    \label{fig:introduction:mechanisms:serial-parallel:industry-robot}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.32\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=1.00\linewidth,%
        height=1.00\linewidth,%
        keepaspectratio,%
      ]{robot-hexa}
    \caption{%
      Parallel mechanism in the design of a Gough-Stewart platform.
    }
    \label{fig:introduction:mechanisms:serial-parallel:gough-stewart}
  \end{subfigure}
  \hfill%
  \begin{subfigure}[t]{0.32\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        width=1.00\linewidth,%
        height=1.00\linewidth,%
        keepaspectratio,%
      ]{robot-delta}
    \caption{%
      Parallel mechanism in the design of a Delta robot.
    }
    \label{fig:introduction:mechanisms:serial-parallel:delta}
  \end{subfigure}
  \caption[%
    Graphical representation of serial and parallel mechanisms.
  ]{%
    Graphical representation of different designs of serial mechanisms~\subref{fig:introduction:mechanisms:serial-parallel:industry-robot} and parallel mechanisms~\subref{fig:introduction:mechanisms:serial-parallel:gough-stewart} and~\subref{fig:introduction:mechanisms:serial-parallel:delta}.
  }
  \label{fig:introduction:mechanisms:serial-parallel}
\end{figure}

When using cable-driven mechanisms, they must not always be set up like cranes.
Two distinct topological mechanism designs exist, namely
%
\begin{inparaenum}[1)]
  \item serial mechanisms, and
  \item parallel mechanisms.
\end{inparaenum}
%
A third design exists, so-called hybrid mechanisms, which combine serial mechanisms and parallel mechanisms.
However, their design is not distinctively different from the already mentioned ones as it is a mere combination thereof.

Serial mechanisms are composed from consecutively attaching actuated joints to one another which provides a final structure similar to the human arm.
Typical serial mechanisms are industrial robots as shown in~\cref{fig:introduction:mechanisms:serial-parallel:industry-robot}.
On the other hand, parallel mechanisms feature actuated joints connected to the same base and the same platform or end effector.
Typical parallel mechanisms are the Gough-Stewart platform \autocite{Stewart1965,Gough.1962} with actuated prismatic joints~(see~\cref{fig:introduction:mechanisms:serial-parallel:gough-stewart}) and the Delta robot with actuated revolute joints~(see~\cref{fig:introduction:mechanisms:serial-parallel:delta}).

Serial mechanisms generally achieve larger workspaces and higher dynamics, whereas stiffness and accuracy of parallel mechanisms renders them superior\textemdash even though the quantitative measure of accuracy is still a topic of discern~\autocite{Briot.2007}.
In principle, however, both serial and parallel designs can be equipped with cables providing cable-driven serial and cable-driven parallel manipulators, respectively.
However, since cables can only exert tensile forces, any serial chain implemented using cables requires two thereof to be a homologous replacement.
This necessity can be exemplified on the human body where flexing of the lower arm is performed by an antagonistic pair of muscles namely the biceps brachii muscle and triceps brachii muscle.
