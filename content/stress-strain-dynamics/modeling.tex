%!TEX root=../../thesis.tex
%!TEX file=content/stress-strain-dynamics/modeling.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Modeling of Fiber Cables}\label{sec:stress-strain:modeling}

While at its microscopic core consisting of hundreds of fibers laid next to each other and woven into strands, their precise interaction is unknown and largely unavailable.
The overall large signal response of cables of such material can be described through combination of the two most simple mechanic components
%
\begin{inparaenum}[1)]
  \item linear spring~(a restorative force) and
  \item linear damper~(a damping force).
\end{inparaenum}
%
A suitable yet minimal choice of serial or parallel combination of these two elasticity and viscosity representing components can provide for dynamics models describing the stress-strain dynamics sufficiently well.
In literature, several well-established material models can readily be found which will be introduced after definition of the most important mechanical concepts of stress and strain.

Stress is a material quantity that expresses the internal forces of neighboring particles\footnote{As such, these measures are along all of the material's principal axes.
However, we will limit the use case to only one-dimensional stress along the material's longest axis.} of a continuous material exerting on each other, while strain is the measure of the~(stress--resulting) deformation of the material.
Assuming the material's mechanical elasticity to obey Hooke's law of a perfect solid, we obtain the relation of applied stress~$\stress*_{ \youngsmodulus }$ and resulting strain~$\strain*_{ \youngsmodulus }$
%
\begin{align*}
  \stress*_{ \youngsmodulus }
    & = \youngsmodulus \,
        \strain*_{ \youngsmodulus } \,
        ,
\end{align*}
%
where~$\youngsmodulus$ is elasticity, usually Young's modulus of elasticity.
Similarly, the material's mechanical viscosity can be described through the variation of the applied stress~$\stress*_{ \viscousmodulus }$ with the rate of change of strain~$\dotstrain*_{ \viscousmodulus }$ assuming Newton's law of a perfect liquid, reading
%
\begin{align*}
  \stress*_{ \viscousmodulus }
    & = \viscousmodulus \,
        \dotstrain*_{ \viscousmodulus } \,
        ,
\end{align*}
%
where~$\viscousmodulus$ is viscosity and~$\dotstrain*_{ \viscousmodulus } = \od{ \strain*_{ \viscousmodulus } }{ t }$ is rate of change of strain.

Following~\textcite{Roylance.2001}, to obtain material models representing physical materials, serial or parallel combinations of a total of~$n_{ \msmaterial }$ elastic and viscous components are created for which the resulting material dynamics formulation can be solved under consideration of realization of the physical relations for
%
\begin{subequations}\label{eqn:stress-strain:modeling:combinations}
  \begin{align}
    \text{serial components:}
      & 
      & \stress*_{ \subsystem }
      & = \stress*_{ k } \,
          ,
      & \strain*_{ \subsystem }
      & = \sum\limits_{ k }^{ n_{ \msmaterial } }{%
            \strain*_{ k }%
          } \,
          ,
          \label{eqn:stress-strain:modeling:combinations:serial} \\
    \text{parallel components:}
      & 
      & \stress*_{ \subsystem }
      & = \sum\limits_{ k }^{ n_{ \msmaterial } }{%
            \stress*_{ k }%
          } \,
          ,
      & \strain*_{ \subsystem }
      & = \strain*_{ k } \,
          ,
          \label{eqn:stress-strain:modeling:combinations:parallel}
  \end{align}
\end{subequations}
%
where~$\strain*_{ \subsystem }$ and~$\stress*_{ \subsystem }$ are the strain and stress of the total system, respectively, and~$\strain*_{ k }$ and~$\stress*_{ k }$ are the~$k$-th component's strain and stress, respectively.
It can be seen, which the novice reader may also infer from experience, that the stress on each component is the same in a serial combination while the strain of each component is the same in a parallel combination.
Conversely, this means that the strain of a serial combination and the stress in a parallel combination behaves opposite to the former behavior.
Using these material and mathematical relationships and their time-derivatives, the resulting material system can be described mathematically, resulting in an \gls{ODE} of stress~$\stress*_{ \subsystem }$ and strain~$\strain*_{ \subsystem }$ for the uniaxial isotropic linear viscoelastic material in the constitutive equation reading
%
\begin{align}
  a_{0} \,
  \stress*_{ \subsystem }
  +
  a_{1} \,
  \dotstress*_{ \subsystem }
  +
  \dotsb
  +
  a_{ n_{ a } } \,
  \od[n_{ a }]{ \stress*_{ \subsystem } }{ t }
    & = b_{0} \,
        \strain*_{ \subsystem }
        +
        b_{1} \,
        \dotstrain*_{ \subsystem }
        +
        \dotsb
        +
        b_{ n_{ b } } \,
        \od[n_{ b }]{ \strain*_{ \subsystem } }{ t } \,
        ,
        \label{eqn:stress-strain:modeling:general-material-ode}
\end{align}
%
where~$a_{ k }$ and~$b_{ k }$ can be resorted from material constants, and in general~${n_{ a } \neq n_{ b }}$.
In the later presented procedure to the investigation of stress-strain dynamics, we will perform analysis not using the time-domain and its associated \gls{ODE}, but we will perform analysis in the frequency-domain using the Laplace transform of the \gls{ODE} from~\cref{eqn:stress-strain:modeling:general-material-ode} which generally reads
%
\begin{align*}
  \tf \of{\s}
    & =
        \frac{
          Y \of{\s}
        }{
          U \of{\s}
        }
      =
        \frac{
          b_{ n_{b} } \,
          \pow[n_{b}]{\s}
          +
          b_{ n_{b} - 1} \,
          \pow[n_{b} - 1]{\s}
          +
          \dotsb
          +
          b_{1} \,
          \s
          +
          b_{0}
        }{
          a_{ n_{a} } \,
          \pow[n_{a}]{\s}
          +
          a_{ n_{a} - 1} \,
          \pow[n_{a} - 1]{\s}
          +
          \dotsb
          +
          a_{1} \,
          \s
          +
          a_{0}
        } \,
        ,
\end{align*}
%
where~$\s = \sigma + \imagu \, \omega$ is a complex number frequency parameter with real numbers~$\sigma$ and~$\omega$,~$Y \of{\s} = \laplace \lof{y} \of{\s}$ and~$U \of{\s} = \laplace \lof{u} \of{\s}$ are the Laplace transform of the output and input, respectively.
We normalize the transfer function with respect~$a_{n_{a}}$ such that we eliminate one free variable yielding the normalized transfer function
%
\begin{align}
  \tf \of{\s}
    & = \frac{
          \frac{
            b_{ n_{b} }
          }{
            a_{ n_{a} }
          } \,
          \pow[n_{b}]{\s}
          +
          \frac{
            b_{ n_{b} - 1}
          }{
            a_{ n_{a} }
          } \,
          \pow[n_{b} - 1]{\s}
          +
          \dotsb
          +
          \frac{
            b_{1}
          }{
            a_{ n_{a} }
          } \,
          \s
          +
          \frac{
            b_{0}
          }{
            a_{ n_{a} }
          }
        }{
          \pow[n_{a}]{\s}
          +
          \frac{
            a_{ n_{a} - 1}
          }{
            a_{ n_{a} }
          } \,
          \pow[n_{a} - 1]{\s}
          +
          \dotsb
          +
          \frac{
            a_{1}
          }{
            a_{ n_{a} }
          } \,
          \s
          +
          \frac{
            a_{0}
          }{
            a_{ n_{a} }
          }
        }
      =
        \frac{
          N \of{\s}
        }{
          D \of{\s}
        } \,
        ,
        \label{eqn:stress-strain:modeling:transfer-function}
\end{align}
%
with numerator polynomial~$N \of{\s}$ and denominator polynomial~$D \of{\s}$.

Polynomial coefficients~$a_{\loopindex}$ of the numerator and~$b_{\loopindex}$ of the denominator can be inferred from the material law put in place, however, only lower-dimensional models can provide for an analytical solution to the mapping.
Higher-order materials composed of three or more parallel Maxwell, respectively serial Kelvin components, yield nonlinear equations in the elastic and viscous parameters.
For such models, we approximate the elastic and viscous parameters by means of solving the root-finding problem defined through the \gls{ODE} in~\cref{eqn:stress-strain:modeling:general-material-ode} and the numerator/denominator values from~\cref{eqn:stress-strain:modeling:transfer-function}.

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.39\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--generalized-maxwell}
    \includegraphics[%
        width=\linewidth,%
      ]{material/generalized-maxwell}
    \tikzexternaldisable%
    \caption{%
      Generalized Maxwell chain material model with a single elastic component of elasticity~$\youngsmodulus_{1}$, parallel to~$n_{\msmaterial}$ parallel \acrlong{acr:MW} of elasticity~$\youngsmodulus_{\loopindex}$ and viscosity~$\viscousmodulus_{\loopindex}$, $\irange{n_{\msmaterial}}{\loopindex}$.
    }
    \label{sec:stress-strain:modeling:generalized-maxwell}
  \end{subfigure}%
  \hfill
  \begin{subfigure}[t]{0.59\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--generalized-kelvin}
    \includegraphics[%
        width=\linewidth,%
      ]{material/generalized-kelvin}
    \tikzexternaldisable%
    \caption{%
      Generalized Kelvin chain material model with a single elastic component of elasticity~$\youngsmodulus_{1}$, in series with~$n_{\msmaterial}$ parallel \acrlong{acr:KV} of elasticity~$\youngsmodulus_{\loopindex}$ and viscosity~$\viscousmodulus_{\loopindex}$, $\irange{n_{\msmaterial}}{\loopindex}$.
    }
    \label{sec:stress-strain:modeling:generalized-kelvin}
  \end{subfigure}%
  \caption[%
    General material models: Maxwell chain and Kelvin chain.
  ]{%
    Generalized Maxwell chain~\subref{sec:stress-strain:modeling:generalized-maxwell} and generalized Kelvin chain~\subref{sec:stress-strain:modeling:generalized-kelvin} material models.
  }
\end{figure}

In general, two viscoelastic material model types of similar structure exist, one being the \gls{acr:GMW} chain, the other being the \gls{acr:GK} chain.
Due to their mechanical structure, \gls{acr:MW} is more commonly used when we prescribe the strain history and are interested in the stress response, whereas \gls{acr:GK} is used in the opposite direction, prescribing stress history and outputting strain response.
Either model consists of a purely elastic component, either in parallel with \gls{acr:MW} components~(see~\cref{sec:stress-strain:modeling:generalized-maxwell}), or in series with \gls{acr:KV} components~(see~\cref{sec:stress-strain:modeling:generalized-kelvin}).
Making use of equality of stress distribution on the parallel components and strain distribution on the serial components, respectively, such that
%
\begin{subequations}\label{eqn:stress-strain:modeling:material-chains}
\begin{align}
  \stress*_{\msmaxwell}
    & = \parentheses*{
          \youngsmodulus_{1}
          +
          \sum\limits_{\loopindex}^{n_{\msmaterial}}{
            \frac{
              \dif
            }{
              \frac{
                \dif
              }{
                \youngsmodulus_{\loopindex}
              }
              +
              \frac{
                1
              }{
                \viscousmodulus_{\loopindex}
              }
            }
          } \,
        } \,
        \strain*_{\msmaxwell}
        ,
        \label{eqn:stress-strain:modeling:material-chains:maxwell}
    \\
  \strain*_{\mskelvinvoigt}
    & = \parentheses*{
          \frac{
            1
          }{
            \youngsmodulus_{1}
          }
          +
          \sum\limits_{\loopindex}^{n_{\msmaterial}}{
            \frac{
              1
            }{
              \youngsmodulus_{\loopindex}
              +
              \viscousmodulus_{\loopindex} \,
              \dif \,
            }
          }
        } \,
        \stress*_{\mskelvinvoigt} \,
        ,
        \label{eqn:stress-strain:modeling:material-chains:kelvin}
\end{align}
\end{subequations}
%
where~$\dif\, \parentheses{} = \od{\parentheses{}}{t}$ is the differential operator with respect to time, and~$\youngsmodulus_{\loopindex}$,~$\viscousmodulus_{\loopindex}$ are the elastic and viscous modulus of the~$\loopindex$-th elastic and viscous component, respectively~\autocite{Findley.1976}\correct{This is a screwed up bibliography entry!}.
Mathematical manipulation of either~\cref{eqn:stress-strain:modeling:material-chains:maxwell} or~\cref{eqn:stress-strain:modeling:material-chains:kelvin} for the respective material yields the desired transfer function, in general terms given through~\cref{eqn:stress-strain:modeling:general-material-ode}.

Depending on the point of view and interest, different system characteristics may be extracted from the stress-strain \gls{ODE}~\cref{eqn:stress-strain:modeling:general-material-ode} \textEg the strain response to a stress-step or vice versa.
Given the stress step response, this equation can be readily used to assess characteristic material properties such as instantaneous elasticity or long-term creep response.
For the application in \caros, we are mostly interested in the transfer function of strain to stress \textIe we desire a description that yields
%
\begin{align*}
  \stress*
    & \coloneqq \stress* \of{\strain*, \dotstrain*, \ddotstrain*, \dotso} \,
      .
\end{align*}

It may lastly be noted for the matter of completeness that the choice of spring or damper as well as the choice of serial or parallel combination of these components must be based on the material's dynamic response obtained through experimental data or through knowledge of the material as well as the direction of transfer \textIe stress to strain or strain to stress.
Without any prior knowledge, finding a suitable material model is an involved task and will not always lead to acceptable results~\autocite{Findley.1976,Brinson.2008,Babaei.2015,Marques.2012,Romanyk.2013}.
We may, of course, simply assume an arbitrary transfer function \textEg~$\PT[1]$ or~$\PT[2]$ and fit this with the experimentally obtained data, however, this completely removes any physical or mechanical meaning from the obtained dynamics and does not allow inferring elastic or viscous parameters.

As already mentioned, finding a suitable material model must thus be based on knowledge of the dynamical stress-strain behavior of the material.
Luckily, literature is full of material models for different kind of visco-, plasto-, and even viscoplastoelastic materials.
Most commonly used material models comprise the very simple \gls{acr:MW} and \gls{acr:KV}, the more advanced \gls{acr:ZM}~(also called \gls{acr:SLS}), and the \gls{acr:BM}.
The former three will be introduced more closely in the following subsections, where we also point out their limitations from a purely analytical perspective without explicit comparison against experimental data.
Afterwards, we will zero in on a general structure of the underlying material model and its form of the equations of stress-strain dynamics to allow for successful experimental studies and estimation of \Dyneema \UHMWPE fibers.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/modeling/maxwell}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/modeling/kelvin-voigt}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/modeling/zener}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/stress-strain-dynamics/modeling/summary}
