%!TEX root=../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/modeling/kelvin-voigt.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Kelvin--Voigt Material}\label{sec:stress-strain:modeling:kelvin-voigt}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--kelvinvoigt--schematic}
    \includegraphics[%
        width=\linewidth,%
      ]{material/kelvinvoigt/schematic}
    \tikzexternaldisable%
    \caption{%
      \Acrlong{acr:KV} model composed of linear spring and linear damper in parallel.%
    }
    \label{fig:stress-strain:modeling:kelvin-voigt:model}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--kelvinvoigt--step-responses}
    \includegraphics[%
        width=1.00\linewidth,%
        axisratio=1.00,%
      ]{material/kelvinvoigt/step-responses}
    \tikzexternaldisable%
    \caption{%
      Step response of the \acrlong{acr:KV} model with~$\youngsmodulus_{1} = \SI{5}{\pascal}$,~$\viscousmodulus_{1} = \SI{5}{\pascal\second}$.
    }
    \label{fig:stress-strain:modeling:kelvin-voigt:step-response}
  \end{subfigure}%
  \caption[%
    Model schematic of \acrlong{acr:KV} and step responses.
  ]{%
    Mechanical model schematic of \acrlong{acr:KV} model~\subref{fig:stress-strain:modeling:kelvin-voigt:model} and stress and strain step response of the material~\subref{fig:stress-strain:modeling:kelvin-voigt:step-response}.
  }
  \label{fig:stress-strain:modeling:kelvin-voigt}
\end{figure}

\Acrfull{acr:KV} is a parallel combination of the same components as \gls{acr:MW}~(see~\cref{fig:stress-strain:modeling:kelvin-voigt}).
This material model is most often used for dynamics simulation of cable robots as it covers instantaneous and short-term dynamics of the cables well~(see~\textcite{Miermeister.2010,Tempel.2015e,Korayem.2017,Zarebidoki.2011,Mousavi.2018}).
Using~\cref{eqn:stress-strain:modeling:material-chains}, we obtain the constitutive material equation
%
\begin{align*}
  \dotstrain*
    + \frac{
        \youngsmodulus_{1}
      }{
        \viscousmodulus_{1}
      } \,
      \strain*
    & = \frac{
          1
        }{
          \viscousmodulus_{1}
        } \,
        \stress* \,
        ,
\end{align*}
%
for which we may state the responses to strain step and stress step as
%
\begin{subequations}\label{eqn:stress-strain:modeling:kelvin-voigt:step-response}
\begin{align}
  \strain* \of{t}
    & = \frac{
          \stress*_{0}
        }{
          \youngsmodulus_{1}
        }
        +
        \parentheses*{
          \strain*_{0}
          -
          \frac{
            \stress*_{0}
          }{
            \youngsmodulus_{1}
          }
        } \,
        \pexp*{
          -
          \frac{
            \youngsmodulus_{1}
          }{
            \viscousmodulus_{1}
          } \,
          t
        } \,
        ,
        \label{eqn:stress-strain:modeling:kelvin-voigt:step-response:strain}
        \\
  \stress* \of{t}
    & = \strain*_{0} \,
        \youngsmodulus_{1}
        +
        \strain*_{0} \,
        \viscousmodulus_{1} \,
        \dirac \of{t}
        ,
        \label{eqn:stress-strain:modeling:kelvin-voigt:step-response:stress}
\end{align}
\end{subequations}
%
with stress step of~$\stress*_{0}$ and strain step~$\strain*_{0}$.

We may directly see a~(numerical) limitation of the model being a dominant effect in \caro simulation.
Conventionally, we want to infer the resulting stress induced on the platform due to cable strain, which, following~\cref{eqn:stress-strain:modeling:kelvin-voigt:step-response:stress} appears to be directly linked to the strain step~$\strain*_{0}$.
However, care needs to be taken as the underlying \gls{ODE} consists of a feedthrough thus is improper~(noncausal) \textIe the material gives instantaneous stress relaxation due to the presence of the dashpot.
Any small change in strain is immediately visible in the stress which numerically causes, during simulation, very small step sizes, in particular when paired with the discontinuity around zero strain~(see~\cref{eqn:fundamentals:dynamics:forward-dynamics:cables:formulation}).

Limitations of the \gls{acr:KV} are covering certain aspects of the cable material under investigation as it does not allow for an instantaneous strain response due to the damper employed in parallel~(see~\cref{fig:stress-strain:modeling:kelvin-voigt:step-response}).
In addition, the material model describes noncausal behavior for the strain-to-stress transfer function \textIe any change in strain~$\strain*$ is directly visible in the stress due to occurrence of derivatives of higher-order on the strain side than on the stress side.
