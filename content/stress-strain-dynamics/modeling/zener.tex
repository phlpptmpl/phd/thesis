%!TEX root=../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/modeling/zener.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Zener Material}\label{sec:stress-strain:modeling:zener}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--zener--schematic}
    \includegraphics[%
        width=\linewidth,%
      ]{material/zener/schematic}
    \tikzexternaldisable%
    \caption{%
      \Acrlong{acr:ZM} model composed of linear spring parallel to a \acrlong{acr:MW} in series; Maxwell representation.%
    }
    \label{fig:stress-strain:modeling:zener:model}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--zener--step-responses}
    \includegraphics[%
        width=\linewidth,%
        axisratio=1.00,%
      ]{material/zener/step-responses}
    \tikzexternaldisable%
    \caption{%
      Step response of the \acrlong{acr:ZM} model with~$\youngsmodulus_{1} = \youngsmodulus_{2} = \SI{5}{\pascal}$,~$\viscousmodulus_{2} = \SI{5}{\pascal\second}$.
    }
    \label{fig:stress-strain:modeling:zener:step-response}
  \end{subfigure}%
  \caption[%
    Model schematic of \acrlong{acr:ZM} and step responses.
  ]{%
    Mechanical model schematic of \acrlong{acr:ZM} model~\subref{fig:stress-strain:modeling:zener:model} and stress and strain step response of the material~\subref{fig:stress-strain:modeling:zener:step-response}.
  }
  \label{fig:stress-strain:modeling:zener}
\end{figure}

\Acrfull{acr:ZM} overcomes limitations of \gls{acr:MW} and \gls{acr:KV} by combining a purely linear spring~$\vectset{ \youngsmodulus_{1} }$ with a \gls{acr:KV}~$\vectset{ \youngsmodulus_{2} ; \viscousmodulus_{2} }$, thus allowing for both an instantaneous strain response as well as a settling strain behavior.
By means of~\cref{eqn:stress-strain:modeling:material-chains}, we obtain the constitutive material equation
%
\begin{align*}
  \dotstrain*
  +
  \frac{
    \youngsmodulus_{1} \,
    \youngsmodulus_{2}
  }{
    \viscousmodulus_{2} \,
    \parentheses*{
      \youngsmodulus_{1}
      +
      \youngsmodulus_{2}
    }
  } \,
  \strain*
    & = \frac{
          1
        }{
          \youngsmodulus_{1}
          +
          \youngsmodulus_{2}
        } \,
        \dotstress*
        +
        \frac{
          \youngsmodulus_{2}
        }{
          \viscousmodulus_{2} \,
          \parentheses*{
            \youngsmodulus_{1}
            +
            \youngsmodulus_{2}
          }
        } \,
        \stress* \,
        ,
\end{align*}
%
for which the responses to strain step and stress step are
%
\begin{subequations}\label{eqn:stress-strain:modeling:zener:step-response}
\begin{align}
  \strain* \of{t}
    & = \stress*_{0} \,
        \parentheses*{
          \frac{
            \youngsmodulus_{1}
            +
            \youngsmodulus_{2}
          }{
            \youngsmodulus_{1} \,
            \youngsmodulus_{2}
          } \,
          +
          \parentheses*{
            \frac{
              1
            }{
              \youngsmodulus_{1}
            }
            -
            \frac{
              \youngsmodulus_{1}
              +
              \youngsmodulus_{2}
            }{
              \youngsmodulus_{1} \,
              \youngsmodulus_{2}
            }
          } \,
          \pexp*{
            -
            \frac{
              \youngsmodulus_{2}
            }{
              \viscousmodulus_{2}
            } \,
            t
          }
        } \,
        ,
        \label{eqn:stress-strain:modeling:zener:step-response:strain}
    \\
  \stress* \of{t}
    & = \frac{
          \youngsmodulus_{1} \,
          \youngsmodulus_{2}
        }{
          \youngsmodulus_{1}
          +
          \youngsmodulus_{2}
        } \,
        \strain*_{0}
        +
        \parentheses*{
          \stress*_{0}
          -
          \frac{
            \youngsmodulus_{1} \,
            \youngsmodulus_{2}
          }{
            \youngsmodulus_{1}
            +
            \youngsmodulus_{2}
          } \,
          \strain*_{0}
        } \,
        \pexp*{
          -
          \frac{
            \youngsmodulus_{1}
            +
            \youngsmodulus_{2}
          }{
            \viscousmodulus_{2}
          } \,
          t
        }
        ,
        \label{eqn:stress-strain:modeling:zener:step-response:stress}
\end{align}
\end{subequations}
%
with initial stress~$\stress*_{0}$ and initial strain~$\strain*_{0}$, respectively, stepping to final stress~$\stress*_{1}$ and final strain~$\strain*_{1}$, respectively.

With \gls{acr:ZM}, a decreasing creep function \textIe an asymptotic strain function at constant stress input can be achieved, which settles in at~$\lim\limits_{t \to \infty} \stress* \of{ t } = \stress*_{ 1 } { \parentheses{ \youngsmodulus_{1} \, \youngsmodulus_{2} } } / { \parentheses{ \youngsmodulus_{1} + \youngsmodulus_{2} } }$~(see~\cref{fig:stress-strain:modeling:kelvin-voigt:step-response}).
