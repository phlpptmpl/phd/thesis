%!TEX root=../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/modeling/burgers.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Burgers Material}\label{sec:stress-strain:modeling:burger}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--burgers--schematic}
    \includegraphics[%
        width=\linewidth,%
      ]{material/burgers/schematic}
    \tikzexternaldisable%
    \caption{%
      Burgers material model composed of a Maxwell model and Kelvin--Voigt model in series.
    }
    \label{fig:stress-strain:modeling:burger:model}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{material--burgers--step-responses}
    \includegraphics[%
        width=1.00\linewidth,%
        axisratio=1.00,%
      ]{material/burgers/step-responses}
    \tikzexternaldisable%
    \caption{%
      Step response of the Burgers material model.
    }
    \label{fig:stress-strain:modeling:burger:step-response}
  \end{subfigure}
  \caption{%
    Mechanical model schematic of the Maxwell material model~\subref{fig:stress-strain:modeling:burger:model} and stress and strain step response of the material~\subref{fig:stress-strain:modeling:burger:step-response} with~${\youngsmodulus_{\msmaxwell} = \youngsmodulus_{\mskelvinvoigt} = \SI{5}{\pascal}}$,~${\viscousmodulus_{2} = \viscousmodulus_{3} = \SI{5}{\pascal\second}}$.
  }
  \label{fig:stress-strain:modeling:burger}
\end{figure}

\Acrfull{acr:BM} conforms best with experimental findings that show an infinite increase of strain as~${t \to \infty}$, yet with decreasing strain rate.
By combining a \gls{acr:MW} model~$\parentheses{ \youngsmodulus_{1}, \viscousmodulus_{2} }$ in series with a \gls{acr:KV}~$\parentheses{ \youngsmodulus_{3}, \viscousmodulus_{2} }$, and employing~\cref{eqn:stress-strain:modeling:open-form}, we obtain the constitutive equation
%
\begin{align*}
  \underbrace{
    \frac{
      \viscousmodulus_{2} \,
      \viscousmodulus_{3}
    }{
      \youngsmodulus_{3}
    }
  }_{
    b_{2}
  } \,
  \ddotstrain*
  +
  \underbrace{
    \vphantom{\frac{\viscousmodulus_{3} }{\youngsmodulus_{3} }}
    \viscousmodulus_{2}
  }_{
    b_{1}
  } \,
  \dotstrain*
    & = \underbrace{
          \frac{
            \viscousmodulus_{2} \,
            \viscousmodulus_{3}
          }{
            \youngsmodulus_{1} \,
            \youngsmodulus_{3}
          }
        }_{
          a_{2}
        } \,
        \ddotstress*
        +
        \underbrace{
          \parentheses*{
            \frac{
              \viscousmodulus_{2}
            }{
              \youngsmodulus_{1}
            }
            +
            \frac{
              \viscousmodulus_{2}
            }{
              \youngsmodulus_{3}
            }
            +
            \frac{
              \viscousmodulus_{3}
            }{
              \youngsmodulus_{3}
            }
          }
        }_{
          a_{1}
        } \,
        \dotstress*
      +
      \underbrace{
        \vphantom{\frac{\viscousmodulus_{3} }{\youngsmodulus_{3} }}
        1
      }_{
        a_{0}
      } \,
      \stress* \, ,
\end{align*}
%
for which we may state the responses to strain step and stress step as
%
\begin{subequations}\label{eqn:stress-strain:modeling:burger:step-response}
\begin{align}
\begin{split}
  \strain* \of{t}
    & = \stress*_{0} \,
        \frac{
          1
        }{
          \youngsmodulus_{1}
        } \,
        \parentheses*{
          1
          +
          \frac{
            b_{2}
          }{
            b_{1}
          } \,
          \parentheses*{
            \frac{
              \youngsmodulus_{1}
            }{
              \viscousmodulus_{3}
            }
            +
            \frac{
              \youngsmodulus_{1}
            }{
              \viscousmodulus_{2}
            }
          }
        }
        + \dotsb
    \\
    & \phantom{=}
      -
      \stress*_{0} \,
        \frac{
          b_{2}
        }{
          b_{1}
        } \,
        \parentheses*{
          \frac{
            1
          }{
            \viscousmodulus_{2}
          }
          -
          \frac{
            1
          }{
            \viscousmodulus_{3}
          }
        } \,
        \pexp*{
          -
          \frac{
            b_{1}
          }{
            b_{2}
          } \,
          t
        }
        +
        \dotsb
    \\
    & \phantom{=}
      +
      \frac{
        \stress*_{1}
      }{
        b_{1}
      } \,
      \parentheses*{
        t
        -
        \parentheses*{
          1
          -
          \frac{
            b_{2}
          }{
            b_{1}
          } \,
          \pexp*{
            -
            \frac{
              b_{1}
            }{
              b_{2}
            } \,
            t
          }
        }
      } \,
      ,
\end{split} \label{eqn:stress-strain:modeling:burger:step-response:strain}
    \\
  \stress* \of{t}
    & = 
      \label{eqn:stress-strain:modeling:burger:step-response:stress}
\end{align}
\end{subequations}
%
with initial stress~$\stress*_{0}$ and initial strain~$\strain*_{0}$, respectively, stepping to final stress~$\stress*_{1}$ and final strain~$\strain*_{1}$, respectively.
