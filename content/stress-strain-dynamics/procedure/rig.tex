%!TEX root=../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/procedure/rig.tex

\subsection{Experimental Setup and Rig}\label{sec:stress-strain:procedure:experimental-rig}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--procedure--experimental-rig}
  \includegraphics[
      width=0.75\linewidth,%
      % height=0.35\textheight,%
    ]{stress-strain/procedure/experimental-rig}
  \tikzexternaldisable%
  \caption{
    Sketch of the stress-strain dynamics identification test bench.
  }
  \label{fig:stress-strain:procedure:experimental-rig}
\end{figure}

In order to approximate the strain-step response of the \Dyneema cable exemplified in~\cref{fig:stress-strain:key-characteristics:strain-relaxation}, we require measurement of the stress given rapid strain changes.
By means of rapidly pulling on the cable at the proximal end and measuring the stress response on the distal end, we obtain a time-dependent stress response of the material to a time-dependent strain input.
This can be accomplished by either letting the cable vertically hang imposing it with various external loads, or by fixing the cable between points of which one is mechanically actuated.
The former method, as implemented by~\textcite{Schmidt.2016,Piao.2017} and others, requires external measurement of the distal cable point to obtain elongation and allows for changing tension in only discrete steps.
The latter method allows for more precise reading of the time-dependent cable stress as well as controlling using cable strain as commanded input.
Since we aim for finding a model suitable of predicting the cable's stress response to a strain input, we choose the latter method allowing us to precisely describe a strain input trajectory.

\begin{table}[tbp]
  \centering
  \smaller[1]
  \caption{%
    Mechanical parameters of the cable identification test rig.
  }
  \label{tbl:stress-strain:procedure:experimental-rig}
  \input{tables/stress-strain/parameters-test-rig}
\end{table}

We develop a cable test bench as sketched in~\cref{fig:stress-strain:procedure:experimental-rig} with a servo motor attached through a gear of ratio~$i_{\ms{g}} = 5$ to a drum of diameter~$\diameter_{\subdrum} = \SI{20}{\centi\meter}$ on the cable proximal end~(left-hand side, \textCf~\cref{fig:stress-strain:procedure:experimental-rig:winch}), and a cylindrical mounting on the distal end~(right-hand side, \textCf~\cref{fig:stress-strain:procedure:experimental-rig:total}).
Further mechanical parameters are tabularized in~\cref{tbl:stress-strain:procedure:experimental-rig}.
This cylindrical mounting in turn is supported on a cantilever arm at the end of which a pressure sensor is attached.
Measured signals obtained during experiments comprise the drive's angular position and the pressure sensor's voltage.
Knowing the pressure sensor composed of a Wheatstone bridge and its data sheet, we can calculate the actual cable tension from the bridge voltage.\question{Need to add the sensor's data sheet?}
The drive's angular position is converted using the gear ratio and drum diameter into a linear position representing the cable elongation, which, given the total unstrained cable length, can be expressed as strain.
Strain rates and accelerations, much like cable force rates are not explicitly measured as these are not required for later parameter estimation.
The experimental procedure implies appropriate creation and application of the input signal as well as measurement of the resulting strain in a time-synchronized manner at rates similar to those of the real \caros control system.
For satisfying these self-imposed requirements, we use the same control system as employed on the \COPacabana \caros, as well as the same servo motor~(see~left side of~\cref{fig:stress-strain:procedure:experimental-rig:total}).

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.3267\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        trim=0px 140px 0px 0px,%
        clip,%
        keepaspectratio,%
        width=\linewidth,%
      ]{cable-testbench/testbench-winch}
    \caption{
      Close up view of the drum attached to the Bosch servo motor with several windings of cable wrapped around.
    }
    \label{fig:stress-strain:procedure:experimental-rig:winch}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.6533\linewidth}
    \centering
    \smaller[1]
    \includegraphics[%
        keepaspectratio,%
        width=\linewidth,%
      ]{cable-testbench/testbench-wide}
    \caption{
      Side-view of the cable test bench with the actuating Bosch servo motor on the left-hand side and the force sensor on the right-hand side; cable in red.
    }
    \label{fig:stress-strain:procedure:experimental-rig:total}
  \end{subfigure}
  \caption{%
    Cable test bench used for identification of stress-strain dynamics.
  }
  \label{fig:stress-strain:procedure:experimental-rig}
\end{figure}

Rapid excitation of the viscoelastic material under investigation is favorable for it allows initially only exciting the elastic component; keeping strain constant allows for the apparent effect of stress relaxation as the parallel chains of \gls{acr:MW} components tend to relax with their specific relaxation time~$\relaxationtime_{\loopindex} = \viscousmodulus_{\loopindex} / \youngsmodulus_{\loopindex}$.
In theory, the system ought be excited with a strain-impulse~$\strain* \of{t} = \dirac \of{t}$ with its Laplace transform~${\laplace \lof{\dirac} \of{\s} = 1}$ such that the system response in Laplace domain equals~$Y \of{\s} = \tf \of{\s} \laplace \lof{\dirac} \of{s} = \tf \of{\s} \, 1$, of which the inverse Laplace transform~$\inv{\laplace} \lof{Y} \of{t} = y \of{t} / u \of{t}$ then returns the system's dynamics.
However, impinging physical systems with a Dirac impulse, an infinitely large input over an infinitely small time span, is physically hardly possible due to limits arising from the system's inability to accelerate indefinitely last its inertia and discrete control system.
Likewise, system identification may be performed given a step input of strain~$\strain*_{0}$ defining the input signal as 
%
\begin{align*}
  \strain* \of{t}
    & = \strain*_{0} \,
        \heaviside \of{t}
      = \begin{cases}
          \strain*_{0} \,
          ,
            & t \geq 0
            \\
          0 \,
          ,
            & t < 0
        \end{cases} \,
        ,
\end{align*}
%
where~$\heaviside \of{t}$ is the Heaviside step function, its Laplace transform~${\laplace \lof{\heaviside} \of{\s} = 1 / s}$.
Due to physics in nature, it is impossible for the drive system to instantaneously transition between an initial strain~$\strain*_{0}$ and target strain~$\strain*_{1}$, which is why we define the system input strain trajectory mathematically through
%
\begin{align*}
  \strain* \of{t}
    & = \begin{cases}
          \lerp[\parentheses*{1 + \frac{t - T}{t_{\ms{flank}}}}]{\strain*_{1}}{\strain*_{0}} \,
          ,
            & T - t_{\ms{flank}} \leq t \land t < T \,
            ,
            \\
          \strain*_{1} \,
          ,
            & \frac{T}{2} \leq t \land t < T - t_{\ms{flank}} \,
            ,
            \\
          \lerp[\parentheses*{1 + \frac{t - \frac{T}{2}}{t_{\ms{flank}}}}]{\strain*_{0}}{\strain*_{1}} \,
          ,
            & \frac{T}{2} - t_{\ms{flank}} \leq t \land t < \frac{T}{2} \,
            ,
            \\
          \strain*_{0} \,
          ,
            & 0 \leq t \land t < \frac{T}{2} - t_{\ms{flank}} \,
            ,
        \end{cases}
\end{align*}
%
with total signal period~$T$, flank time of transition~$t_{\ms{flank}}$, and initial and final strain~$\strain*_{0}$ and~$\strain*_{1}$, respectively.

Despite real world applications of \caros allowing for rapid cable velocities of up to~$\SI{30}{\meter\per\second}$~\autocite{Pott2012b}, cable strain does not vary similarly drastically.
We can size up strain and strain rate from the cable force limits and an ideal cable force controller achieving control error reduction within one control cycle\footnote{Which, physically speaking, is very unlikely to be sustainable, yet assumed for the sake of supporting the example.}.
Letting the cable forces be bound in~${\cabforce \in \interval{\cabforcemin}{\cabforcemax} = \SIrange[range-units=brackets,open-bracket={[},close-bracket={]},range-phrase={,}]{100}{3000}{\newton}}$~(values taken from \IPAnema, \textCf~\textcite{Pott.2018}), the control cycle be~$\stepsize = \SI{1}{\milli\second}$, and the cable force error be~$\Delta \cabforce = \cabforcemax - \cabforcemin = \SI{2900}{\newton}$.
Then, under the assumption of a purely linear elastic cable with stress proportional to strain by~${\stress* = \strain* \, \youngsmodulus}$ and stress proportional to applied tensile force by~${\stress* = \cabforce / \crosssection}$, we obtain the change of stress~$\Delta \strain* = \Delta \cabforce / \youngsmodulus \, \crosssection$.
Which, for a cable of diameter~$\diameter = \SI{6}{\milli\meter}$ and Young's modulus~$\youngsmodulus = \SI{15}{\giga\pascal}$, gives us~$\Delta \strain* = \num{0.006837768}$, and likewise as strain rate~$\dotstrain* = \Delta \strain* / \stepsize = \SI{6.837767925}{\per\second}$.
Since it is very unlikely for the cable force controller to be parametrized with such a quick control loop time, last because it may then be inherently unstable, the actual strain rate will be noticeably lower.
For example, the cable force controller on the \IPAnemaThree is parametrized with a control cycle time of~$\stepsize = \SI{17}{\milli\second}$~\autocite{Kraus2015b} implemented as a~$\PT[1]$ controller, thus it must achieve strain rates of less than~$\max \of{\dotstrain*} = \SI{0.100555411}{\per\second}$.
