%!TEX root=../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/procedure/fitting.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Fitting of Transfer Function}\label{sec:stress-strain:procedure:fitting}

We assume the system dynamics be continuous in time, however, our control system operates only discrete in time, consequently we have measurements of strain input trajectory~$u \of{t} = \strain* \of{t_{\loopindex}}$ and stress output~$y \of{t} = \stress* \of{t_{\loopindex}}$ at discrete time values~$t_{\loopindex}$ for time values sampled at~$\Delta t$.
To estimate the transfer function, we require the experimental measurements be given in Laplace domain, which due to their discrete nature, are calculated by the discrete\footnote{To be precise, the discrete Laplace transform is not discrete in ways the discrete Fourier transform is.
The former evaluates a function at an infinite number of points and returns a continuous function, whereas the latter operates on a finite sequence and returns a finite sequence.} Laplace transform
%
\begin{align*}
  \laplace_{\Delta t} \lof{f} \of{\s}
    & = \Delta t \,
        \sum\limits_{\loopindex = 0}^{\infty}{
          f \of{\loopindex \, \Delta t} \,
          \pexp{
            -
            \s \,
            \loopindex \,
            \Delta t
          }
        } \,
        ,
\end{align*}
%
of a discrete function~$f \of{t_{\loopindex}}$ and step size~$\Delta t$.
Setting~$\Delta t = 1$ and~$z = \pexp{-\s}$, we obtain the~$z$-transform of~$f \of{t}$ at nonnegative integers.

Using the continuous Laplace transform of our transfer function, its parameters can then be estimated by a nonlinear least-squares problem such that
%
\begin{align}
  \min_{D, N}
  \sum\limits_{j}^{n_{f}}{
    \pow[2]{
      \abs*{
        \frac{
          W \of{s_{j}}
        }{
          D \of{s_{j}}
        }
        \parentheses[\Big]{
          D \of{s_{j}} \,
          \laplace \lof{y} \of{s_{j}}
          -
          N \of{s_{j}} \,
          \laplace \lof{u} \of{s_{j}}
        }
      }
    }
  } \,
  ,
  \label{eqn:stress-strain:procedure:fitting:least-squares-problem}
\end{align}
%
is the loss function to be minimized with numerator~$N \of{\s}$ and denominator~$D \of{\s}$, weight polynomial~$W \of{\s}$, and number of frequenices~$n_{f}$ defined by the Nyquist–Shannon sampling theorem.
By choosing a non-unitary polynomial for~$W \of{\s}$ \textIe~$W \of{\s} \neq 1$, we can introduce frequency-dependent weights on the data \textEg desiring better approximation of lower or higher frequencies.
For means of this thesis, we choose~$W \of{s} \equiv 1$, putting equal trust in all frequency data.
If estimation is to be performed over a set of~$n_{\ms{trial}}$ trials yet for the same transfer function, then~\cref{eqn:stress-strain:procedure:fitting:least-squares-problem} reads
%
\begin{align}
  \min_{D, N}
  \sum\limits_{j}^{n_{f}}{
    \pow[2]{
      \abs*{
        \frac{
          W \of{s_{j}}
        }{
          D \of{s_{j}}
        }
        \parentheses[\Big]{
          \sum\limits_{\iterindex}^{n_{\ms{trial}}}{
            D \of{s_{j}} \,
            \laplace \lof{y_{\iterindex}} \of{s_{j}}
            -
            N \of{s_{j}} \,
            \laplace \lof{u_{\iterindex}} \of{s_{j}}
          }
        }
      }
    }
  } \,
  .
  \label{eqn:stress-strain:procedure:fitting:least-squares-problem:trials}
\end{align}

The least-squares problem is solved using S-K iteration~\autocite{Sanathanan.1963}, a technique developed for vector fitting of frequency-domain system identification from sampled data~\autocite{Drmac.2015,Garnier.2003,Voorhoeve.2014}, which iteratively solves
%
\begin{align*}
  \min_{D_{\loopindex}, N_{\loopindex}}
  \sum\limits_{j}^{n_{f}}{
    \pow[2]{
      \abs*{
        \frac{
          W \of{s_{j}} \,
        }{
          D_{\loopindex - 1} \of{s_{j}}
        }
        \parentheses[\Big]{
          D_{\loopindex} \of{s_{j}} \,
          \laplace \lof{y} \of{s_{j}}
          -
          N_{\loopindex} \of{s_{j}} \,
          \laplace \lof{u} \of{s_{j}}
        }
      }
    }
  } \,
\end{align*}
%
where~$D_{\loopindex - 1} \of{\s}$ is the denominator identified in the previous iteration step, initialized with~$D_{0} \of{\s} = 1$.
In the first iteration step, polynomials~$D_{1} \of{\s}$ and~$N_{1} \of{\s}$ are expressed in monomial basis, while all further iterations express them in terms of orthogonal rational bases functions on the unit disk.
The~${j\text{-th}}$ basis function reads
%
\begin{align*}
  B_{j, \loopindex} \of{\s}
    & = \frac{
          \sqrt{
            1
            -
            \pow[2]{
              \abs*{
                \pole_{j, \loopindex - 1}
              }
            }
          }
        }{
          q
          -
          \pole_{j, \loopindex - 1}
        } \,
        \prod\limits_{r = 0}^{j - 1}{
          \frac{
            1
            -
            \conjugate{
              \pole
            }_{j, \loopindex - 1} \,
            q
          }{
            q
            -
            \pole_{r, \loopindex - 1}
          }
        } \,
\end{align*}
%
where~$q = q \of{\s}$ is the frequency-domain variable on the unit disk,~$\pole_{j, \loopindex}$ the~$j$-th pole identified at step~$\loopindex$, and~$\conjugate{\pole}_{j, \loopindex}$ its complex conjugate pole.

We may further enforce stability of the estimated transfer function which may lead to a decrease in goodness of fit, however, allows to estimate physically coherent, in particular stable, models\footnote{%
  Additionally requiring stability may come as limitation, however, it will ensure that the estimated transfer function is physically more meaningful since the physical cable's stress-strain dynamics are inherently stable.%
}.
For a linear time-invariant transfer function such as~\cref{eqn:stress-strain:modeling:transfer-function}, it is sufficient to have all poles\footnote{Poles are the roots of the denominator polynomial~$D \of{\s}$.} in the left semi-plane of the complex variable~$\s \in \mathds{C}$.
In summary, the nonlinear least-squares problem is to minimize the~(weighted) prediction error norm over all data samples.

Quantitative measure of estimation quality will be expressed as goodness of fit~$\gof$ given in percent such that
%
\begin{align*}
  \gof
    & = \SI{100}{\percent} \,
        \parentheses*{
          1
          -
          \frac{
            \norm{
              y_{\ms{exp}}
              -
              y_{\ms{mdl}}
            }
          }{
            \norm{
              y_{\ms{mdl}}
            }
          }
        } \,
        ,
\end{align*}
%
where~$y_{\ms{exp}}$ are the experimental data,~$y_{\ms{mdl}}$ the simulated model data and~$\norm{}$ indicates the~$2$-norm of the time-series vectors~$y_{\ms{exp}}$ and~$y_{\ms{mdl}}$.
Goodness of fit can vary between~${g = -\infty}$~(bad fit) and~$g = \num{100}$~(perfect fit), with a value of~$\num{0}$ indicating no better fit than a straight line equal the data mean.
