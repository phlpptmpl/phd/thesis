%!TEX root=../../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/results/estimation/number-of-elements.tex

\subsubsection{Impact of Number of Elements}\label{sec:stress-strain:results:estimation:number-of-elements}

\begin{table}[tbp]
  \centering
  \smaller[1]
  \caption[%
      Estimated elastic and viscous parameters of cable model.
    ]{%
      Values of estimated elastic and viscous parameters of the \gls{acr:GMW} model with~$n_{\msmaxwell} \in \numset{2;3;4}$.
      Data are obtained from the estimated strain step responses graphed in~\cref{fig:stress-strain:results:estimation:number-of-elements:mechanical-parameters}.
    }
  \label{tbl:stress-strain:results:estimation:number-of-elements:mechanical-parameters}
  \input{tables/stress-strain/trapezoidal-tf-00150_mechpars_session}
\end{table}

While Maxwell-chain material models comprise multiple elastic and viscous components, the number of \gls{acr:MW} elements marks the only independent parameter for representing a physical viscoelastic material response.
In fact, each \gls{acr:MW} can be parametrized by a single parameter, its relaxation time~$\relaxationtime_{\loopindex} = \viscousmodulus_{\loopindex} / \youngsmodulus_{\loopindex}$, showing us that elasticity and viscosity are proportional to each other and may not be chosen uniquely without incorporating further data.
Since each \gls{acr:MW} reflects stress relaxation occurring at a given time instance~$\relaxationtime_{\loopindex}$ and vanishes before then \textIe vanishes for~$t < \relaxationtime_{\loopindex}$, the number of elements can be chosen from strain-step response given infinite observation of stress.
However, from a practical perspective, such very slow relaxation is not of major interest for the latter use of the proposed model since \caros will not remain steady for long terms.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--estimation--mechanical-parameters}
  \includegraphics[%
    ]{stress-strain/estimation/mechanical-parameters}
  \tikzexternaldisable%
  \caption[%
    Identified viscoelastic parameter of \acrlong{acr:GMW}.
  ]{%
    Elastic and viscous parameters of \gls{acr:GMW} model estimated over~$n_{\ms{trial}} = 10$ trials for~${n_{\msmaxwell} \in \numset{2;3;4}}$.
    Purely elastic behavior, parametrized solely by~$\youngsmodulus_{1}$ is estimated to~$\youngsmodulus_{1} \approxeq \SI{7}{\giga\pascal}$, conforming with manufacturers' data sheets.
    Further mechanical parameters~$\youngsmodulus_{\loopindex}$ and~$\viscousmodulus_{\loopindex}$ ~$\parentheses*{ \loopindex \in \numset{2;3;4} }$ are not available in data sheets.
    Numeric values are given in~\cref{tbl:stress-strain:results:estimation:number-of-elements:mechanical-parameters}.
    Note the first \gls{acr:MW} element contains no damper, which can be expressed by~$\viscousmodulus_{1} = \infty$.
  }
  \label{fig:stress-strain:results:estimation:number-of-elements:mechanical-parameters}
\end{figure}

It is clear from~\cref{fig:stress-strain:key-characteristics:strain-relaxation} that a single spring is not sufficiently covering the relaxing strain-step behavior.
As such, we investigate a Maxwell-chain material model with~${n_{\msmaterial} \in \numset{2;3;4}}$ such that at least one \gls{acr:MW} is included.
Incorporating one component, we see instantaneous relaxation as the strain-step ends, as well as the anticipated stress relaxation during constant strain~(see~\cref{fig:stress-strain:results:estimation:number-of-elements:varying-elements}).
However, the instantaneous peak of stress at~$t = \SI{45}{\second}$ as anticipated by the estimated transfer function does not reach the same absolute values\textemdash the error between measurement and model maximizes up to~$\Delta \cabforce = \SI{53.8}{\newton} \approxeq \SI{10.374084073}{\percent}$.
On the other hand, during instantaneous relaxation at~$t = \SI{90}{\second}$, the model predicts the cable force at a maximum error of~$\Delta \cabforce = \SI{-1.646}{\newton} \approxeq \SI{-41.088367449}{\percent}$, which is a non-negligible underestimation of the cable force.
Increasing the number of \gls{acr:MW} components to~$n_{\msmaxwell} = 3$ we obtain smooth approximation of the stress relaxation, in particular the dogleg after strain increase gets smoothed noticeably, decreasing the error of approximation and improving goodness of fit from~$\SI{95.7}{\percent}$ to~$\SI{98.1}{\percent}$.
Further increasing the number of \gls{acr:MW} elements does not improve goodness of fit noticeably, which is expected as the duration of the strain plateau is kept reasonably short with only~$t_{\ms{plateau}} = \SI{45}{\second}$.
With longer plateau times, the cable will tend to relax farther with an asymptotic response that will ultimately converge toward the initial tension implicating \gls{acr:MW} components with very low relaxation times~$\relaxationtime_{\loopindex} = \viscousmodulus_{\loopindex} / \youngsmodulus_{\loopindex}$.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{stress-strain--estimation--varying-nelements}
  \includegraphics[%
      width=\linewidth,%
      axisratio=2.00,%
    ]{stress-strain/estimation/varying-nelements}
  \tikzexternaldisable%
  \caption[%
    Estimated strain-step response of \acrlong{acr:MW}.
  ]{%
    Estimated strain-step response of \Dyneema~SK78 cable with number of \gls{acr:MW} varying from~$n_{\msmaxwell} = 2$ to~$n_{\msmaxwell} = 4$.
    Increasing the number of \gls{acr:MW} elements smooths the instantaneous step-response of the material at~$t_{\ms{step}} = \SI{45}{\second}$.
    Numerical values of parameters obtained from estimation are given in~\cref{tbl:stress-strain:results:estimation:number-of-elements:mechanical-parameters}.
  }
  \label{fig:stress-strain:results:estimation:number-of-elements:varying-elements}
\end{figure}

Comparing numerical values of elasticity and viscosity given in~\cref{tbl:stress-strain:results:estimation:number-of-elements:mechanical-parameters} with those by the manufacturer is doomed to fail due to the manufacturer providing only yield strengths and maximum strain under high load.
Since these values are obtained under different conditions and different scenarios\textemdash tensile strength and maximum strain are provided at break\textemdash their transferability seems very underhanded.
Results presented by other research groups like~\textcite{Piao.2017,Sanborn.2015,Vlasblom.2006,Lv.2017,OMasta.2014,Rost2013,Takata.2018}, though partially obtained with cables of different diameter, show both elastic and viscous parameters are within \SI{8}{\percent} of the data obtained in aforementioned contributions.
