%!TEX root=../../../../thesis.tex
%!TEX file=content/stress-strain-dynamics/results/estimation/constancy.tex

\subsubsection{Variation of Mechanical Properties}\label{sec:stress-strain:results:estimation:constancy}

In~\cref{sec:stress-strain:results:estimation:number-of-elements}, we Estimated viscoelastic material parameters of elasticity and viscosity over a set of~$n_{\ms{trial}} = 10$ trials \textIe estimation solved the least-squares problem minimizing the least squares problem from~\cref{eqn:stress-strain:procedure:fitting:least-squares-problem:trials}.
As such, the result is the best fit to a set of data implying averaged well fit.
In this section, we will focus on the constancy of mechanical properties by performing estimation on each of the~$n_{\ms{trial}} = 10$ trials one trial at a time.
If we find out that spread / standard deviation of mechanical parameters is comparatively low among trials we can conclude that only few experiments are needed to obtain a consistently parametrized model, whereas a large standard deviation would imply the opposite.
Further, small standard deviation in the mechanical parameters are favorable for on-line parameter estimation enabling us to incorporate an on-line estimation process like ARMAX into the robot.
ARMAX is a model for statistical analysis of time series based on autoregressive–moving-average models~(ARMA models) extending it by including also models with exogenous inputs model\textemdash in some software algorithms, exogenous inputs are also called independent inputs~\autocite{Whittle.1953,Hannan.1970,Whittle.1983}.
With ARMA and ARMAX models, time-series of data are regressed on their own past values~(AR component) in order to understand or predict future values in the series and linear modeling of the error term at coetaneous and previous time values~(MA component).

Instantaneous elasticity with median~$\median[\youngsmodulus]_{1} \approxeq \SI{7.238995}{\pascal}$ and small \nth{25} and \nth{75} percentile emerge, indicating very closely lying estimates of the purely elastic element even for every single trial~(data are tabulated in~\cref{tbl:stress-strain:results:estimation:constancy:mechpars_n2,tbl:stress-strain:results:estimation:constancy:mechpars_n3,tbl:stress-strain:results:estimation:constancy:mechpars_n4}, mean values are visualized in~\cref{fig:stress-strain:results:estimation:number-of-elements:mechanical-parameters}).
Variance~$\var[\youngsmodulus_{1}]$ does not increase significantly when adding more elements to the model indicating what is to be assumed: the instantaneous stress response is captured by a purely elastic response of the cable material.
With increasing number of elements, the median of the second elasticity~$\youngsmodulus_{2}$ does not change significantly, however, its variance increases as we go from a~$2$-element \gls{acr:GMW} to a~$4$-element \gls{acr:GMW}.
Since the measured stress response is the superposed relaxation of every single \gls{acr:MW} element, it makes distinguishing two elements more convoluted when only relatively short-in-time data are available.
We can similarly argue for the third elasticity~$\youngsmodulus_{3}$ for a \gls{acr:GMW} with~$n_{\msmaxwell} = 3$ and~$n_{\msmaxwell} = 4$ elements.
What needs to be pointed out though is the near-constancy of elastic components throughout adding more \gls{acr:MW} elements to the model.
In fact, a decay in elasticity~$\youngsmodulus_{2}$ is to be expected with increasing number of \gls{acr:MW} elements since each element represents a single relaxation time.
With more elements added, more stress relaxation transients can be approximated allowing for each element to converge closer to its specific relaxation time rather than being the least-squares fit over multiple relaxation times.

Viscous parameters~$\viscousmodulus_{\loopindex}$, on the other hand, show larger spread of data, which is to be anticipated since viscous parameters more largely affect the stress relaxation times~$\relaxationtime_{\loopindex} = \viscousmodulus_{\loopindex} / \youngsmodulus_{\loopindex}$.
With increasing number of \gls{acr:MW} elements, we observe a decrease in the second \gls{acr:MW} element's viscosity~$\viscousmodulus_{2}$, corresponding well with the model's assumption.
Assuming only one \gls{acr:MW} element, the material's stress relaxation has to be captured by a single relaxation time, which will ultimately, due to the estimation algorithm, lead to the parameter of viscosity being the least squares fit over all physical relaxations.
When more elements are added, further stress relaxations can be captured \textEg for a two-\gls{acr:MW} element material, the first \gls{acr:MW} element may be characteristic for instantaneous stress relaxation, whereas the second \gls{acr:MW} element is responsible for short-term stress relaxation.
Consequently, adding more \gls{acr:MW} elements allow for capturing more unique stress relaxations and preceding viscous parameters will decrease in their value.

Combining data from elastic and viscous parameters, we can confirm our assumptions that a material model with more \gls{acr:MW} elements allows for better approximation of multiple unique stress relaxations compared to a material model with fewer \gls{acr:MW} elements.
In the data given, for the~$n_{\msmaxwell} = 4$ \gls{acr:GMW}, we can see a decrease in the elastic parameters of each \gls{acr:MW} element while the viscous parameters increase for each \gls{acr:MW} element.
Recalling stress relaxation time of element~$\loopindex$ is defined through~$\relaxationtime_{\loopindex} = \viscousmodulus_{\loopindex} / \youngsmodulus_{\loopindex}$, a decreasing elastic and increasing viscous parameter relates to an increasing relaxation time.
