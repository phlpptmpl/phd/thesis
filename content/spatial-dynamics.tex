%!TEX root=../thesis.tex
%!TEX file=content/spatial-dynamics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Spatial Cable Dynamics}\label{chap:spatial-dynamics}

\input{content/spatial-dynamics/intro}

We want to describe the spatial dynamics of \caros%
\footnote{%
Spatial dynamics are not limited to \caros with more than one \gls{DOF}, they are also applicable to \MP{1} \caros.
An intuitive example of such \caros, though technically not fully coherent, is an elevator which experiences lateral cable vibration due to uneven guiding and uneven weight-distribution of the elevator car.%
} %
by providing a more detailed model for the dynamics of the force and motion transmitting elements.
Physically correct and meaningful, these cables ought be considered a continuum of certain unit density, length, cross-section, and possibly other mechanical properties like elasticity of viscosity.
On an even higher level of detail, modeling the cables can be split down to modeling every single fiber the cable is composed of, while each fiber may again be considered a continuum with former properties.
However, such models become highly complex as there is no readily applicable literature to describe the governing dynamics.
We limit the model to some key dynamics by wanting a cable that
%
\begin{enumerate}
  \item describes the large signal response\footnote{We may also use the term ``macroscale signal response'' for better understanding.} of the cable to a given distal point motion, as such it
  \item neglects internal kinematics and dynamics reaching from interaction of all fibers\footnote{As stated in the assumptions in~\cref{sec:introduction:problem-statement}, we consider the cable of full circular cross-section.}.
\end{enumerate}


One key challenge in modeling cables of \caros is their varying unstrained length between pulley leave point and platform anchor point, resulting in more involved equations.
Assuming the cable to be of constant unstrained length makes for simple dynamics as there is no mass flow resulting from cable coiling or uncoiling.
Such models, however, do not enable the \caro to be positioned in space as the cable length cannot be changed.
The only resulting change in spatial position of the mobile platform results from elastic strain of the cables and the response to external forces.
Their applicability to dynamics simulation is thus limited if not equivalent none, yet they may be used for kinetostatic analysis of the \caro under design.
Ultimately, we may also consider the cable of constant unstrained length between its very proximal point where it is physically attached to the winch and its very distal point where it is attached to the platform.
While this approach may seem less challenging to derive, it introduces more complicated phenomena such as unilateral interaction forces of the cables and its guiding structures, but also friction of the cable on the guiding pulleys and the winch.

Formulating these interactions requires finding values for mechanical parameters that are difficult to physically obtain like the coefficient of static and kinetic friction.
Without rigorous analysis of the material properties of the cables and all their contacting objects' materials, a thorough modeling of the contact mechanics and contact dynamics becomes a mathematically trivial task compared to finding the nominal parameter values.
\Citeauthor{Kraus.2015b,Choi.2017} have previously investigated pulley-cable friction~\autocite{Kraus.2015b} and pulley-bearing friction~\autocite{Choi.2017}, respectively, showing that Dahl friction more precisely captures the friction emerging in their respective demonstrators.
Additionally considering the cable's tendency to strain, the exact points of contact of the cable and its surrounding are subject to the kinematics and dynamics of the stress-strain model and as such more arduous to derive.

In this chapter, \cref{sec:spatial-dynamics:preliminaries:modeling-approaches} presents two different approaches for describing spatial cable dynamics as often found in literature.
Each of these models is based on different assumptions and results in differently complex formulations.
These key differences and model equations are pointed out, analyzed, and evaluated with regards to applicability to \caro simulation.
For further analysis of the cable model, \cref{sec:spatial-dynamics:preliminaries:comparison-model} will lay the analytical foundation of analyzing the later derived model's goodness of fit.
Following these sections will be the proposed cable model for \caro simulation for which we first state the kinematics in~\cref{sec:spatial-dynamics:kinematics}, then present one way to obtain the equations of motion in~\cref{sec:spatial-dynamics:equations-of-motion}, followed by the discretization approach of Rayleigh-Ritz in~\cref{sec:spatial-dynamics:discretization}, needed to obtain a finite set of equations.
As we make use of Rayleigh-Ritz, we present the selection of bases functions in~\cref{sec:spatial-dynamics:basis-functions}, allowing us to provide a purely geometric description of the cable shape.
Subsequent~\cref{sec:spatial-dynamics:natural-frequencies,sec:spatial-dynamics:statics} present, respectively, numerical comparison of the model with its analytical counterpart, as well as the statics solution in comparison to the Irvine cable model from~\cref{sec:fundamentals:inverse-kinematics:mechanical}.
\Cref{sec:spatial-dynamics:numerical-complexity} gives some insight into the numerical complexity of the proposed model, while~\cref{sec:spatial-dynamics:conclusions} concludes the chapter with closing remarks.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/preliminaries}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/kinematics}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/discretization}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/basis-functions}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/natural-frequencies}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/statics}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/dynamics}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/numerical-complexity}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/conclusions}
