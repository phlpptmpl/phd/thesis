%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/bases-functions/b-splines.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{B-Splines}\label{sec:cable-dynamics:rayleigh-ritz:bases-functions:bsplines}

A more general approach to composite curves are B-splines, or basis splines, are spline functions defined on only a local interval and adjoined at the interval boundaries, so-called knots.
Each spline control point is associated with a basis function such that
%
\begin{align}
  \vect{C} \of{\pathcoord}
    & = \sum\limits_{\funcindex = 0}^{\numctrlpts}{
          \Pi_{\funcindex,\polydeg} \of{\pathcoord} \,
          \ctrlpoint_{\funcindex}
        } \,
        ,
        \label{eqn:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:general-spline}
\end{align}
%
in which~$\Pi_{\funcindex,\polydeg} \of{\pathcoord}$ is the~$\funcindex$-th basis function of polynomials of degree~$\polydeg$ and~$\ctrlpoint_{\funcindex}$ is the associated control point.
With B-splines, the degree~$\polydeg$ of the bases functions is not dependent of the number of control points~$\numctrlpts$\textemdash which is the case for \beziercurves where~$\polydeg = \numctrlpts - 1$.
\Cref{eqn:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:general-spline} defines a general 
polynomial spline which can be chosen with an arbitrary degree per basis function.
Each basis function is a recursive formulation of linear combinations of lower-dimensional bases functions on the local interval of validity.
In general, spline functions can be of any sort as long as they satisfy the requirements of spline functions, the most common spline functions however are lineary interpolation functions, but alo hyperbolic ones~\autocite{Lu.2002} or logarithmic ones~\autocite{Schroppel.2016} exist.
The recursive formulation for the~$\funcindex$-th spline basis function~$\Pi_{\funcindex,0} \of{\pathcoord}$ is given as
%
\begin{subequations}\label{eqn:cable-dynamics:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:recursive-formula}
\begin{align}
  \Pi_{\funcindex,0} \of{\pathcoord}
    & = \begin{cases}
          1
            & \text{if } \pathcoord \in \interval[open right]{\pathcoord_{\funcindex}}{\pathcoord_{\funcindex + 1}} \,
            ,
            \\
          0
            & \text{else,}
        \end{cases} \,
        ,
    \\
  \shapefun_{\funcindex,\polydeg} \of{\pathcoord}
    & = \frac{
          \pathcoord
          -
          \pathcoord_{\funcindex}
        }{
          \pathcoord_{\funcindex + \polydeg}
          -
          \pathcoord_{\funcindex}
        } \,
        \Pi_{\funcindex, \polydeg - 1} \of{\pathcoord}
        +
        \frac{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord
        }{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord_{\funcindex + 1}
        } \,
        \Pi_{\funcindex + 1, \polydeg - 1} \of{\pathcoord} \,
        ,
\end{align}
\end{subequations}
%
which is the Cox-de Boor recursion formula~\autocite{Piegl.1997}.
Any B-spline of degree~$\polydeg$ is composed of piecewise polynomial functions in~$\pathcoord$ of degree~$\polydeg - 1$.
These piecewise functions are defined over only~$\polydeg + 1$ points~$\pathcoord_{j}$, called knots, which are collected in the nondecreasing knot vector~$\knotvector$.
This also implies that, contrary to \beziercurves, each B-spline contributes only locally in the range between its first and last knots.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--bases-functions--blending-functions}
  \includegraphics[
      % width=0.75\linewidth,%
      % height=0.50\textheight,%
    ]{cosserat-rod/bsplines/bases-functions/blending-functions}
  \tikzexternaldisable%
  \caption{%
    Blending functions~$\shapefun_{k,\polydeg}$,~$\parentheses{\irange{\polydeg}{k}}$ of a linear B-spline with~$\numsegments = 5$ segments and local polynomial degree~$\polydeg = 3$.%
  }
\end{figure}

The~$j$-th derivative a B-spline can be found by induction and is given purely for consistency and completeness to
%
\begin{align*}
  \od[j]{
  }{
    \pathcoord
  }
    & = \frac{
          \polydeg
        }{
          \pathcoord_{\funcindex + \polydeg}
          -
          \pathcoord_{\funcindex}
        } \,
        \od[j - 1]{
        }{
          \pathcoord
        } \,
        \Pi_{\funcindex, \polydeg - 1} \of{\pathcoord}
        -
        \frac{
          \polydeg
        }{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord_{\funcindex + 1}
        } \,
        \od[j-1]{
        }{
          \pathcoord
        } \,
        \Pi_{\funcindex + 1, \polydeg - 1} \of{\pathcoord} \,
        ,
\end{align*}
%
which holds for all~$j > 0$, otherwise~\cref{eqn:cable-dynamics:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:recursive-formula} holds.


\amend[inline]{Need to talk about the weighted/rationality of the used B-splines as well as introduce the number of segments~$\numsegments$, polynomial degree~$\polydeg$, and number of control points~$\numctrlpts$, especially how they are connected through~$\numctrlpts = \numsegments + \polydeg$.}
