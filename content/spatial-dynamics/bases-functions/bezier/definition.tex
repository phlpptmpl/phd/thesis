%!TEX root=../../../../thesis.tex
%!TEX file=content/spatial-dynamics/bases-functions/bezier/definition.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{bezier-curve}
  \includegraphics[
      % width=0.75\linewidth,%
      height=0.25\textheight,%
    ]{bezier-curve}
  \tikzexternaldisable%
  \caption{
    A cubic \beziercurve~(blue) with four control points~$\ctrlpoint_{0}$ through~$\ctrlpoint_{3}$ and their convex hull~(dashed).
  }
  \label{fig:spatial-dynamics:bases-functions:bezier:bezier-curve}
\end{figure}

Used in computer graphics, a \beziercurve~(see~\cref{fig:spatial-dynamics:bases-functions:bezier:bezier-curve}) is simply a parametric polynomial curve in space\textemdash either 2D or 3D\textemdash with Bernstein polynomials of certain degree as its basis.
For any polynomial of degree~$\polydeg$, a \beziercurve is defined as
%
\begin{align}
  \vect{C}_{\funcindex,\polydeg} \of{ \pathcoord }
    & = \sum\limits_{\funcindex = 0}^{\polydeg}{
          \binom{\polydeg}{\funcindex} \,
          \pow[\funcindex]{
            \pathcoord
          } \,
          \pow*[\polydeg - \funcindex]{
            1 - \pathcoord
          } \,
          \ctrlpoint_{\funcindex}
        } \,
      ,
    &
    & 0 \leq \pathcoord \leq 1 \,
      ,
      \label{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:definition-explicit}
\end{align}
%
where~$\binom{\polydeg}{\funcindex} = \frac{\factorial{\polydeg}}{\factorial{\funcindex} \factorial{\parentheses{\polydeg - \funcindex}}}$ are the binomial coefficients and~$\ctrlpoint_{\funcindex}$ are the so-called control points of the \beziercurve.
It must be noted that the control points do not in general lie on the \beziercurve but are merely defining the convex hull of the \beziercurve~(see also~\cref{fig:spatial-dynamics:bases-functions:bezier:bezier-curve}).
In~\cref{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:definition-explicit}, the summands are the~$\funcindex$-th Bernstein polynomials of degree~$\polydeg$ \textIe
%
\begin{align}
  b_{\funcindex,\polydeg} \of{ \pathcoord }
    & = \binom{\polydeg}{\funcindex} \,
        \pow[\funcindex]{
          \pathcoord
        } \,
        \pow*[\polydeg - \funcindex]{
          1 - \pathcoord
        } \,
      .
      \label{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:bernstein}
\end{align}
%
\Cref{fig:spatial-dynamics:bases-functions:bezier:bezier-curve:bases-functions} shows the~$\polydeg + 1$ Bernstein basis polynomials for~$\polydeg = 5$, as well as their first two derivatives.
The space of unity as well as positiveness make them an orthonormal polynomial basis with all its properties.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bezier--bases-functions--bases}
  \includegraphics[
      % width=1.00\linewidth,%
      % height=0.25\textheight,%
    ]{cosserat-rod/bezier/bases-functions/bases}
  \tikzexternaldisable%
  \caption[%
    Bernstein polynomial of degrees~$\polydeg \in \numset{1;2;3}$.%
  ]{%
    Bernstein polynomial of degrees~$\polydeg \in \numset{1;2;3}$ interpolated over path coordinate~${\xi \in \interval{0}{1}}$.
  }
  \label{fig:spatial-dynamics:bases-functions:bezier:bezier-curve:bases-functions}
\end{figure}

One can see that~$\polydeg$-th degree \beziercurves are linear interpolations of corresponding points in two~$\polydeg - 1$-th degree \beziercurves such that a recursive formulation may be given
%
\begin{align}
  \vect{C}_{\funcindex,\polydeg} \of{\pathcoord}
    & = \parentheses{
          1
          -
          \pathcoord
        } \,
        \vect{C}_{\funcindex, \polydeg - 1} \of{\pathcoord}
        +
        \pathcoord \,
        \vect{C}_{\funcindex - 1, \polydeg - 1} \of{\pathcoord} \,
        ,
        \label{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:definition-recursive}
\end{align}
%
in which we use
%
\begin{align*}
  \ctrlpoint_{0,0}
    & \coloneqq \ones \,
        ,
    & \ctrlpoint_{\funcindex < 0, \polydeg}
    & \coloneqq \zeros \,
        ,
    & \ctrlpoint_{\funcindex > \polydeg, \polydeg}
    & \coloneqq \zeros \,
        .
\end{align*}

In order to employ \beziercurves in our cable model, we require not only the curve shape, but also their first and second derivative \textWrt~$\pathcoord$~(see~\cref{eqn:global-rayleigh-ritz:director-1,eqn:global-rayleigh-ritz:director-2}).
As such, we can make use of the recursive formula for the~$d$-th derivative of \beziercurves reading
%
\begin{align}
  \vect{C}_{\funcindex,\polydeg}^{\parentheses{\numctrlpts}}
    & = \numctrlpts \,
        \parentheses*{
          \vect{C}_{\funcindex - 1, \polydeg - 1}^{\parentheses{\numctrlpts - 1}} \of{ \pathcoord }
          -
          \vect{C}_{\funcindex, \polydeg - 1}^{\parentheses{\numctrlpts - 1}} \of{ \pathcoord }
        } \,
        ,
        \label{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:derivative:definition-recursive}
\end{align}
%
valid for~$d > 0$, else~\cref{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:definition-recursive} holds.
Proof of~\cref{eqn:spatial-dynamics:rayleig-ritz:bases-functions:bezier-curve:derivative:definition-recursive} can be shown through strong induction and is left to the novice reader.

\subsubsection*{Composite \Bezier Curves}\label{sec:spatial-dynamics:rayleig-ritz:bases-functions:composite-bezier}

Composite \beziercurves, also referred to as \beziersplines, are compose of piecewise \beziercurve resulting in a continuous curve at the joints.
Simply put, a series of \beziercurves are joined end to end where one curve's end point is the start point of another curve.
Different smoothness requirements like~$C^{1}$ or~$C^{2}$, may be imposed depending on the requirements.
When joining \beziercurves to form composite \beziersplines, the continuity and smoothness requirements can be translated into equalities\textemdash or equations\textemdash for the control points.
Assuming at least~$C^{0}$ continuity of two \beziercurves~$\vect{C}^{\parentheses{0}}$ and~$\vect{C}^{\parentheses{1}}$~(both of same degree~$\polydeg$, each with control points~$\ctrlpoint_{k}^{\parentheses{1}}$,~$\ctrlpoint_{k}^{\parentheses{2}}$, respectively) requires
%
\begin{align*}
  \vect{C}^{\parentheses{0}} \of{\pathcoord^{\parentheses{0}} = 1}
    & = \vect{C}^{\parentheses{1}} \of{\pathcoord^{\parentheses{1}} = 0} \,
    \\
\shortintertext{or equivalently}
  \ctrlpoint_{\polydeg + 1}^{\parentheses{0}}
    & = \ctrlpoint_{0}^{\parentheses{1}} \,
        ,
\end{align*}
%
to be satisfied, that is the last control point of the first \beziercurve must be the same as the first point of the second \beziercurve\textemdash which is to be expected intuitively.
Requiring~$C^{1}$ continuity leads to necessity of having the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{1}^{\parentheses{1}}
\end{align*}
%
be collinear.
Similarly,~$C^{2}$ continuity requires the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg - 1}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{1}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{2}^{\parentheses{1}}
\end{align*}
%
to be collinear.
Consequently, for~$C^{j}$ continuity, the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg - j + 1}^{\parentheses{0}} \,
  ,
  \ctrlpoint_{\polydeg - j}^{\parentheses{0}} \,
  ,
  \dots
  ,
  \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
  ,
  \ctrlpoint_{1}^{\parentheses{1}} \,
  ,
  \dots
  ,
  \ctrlpoint_{j}^{\parentheses{1}}
\end{align*}
%
must be collinear.
Following this recursive definition, it is apparent that higher continuity levels require a higher polynomial degree of the connecting \beziercurves, as the polynomial degree is directly linked to the number of control points with~$\polydeg = \numctrlpts - 1$.
In addition, while the constraints resulting from continuity may be integrated into the dynamics model by either further holonomic constraints, they may also be incorporated into the equations for the coefficients themselves rendering some generalized coordinates redundant.
Since every \Bezier control point has global support\textemdash in our case the \Bezier control points translate to be the generalized coordinates \textIe \glspl{DOF} of our system\textemdash, requiring higher continuity of the curves causing a kinematically tightly constrained system.
