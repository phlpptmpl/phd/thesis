%!TEX root=../../../../thesis.tex
%!TEX file=content/spatial-dynamics/bases-functions/lagrange/definition.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Given a set of~$\polydeg + 1$ tuples~$\parentheses*{\pathcoord_{\funcindex}, y_{\funcindex}}$, with no two~$\pathcoord_{\funcindex}$ equal, the Lagrange interpolating polynomial reads
%
\begin{align}
  L \of{ \pathcoord }
    & = \sum\limits_{\funcindex = 0}^{\polydeg}{
          l_{\funcindex} \of{\pathcoord} \,
          y_{\funcindex}
        } \,
        ,
        \label{eqn:spatial-dynamics:bases-functions:lagrange:definition:one-dim-interpolating-form}
    \\
  \shortintertext{with the Lagrange basis polynomials}
  l_{\funcindex} \of{\pathcoord}
    & \coloneqq \prod\limits_{\substack{0 \leq m \leq \polydeg\\m \neq \funcindex}}^{}{
          \frac{
            \pathcoord
            -
            \pathcoord_{m}
          }{
            \pathcoord_{\funcindex}
            -
            \pathcoord_{m}
          }
        } \,
        ,
        \label{eqn:spatial-dynamics:bases-functions:lagrange:definition:basis-polynomial}
\end{align}
%
for~$0 \leq \funcindex \leq \polydeg$~(see~\cref{fig:spatial-dynamics:bases-functions:lagrange:definition:bases} for the Lagrange polynomials of degrees~$\polydeg \in \numset{1;2;4}$).
Such polynomials are used for interpolation of data sets where the Lagrange polynomial is the polynomial of lowest degree that passes through each data set~$\parentheses*{\pathcoord_{\funcindex}, y_{\funcindex}}$ exactly.
\Cref{eqn:spatial-dynamics:bases-functions:lagrange:definition:basis-polynomial} is well-defined for all~$\pathcoord_{\funcindex}$ since no two of them are the same \textIe~$\pathcoord_{\funcindex} - \pathcoord_{l} \neq 0$~$\parentheses*{\funcindex \neq l}$.
From this follows also that all basis polynomials are equal zero at~$\pathcoord_{\funcindex}$ \textIe~$l_{\funcindex} \of{\pathcoord{\funcindex}} \equiv 0$~$\parentheses*{j \neq \funcindex}$, however, polynomial~$l_{\funcindex} \of{\pathcoord_{\funcindex}}$ is the only nonzero polynomial and, due to the weighting it follows that~$l_{\funcindex} \of{\pathcoord_{\funcindex}} \equiv 1$ meaning the Lagrange polynomial exactly interpolates\footnote{This is merely a feature of convenience, especially when comparing Lagrange polynomials to \textEg \beziercurves.
In fact, Lagrange polynomials are interpolating polynomials whereas \beziercurves are only approximating functions.} the function defined through the~$y_{\funcindex}$.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--lagrange--bases-functions--bases}
  \includegraphics[
      % width=1.00\linewidth,%
      % height=0.25\textheight,%
    ]{cosserat-rod/lagrange/bases-functions/bases}
  \tikzexternaldisable%
  \caption[%
    Lagrange polynomial of degrees~$\polydeg \in \numset{1;2;3}$.%
  ]{
    Lagrange polynomial of degrees~$\polydeg \in \numset{1;2;3}$ evaluated over path coordinate~${\pathcoord \in \interval{0}{1}}$.
  }
  \label{fig:spatial-dynamics:bases-functions:lagrange:definition:bases}
\end{figure}

Using the one-dimensional expression of the Lagrange interpolating polynomial from~\cref{eqn:spatial-dynamics:bases-functions:lagrange:definition:one-dim-interpolating-form}, we can create a smooth curve in either 2D or 3D when assuming the~$y_{\funcindex}$ being control points~$\ctrlpoint_{\funcindex} \in \mathds{R}^{3}$ such that
%
\begin{align*}
  \vect{C} \of{\pathcoord}
    & = \sum\limits_{\funcindex = 0}^{\numctrlpts}{
          l_{\funcindex} \of{\pathcoord} \,
          \ctrlpoint_{\funcindex}
        } \,
        ,
\end{align*}
%
where the number of control points~$\numctrlpts$ implies the polynomial degree.
This approach is very similar to the ones following in this section as it is a very common procedure to define smooth spatial curves in this manner.

By choosing Lagrange's interpolating polynomial as the set of bases functions, we can define a knot vector~$\knotvector$ that contains a uniformly or nonuniformly spaced vector along the path coordinate of the cable reading
%
\begin{align*}
  \knotvector
    & = \transpose{
          \begin{bmatrix}
            0 \, ,
              & h \, ,
              & 2 \, h \, ,
              & \dots
              & \funcindex \, h \, ,
              & \dots
              & \polydeg \, h
          \end{bmatrix}
        } \,
\end{align*}
%
for~$0 \leq \funcindex \leq \polydeg$ and the step increment~$h = 1 / \parentheses*{\numctrlpts + 1 }$.

The first two derivatives with respect to~$\pathcoord$ of the Lagrange basis polynomial of degree at least~$\polydeg \geq 3$ read
%
\begin{align*}
  \p{l_{\funcindex}} \of{\pathcoord}
    & \coloneqq \sum\limits_{i = 0, i \neq \funcindex}^{k}{
        \parentheses*{
          \frac{
            1
          }{
            x_{\funcindex}
            -
            x_{i}
          } \,
          \prod\limits_{m = 0, m \neq \parentheses{i , \funcindex}}^{k}{
            \frac{
              x
              -
              x_{m}
            }{
              x_{\funcindex}
              -
              x_{m}
            }
          }
        }
      } \,
      ,
      \\
  \pp{l_{\funcindex}} \of{\pathcoord}
    & \coloneqq \sum\limits_{i = 0, i \neq \funcindex}^{k}{
        \frac{
          1
        }{
          x_{\funcindex}
          -
          x_{i}
        } \,
        \parentheses*{
          \sum\limits_{m = 0, m \neq \parentheses{i, \funcindex}}^{k}{
            \parentheses*{
              \frac{
                1
              }{
                x_{j}
                -
                x_{m}
              } \,
              \prod\limits_{k = 0, k \neq \parentheses{i, \funcindex, m}}^{k} {
                \frac{
                  x
                  -
                  x_{k}
                }{
                  x_{\funcindex}
                  -
                  x_{k}
                }
              }
            }
          }
        }
      }
        \,
      ,
\end{align*}
%
where higher order derivatives can be calculated form recursion.
