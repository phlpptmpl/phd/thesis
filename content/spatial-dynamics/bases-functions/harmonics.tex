%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/bases-functions/harmonics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Harmonics}\label{sec:cable-dynamics:rayleig-ritz:bases-functions:harmonics}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}{0.315\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{cosserat-rod--harmonics--bases-functions--basis-d0}
    \includegraphics[
        % width=\linewidth,
        % height=0.3\textheight,%
      ]{cosserat-rod/harmonics/bases-functions/basis-d0}
    \tikzexternaldisable%
    \caption{
      Harmonics bases functions~$b_{\funcindex,\polydeg} \of{\pathcoord}$,~$\parentheses*{\irange{\polydeg}{\funcindex}}$ of degree~$\polydeg = 3$ and their total sum.
    }
    \label{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-0}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}{0.315\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{cosserat-rod--harmonics--bases-functions--basis-d1}
    \includegraphics[
        % width=\linewidth,
        % height=0.3\textheight,%
      ]{cosserat-rod/harmonics/bases-functions/basis-d1}
    \tikzexternaldisable%
    \caption{
      First derivative of harmonics bases functions~$b_{\funcindex,\polydeg}^{\prime} \of{\pathcoord}$,~$\parentheses*{\irange{\polydeg}{\funcindex}}$ of degree~$\polydeg = 3$.
    }
    \label{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-1}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}{0.315\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{cosserat-rod--harmonics--bases-functions--basis-d2}
    \includegraphics[
        % width=\linewidth,
        % height=0.3\textheight,%
      ]{cosserat-rod/harmonics/bases-functions/basis-d2}
    \tikzexternaldisable%
    \caption{
      Second derivative of harmonics bases functions~$b_{\funcindex,\polydeg}^{\prime\prime} \of{\pathcoord}$,~$\parentheses*{\irange{\polydeg}{\funcindex}}$ of degree~$\polydeg = 3$.
    }
    \label{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-2}
  \end{subfigure}
  \caption{
    Harmonics basis fucntions of degree~$\polydeg = 3$~\subref{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-0}, its first derivatives~\subref{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-1}, and second derivatives~\subref{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:bases-functions:derivative-2}.
  }
  \label{fig:cable-dynamics:rayleigh-ritz:bases-functions:harmoncis:basis-functions}
\end{figure}

Normalized harmonic shape functions are very often used in case of elastic deformation and stems from it being employed by Rayleigh and Ritz first for the static deflection of a beam~\autocite{Leissa.2005}.
As such, using harmonic shape functions as bases functions for the cable model suggests itself.

We define the~$\polydeg$-th degree harmonic shape function using basic trigonometric functions to read
%
\begin{align*}
  b_{\funcindex, \polydeg} \of{\pathcoord}
    & = \psin*{
          \frac{
            \funcindex \,
            \pi
          }{
            2
          } \,
          \pathcoord
        }
        +
        \pcos*{
          \frac{
            \funcindex \,
            \pi
          }{
            2
          } \,
          \pathcoord
        } \,
        ,
\end{align*}
%
for~$\pathcoord \in \interval{0}{1}$, with factor~$\frac{\funcindex \, \pi}{2}$ used for scaling, and valid for~$\irange{\polydeg}{\funcindex}$.
From this follows directly in a linear combination of control points~$\ctrlpoint_{\funcindex}$ the spatial smooth curve representing the cable
%
\begin{align}
  \vect{C}_{\polydeg} \of{\pathcoord}
    & = \ctrlpoint_{0}
        +
        \sum\limits_{\funcindex = 1}^{\polydeg}{
          \psin*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex}
          +
          \pcos*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex + d}
        } \,
        .
        \label{eqn:cable-dynamics:rayleig-ritz:bases-functions:harmonics:definition}
\end{align}
%
The unweighted control point~$\ctrlpoint_{0}$ allows for placing the initial point of the cable at arbitrary positions in space which were otherwise not possible with conventional harmonic shape functions.
In addition to the basis function for the smooth curve, we also require a formulation of the first and second derivative, which can be obtained straightforward from~\cref{eqn:cable-dynamics:rayleig-ritz:bases-functions:harmonics:definition} yielding
%
\begin{align*}
  \vect{C}_{\polydeg}^{\prime} \of{\pathcoord}
    & = \ctrlpoint_{0}
        +
        \sum\limits_{\funcindex = 1}^{\polydeg}{
          \frac{
            \pi
          }{
            2
          } \,
          \pcos*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex}
          -
          \frac{
            \pi
          }{
            2
          } \,
          \psin*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex + d}
        } \,
        ,
    \\
  \vect{C}_{\polydeg}^{\prime\prime} \of{\pathcoord}
    & = \ctrlpoint_{0}
        +
        \sum\limits_{\funcindex = 1}^{\polydeg}{
          -
          \pow*[2]{
            \frac{
              \pi
            }{
              2
            }
          } \,
          \psin*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex}
          -
          \pow*[2]{
            \frac{
              \pi
            }{
              2
            }
          } \,
          \pcos*{
            \frac{
              \funcindex \,
              \pi
            }{
              2
            } \,
            \pathcoord
          } \,
          \ctrlpoint_{\funcindex + d}
        } \,
        .
\end{align*}
