%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/basis-functions.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Selection of Bases Functions}\label{sec:spatial-dynamics:basis-functions}

The equation of motion from~\cref{eqn:spatial-dynamics:discretization:forces} derived in~\cref{sec:spatial-dynamics:equations-of-motion} and discretized in~\cref{sec:spatial-dynamics:discretization} are widely applicable to the simulation of Cosserat rods and cables.
We have not made any assumptions as to what the kinematic constraints of the system are in terms of shape-describing functions~$\shapefuns \of{\pathcoord}$.
In other words, the kinematics of the cable are defined allowing us to determine forces based on cable displacement and deformation.
A geometric representation of the cable is yet to be found that, together with the kinematics of the cable, allow for the spatial description of the cable undergoing displacement and deformation\footnote{In other words, previous sections presented a cable formulation on the level of forces.
This section presents a purely geometric description of the cable without considering any mechanics or kinematics.
It is only due to injecting the discretization of~\cref{sec:spatial-dynamics:discretization} that we can link a geometric formulation of the cable shape with its kinematic formulation.
Down the road, we cannot independently observe the geometric description of the cable without respecting its kinematics.
It may be clear to the experienced reader that a geometric curve cannot be aware of its elasticity whereas an elastic deformation is unaware of its underlying geometric reasoning.}.
In this section, we present and evaluate the set of bases functions that produce a low-dimensional yet accurate approximation of the most vital characteristics of the cable: natural frequencies and static response.

A straightforward formulation of the shape functions can be found in a polynomial basis such that~$\shapefuns_{\funcindex} = \pow[\funcindex]{\pathcoord}$ for~$\funcindex \in \interval{0}{\numshapefuns}$.
\Citeauthor{AyalaCuevas.2018} employed the same technique in their flexible yet inelastic cable model, however, truncated the series at~$\numshapefuns \equiv 2$~\autocite{AyalaCuevas.2018}.
As will be shown later, second-order polynomials yield no good approximation of the resulting cable shape, which may also be inferred from~\citeauthor{Irvine.1974}'s cable model~(see~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}).
A similar approach, though for simulation of stress waves inside the cable was followed by~\citeauthor{Godbole.2018} who used harmonics as their bases functions~\autocite{Godbole.2018}.
To describe the longitudinal vibration characteristics, \citeauthor{Godbole.2018} designed their harmonic bases functions scaled to unit length such that
%
\begin{align*}
  \shapefuns_{\funcindex}
    & = \psin*{
          \frac{
            \parentheses{
              \funcindex
              +
              1
            } \,
            \pi
          }{
            2
          }
        } \,
        .
\end{align*}
%
With appropriate scaling to the current cable length, high-frequency cable stress oscillation can be simulated affecting the position of the mass attached to the cable distal end.
\citeauthor{Zhang.2016} presented promising results on the statics analysis of a beam subject to external loads, deformation forces, and support along its chord~\autocite{Zhang.2016}.
Their discretization is based on~\gls{NURBS} for both the linear displacement field of a beam as well as the rotational displacement field.
Similarly, the kinematics are derived using Cosserat's rod theory, however, no dynamic forces nor forward dynamics simulation is performed.

It suggests itself to use an orthogonal basis composed of univariate polynomials as shape functions \textEg~$\set{ 1, x, \parentheses*{3 \, \pow{x} - 1} / 2, \dotsc}$, as this is common procedure in interpolation and approximation of data sets\footnote{In interpolation, points of the basis figure are located on the created figure whereas in approximation, points on the basis figure need not be on the created figure but in some proximity.} using \textEg Lagrange interpolation, Hermite interpolation, or Newton's series~\autocite{Funaro.1992,Farouki.2003,Davis.1975,Greenstein.1965,Phillips.2003}.
When it comes to solving differential equations, the homogeneous solution can be obtained by an exponential description, which in itself is simply an infinite sum of~(scaled) monomials.
As such, from a mathematical point of view, we may directly inject polynomial bases into the solution of differential equations, as performed by~\citeauthor{IdreesBhatti.2007} who used Bernstein basis polynomials for solutions of differential equations~\autocite{IdreesBhatti.2007}.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{bezier-curve}
  \includegraphics[
      % width=0.75\linewidth,%
      height=0.25\textheight,%
    ]{bezier-curve}
  \tikzexternaldisable%
  \caption[%
      Cubic \beziercurve with control points and convex hull.
    ]{%
      A cubic \beziercurve~(blue) with four control points~$\ctrlpoint_{0}$ through~$\ctrlpoint_{3}$ and their convex hull~(dashed).
    }
  \label{fig:spatial-dynamics:bases-functions:bezier:curve}
\end{figure}

A set of bases functions often found in the analysis of mechanical structures is composed of Bernstein polynomials, which in the context of spatial curves can be found in computer graphics, where they are called \beziercurves.
Used in computer graphics, a \beziercurve~(see~\cref{fig:spatial-dynamics:bases-functions:bezier:curve}) is simply a parametric polynomial curve in space\textemdash either 2D or 3D\textemdash with Bernstein polynomials of certain degree as its basis.
For any polynomial of degree~$\polydeg$, a \beziercurve is defined as
%
\begin{align}
  \vect{C}_{\funcindex,\polydeg} \of{ \pathcoord }
    & = \sum\limits_{\funcindex = 0}^{\polydeg}{
          \binom{\polydeg}{\funcindex} \,
          \pow[\funcindex]{
            \pathcoord
          } \,
          \pow*[\polydeg - \funcindex]{
            1 - \pathcoord
          } \,
          \ctrlpoint_{\funcindex}
        } \,
      ,
    &
    & 0 \leq \pathcoord \leq 1 \,
      ,
      \label{eqn:spatial-dynamics:bases-functions:bezier:definition-explicit}
\end{align}
%
where~$\binom{\polydeg}{\funcindex} = \frac{\factorial{\polydeg}}{\factorial{\funcindex} \factorial{\parentheses{\polydeg - \funcindex}}}$ are the binomial coefficients and~$\ctrlpoint_{\funcindex}$ are the so-called control points of the \beziercurve.
It must be noted that the control points do not in general lie on the \beziercurve but are merely defining the convex hull of the \beziercurve~(see also~\cref{fig:spatial-dynamics:bases-functions:bezier:curve}).
In~\cref{eqn:spatial-dynamics:bases-functions:bezier:definition-explicit}, the summands are the~$\funcindex$-th Bernstein polynomials of degree~$\polydeg$ \textIe
%
\begin{align}
  b_{\funcindex,\polydeg} \of{ \pathcoord }
    & = \binom{\polydeg}{\funcindex} \,
        \pow[\funcindex]{
          \pathcoord
        } \,
        \pow*[\polydeg - \funcindex]{
          1 - \pathcoord
        } \,
      .
      \label{eqn:spatial-dynamics:rayleig-ritz:bezier:bernstein}
\end{align}
%
\Cref{fig:spatial-dynamics:bases-functions:bezier:bezier:bases-functions} shows~$\polydeg + 1$ Bernstein basis polynomials for several polynomial degrees~${\polydeg \in \numset{1;2;3}}$ over their interval of validity~$\pathcoord \in \domainpathcoord = \interval{0}{1}$.
Bernstein basis polynomials satisfy positiveness~$b_{\funcindex, \polydeg} \of{\pathcoord} \geq 0$, symmetry~$b_{\funcindex, \polydeg} \of{\pathcoord} = b_{\polydeg - \funcindex, \polydeg} \of{\pathcoord}$, and form a partition of unity~$\sum b_{\funcindex, \polydeg} \of{\pathcoord} = 1$ on interval~$\interval{0}{1}$.
Further useful properties are also satisfied, yet not discussed in detail here as these are of less significance to the applied use case.
The interested reader is referred to~\textcite{Farouki.2012,Dadkhah.2015,Bellucci.2014}.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bezier--bases-functions--bases}
  \includegraphics[
      % width=1.00\linewidth,%
      % height=0.25\textheight,%
    ]{cosserat-rod/bezier/bases-functions/bases}
  \tikzexternaldisable%
  \caption[%
      Bernstein polynomials of various degree.
    ]{%
      Bernstein polynomials of degrees~$\polydeg \in \numset{1;2;4}$ interpolated over path coordinate~${\pathcoord \in \interval{0}{1}}$.
    }
  \label{fig:spatial-dynamics:bases-functions:bezier:bezier:bases-functions}
\end{figure}

While we may add arbitrarily many control points~$\ctrlpoint_{\funcindex}$ to the \beziercurve to increase its goodness of interpolation, it entails introducing further kinematic constraints into the system as well as numerical instabilities.
In addition, high polynomial degrees are not necessarily required from the kinematic definition of our cable\textemdash we merely require the curve to be~$\curve \in C^{3}$ in order to have at least continuous torsion.
As such, we may employ composite \beziercurves where we adjoin several \beziercurves at their respective end and beginning.

Assuming at least~$C^{0}$ continuity of two \beziercurves~$\vect{C}^{\parentheses{0}}$ and~$\vect{C}^{\parentheses{1}}$~(both of same degree~$\polydeg$, each with control points~$\ctrlpoint_{k}^{\parentheses{1}}$,~$\ctrlpoint_{k}^{\parentheses{2}}$, respectively) requires
%
\begin{align*}
  \vect{C}^{\parentheses{0}} \of{\pathcoord^{\parentheses{0}} = 1}
    & = \vect{C}^{\parentheses{1}} \of{\pathcoord^{\parentheses{1}} = 0} \,
    \\
\shortintertext{or equivalently}
  \ctrlpoint_{\polydeg + 1}^{\parentheses{0}}
    & = \ctrlpoint_{0}^{\parentheses{1}} \,
        ,
\end{align*}
%
to be satisfied, that is the last control point of the first \beziercurve must coincide with the first point of the second \beziercurve\textemdash which is to be expected intuitively.
Requiring~$C^{1}$ continuity leads to necessity of having the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{1}^{\parentheses{1}}
\end{align*}
%
collinear.
Similarly,~$C^{2}$ continuity requires the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg - 1}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg}^{\parentheses{0}} \,
    ,
    \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{1}^{\parentheses{1}} \,
    ,
    \ctrlpoint_{2}^{\parentheses{1}}
\end{align*}
%
to be collinear.
Consequently, for~$C^{j}$ continuity, the vertices
%
\begin{align*}
  \ctrlpoint_{\polydeg - j + 1}^{\parentheses{0}} \,
  ,
  \ctrlpoint_{\polydeg - j}^{\parentheses{0}} \,
  ,
  \dots
  ,
  \ctrlpoint_{\polydeg + 1}^{\parentheses{0}} = \ctrlpoint_{0}^{\parentheses{1}} \,
  ,
  \ctrlpoint_{1}^{\parentheses{1}} \,
  ,
  \dots
  ,
  \ctrlpoint_{j}^{\parentheses{1}}
\end{align*}
%
must be collinear.
To gain~$\pow[2]{C}$ continuity, the \beziercurve loses local control as the continuity requirement makes control points next to the adjoining point be dependent resulting in the whole spline being affected if a single control point moves.

Following this recursive definition, it is apparent that higher levels of continuity require a higher polynomial degree of the connecting \beziercurves, as the polynomial degree is directly linked to the number of control points with~${\polydeg = \numctrlpts - 1}$.
In addition, while the constraints resulting from continuity may be integrated into the dynamics model by either further holonomic constraints, they may also be incorporated into the equations for the coefficients themselves rendering some generalized coordinates redundant.
Since every \Bezier control point has global support\textemdash in our case the \Bezier control points translate to be the generalized coordinates \textIe the system's mechanical \glspl{DOF}\textemdash, requiring higher continuity of the curves causes a kinematically tightly constrained system.

Remedy for this drawback of \beziercurves comes from using functions of a given degree with only minimal support that provide smoothness and domain partition.
In the field of numerical analysis, such functions are referred to as basis splines, or B-splines, as coined by Isaac Jacob Schoenberg~\autocite{Boor.1978}.
B-splines can have continuity in \textEg~$\pow[2]{C}$ with only local control, but they loose piecewise \Bezier's interpolation property.
The points where piecewise B-splines meet are called knots and allow for simple incorporation of certain continuity requirements.
Each spline control point is associated with a basis function such that a curve~$\vect{C} \in \mathds{R}^{2}$ or~$\vect{C} \in \mathds{R}^{3}$ reads
%
\begin{align}
  \vect{C} \of{\pathcoord}
    & = \sum\limits_{\funcindex = 0}^{\numctrlpts}{
          \Pi_{\funcindex,\polydeg} \of{\pathcoord} \,
          \ctrlpoint_{\funcindex}
        } \,
        ,
        \label{eqn:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:general-spline}
\end{align}
%
in which~$\Pi_{\funcindex,\polydeg} \of{\pathcoord}$ is the~$\funcindex$-th basis function of polynomials of degree~$\polydeg$ and~$\ctrlpoint_{\funcindex}$ is the associated control point~(with planar~$\ctrlpoint_{\funcindex} \in \mathds{R}^{2}$ and spatial~$\ctrlpoint_{\funcindex} \in \mathds{R}^{3}$).
With B-splines, the degree~$\polydeg$ of the bases functions is not dependent of the number of control points~$\numctrlpts$\textemdash which is the case for \beziercurves where~${\polydeg = \numctrlpts - 1}$.
\Cref{eqn:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:general-spline} defines a general polynomial spline which can be chosen with an arbitrary degree per basis function.
Each basis function is a recursive formulation of linear combinations of lower-dimensional bases functions on the local interval of validity.
In general, spline functions can be of any sort as long as they satisfy the requirements of spline functions, the most common spline functions however are linear interpolation functions, but also hyperbolic ones~\autocite{Lu.2002} or logarithmic ones~\autocite{Schroppel.2016} exist.
The recursive formulation for the~$\funcindex$-th spline bases functions~$\Pi_{\funcindex, \polydeg} \of{\pathcoord}$ is given as
%
\begin{subequations}\label{eqn:cable-dynamics:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:recursive-formula}
\begin{align}
  \Pi_{\funcindex, 0} \of{\pathcoord}
    & = \begin{cases}
          1 \,
          ,
            & \pathcoord \in \interval[open right]{\pathcoord_{\funcindex}}{\pathcoord_{\funcindex + 1}} \,
            ,
            \\
          0 \,
          ,
            & \pathcoord \notin \interval[open right]{\pathcoord_{\funcindex}}{\pathcoord_{\funcindex + 1}}
        \end{cases} \,
        ,
    \\
  \shapefun_{\funcindex, \polydeg} \of{\pathcoord}
    & = \frac{
          \pathcoord
          -
          \pathcoord_{\funcindex}
        }{
          \pathcoord_{\funcindex + \polydeg}
          -
          \pathcoord_{\funcindex}
        } \,
        \Pi_{\funcindex, \polydeg - 1} \of{\pathcoord}
        +
        \frac{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord
        }{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord_{\funcindex + 1}
        } \,
        \Pi_{\funcindex + 1, \polydeg - 1} \of{\pathcoord} \,
        ,
\end{align}
\end{subequations}
%
which is the Cox-de Boor recursion formula~\autocite{Piegl.1997}.
Any B-spline of degree~$\polydeg$ is composed of piecewise polynomial functions in~$\pathcoord$ of degree~${\polydeg - 1}$.
These piecewise functions are defined over only~${\polydeg + 1}$ points~$\pathcoord_{j}$, called knots, which are collected in the nondecreasing knot vector~$\knotvector$.
This also implies that, contrary to \beziercurves, each B-spline contributes only locally in the range between its first and last knot.
A B-spline's~$j$-th derivative can be found by induction and is given purely for consistency and completeness to
%
\begin{align*}
  \od[j]{
  }{
    \pathcoord
  }
    & = \frac{
          \polydeg
        }{
          \pathcoord_{\funcindex + \polydeg}
          -
          \pathcoord_{\funcindex}
        } \,
        \od[j - 1]{
        }{
          \pathcoord
        } \,
        \Pi_{\funcindex, \polydeg - 1} \of{\pathcoord}
        -
        \frac{
          \polydeg
        }{
          \pathcoord_{\funcindex + \polydeg + 1}
          -
          \pathcoord_{\funcindex + 1}
        } \,
        \od[j-1]{
        }{
          \pathcoord
        } \,
        \Pi_{\funcindex + 1, \polydeg - 1} \of{\pathcoord} \,
        ,
\end{align*}
%
which holds for all~$j > 0$, otherwise~\cref{eqn:cable-dynamics:cable-dynamics:rayleigh-ritz:bases-functions:bsplines:recursive-formula} holds.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--bases-functions--blending-functions}
  \includegraphics[
      % width=0.75\linewidth,%
      % height=0.50\textheight,%
    ]{cosserat-rod/bsplines/bases-functions/blending-functions}
  \tikzexternaldisable%
  \caption[%
      Blending functions of B-spline of different degree.
    ]{%
      Blending functions~$\shapefun_{\funcindex, \polydeg}$, $\funcindex = \parentheses*{0, \dotsc, \polydeg}$ of a B-spline with~$\numsegments = 5$ segments and local polynomial degree~$\polydeg = 3$.
      The~$\funcindex$-th blending function~$\shapefun_{\funcindex, \polydeg}$ is obtained through convolution of blending function~$\shapefun_{\funcindex, \polydeg - 1}$ with the latter.
    }
\end{figure}

By choosing B-splines as basis of shape functions~$\shapefuns \of{\pathcoord}$, we achieve in a first step discretization of the continuum into a set of polynomial bases.
That is, the resulting cable shape can be composed of superimposing these polynomial bases.
Further, with B-splines per-definition being composed of piecewise local functions, we introduce an additional spatial discretization of the cable into local shape functions that are in addition only locally valid.
This removes dependence of control points\textemdash which in our case are going to be the generalized coordinates\textemdash and thus removes kinematically constraining generalized coordinates simply due to our choice of bases functions.
Lastly, due to the spatial discretization into splines we can achieve better approximation of more complex shapes of the cable\footnote{Whether this be necessary is out of the scope of this section.} while using lower polynomial degree.
As shown by~\textcite{Dokken.1990}, a circle may only be approximated using a single \beziercurve with~$\polydeg = 3$~(neglecting possibility of employing composite \beziercurves), whereas an~$\numsegments = 4$ B-spline of local polynomial degree~$\polydeg = 2$ suffices to perfectly describe the circle.
