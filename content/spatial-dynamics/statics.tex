%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/statics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Statics}\label{sec:spatial-dynamics:statics}

Besides analyzing the goodness of approximation of natural frequencies, we focus our attention in this section toward the accuracy of predicting the static cable shape.
The reference implementation we use is~\citeauthor{Irvine.1974}'s cable model we briefly presented in~\cref{sec:fundamentals:inverse-kinematics:mechanical}.
While this model is also a simplification of a string spanned between two points due to its neglecting bending stiffness, it provides an analytic solution that can be used in comparison.
Since the cable model presented in this chapter aims at extending the framework of \caro analysis, its main purpose is to provide a coherent description of the cable shape in space between the cable drawing point and the platform anchor.
This is of particular interest in the design process of \caros in order to allow for evaluating the robot design for possible interference with the environment, but also for the analysis of the static workspace.

Since the static cable shape is evaluated, we can without loss of generality confine the motion of the cable to the vertical plane spanned by the~{$\evecx\evecz$-pair} of vectors.
This holds true as we may find a constant transformation matrix that maps the global Cartesian space~$\mathds{E}^{3}$ into a subspace~$\mathds{E}^{2}$ by simply applying a rotation about the~$\evecz$-axis.
Let us assume the cable be fixed at its proximal point with~$\posconstraints_{\subproximal} \of{t} = \zeros \in \mathds{R}^{2}$ where we choose the origin~$\zeros$ for mere convenience.
Further, we set the mechanical properties of the cable according to the \emph{Rubber-like steel cable} from~\cref{tbl:cable-dynamics:cable:mechanical-properties} which allow for more severe cable sag.
The distal cable point is assumed to be defined through the geometric constraint given as
%
\begin{align*}
  \posconstraints_{\subdistal, k} \of{t}
  =
  \posconstraints_{\subdistal, \funcindex_{1}, \funcindex_{2}}
    & \coloneqq \cablen_{\funcindex_{1}} \,
        \begin{bmatrix}
          \cos \beta_{\funcindex_{2}} \\
          \sin \beta_{\funcindex_{2}}
        \end{bmatrix} \,
        ,
    &
    &
    \\
  \shortintertext{with}
  \cablen_{\funcindex_{1}}
    & = \funcindex_{1} \,
        \cablen_{\subunstrained} \,
        ,
    & \funcindex_{1}
    & \in \numset[round-mode=off]{0.6;0.8;1.0} \,
      ,
    \\
  \beta_{\funcindex_{2}}
    & = \frac{
          2 \,
          \funcindex_{2}
        }{
          5
        } \,
        \pi \,
        ,
    & \funcindex_{2}
    & \in \numsetlist[open-bracket={\lbrace},close-bracket={\rbrace},range-phrase={, \dotsc ,}]{0;4} \,
      ,
\end{align*}
%
yielding uniformly spaced points on a unit circle around the origin.

We will consider two quantitative measure of goodness of approximation of our B-spline cable model with respect to the~\citeauthor{Irvine.1974} cable model.
First, let us assume the B-spline cable to be given by curve~$\curve_{\msbspline}$ and~\citeauthor{Irvine.1974} cable to be given by curve~$\curve_{\msirvine}$.
Since both curves are parametrized by unstrained path coordinate~$\pathcoord$, we obtain parametric curves~$\curve_{\msbspline} \of{\pathcoord}, \curve_{\msirvine} \of{\pathcoord} \colon \interval{0}{1} \to \mathds{R}^{2}$.
As such calculating the deviation is less straightforward than simply subtracting the curves.
To remedy this, we introduce the \textit{residual curve} $\curve_{\residual}$ defined as
%
\begin{align*}
  \curve_{\residual} \of{\pathcoord}
    & = \curve_{\msirvine} \of{\pathcoord}
        -
        \curve_{\msbspline} \of{\pathcoord} \,
        ,
\end{align*}
%
which can be understood as a vector field resulting from mapping the error between B-spline curve and~\citeauthor{Irvine.1974} into~$\mathds{R}^{2}$.
That is, at coordinate~$\pathcoord^{\ast}$ along the path,~$\curve_{\residual} \of{\pathcoord^{\ast}}$ points from the B-spline cable curve toward the~\citeauthor{Irvine.1974} cable.
We then define two measures of interest for quantifying the residual such that we obtain a formulation of nominal deviation along unstrained path coordinate~$\pathcoord$, and a total area of deviation.
Let our \textit{residual vector norm} deviation be defined by
%
\begin{align}
  \residual_{\curve} \of{\pathcoord}
    & = \norm{
          \curve_{\msirvine} \of{\pathcoord}
          -
          \curve_{\msbspline} \of{\pathcoord}
        } \,
        ,
        \label{eqn:spatial-dynamics:statics:residual-vector}
\end{align}
%
along the curve \textIe for~$\pathcoord \in \domainpathcoord = \interval{0}{1}$.
This gives us the residual mapping~$\residual_{\curve} \colon \interval{0}{1} \ni \pathcoord \mapsto \residual_{\curve} \of{\pathcoord} \in \mathds{R}_{\geq 0}$, where each point along the curve is associated with a scalar residual.
Further, let us take the vectorial residual~$\residual_{\curve}$ and associate it with a scalar value called \textit{residual area norm} such that
%
\begin{align}
  \residual*_{\curve}
    & = \int\limits_{0}^{1}{
          \residual_{\curve} \of{\pathcoord} \,
          \td{\pathcoord}
        } \,
        ,
        \label{eqn:spatial-dynamics:statics:residual-scalar}
\end{align}
%
which simply denotes the area underneath residual curve~\cref{eqn:spatial-dynamics:statics:residual-vector}.
With this definition, we can quantify the error between the~\citeauthor{Irvine.1974} cable and the B-spline cable where a value close to zero represents perfect approximation of the cable shape; and larger values correspond to worse approximation.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--statics--144-deg--varying-segments-with-error}
  \includegraphics[%
    ]{cosserat-rod/bsplines/statics/144-deg/varying-segments-with-error}
  \tikzexternaldisable%
  \caption[%
    Catenary versus B-spline cable:~${\polydeg = 4}$, ${\numsegments \in \numset{1;5;10;20}}$.
  ]{%
    Static cable shape approximation of catenary solution~(left) for cable spanned at~$\beta_{\funcindex_{2}} = \SI{144}{\degree}$ with varying number of segments in~$\numsegments \in \numset{1;5;10;20}$; local polynomial degree fixed to~$\polydeg = 4$.
    Residual vector norm~$\residual_{\curve}$~(right) between catenary and B-spline cable shows increase of residual error as number of segments increase; when~$\numsegments \geq 10$ the residual error norm decreases and converges.
  }
  \label{fig:spatial-dynamics:statics:144deg:varying-segments}
\end{figure}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--statics--144-deg--varying-degree-with-error}
  \includegraphics[%
    ]{cosserat-rod/bsplines/statics/144-deg/varying-degree-with-error}
  \tikzexternaldisable%
  \caption[%
    Catenary versus B-spline cable: ${\numsegments = 10}$, ${\polydeg \in \numset{3;4;5;6}}$.
  ]{%
    Static cable shape approximation of catenary solution~(left) for cable spanned at~$\beta_{\funcindex_{2}} = \SI{144}{\degree}$ with varying polynomial degree fixed~$\polydeg \in \numset{3;4;5;6}$; number of segments in~$\numsegments = 10$; 
    Residual vector norm~$\residual_{\curve}$~(right) between catenary and B-spline cable shows nearly no difference between different local polynomial degree.
  }
  \label{fig:spatial-dynamics:statics:144deg:varying-degree}
\end{figure}

Spanning the cable horizontally \textIe inducing the most evenly distributed sag, better approximation of the catenary shape can be more easily achieved by increasing the number of segments and leaving the local polynomial degree low at~${\polydeg = 4}$.
Throughout all scenarios assessing the influence of the number of segments on the resulting cable shape, a cable with small number of segments \textEg in our case~$\numsegments \leq 3$, does show smaller mean residual error, however, it is incapable of capturing well the point of maximum sag at~$\pathcoord = 0.5$~(see~\cref{fig:spatial-dynamics:statics:144deg:varying-segments}).
A cable with only~$\numsegments = 1$ segment is less flexible than its physical counterpart and behaves much more like a highly flexible beam.
This evinces in the resulting sagging cable shape being rather parabolic than hyperbolic.
We may equivalently infer this observation from approximation of natural frequencies since only the first two natural frequencies can be approximated by a one-segment cable~(see~\cref{fig:spatial-dynamics:natural-frequencies:bsplines:varying-segment}).

With increasing number of segments, the residual error norm first increases, up until it reaches a maximum at~$\numsegments = 10$~(in case of~$\polydeg = 4$) and then converges toward an error of~$\residual*_{\curve} \leq \num{0.02}$~(see~\cref{fig:spatial-dynamics:statics:variation-of-error-over-segments:radius-1.00}).
Even with cables in \caros being most often taut diagonally, since a horizontally tauten cable is both impracticable and technically difficult to achieve, a higher number of segments does show both more symmetric residual error over unstrained path coordinate, as well as better approximation of the maximum sag at~$\pathcoord = 0.5$.
An increase in the number of segments from \numrange{1}{20} drastically decreases the deviation between the catenary cable and the B-spline cable.
With~$\numsegments = 20$ segments, equivalent to a discretization of the cable into segments of length~$\SI{5}{\centi\meter}$, the approximation is lowest independent of the general cable direction.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--catenary-comparison--radius-0d80}
  \includegraphics[%
    ]{cosserat-rod/bsplines/catenary-comparison/radius-0d80}
  \tikzexternaldisable%
  \caption[%
    Residual area norm of B-spline cable of short span.
  ]{%
    Evolution of the residual area norm~$\residual*_{\curve} \of{\numsegments}$ as function of the number of segments for cable spanned at angles~$\beta_{\funcindex_{2}} \in \SIset{0;144;288}{\degree}$ and radius~${L_{k_{1}} = \num{0.80} \, \cablen_{\subunstrained}}$.%
  }
  \label{fig:spatial-dynamics:statics:variation-of-error-over-segments:radius-0.80}
\end{figure}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--catenary-comparison--radius-1d00}
  \includegraphics[%
    ]{cosserat-rod/bsplines/catenary-comparison/radius-1d00}
  \tikzexternaldisable%
  \caption[%
    Residual area norm of B-spline cable of large span.
  ]{%
    Evolution of residual area norm~$\residual*_{\curve} \of{\numsegments}$ as function of the number of segments for cable spanned at angles~$\beta_{\funcindex_{2}} \in \SIset{0;144;288}{\degree}$ and cable radius~${L_{k_{1}} = \num{1} \, \cablen_{\subunstrained}}$.%
  }
  \label{fig:spatial-dynamics:statics:variation-of-error-over-segments:radius-1.00}
\end{figure}

Influence of local polynomial degree, however, is far less significant to the overall goodness of approximation.
\Cref{fig:spatial-dynamics:statics:144deg:varying-degree} show the vectorial residual norm~$\residual_{\curve}$ for a nearly diagonally spanned cable with number of segments fixed to~$\numsegments = 10$ and local polynomial degree varying in~$\polydeg \in \numset{3;4;5;6}$.
Each polynomial degree is capable of capturing well the maximum sag at~$\pathcoord = 0.5$ with the residual vector norm~${\residual_{\curve} \of{\pathcoord = 0.5} \in \interval{\num[scientific-notation=engineering]{0.0002390152}}{\num[scientific-notation=engineering]{0.0003576394}}}$.
Result of the cable shape differs in particular for largely sagging cables \textIe with the unstrained cable length longer than the direct distance between proximal and distal point and a non-diagonally hanging cable, see \textEg~\cref{fig:spatial-dynamics:statics:variation-of-error-over-segments:radius-0.80} where the distal point is on the circumference of a circle with radius~$\num{0.8} \, \cablen_{\subunstrained}$.
With few segments, a higher local polynomial degree~$\polydeg$ is necessary to approximate the sagging cable shape well, whereas cables with lower local polynomial degree~$\polydeg$ show quicker convergence of the residual area norm as the number of segments increases.

Concluding, local polynomial degree~$\polydeg$ shows minor impact on the quality of approximating~\citeauthor{Irvine.1974}'s cable model in various scenarios of different spanning angles as well as for various unstrained cable lengths as long as the number of segments is chosen to be~$\numsegments \geq 10$.
While a cable of local polynomial degree~$\polydeg = 2$ shows the largest error, it is also not suitable for simulation of a spatial \caros since its third derivative then contains discontinuities, which is in contradiction to the requirements posed in context with~\cref{eqn:spatial-dynamics:kinematics:tnb-frame:bases,eqn:spatial-dynamics:kinematics:curvature-torsion}\footnote{It may be worth noting that planar \caros do not require a parametric curve with continuous third-derivative since in the planar case, the cable does not exhibit torsion as per the Frenet-Serret formulation.
In the spatial case, however, a~$\polydeg = 2$ model is not suitable for lack of continuity of derivatives.}.
Focusing on the number of segments, a far more significant impact of the selected number on the quality of approximation is apparent\textemdash which is what was to be expected.

Much like in~\cref{sec:spatial-dynamics:preliminaries:modeling-approaches:segmentized}, the more segments we use for discretization, the better approximation of both natural frequencies as well as static shape is.
However, in the former case, it comes at the cost of much higher computational expenses due to large dense mass matrices, while in the latter case there is no major numerical impact noticeable due to B-splines only local support of its segments.
It may be favorable to set the number of segments to at least~$\numsegments = 10$, while~$\numsegments \geq 15$ shows even smaller residual vector norm and residual area norm in comparison to the~\citeauthor{Irvine.1974} cable model.
Care needs to be taken in ultimate comparison of the two models~(B-spline \textVs~\citeauthor{Irvine.1974}) since the former includes bending stiffness, whereas the latter neglects this mechanical property.
As such, it must not be possible to obtain a B-spline model that matches perfectly with the~\citeauthor{Irvine.1974} model.
