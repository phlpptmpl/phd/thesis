%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/conclusions.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions}\label{sec:spatial-dynamics:conclusions}

The cable model presented in this chapter based on the Cosserat-rod theory and discretized using B-splines renders itself a suitable formulation for cables as those used in \caros.
In the beginning of this chapter, we proclaimed wanting to find a cable model that captures the large signal response and neglects internal kinematics from fiber interaction.
These two goals have been reached since the B-spline cable considers the cable a consistent \textIe~full cross-section body and thus neglects intra-fiber dynamics.
Further, it describes only the overall dynamics of cable motion in space while describing only strain and bending deformation on the material level due to mere necessity for mechanical consistency.
While the fundamental elastica theory underlying the cable model has been known for more than a century and has been applied to the modeling and simulation of rods, fibers, and hair, its application to \caros is new.
Cosserat rod-based cables provide a more holistic formulation of the internal and external forces of the cable collected in a continuum model.
Possible side effects of inaccuracy or artificial vibration as found in the finite element method are inherently avoided by means of this model.

B-spline cables have two independent parameters that affect their quality of approximation, one being the local polynomial degree~$\polydeg$, the other being the number of segments~$\numsegments$.
As expected, local polynomial degree~$\polydeg$ has smaller impact on both the natural frequencies as well as the static cable shape than does the number of segments~$\numsegments$.
It is insufficient to choose a cable with only one or two segments since the static catenary cable shape cannot be well approximated.
Likewise, choosing an arbitrarily large number of segments yields a real continuum formulation for one thing, yet for another it does not provide better approximation of the static catenary shape.
Concluding, while it is, to some extent, still a question of the specific use case, a cable model of local polynomial degree~$\polydeg = 4$ suffices with a sufficiently large number of segments~$\numsegments \geq 10$.
The number of segments and local polynomial degree must not be determined by the number of natural frequencies to-be-approximated, but by the final use case with respect to how much cable sag is anticipated.
Lastly, a B-spline cable of local polynomial degree~$\polydeg = 4$ and $\numsegments = 3$~segments can suffice for cable robots like \IPAnemaMini, whereas they are insufficient for large scale robots like \Expo or \CoGiRo~(\textCf~\cref{sec:caro-dynamics:statics}).

The next steps toward further improving the spatial cable model given in this thesis would include physical damping and explicitly considering guiding and winding of the cable.
While this may seem a simple and straightforward task, it requires not only integrating inequality constraints but also finding suitable numerical integration schemes for this task.
Inequality constraints result from the cable being in one-sided contact with its surrounding such as \textEg on guiding pulleys or on the winch.
With the cable wrapped around an object, it cannot penetrate through the surface, however, it may without loss of generality deflect from the surface and then be unconstrained again.
Such problems are particularly involved to handle numerically, since the point in time of surface contact needs to be captured precisely in order to switch from unconstrained to constrained simulation \textIe from not-in-contact to an in-contact model description.
