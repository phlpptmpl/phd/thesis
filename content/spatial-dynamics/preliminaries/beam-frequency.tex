%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/preliminaries/beam-frequency.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mechanical Model of Comparison}\label{sec:spatial-dynamics:preliminaries:comparison-model}

Validating accuracy of the cable model presented in this section and incorporated into a forward dynamics simulation of \caros in the following chapter will be performed twofold.
On the one hand, an analytical expression for a hefty cable with axial elasticity is given by~\citeauthor{Irvine.1974}'s cable model, introduced in~\cref{sec:fundamentals:inverse-kinematics:mechanical}.
Comparison against this model can be performed straightforward and aims at identifying the amount of congruency of this thesis' cable model.
On the other hand, with the model presented in the following section approximating a taut string or an axially pre-tensioned beam, evaluating the model's quality of approximation of the natural frequencies of said beam implies itself.
The reference for comparison will thus be the analytical solution to the natural frequencies of a simply-supported beam, both in the tension-free and a pre-tensioned configuration.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{simply-supported-beam}
  \includegraphics[%
      width=0.75\linewidth,%
    ]{simply-supported-beam}
  \tikzexternaldisable%
  \caption[%
    Simply-supported beam and its deflection shape under a uniform load.%
  ]{%
    Simply-supported beam of length~$\cablen$ normalized to~$\bar{\cablen} = 1$ with uniform load~$g_{0} \of{\pathcoord}$ along the due to to \textEg gravitational forces resulting in deflection~$u \of{\pathcoord, t}$ of the neutral axis.%
  }
  \label{fig:spatial-dynamics:preliminaries:simply-supported-beam}
\end{figure}

We assume a perfect beam of length~$\cablen$, with cross-sectional area~$\crosssection$, Young's modulus of elasticity~$\youngsmodulus$, and second moment of area~$\secondmomentofarea$ as depicted in~\cref{fig:spatial-dynamics:preliminaries:simply-supported-beam}.
Following~\textcite{Labuschagne.2009}, the equations of motion for the beam deflection function~$u \of{\pathcoord, t}$ follows the partial differential equation in path coordinate~${\pathcoord \in \interval{0}{1} \eqqcolon \domainpathcoord}$ and time~$t$
%
\begin{align*}
  \pd[4]{
    u \of{ \pathcoord, t }
  }{
    \pathcoord
  }
  +
  \frac{
    \unitdensity \,
    \crosssection
  }{
    \youngsmodulus \,
    \secondmomentofarea
  } \,
  \pd[2]{
    u \of{ \pathcoord, t }
  }{
    t
  }
    & = 0 \,
    .
\end{align*}

After employing Rayleigh-Ritz with shape function~$u \of{\pathcoord, t}$ split into a path-dependent term~$w \of{\pathcoord}$ and time-dependent term~$\genq* \of{t}$ such that
%
\begin{align*}
  u \of{\pathcoord, t}
    & = w \of{\pathcoord} \,
        \genq* \of{t} \,
        ,
    \\
\intertext{we obtain the recast equation of motion}
  -
  \frac{
    1
  }{
    w \of{\pathcoord}
  } \,
  \frac{
    \youngsmodulus \,
    \secondmomentofarea
  }{
    \unitdensity \,
    \crosssection
  } \,
  \pd[4]{
    w \of{\pathcoord}
  }{
    \pathcoord
  }
  & = \frac{
        1
      }{
        \genq* \of{t}
      } \,
      \pd[2]{
        \genq* \of{t}
      }{
        t
      } \,
    = -\naturalfrequency_{k}^{2}
    \equiv \text{const} \,
    .
\end{align*}
%
Using this formula, we can determine the~$k$-th natural frequency for the twofold simply-supported beam reading
%
\begin{align}
  \naturalfrequency_{k}
    & = \pow*[2]{
          \frac{
            k \,
            \pi
          }{
            L
          }
        } \,
        \sqrt{
          \frac{
            \youngsmodulus \,
            \secondmomentofarea
          }{
            \unitdensity \,
            \crosssection
          }
        } \,
        .
        \label{eqn:spatial-dynamics:preliminaries:natural-frequencies:tension-free}
\end{align}

\begin{table}[tbp]
  \centering
  \caption[%
    Mechanical cable properties used for numerical model evaluation.
  ]{%
    Mechanical cable properties used throughout this chapter for numerical analysis, evaluation and comparison of cable model and its parametrization.
  }
  \label{tbl:cable-dynamics:cable:mechanical-properties}
  \input{tables/cable-robots/default/cable_rubbersteel}
\end{table}

In addition, since cables are under axial tension when under operation, we want to assess model quality also by evaluating natural frequencies against the beam under axial tensile load.
Given this load as axial tensile force~$\stress*_{\ms{ax}}$, we can determine the~$k$-th natural frequency for the same mechanical beam following~\textcite{Bokaian.1990,Hamed.2006} as
%
\begin{align}
  \naturalfrequency_{k}
    & = \pow*[2]{
          \frac{
            k \,
            \pi
          }{
            L
          }
        } \,
        \sqrt{
          \frac{
            \youngsmodulus \,
            \secondmomentofarea
          }{
            \unitdensity \,
            \crosssection
          }
        } \,
        \sqrt{
          1
          +
          \frac{
            \stress*_{\ms{ax}} \,
            \pow{
              \cablen
            }
          }{
            \youngsmodulus \,
            \secondmomentofarea \,
            \pow*{
              k \,
              \pi
            }
          }
        } \,
        .
        \label{eqn:spatial-dynamics:preliminaries:natural-frequencies:pretensioned}
\end{align}
%
Comparing our cable model also against a beam with an axially preloaded tensile force provides deeper insight into the range of validity of the model.
Since the tensile range of cables in \caros can range anywhere from a few millinewton up to several hundred kilonewton, the model should ideally be applicable to simulation of all these \caros.
Based on requirements of the \IPAnemaThree \caro, we assess validity over an axial tensile force range taken such that~$\stress*_{\ms{ax}} \in \SIset{0;100;1500;3000}{\newton}$, yet we use for the sake of more significant results a cable with mechanical parameters as given in~\cref{tbl:cable-dynamics:cable:mechanical-properties}\footnote{This is a mere choice to highlight the wide range of applicability of the cable model to \caros with elastic and flexible cables.
It further allows for visibly more distinguishable results since cable sag and elongation are much more prominent with an elastic modulus and density as chosen here.}.
