%!TEX root=../../../../thesis.tex
%!TEX file=content/spatial-dynamics/preliminaries/modeling-approaches/wave-equation.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linear Wave Equation}\label{sec:spatial-dynamics:preliminaries:modeling-approaches:wave-equation}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--wave-equation--kinematics}
  \includegraphics[%
      width=0.75\linewidth,%
    ]{kinematics/wave-equation/kinematics}
  \tikzexternaldisable%
  \caption[%
    Kinematics model of linear wave equation.
  ]{%
    Simplified depiction of the kinematics model of the linear wave equation describing wave perturbation function~$\wavepertubation \of{\pathcoord, t}$ of a string of length~$\cablen_{\subunstrained}$ normalized to~$\bar{\cablen}_{\subunstrained} = 1$, with uniform load~$g_{0} \of{\pathcoord}$ along the cord resulting from at least gravitational forces.
  }
  \label{fig:spatial-dynamics:preliminaries:modeling-approaches:wave-equation}
\end{figure}

In this formulation, the cable of unstrained length~$\cablen_{\subunstrained}$ is subject to drastic simplifications like perfect flexibility, constant tension, constancy of length, yet may be subject to external loads~(see~\cref{fig:spatial-dynamics:preliminaries:modeling-approaches:wave-equation}).
The most limiting simplification is requiring small displacements and slopes allowing for simplification of trigonometric functions \textIe~${\sin{x} \approx x}$, ${\cos{x} \approx 1 - \pow{x} / 2 \approx 1}$.
To derive the equations of motion of the cable, we introduce the perturbation functions~$\wavepertubation \of{\pathcoord, t}$ with~$\pathcoord \in \domainpathcoord = \interval{0}{1}$, which can be obtained from the perturbation formulation~$\hat{\wavepertubation} \of{\lpathcoord, t}$ with~$\lpathcoord \in \domainlpathcoord = \interval{0}{\cablen_{\subunstrained}}$ by means of a coordinate transformation~$\pathcoord = \lpathcoord / \cablen_{\subunstrained}$.
The resulting perturbation function can then conveniently be described by the linear wave equation reading in short form
%
\begin{subequations}\label{eqn:cable-dynamics:wave-equation:inhomogeneous-with-large-displacement}
\begin{align}
  \pd[2]{
    \wavepertubation \of{\pathcoord, t}
  }{
    t
  }
  +
  2 \,
  k \,
  \pd{
    \wavepertubation \of{\pathcoord, t}
  }{
    t
  }
  & = \pow[2]{
        \wavespeed
      } \,
      \pd[2]{
        \wavepertubation \of{\pathcoord, t}
      }{
        \pathcoord
      }
      +
      \residual* \of{\pathcoord, t} \,
      ,
  \\
\intertext{with boundary constraints}
  \wavepertubation \of{0, t}
    & = 0 \,
        ,
    \\
  \wavepertubation \of{1, t}
    & = 0 \,
        ,
    \\
\shortintertext{with initial conditions}
  \wavepertubation \of{\pathcoord, 0}
    & = \waveinitialfunc_{0} \of{\pathcoord} \,
      ,
    \\
  \od{
  }{
    t
  } \,
  \wavepertubation \of{\pathcoord, 0}
    & = \waveinitialfunc_{1} \of{\pathcoord} \,
      ,
  \\
\shortintertext{with residual}
  \residual* \of{\pathcoord, t}
    & = \force \of{\pathcoord, t}
        -
        \pd[2]{
          r \of{\pathcoord, t}
        }{
          t
        }
        +
        2 \,
        k \,
        \pd{
          r \of{\pathcoord, t}
        }{
          t
        } \,
        ,
        \\
\intertext{where external forces consider gravitational load}
  \force \of{\pathcoord, t}
    & = g_{0} \of{\pathcoord} \,
        ,
        \\
\intertext{and the global motion function of proximal point~$\waveboundaryfunc_{\subproximal} \of{t}$ and distal point~$\waveboundaryfunc_{\subdistal} \of{t}$}
  r \of{\pathcoord, t}
    & = \waveboundaryfunc_{\subproximal} \of{t}
        +
        \parentheses*{
          1
          -
          \pathcoord
        } \,
        \waveboundaryfunc_{\subdistal} \of{t} \,
        ,
\end{align}
\end{subequations}

This formulation covers the most widely applicable form of the linear wave equation allowing for both the proximal and distal end to move in space over time as described by functions~$\waveboundaryfunc_{\subproximal} \of{t}$ and~$\waveboundaryfunc_{\subdistal} \of{t}$, respectively.
In simplified ways, it has been applied to several cases of vibration analysis of large scale \caros like the \Fast telescope~\autocite{Diao.2009}, but also in the simulation and visualization of harbor cranes~\autocite{GarciaFernandez.2011}.
Parameter~$\wavespeed = \sqrt{\tension* / \parentheses*{\unitdensity \, \crosssection}}$ describes the speed of a wave traveling through the string which is proportional to the tension applied to the string.

In general, the speed of wave~$\wavespeed$ is assumed constant with respect to its arguments\footnote{Which holds true if at least density and cross-sectional area do not change with time}, however, with time-dependent tension, the speed of wave also becomes time-dependent.
This can be circumvented in twofold ways: %
\begin{inparaenum}[1)]
  \item assuming small changes in tension during small increments of time, linearly approximating the tension yields constancy of speed of waves, or
  \item under the assumption of discrete-time simulation or control, at a given moment in time, the applied tension is assumed constant thus time-dependence vanishes~(this is also employed by \textEg~\textcite{GarciaFernandez.2007b,GarciaFernandez.2008} in their application of the linear wave equation to the simulation of harbor cranes).
\end{inparaenum}

\Cref{eqn:cable-dynamics:wave-equation:inhomogeneous-with-large-displacement} describes a \gls{PDE} of the wave perturbation function in both path abscissae~$\pathcoord$ and time~$t$, which can only in some cases be solved for analytically.
The most commonly used approach is discretization of the path abscissae~$\pathcoord$ into intervals of fixed length yielding coupled \glspl{ODE}, which can be integrated over time.

Mostly due to the requirement of constant tension and no elongation, the linear wave equation does not render itself applicable to the simulation of \caros.
\Citeauthor{Liu.2013} have applied this technique in their research on longitudinal vibration characteristics of the \Fast \caro showing that linear wave equations may be used in the static case for such investigations~\autocite{Liu.2013}.
By means of superimposing the linear wave equation to the catenary solution of a statically sagging cable, \citeauthor{Liu.2013} showed that transversal flexibility considerations of the cable show a decrease in the natural frequencies yielding a less stiff cable robot.

\amend{Make a nice transition here to the now following sections}
