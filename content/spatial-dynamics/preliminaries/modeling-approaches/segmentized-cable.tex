%!TEX root=../../../../thesis.tex
%!TEX file=content/spatial-dynamics/preliminaries/modeling-approaches/segmentized-cable.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Segmentized Cable}\label{sec:spatial-dynamics:preliminaries:modeling-approaches:segmentized}

An extension to the approach presented by~\citeauthor{Bedoustani.2011} introduces more than one linear elastic cylinder in the cable model yielding a spatially discretized cable model as shown in~\cref{fig:spatial-dynamics:preliminaries:modeling-approaches:segmentized:global}.
While~\citeauthor{Tempel.2018c} have shown this model applicable to simulation of \caros in the static or quasi-static case, they also highlight two of the major drawbacks of said model~\autocite{Tempel.2018c}.
Consequentially, the cable model only forms a discrete approximation of the actual cable, resulting in the final cable shape matching the physical cable in only discrete points.
In addition, choosing elongation and orientation of each segment as minimal coordinates, a highly coupled system of nonlinear equations is derived causing expensive evaluation of the dynamics.
Further challenges arise from incorporating varying cable length into the discretized cable model as this is a task that is more challenging than it seems at first.
In particular, we may choose to have each segment length vary with time, however, this implies impelling or expelling mass along the cable which is physically incorrect.
We may also choose to change the length of only the very first segment, however, this becomes involved when its length reaches zero.

In case of the first segment being of near-zero length, re-discretization of the model with one fewer segment may be performed, however, this comes at both computational expense as well as imperfect transition between two physically related but not equivalent models.
Applicability of the approach is further limited by the fact that guiding of the discretized cable over circular shapes like a pulley or winch inevitable causes induction of artificial vibration.
Since a chain of discrete rigid links cannot perfectly wrap around a circular shape\footnote{This holds true unless the segment length is negligibly small, in which case the discrete model may already be considered an infinitesimal description of a continuous cable.} there is a discrete number of points of contact between cable and pulley.
Any longitudinal motion of the cable results in motion tangential to the circumference ultimately inducing vibration where there physically is none~\autocite{Szczotka.2010}.
Similar findings were presented by~\textcite{Lambert.2006,Quisenberry.2006,Spak.2014,Michelin.2015}, as well.

To briefly summarize the challenges of a segmentized cable model, let us introduce its kinematics and the overall structure of the resulting \gls{DAE}.
For means of simplicity, the cable model under investigation will be a planar cable in the~$\evecx\evecz$-plane in which both linear and rotational deformation \textIe strain and bending, are considered.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--segmentized--global}
  \includegraphics[%
      width=\linewidth,%
    ]{kinematics/segmentized/global}
  \tikzexternaldisable%
  \caption[%
    Global view of cable discretized spatially into~$\rfemnumsegments$ linear-elastic bodies.
  ]{%
    Spatially discretized cable into~$\rfemnumsegments$ segments interconnected by angular spring-damper elements at nodes~$\rfemofsegment{\linpos}{}$, each segment composed of two rigid bodies linked by a linear spring-damper element with extension~$\rfemofsegment{\rfemgenqelong*}{}$ and orientation~$\rfemofsegment{\rfemgenqrotation*}{}$ \textWrt the horizontal axis.
  }
  \label{fig:spatial-dynamics:preliminaries:modeling-approaches:segmentized:global}
\end{figure}

\Cref{fig:spatial-dynamics:preliminaries:modeling-approaches:segmentized:global} shows a global sketch of the cable divided into~$\rfemnumsegments$ segments.
We assume the cable to be of total unstrained length~$\cablen_{\subunstrained}$, of circular diameter~$\diameter$ thus of cross-section area~${{\crosssection = \pi \, \pow[2]{ \diameter } / 4}}$, and of unit density~$\unitdensity$, from which follows directly the length and mass of the~$\rfemsegmentindex$-th segment\footnote{In this section, lower-case Latin letters \textEg~$j, k, l$, are~$\irange{\rfemnumsegments}{\cparentheses*{j, k, l}}$ denoting the segment, whereas lowercase Greek letters \textEg~$\alpha$ are~$\alpha \in \numset{1;2}$ denoting the body part of the respective segment.
If no superscript is given, then the quantity is assumed to refer to the respective segment.}.
Using this segmentation approach, we obtain a system of free-floating rigid bodies that need additional kinematics to allow for capturing elasticity and flexibility.
Assuming the cable material time- and space-independent \textIe with constant Young's modulus~$\youngsmodulus$, constant cross-section~$\crosssection$, thus constant second moment of area, we introduce linear \glspl{acr:SDE} in of each segment, and rotational \glspl{acr:SDE} between adjacent segments~$\rfemsegmentindex$ and~$\rfemsegmentindex + 1$.
This enables capturing of both cable strain and cable bending.
\Cref{fig:spatial-dynamics:preliminaries:modeling-approaches:segmentized:segment} shows a close-up of the~$\rfemsegmentindex$-th segment, which clearly depicts it as being split into two rigid bodies indexed~$\rfemofbody{}{1}$ and~$\rfemofbody{}{2}$.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \includegraphics[%
      width=0.75\linewidth,%
    ]{kinematics/segmentized/segment}
  \caption[%
    Single segment of the spatially discretized cable model.
  ]{%
    Single segment $\rfemsegmentindex$ of the rigid finite element cable composed of a linear \acrshort{acr:SDE} linking rigid bodies $\rfemofbody{}{1}$ to $\rfemofbody{}{2}$, and an angular \acrshort{acr:SDE} linking segments $\rfemofsegment{}{\rfemsegmentindex}$ to previous segment~$\rfemofsegment{}{\rfemsegmentindex - 1}$ and subsequent segment~$\rfemofsegment{}{\rfemsegmentindex + 1}$.
  }
  \label{fig:spatial-dynamics:preliminaries:modeling-approaches:segmentized:segment}
\end{figure}


Both rigid bodies share the same mechanical properties \textIe lengths~$\rfemofbody{\rfemofsegment{\rfembodylength }{\rfemsegmentindex } }{\bodyindex }$, linear inertias~$\rfemofbody{\rfemofsegment{\mass}{\rfemsegmentindex}}{\bodyindex}$, and rotational inertias~$\rfemofbody{\rfemofsegment{\inertia }{\rfemsegmentindex } }{\bodyindex }$, which may be calculated from the global cable properties.
%
Both rigid bodies are connected through a linear \acrshort{acr:SDE} which obeys Hooke's law for its restoring stress thus reading
%
\begin{align*}
  \rfemofsegment{\stress*}{\rfemsegmentindex}
    & = \rfemofsegment{\youngsmodulus}{\rfemsegmentindex} \,
        \crosssection \,
        \parentheses*{
          \frac{
            \rfemofsegment{
              \rfemgenqelong*
            }{
              \rfemsegmentindex
            }
          }{
            \rfemofsegment{
              \rfembodylength
            }{
              \rfemsegmentindex
            }
          }
          -
          1
        } \,
        .
\end{align*}

All but the first and last segments' proximal and distal ends are connected with rotational \acrshort{acr:SDE} to the precedent~$\rfemsegmentindex - 1$-th and subsequent~$\rfemsegmentindex + 1$-th segment, respectively, with their restoring moment
%
\begin{subequations}
\begin{align*}
  \rfemofsegment{\torque*}{\rfemsegmentindex - 1, \rfemsegmentindex}
    & = \rfemofsegment{\youngsmodulus}{\rfemsegmentindex} \,
        \secondmomentofarea \,
        \parentheses*{
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
          -
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex - 1}
        } \,
    \\
  \rfemofsegment{\torque*}{\rfemsegmentindex, \rfemsegmentindex + 1}
    & = \rfemofsegment{\youngsmodulus}{\rfemsegmentindex} \,
        \secondmomentofarea \,
        \parentheses*{
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex + 1}
          -
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
        } \,
\end{align*}
\end{subequations}
%
yielding the resulting moment on the~$\rfemsegmentindex$-th segment as
%
\begin{align*}
  \rfemofsegment{\torque*}{\rfemsegmentindex}
    & = \rfemofsegment{\youngsmodulus}{\rfemsegmentindex} \,
        \secondmomentofarea \,
        \parentheses*{
          2 \,
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
          -
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex - 1}
          -
          \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex + 1}
        } \,
        .
\end{align*}

Segment specific {elasticities~$\rfemofsegment{\youngsmodulus}{\rfemsegmentindex}$} can be calculated from {elasticity~$\youngsmodulus$} of the cable under the assumption of a series of~$\rfemnumsegments$ linear springs of same elasticity with equivalent spring stiffness
%
\begin{align*}
  \frac{
    1
  }{
    \youngsmodulus
  }
    & = \sum\limits_{\rfemsegmentindex = 1}^{\rfemnumsegments}{
          \frac{
            1
          }{
            \rfemofsegment{
              \youngsmodulus
            }{
              \rfemsegmentindex
            }
          }
        }
      = \frac{
          \rfemnumsegments
        }{
          \rfemofsegment{
            \youngsmodulus
          }{
            \rfemsegmentindex
          }
        } \,
        ,
\end{align*}
%
and thus~$\rfemofsegment{\youngsmodulus}{\rfemsegmentindex} = \youngsmodulus \, \rfemnumsegments$.
%

Let the point of reference of the~$\rfemsegmentindex$-th segment be defined through the position of the~$\rfemsegmentindex$-th node located at the proximal end of the segment.
Then follows for the~$\rfemsegmentindex$-th node positions~$\rfemofsegment{\linpos}{\rfemsegmentindex}$ recursively with~$\rfemsegmentindex = \parentheses*{ 1 , \dotsc , \rfemnumsegments }$
%
\begin{align*}
  \rfemofsegment{\linpos}{0}
    & = \linpos_{\subproximal} \,
      ,
    \\
  \rfemofsegment{\linpos}{\rfemsegmentindex}
    & = \rfemofsegment{\linpos}{\rfemsegmentindex - 1}
        +
        \parentheses*{
          \rfemofsegment{\rfembodylength}{l}
          +
          \rfemofsegment{\rfemgenqelong*}{l}
        } \,
        \begin{bmatrix}
          \cos \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex - 1}
            \\
          \sin \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex - 1}
        \end{bmatrix} \,
        ,
\end{align*}
%
where~$\rfemproximal = \transpose{ \left[ x_{0}, z_{0} \right] }$ is a possibly time-dependent arbitrary point in~$\mathds{R}^{2}$.
We obtain~$\numnodes = \rfemnumsegments + 1$ nodes, including the distal end of the last segment~$\rfemsegmentindex = \rfemnumsegments$.
Alternatively, we can deduce an explicit form of~$\rfemofsegment{\linpos}{\rfemsegmentindex}$
%
\begin{align}
  \rfemofsegment{\linpos}{\rfemsegmentindex}
    & = \rfemproximal
        +
        \sum\limits_{l = 0}^{\rfemsegmentindex - 1}{
          \parentheses*{
            \rfemofsegment{\rfembodylength}{l}
            +
            \rfemofsegment{\rfemgenqelong*}{l}
          } \,
          \begin{bmatrix}
            \cos \rfemofsegment{\rfemgenqrotation*}{l}
              \\
            \sin \rfemofsegment{\rfemgenqrotation*}{l}
          \end{bmatrix}
        } \,
        .
        \label{eqn:cable-dynamics:segmentized:minimal-coordinates:position-node}
\end{align}

We define the position vectors of the~$j$-th segment's two rigid bodies' center of mass~$\rfemofbody{\rfemofsegment{\linpos}{\rfemsegmentindex}}{1}$ and~$\rfemofbody{\rfemofsegment{\linpos}{\rfemsegmentindex}}{2}$ to yield
%
\begin{subequations}\label{eqn:cable-dynamics:segmentized:minimal-coordinates:position-bodies}
\begin{align}
  \rfemofbody{ \rfemofsegment{\linpos}{\rfemsegmentindex} }{ 1 }
    & = \rfemofsegment{\linpos}{\rfemsegmentindex}
        +
        \rfemofbody{\rfemofsegment{\rfembodycom}{\rfemsegmentindex}}{1} \,
        \begin{bmatrix}
          \cos \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
            \\
          \sin \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
        \end{bmatrix} \,
      ,
      \label{eqn:cable-dynamics:segmentized:minimal-coordinates:position-bodies:1}
    \\
  \rfemofbody{ \rfemofsegment{\linpos}{\rfemsegmentindex} }{ 2 }
    & = \rfemofsegment{\linpos}{\rfemsegmentindex}
        +
        \parentheses*{
          \rfemofbody{\rfemofsegment{\rfembodycom}{\rfemsegmentindex}}{2} \,
          +
          \rfemofsegment{\rfemgenqelong*}{\rfemsegmentindex}
        } \,
        \begin{bmatrix}
          \cos \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
            \\
          \sin \rfemofsegment{\rfemgenqrotation*}{\rfemsegmentindex}
        \end{bmatrix} \,
      \,
      .
      \label{eqn:cable-dynamics:segmentized:minimal-coordinates:position-bodies:2}
\end{align}
\end{subequations}

Defining constraints on the cable distal point~$\linpos_{\rfemnumsegments} \of{\genq}$ requiring it to be located at a desired distal point~$\linpos_{\subdistal} \of{t}$ imposes a kinematic constraint of form
%
\begin{align*}
  \posconstraints \of{\genq, t}
    & = \linpos_{\rfemnumsegments} \of{\genq}
        -
        \linpos_{\subdistal} \of{t}
        \,
        ,
\end{align*}
%
which, after extensive mathematical manipulation~(exact derivation can be found in~\textcite{AdamiecWojcik.2015,Fritzkowski.2013}) yields a \gls{DAE}-formulation of the system reading
%
\begin{align*}
  \Mass \of{\genq} \,
  \ddotgenq
    & = - \fictitiousforces \of{\genq}
        +
        \forcevec_{\msinternal} \of{\genq}
        -
        \forcevec_{\msexternal}  \of{\genq} \,
    \\
  \zeros
  = \posconstraints \of{\genq, t}
    & = \linpos_{\rfemnumsegments + 1} \of{\genq}
        -
        \linpos_{\subdistal} \of{t} \,
        .
\end{align*}

We exemplify the system's complexity by showing the structure of the state-dependent mass-matrix~$\Mass \of{\genq}$ reading
%
\begin{subequations}\label{eqn:cable-dynamics:segmentized:minimal-coordinates:dynamics:mass}
\begin{align}
  \Mass \of{\genq}
    & = \begin{bmatrix}
          \Mass_{\rfemproximal\rfemproximal}
            & \Mass_{\rfemproximal 1}
            & \dots
            & \Mass_{\rfemproximal k}
            & \dots
            & \Mass_{\rfemproximal \rfemnumsegments}
            \\
          \Mass_{1 \rfemproximal}
            & \Mass_{1 1}
            & \dots
            & \Mass_{1 k}
            & \dots
            & \Mass_{1 \rfemnumsegments}
            \\
          \vdots
            & \vdots
            & 
            & 
            & 
            & \vdots
            \\
          \Mass_{k \rfemproximal}
            & \Mass_{k 1}
            & \dots
            & \Mass_{k k}
            & \dots
            & \Mass_{k \rfemnumsegments}
            \\
          \Mass_{\rfemnumsegments \rfemproximal}
            & \Mass_{\rfemnumsegments 1}
            & \dots
            & \Mass_{\rfemnumsegments k}
            & \dots
            & \Mass_{\rfemnumsegments \rfemnumsegments}
            \\
        \end{bmatrix} \,
        ,
    \\
\intertext{with the sub-mass matrices}
  \Mass_{\rfemproximal \rfemproximal}
    & = \begin{bmatrix}
          \mass_{\subcable}
            & 0
            \\
          0
            & \mass_{\subcable}
        \end{bmatrix} \,
        ,
    \\
  \rfemofsegment{\Mass}{\rfemproximal l}
  = \transpose{
      \rfemofsegment{\Mass}{l \rfemproximal}
    }
    & = \begin{bmatrix}
          A_{l} \,
          \cos \rfemofsegment{\rfemgenqrotation*}{l}
            & -B_{l} \,
              \sin \rfemofsegment{\rfemgenqrotation*}{l}
            \\
          A_{l} \,
          \sin \rfemofsegment{\rfemgenqrotation*}{l}
            & B_{l} \,
              \cos \rfemofsegment{\rfemgenqrotation*}{l}
        \end{bmatrix} \,
        ,
    \\
  \rfemofsegment{\Mass}{k l}
    & = \begin{bmatrix}
          \rfemofsegment{a}{kl}^{\strain} \,
            \pcos*{
              \rfemofsegment{\rfemgenqrotation*}{k}
              -
              \rfemofsegment{\rfemgenqrotation*}{l}
            }
            & \rfemofsegment{b}{kl}^{\strain} \,
                \psin*{
                  \rfemofsegment{\rfemgenqrotation*}{k}
                  -
                  \rfemofsegment{\rfemgenqrotation*}{l}
                }
            \\
          -\rfemofsegment{a}{kl}^{\bend} \,
            \psin*{
              \rfemofsegment{\rfemgenqrotation*}{k}
              -
              \rfemofsegment{\rfemgenqrotation*}{l}
            }
            & \rfemofsegment{b}{kl}^{\bend} \,
                \pcos*{
                  \rfemofsegment{\rfemgenqrotation*}{k}
                  -
                  \rfemofsegment{\rfemgenqrotation*}{l}
                }
        \end{bmatrix} \,
        .
\end{align}
\end{subequations}
%
and the shorthand substitutions
%
\begin{align*}
  \mass_{jl}
    & \coloneqq \begin{cases}
          \rfemofsegment{\mass}{j} \,
          ,
            & l < j \,
            ,
            \\
          \rfemofbody{\rfemofsegment{\mass}{j}}{2} \,
          ,
            & l \equiv j \,
            ,
        \end{cases}
    & n_{jl}
    & \coloneqq \begin{cases}
          \rfemofsegment{\mass}{j} \,
          \parentheses*{
            \rfemofsegment{\rfembodylength}{l}
            +
            \rfemofsegment{\rfemgenqelong*}{l}
          } \,
          ,
            & l < j \,
            ,
            \\
          \rfemofsegment{\alpha}{l}
          +
          \rfemofbody{\rfemofsegment{\mass}{l}}{2} \,
          \rfemofsegment{\rfemgenqelong*}{l} \,
          ,
            & l \equiv j \,
            .
        \end{cases}
    \\
%
  \rfembodylength_{jl}
    & \coloneqq \begin{cases}
          \rfemofsegment{\rfembodylength}{l}
          +
          \rfemofsegment{\rfemgenqelong*}{l} \,
          ,
            & l < j \,
            ,
            \\
          \rfemofbody{\rfemofsegment{\rfembodycom}{j}}{2} \,
          ,
            & l \equiv j \,
            ,
        \end{cases}
    & \alpha_{jl}
    & \coloneqq \begin{cases}
          \rfemofsegment{\alpha}{j}
          +
          \rfemofsegment{\mass}{j} \,
          \rfemofsegment{\rfemgenqelong*}{j} \,
          ,
            & l < j \,
            ,
            \\
          \pow[2]{\rfemofsegment{\mass}{j}} \,
          \parentheses*{
            \rfemofbody{\rfemofsegment{\rfembodycom}{j}}{2}
            +
            \rfemofsegment{\rfemgenqelong*}{j}
          } \,
          ,
            & l \equiv j \,
            ,
        \end{cases}
    \\
    \mass_{\subcable}
    & = \sum\limits_{j = 1}^{\numsegments}{
          m_{js}
        } \,
        ,
    & \beta_{jl}
    & \coloneqq
      \begin{cases}
        \parentheses*{
          \alpha_{j}
          +
          \rfemofbody{\rfemofsegment{\mass}{j}}{2} \,
          \rfemofsegment{\rfemgenqelong*}{j}
        } \,
        \parentheses*{
          \rfemofsegment{\rfembodylength}{l}
          +
          \rfemofsegment{\rfemgenqelong*}{l}
        } \,
        ,
          & l < j \,
          ,
          \\
        \rfemofbody{\rfemofsegment{\mass}{j}}{2} \,
        \pow*[2]{
          \rfemofbody{\rfemofsegment{\rfembodycom}{l}}{2}
          +
          \rfemofsegment{\rfemgenqelong*}{j}
        }
        +
        \rfemofsegment{\inertia}{j} \,
        ,
          & l \equiv j \,
          .
      \end{cases}
  \\
%
  \mass_{jkl}
    & \coloneqq \begin{cases}
          \mass_{jl} \,
          ,
            & k < j \,
            ,
            \\
          \rfemofbody{\rfemofsegment{\mass}{j}}{2} \,
          ,
            & k \equiv j \,
            ,
        \end{cases}
    & n_{jkl}
    & \coloneqq \begin{cases}
          n_{jl} \,
          ,
            & k < j \,
              ,
            \\
          \rfemofbody{\rfemofsegment{\mass}{j}}{2}
          \parentheses*{
            \rfemofsegment{\rfembodylength}{jl}
            +
            \rfemofsegment{\rfemgenqelong*}{j}
          } \,
          ,
            & k = j \,
              ,
        \end{cases} \,
        ,
    \\
  \alpha_{jkl}
    & \coloneqq \begin{cases}
          \parentheses*{
            \rfemofsegment{\rfembodylength}{k}
            +
            \rfemgenqelong*_{jl}
          } \,
          m_{jl} \,
          ,
            & k < j \,
            ,
            \\
          \alpha_{jl} \,
          ,
            & k \equiv j \,
            ,
        \end{cases}
    & \beta_{jkl}
    & \coloneqq \begin{cases}
          \parentheses*{
            \rfemofsegment{\rfembodylength}{k}
            +
            \rfemgenqelong*_{jl}
          } \,
          n_{jl} \,
          ,
            & k < j \,
            ,
            \\
          \beta_{jl} \,
          ,
            & k \equiv j \,
            .
        \end{cases}
  \\
%
  a_{kl}^{\strain}
    & = \sum\limits_{j = \max \of{k,l}}^{\numsegments}{
          m_{jkl}
        } \,
        ,
    & b_{kl}^{\strain}
    & = \sum\limits_{j = \max \of{k, l}}^{\numsegments}{
          n_{jkl} 
        } \,
        ,
    \\
  a_{kl}^{\bend}
    & = \sum\limits_{j = \max \of{k,l}}^{\numsegments}{
          \alpha_{jkl}
        } \,
        ,
    & b_{kl}^{\bend}
    & = \sum\limits_{j = \max \of{k, l}}^{\numsegments}{
          \beta_{jkl} 
        } \,
        .
  \\
%
  A_{l}
    & = \sum\limits_{j = l}^{\numsegments}{
          \mass_{jl}
        } \,
        ,
    & B_{l}
    & = \sum\limits_{j = l}^{\numsegments}{
          n_{jl}
        } \,
        .
\end{align*}

We see a strict state-dependence of the dense mass matrix~$\Mass \of{\genq}$ which renders its evaluation computationally expensive, already for small numbers of segments \textIe~$\rfemnumsegments \geq 6$~\autocite{Tempel.2018c,AdamiecWojcik.2013}.
In addition, due to its state-dependence, it must be evaluated at every time step of integration and cannot be pre-calculated prior to simulation.
It is in fact the integration of linear elasticity \textIe strain that makes deriving the equations of motion involved, and introduces numerical issues and challenges; more details on which can be found in~\textcite{Tempel.2018c}.
We may circumvent dense mass matrices in the model formulation by designing the rigid cable as a constrained multibody system where the segments are joined through kinematic constraints, then given in maximal coordinates of each body's center of mass position and orientation.
While this does reduce the dense mass matrix to block-diagonal form rendering it sparse, this design entails adding two kinematic constraints per segment\footnote{In the planar case we obtain two constraints in the horizontal and vertical coordinate, in the spatial case we obtain three constraints in the two horizontal and one vertical coordinate.} turning the conventional \gls{ODE} into a \gls{DAE} system.
Both formulations are equivalent as the kinematic constraint can just be introduced into the states by transforming it from maximal coordinates to minimal coordinates.
In fact, \autocite{AdamiecWojcik.2013} show the latter, constrained formulation provides faster simulation results since evaluating the dynamics is less expensive and evaluation of the additional kinematic constraints does not increase computational cost.
However, this does not remedy another problem inherent to the modeling approach.

The model introduced here portrays fixed-length cables, which shows the complexity of the model when introducing time-varying length through dynamic rescaling of the unstrained length of each segment.
Lastly, we may use the model as-is with fixed-length, then requiring to introduce winding and guiding mechanisms by means of kinematic inequalities, which renders the problem complex on the numerical scale while not remedying the burden of evaluating a state-dependent mass matrix.
Atop, with a fixed-length and guided cable, the segment size must be chosen so small as to not introduce numerically artificial vibration resulting from guiding a chain-like cable over round surfaces.
As~\autocite{AdamiecWojcik.2015,Dreyer.1984,Huston.1982} point out, segment length~$\rfemofsegment{\rfembodylength}{\rfemsegmentindex}$ must satisfy~$\rfemofsegment{\rfembodylength}{\rfemsegmentindex} \leq \pow[2]{\radius}$ if~$\radius$ be the radius of the smallest circular surface the cable is guided over.
In practical use cases of \caros, the smallest pulley features a diameter of~$\diameter = \SI{5}{\centi\meter}$~\autocite{Pott2012b} which implies segment lengths of~$\rfemofsegment{\rfembodylength}{\rfemsegmentindex} \leq \SI{0.000625}{\meter}$ resulting in~$\rfemnumsegments = \num{32000}$ segments for a~$\cablen_{\subunstrained} = \SI{20}{\meter}$ long cable.

% \addref[inline]{Somewhere \autocite{Dreyer.1984,Huston.1982} on the problem of cable discretization; somwhere \autocite{GarciaFernandez.2007} on the problem of cable-pulley interaction dynamics modeling}

To circumvent the challenges and issues arising from a spatially discretized cable model, we shift our interest toward infinitesimal formulations of the cable resulting in a continuum formulation of the dynamics.
The most common representatives of this formulation are the nonlinear wave equation and its linearized form.
