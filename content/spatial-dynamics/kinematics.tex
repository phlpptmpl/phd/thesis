%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/kinematics.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Kinematics}\label{sec:spatial-dynamics:kinematics}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--cosserat-rod--spatial}
  \includegraphics[%
      width=0.75\linewidth,%
    ]{kinematics/cosserat-rod/spatial}
  \tikzexternaldisable%
  \caption[%
    Spatial depiction of cable based on Cosserat rod theory.
  ]{%
    Spatial depiction of cable based on Cosserat rod theory with undeformed configuration~$\Curve \of{\lpathcoord} \in \Cablebody$ and deformed cable~$\curve \of{ \lpathcoord, t } \in \cablebody$.
    Local material frame at unstrained path coordinate~$\lpathcoord$ is defined through the frame spanned by its orthonormal director triad~$\vectset{ \tangent ; \normal ; \binormal }$.
  }
  \label{fig:spatial-dynamics:kinematics:spatial}
\end{figure}

In general, a cable may be assumed a very slender rod, consequently making it mechanically equivalent to a beam of given mechanical properties.
As such, we manage with formulation of beam kinematics given as geometrically-exact beams presented by~\textcite{Zhang.2016,Lang.2011,Boyer.2011,Soler.2018,VuQuoc.1995,Antman.2005}, and of differential geometry as found in~\textcite{Cao.2008,Bensoam.2013}.

Let~$\cablebody \subset \mathds{E}^{3}$ be the cable in Euclidean space~$\mathds{E}^{3}$ at current point in time called the \emph{current configuration} or \emph{deformed configuration}, while~$\Cablebody \subset \mathds{E}^{3}$ be the undeformed cable called  the \emph{reference configuration} or \emph{undeformed configuration}.
We shall denote the function mapping a point along the unstrained path coordinate~$\lpathcoord \in \interval{0}{\cablen} = \domainlpathcoord$\textemdash equivalently its arc length\textemdash into the reference configuration by~$\Curve$.
This configuration be defined such that ${\Curve: \domainlpathcoord = \interval{0}{\cablen} \to \Curve \of{\lpathcoord} \in \Cablebody \subset \mathds{E}^{3}}$.
Further, the function mapping a point along the unstrained path coordinate~$\lpathcoord$ and time~$t \in \interval[open right]{0}{\infty}$%
\footnote{Without loss of generality we can assume the open interval of positive real numbers for time~${t \in \mathds{R}_{\geq 0} = \interval[open right]{0}{\infty}}$.
This mostly depends on assuming that material properties like elasticity or viscosity are time-invariant as such do not change over time.
In fact, this property may be further investigated down the line, but for the initial modeling of cables, it is assumed to hold true.
} into the current configuration shall be given through
%
\begin{align*}
  \curve: \domainlpathcoord \times \mathds{R}_{\geq 0} = \interval{0}{\cablen} \times \interval[open right]{0}{\infty} \to \curve \of{ \lpathcoord, t } \in \cablebody \subset \mathds{E}^{3} \,
    .
\end{align*}

Without loss of generality, the undeformed cable configuration~$\Cablebody$ is assumed collinear with the world~$\evecx[E]$-axis such that~$\Curve \in \mathds{E}^{3}$ reads
%
\begin{align*}
  \Curve \of{ \lpathcoord }
    & = \begin{bmatrix}
          \lpathcoord \\
          0 \\
          0
        \end{bmatrix}
        =
        \lpathcoord \,
        \evecx[E] \,
        ,
\end{align*}
%
whereas the current cable configuration~$\cablebody$ is not imposed similar restrictions and as such~$\curve \in \mathds{E}^{3}$ most generally reads
%
\begin{align*}
  \curve \of{ \lpathcoord, t }
    & = \begin{bmatrix}
          x \of{ \lpathcoord, t } \\
          y \of{ \lpathcoord, t } \\
          z \of{ \lpathcoord, t } \\
        \end{bmatrix}
        =
        x \of{ \lpathcoord, t } \,
        \evecx[E]
        +
        y \of{ \lpathcoord, t } \,
        \evecy[E]
        +
        z \of{ \lpathcoord, t } \,
        \evecz[E] \,
        .
\end{align*}

To derive forces and moments resulting from arbitrary deformation of the cable in~\cref{sec:spatial-dynamics:equations-of-motion}, a local frame or material frame must be introduced that allows for measuring such deformations.
A convenient definition of such a material frame can be found in the Frenet-Serret frame or \TNBFrame which is a geometric invariant of every curve.
As such, its expression is independent of the explicit parametrization of any curve~$\curve$ given there exists a parametrization in terms of a single parameter such that~$\curve = \curve \of{ \lpathcoord, t }$ where~$\lpathcoord \in \interval{a}{b}$ with~$a < b$.
To further ease application of the \TNBFrame, we may parametrize the curve in terms of its arc length such that~$\lpathcoord \in \interval{0}{\cablen}$ where now~$\cablen$ is the arc length of curve~$\curve \of{ \lpathcoord, t }$.

The \TNBFrame is defined such that there exists a orthonormal triad of three vectors~$\tangent, \normal, \binormal$ that uniquely define a right-hand coordinate system at~$\curve \of{ \lpathcoord, t }$ with only very few assumptions on the curve.
In the notation of Frenet and Serret, these vectors are tangent vector~$\tangent$, normal vector~$\normal$, and binormal vector~$\binormal$.
Their mathematical formulation is given completely through curve~$\curve \of{ \lpathcoord, t }$ and its derivatives denoted by~$\p{ \parentheses{} } = \pd{\parentheses{}}{\lpathcoord}$ such that
%
\begin{subequations}\label{eqn:spatial-dynamics:kinematics:tnb-frame:bases}
\begin{align}
  \tangent
    & = \frac{
          \p{\curve}
        }{
          \norm{\p{\curve}}
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:tnb-frame:tangent}
    \\
  \normal
    & = \frac{
          \p{\tangent}%
        }{
          \norm{
            \p{\tangent}
          }
        }
      = \frac{
          \crossp{
            \p{\curve}
          }{
            \crossp*{
              \pp{\curve}
            }{
              \p{\curve}
            }
          }
        }{
          \norm{
            \crossp{
              \p{\curve}
            }{
              \crossp*{
                \pp{\curve}
              }{
                \p{\curve}
              }
            }
          }
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:tnb-frame:normal}
    \\
  \binormal
    & = \crossp{
          \tangent
        }{
          \normal
        }
      = \frac{
          \crossp{
            \p{\curve}
          }{
            \pp{\curve}
          }
        }{
          \norm{
            \crossp{
              \p{\curve}
            }{
              \pp{\curve}
            }
          }
        } \,
        .
        \label{eqn:spatial-dynamics:kinematics:tnb-frame:binormal}
\end{align}
\end{subequations}

\Cref{eqn:spatial-dynamics:kinematics:tnb-frame:bases} define a relatively parallel adapted frame~\autocite{Bishop.1975} moving along the curve and allowing for tracking of its change of frame while always being relatively parallel to the frame of~$\mathds{E}^{3}$ under only rigid body translation.
As such, the \TNBFrame defines the material deformation at every point~$\lpathcoord \in \domainlpathcoord$ allowing for measuring material deformation in global coordinates.
To track the changes of the orthonormal triad vectors~$\tangent$,~$\normal$, and~$\binormal$ as we move along the curve, we require the evolution of the former vectors with respect to~$\lpathcoord$ \textIe~$\pd{\parentheses{}}{\lpathcoord}$ for every triad vector.
Following the Frenet-Serret formula, we find
%
\begin{subequations}\label{eqn:spatial-dynamics:kinematics:tnb-evolution}
\begin{align}
  \pd{
    \tangent
  }{
    \lpathcoord
  }
    & = \curvature \,
        \normal \,
        ,
        \label{eqn:spatial-dynamics:kinematics:tnb-evolution:tangent}
    \\
  \pd{
    \normal
  }{
    \lpathcoord
  }
    & = - \curvature \,
        \tangent
        +
        \torsion \,
        \binormal \,
        ,
        \label{eqn:spatial-dynamics:kinematics:tnb-evolution:normal}
    \\
  \pd{
    \binormal
  }{
    \lpathcoord
  }
    & = - \torsion \,
        \normal \,
        ,
        \label{eqn:spatial-dynamics:kinematics:tnb-evolution:binormal}
\end{align}
\end{subequations}
%
in which the curvature~$\curvature$ measures how much the curve fails to form a straight line, and the torsion~$\torsion$ measures the failure of the curve to be planar.
Intuitively, this can be exemplified on a helix which has constant curvature and torsion as we move along the curve.
Here, constant curvature results in a left-handed circular motion around its center and constant torsion resulting in mathematically positive elevation along the path.

Curvature~$\curvature$ and torsion~$\torsion$ are invariant under a parametrization of the curve and can always be expressed as
%
\begin{subequations}\label{eqn:spatial-dynamics:kinematics:curvature-torsion}
\begin{align}
  \curvature
    & = \frac{
          \norm{
            \crossp{
              \p{\curve}
            }{
              \pp{\curve}
            }
          }
        }{
          \pow[3]{
            \norm{
              \p{\curve}
            }
          }
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:curvature-torsion:curvature}
    \\
  \torsion
    & = \frac{
          \left[
            \p{\curve}
            ,
            \pp{\curve}
            ,
            \ppp{\curve}
          \right]
        }{
          \pow[2]{
            \norm{
              \crossp{
                \p{\curve}
              }{
                \pp{\curve}
              }
            }
          }
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:curvature-torsion:torsion}
\end{align}
\end{subequations}
%
where~$\left[ \p{\curve}, \pp{\curve}, \ppp{\curve} \right]$ is the scalar triple product defined as
%
\begin{align*}
  \left[
    \p{\curve}
    ,
    \pp{\curve}
    ,
    \ppp{\curve}
  \right]
    & = \det \of{
          \p{\curve}
          ,
          \pp{\curve}
          ,
          \ppp{\curve}
        }
      = \dotp{
          \p{\curve}
        }{
          \crossp*{
            \pp{\curve}
          }{
            \ppp{\curve}
          }
        } \,
        ,
\end{align*}
%
providing a signed measure for the volume of the parallelepiped spanned by the three vectors.

We can equivalently write~\cref{eqn:spatial-dynamics:kinematics:tnb-evolution} in matrix form
%
\begin{align*}
  \begin{bmatrix}
    \horzbar
      & \p{\tangent}
      & \horzbar
      \\
    \horzbar
      & \p{\normal}
      & \horzbar
      \\
    \horzbar
      & \p{\binormal}
      & \horzbar
  \end{bmatrix}
    & = \underbrace{
          \begin{bmatrix}
            0
              & \curvature
              & 0
              \\
            -\curvature
              & 0
              & \torsion
              \\
            0
              & -\torsion
              & 0
          \end{bmatrix}
        }_{
          \transpose{
            \skewm{\angvel}
          }
        }
        \,
        \begin{bmatrix}
          \horzbar
            & \tangent
            & \horzbar
            \\
          \horzbar
            & \normal
            & \horzbar
            \\
          \horzbar
            & \binormal
            & \horzbar
        \end{bmatrix}
\end{align*}
%
yielding the relations
%
\begin{subequations}\label{eqn:spatial-dynamics:kinematics:rate-of-change}
\begin{align}
  \p{\tangent}
    & = \crossp{
          \angvel
        }{
          \tangent
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:rate-of-change:tangent}
    \\
  \p{\normal}
    & = \crossp{
          \angvel
        }{
          \normal
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:rate-of-change:normal}
    \\
  \p{\binormal}
    & = \crossp{
          \angvel
        }{
          \binormal
        } \,
        ,
        \label{eqn:spatial-dynamics:kinematics:rate-of-change:binormal}
\end{align}
\end{subequations}
%
from which we can see that~$\skewm{\angvel}$ is the skew-symmetric matrix to the vector~$\angvel$.
To the experienced reader, this relationship seems rather familiar if we substitute the derivative \textWrt path coordinate~$\lpathcoord$ by the derivative \textWrt time~$t$ yielding the relationship of angular velocity given the time rate of a rotation matrix~$\dotrotmatr$ and the rotation matrix~$\rotmatr$ itself such that~${ \skewm{\angvel} = \dotrotmatr \, \transpose{\rotmatr} }$~\autocite{Mladenova1999,Eade.2017,Gamkrelidze.1993}.
The angular velocity~${ \angvel = \transpose{ \left[ \torsion, 0, \curvature \right] } }$ used in~\cref{eqn:spatial-dynamics:kinematics:rate-of-change} is given in terms of the local \TNBFrame showing the rotation happens only about the~$\tangent$ and~$\binormal$ vectors with rates of torsion~$\torsion$ and curvature~$\curvature$, respectively.
In terms of a local material-fixed coordinate frame, the rate of change of the \TNBFrame reads~$\angvel = \TNB \, \angvel^{\prime}$ which can also be inferred from the Darboux vector~\autocite{Edelsbrunner.2008,Oprea.2007} given as
%
\begin{align}
  \darboux
    & = \torsion \,
        \tangent
        +
        \curvature \,
        \binormal \,
        .
        \label{eqn:spatial-dynamics:kinematics:darboux-vector}
\end{align}

For consistency with later derived virtual work of the cable and the resulting equations of motion, we represent the transformation $\TNB$ from the \TNBFrame into~$\mathds{E}^{3}$ by the exponential rotation mapping that affects the cross product with rotation axis~$\rotvector \colon \rotmatr \of{\vect{v}} = \crossp{\rotvector}{\vect{v}}$.
The axis-angle representation~$\rotvector = \rotvector* \vect{\mu}$ of~$\TNB$ can be calculated with the angle of rotation~$\rotvector$ from
%
\begin{align*}
  \rotvector*
    & = \parccos*{
          \frac{
            \ptrace{\TNB}
            -
            1
          }{
            2
          }
        }
\end{align*}
%
and the normalized axis of rotation~$\vect{\mu}$ from
%
\begin{align}
  \vect{\mu}
    & = \frac{
          1
        }{
          2 \,
          \sin \rotvector*
        } \,
        \begin{bmatrix}
          \normal_{3}
          -
          \binormal_{2}
          \\
          \binormal_{1}
          -
          \tangent_{3}
          \\
          \tangent_{2}
          -
          \normal_{1}
        \end{bmatrix} \,
        ,
        \label{eqn:spatial-dynamics:kinematics:rotation-vector}
\end{align}
%
where the vector subscript index~$\parentheses{}_{k}$ corresponds to the~$k$-th entry of the respective vector.
The change of orientation angles along the curve~$\pd{\rotvector}{\lpathcoord}$ can be inferred directly from the Darboux vector~(see~\cref{eqn:spatial-dynamics:kinematics:darboux-vector}) such that
%
\begin{align}
  \pd{
    \rotvector
  }{
    \lpathcoord
  }
  = \p{\rotvector}
    & = \darboux
      = \torsion \,
        \tangent
        +
        \curvature \,
        \binormal \,
        .
        \label{eqn:spatial-dynamics:kinematics:rotation-vector-partial}
\end{align}

With the kinematics formulation at hand, we can now derive the equations of motion using d'Alembert's Principle.
