%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/equations-of-motion/internal.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Internal Forces}\label{sec:spatial-dynamics:equations-of-motion:internal}

Forces doing internal virtual work result from deformation stresses inside a body, stemming from extension, shear, torsion, or bending of the material.
For a Cosserat Rod, the internal virtual work is composed of strain and shear resultants~$\stress$ and bending and torsion resultants~$\torque$ such that it reads
%
\begin{align}
  \virtualworkinternal
    & = \int\limits_{\cablebody}^{}{
          \transpose{
            \stress
          } \,
          \virtual{
            \p{\curve}
          }
          +
          \transpose{
            \stress
          } \,
          \crossp*{
            \p{\curve}
          }{
            \virtual{\rotvector}
          }
          +
          \transpose{
            \torque
          } \,
          \virtual{%
            \p{\rotvector}
          }
        } \,
        .
        \label{eqn:spatial-dynamics:equations-of-motion:internal}
\end{align}

Deformation measures of the physical cable are given in terms of strain measures~$\strainmeasure$ and bending measures~$\bendingmeasure$ as
%
\begin{subequations}\label{eqn:spatial-dynamics:equations-of-motion:internal:deformation-measures}
\begin{align}
  \strainmeasure
    & = \pd{
          \curve
        }{
          \lpathcoord
        }
        -
        \crossp{
          \rotvector
        }{
          \tangent
        }
      = \begin{bmatrix}
          \strainmeasure*_{\subtangent}
            \\
          \strainmeasure*_{\subnormal}
            \\
          \strainmeasure*_{\subbinormal}
        \end{bmatrix} \,
        ,
        \label{eqn:spatial-dynamics:equations-of-motion:internal:deformation-measures:strain}
    \\
  \bendingmeasure
    & = \pd{
          \rotvector
        }{
          \lpathcoord
        }
      = \begin{bmatrix}
          \bendingmeasure*_{\subtangent}
            \\
          \bendingmeasure*_{\subnormal}
            \\
          \bendingmeasure*_{\subbinormal}
        \end{bmatrix} \,
        .
        \label{eqn:spatial-dynamics:equations-of-motion:internal:deformation-measures:bending}
\end{align}
\end{subequations}
%
If the tangent is not of unit length~$\norm{\pd{\curve}{\lpathcoord}} \neq 1$ \textIe the length changes compared to the undeformed configuration, the cable is subject to stretch\textemdash or compression, for that matter.
If the normal to the cross-section surface is noncollinear to the tangent~$\crossp{ \rotvector }{ \tangent } \neq \zeros$, the cable is subject to shear deformation.

The constitutive relations between deformation measures and resulting forces and moments in the local material frame read
%
\begin{subequations}\label{eqn:spatial-dynamics:equations-of-motion:internal:restoring-forces-moments}
\begin{align}
  \vect{N}%\stress
    & = \begin{bmatrix}
          \youngsmodulus \,
            \crosssection_{\subtangent}
            & 0
            & 0
            \\
          0
            & \shearmodulus \,
              \crosssection_{\subnormal}
            & 0
            \\
          0
            & 0
            & \shearmodulus \,
              \crosssection_{\subbinormal}
        \end{bmatrix}
        \,
        \begin{bmatrix}
          \strainmeasure*_{\subtangent}
            \\
          \strainmeasure*_{\subnormal}
            \\
          \strainmeasure*_{\subbinormal}
        \end{bmatrix}
      = \matr{C}^{\strainmeasure*} \,
        \strainmeasure \,
        ,
        \label{eqn:spatial-dynamics:equations-of-motion:internal:restoring-forces-moments:stress}
    \\
  \vect{M}%\torque
    & = \begin{bmatrix}
          \shearmodulus \,
            \secondmomentofarea_{\subtangent}
            & 0
            & 0
            \\
          0
            & \youngsmodulus \,
              \secondmomentofarea_{\subnormal}
            & 0
            \\
          0
            & 0
            & \youngsmodulus \,
              \secondmomentofarea_{\subbinormal}
        \end{bmatrix}
        \,
        \begin{bmatrix}
          \bendingmeasure*_{\subtangent}
            \\
          \bendingmeasure*_{\subnormal}
            \\
          \bendingmeasure*_{\subbinormal}
        \end{bmatrix}
      = \matr{C}^{\bendingmeasure*} \,
        \bendingmeasure \,
        ,
        \label{eqn:spatial-dynamics:equations-of-motion:internal:restoring-forces-moments:moments}
\end{align}
\end{subequations}
%
where~$\youngsmodulus$ is Youngs Modulus,~$\shearmodulus$ is shear modulus with~${ \shearmodulus = \youngsmodulus / \parentheses*{ 2 + 2 \poissonratio } }$ with~$\poissonratio$ being Poisson's ratio,~$\crosssection_{k}$ are the effective cross-sectional area about the~$k$-th axis, and~$\secondmomentofarea_{k}$ is the second moment of area about the~$k$-th axis.
We assume circular cross-section thus
%
\begin{align*}
  \crosssection_{\subtangent}
  = \crosssection_{\subnormal}
  = \crosssection_{\subbinormal}
    & = \pi \,
        \pow[2]{
          \radius
        } \,
        ,
    \\
  \secondmomentofarea_{\subtangent}
    & = \frac{
          \pi \,
          \pow[4]{
            \radius
          }
        }{
          2
        } \,
        ,
    \\
  \secondmomentofarea_{\subbinormal}
  = \secondmomentofarea_{\subnormal}
    & = \frac{
          \pi \,
          \pow[4]{%
            \radius
          }
        }{
          4
        } \,
        .
\end{align*}

In global coordinates of~$\mathds{E}^{3}$, the strain measures simply need rotation about the relatively parallel adapted frame thus
%
\begin{subequations}\label{eqn:spatial-dynamics:equations-of-motion:internal:global-restoring-forces-moments}
\begin{align}
  \stress
    & = \TNB \,
        \vect{N} \,
        ,
    \\
  \torque
    & = \TNB \,
        \vect{M} \,
        .
\end{align}
\end{subequations}
