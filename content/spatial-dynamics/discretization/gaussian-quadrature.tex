%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/discretization/gaussian-quadrature.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gaussian Quadrature}\label{sec:spatial-dynamics:discretization:gaussian-quadrature}

Analytical solutions to the energy and force integrals can not generally be obtained due to the unknown nature of displacement functions~$\discretedisplacement \of{ \pathcoord, \genq \of{t} }$, or simply due to the complexity of the integrands. % \textIa~$\norm{\pp{\curve}}$.
In addition, while the integrals may be determined during numerical integration of the system, this is a computationally very expensive task and one needs to find good step sizes for the spatial integration.
However, when imposing only few assumptions on the selection of displacement functions, we can evaluate the integrals a priori \textIe once before simulation starts which noticeably reduces computational cost and increases simulation speed.
To numerically solve definite integrals, we employ quadrature rules, given as weighted sums of the integrand evaluated at specific points within the domain of integration.

Assuming function~$f \of{x}$, we can recast the integral over domain~$\interval{a}{b}$ into a weighted sum of~$\nquadraturepoints$ function values
%
\begin{align}
  \int\limits_{a}^{b}{
    f \of{x} \,
    \td{x}
  }
    & = \frac{
          b - a
        }{
          2
        } \,
        \sum\limits_{k}^{\nquadraturepoints}{
          w_{k} \,
          f \of*{
            \frac{
              b - a
            }{
              2
            } \,
            x_{k}
            +
            \frac{
              a + b
            }{
              2
            }
          }
        } \,
      ,
      \label{eqn:spatial-dynamics:discretization:quadrature-brief}
\end{align}
%
with weights~$w_{k}$ and function $f \of{x}$ evaluated at discrete points inside the interval.
By using the left-hand side of~\cref{eqn:spatial-dynamics:discretization:quadrature-brief} for the forces from~\cref{eqn:spatial-dynamics:discretization:forces} and the energies from~\cref{eqn:spatial-dynamics:discretization:energies}, we can evaluate the integrals independent of the state of the cable a priori to simulation.

A short introduction into numerical integration using quadrature rules, an overview of available quadrature techniques, and how to calculate the weights~$w_{k}$ is given in~\cref{app:chap:quadrature}.
