%!TEX root=../../../thesis.tex
%!TEX file=content/spatial-dynamics/discretization/time-varying-length.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Consideration of Time-Varying Length}\label{sec:spatial-dynamics:discretization:varying-length}

Remembering the upper integral boundary of the virtual works being the unstrained cable length~$\cablen$~(\textCf \cref{eqn:spatial-dynamics:discretization:virtual-works}), which we require to be time-varying \textIe non-fixed, we obtain integral formalations of varying upper bound.
To accommodate for this, we introduce a coordinate transformation of path abscissae~$\lpathcoord \in \domainlpathcoord$ such that its domain~$\domainlpathcoord \coloneqq \interval{0}{\cablen}$ becomes~$\domainpathcoord \coloneqq \interval{0}{1}$.
Let us introduce~$\pathcoord \in \domainpathcoord$ for which holds
%
\begin{align*}
  \pathcoord
    & = \frac{
          1
        }{
          \cablen
        } \,
        \lpathcoord \,
        ,
        \\
  \shortintertext{and equivalently}
    \lpathcoord
      & = \cablen \,
          \pathcoord \,
          .
\end{align*}
%
Further follows for the total differential~$\td{\pathcoord}$ with respect the original variable's total differential~$\td{\lpathcoord}$
%
\begin{align*}
  \td{\lpathcoord}
    & = \cablen \,
        \td{\pathcoord} \,
        .
\end{align*}

Substituting our new path coordinate into the discrete displacement function~\cref{eqn:spatial-dynamics:discretization:discrete-displacement-function} and consequently into~\cref{eqn:spatial-dynamics:discretization:curve-shape-funs} yields equivalence of both curve functions
%
\begin{align*}
  \inlambda{\shapefuns} \of{\lpathcoord} \,
  \genq \of{t}
  = 
  \inlambda{\curve} \of{\lpathcoord, t}
    & = \inxi{\curve} \of{\pathcoord, t}
      = \inxi{\shapefuns} \of{\pathcoord} \,
        \genq \of{t} \,
        .
\end{align*}

Partial derivatives of~$\inxi{\curve} \of{\pathcoord, t}$ \textWrt~$\lpathcoord$ introduce a scaling factor due to the chain rule such that
%
\begin{align*}
  \p{\inlambda{\curve}} \of{\lpathcoord, t}
    & = \frac{
          1
        }{
          \cablen
        } \,
        \p{\inxi{\curve}} \of{\pathcoord, t} \,
        ,
    \\
  \pp{\inlambda{\curve}} \of{\lpathcoord, t}
    & = \frac{
          1
        }{
          \pow{
            \cablen
          }
        } \,
        \pp{\inxi{\curve}} \of{\pathcoord, t} \,
        ,
    \\
  \ppp{\inlambda{\curve}} \of{\lpathcoord, t}
    & = \frac{
          1
        }{
          \pow[3]{
            \cablen
          }
        } \,
        \ppp{\inxi{\curve}} \of{\pathcoord, t} \,
        ,
\end{align*}
%
while the total time-derivatives of our new curve~$\curve \of{ \pathcoord, t }$ read
%
\begin{align*}
  \od{
    \inlambda{\curve} \of{ \lpathcoord, t }
  }{
    t
  }
    & = -\frac{
          \dotcablen
        }{
          \cablen
        } \,
        \pathcoord \,
        \pd{
          \inxi{\curve} \of{\pathcoord, t}
        }{
          \pathcoord
        } \,
        +
        \pd{
          \inxi{\curve} \of{ \pathcoord, t }
        }{
          t
        } \,
        ,
    \\
\begin{split}
  \od[2]{
    \inlambda{\curve} \of{\lpathcoord, t}
  }{
    t
  }
    & = \pd[2]{
          \inxi{\curve} \of{\pathcoord, t}
        }{
          t
        }
        -
        \frac{
          \ddotcablen \,
          \cablen
          -
          2 \,
          \pow{
            \dotcablen
          }
        }{
          \pow{
            \cablen
          }
        } \,
        \pathcoord \,
        \pd{
          \inxi{\curve} \of{\pathcoord, t}
        }{
          \pathcoord
        }
        + \dotsb
    \\
    & \quad%\phantom{=}
        -
        2 \,
        \frac{
          \dotcablen
        }{
          \cablen 
        } \,
        \pathcoord \,
        \md{
          \inxi{\curve} \of{\pathcoord, t}
        }{
          2
        }{
          \pathcoord
        }{
        }{
          t
        }{
        }
        +
        \pow{
          \parentheses*{
            \frac{
              \dotcablen 
            }{
              \cablen
            } \,
            \pathcoord
          }
        } \,
        \pd[2]{
          \inxi{\curve} \of{\pathcoord, t}
        }{
          \pathcoord
        } \,
        .
\end{split}
\end{align*}
%
The latter equation shows the impact of a time-varying cable length on the overall dynamics.
Not only are the equations more involved, it also shows necessity of cable velocities~$\dotcablen$ and accelerations~$\ddotcablen$.
Partial time-derivatives~$\pd{ \curve \of{ \pathcoord, t } }{t}$ and~$\pd[2]{ \curve \of{ \pathcoord, t } }{t}$, respectively, simplify to read
%
\begin{align*}
  \pd{
    \inxi{\curve} \of{\pathcoord, t}
  }{
    t
  }
    & = \inxi{\shapefuns} \of{\pathcoord} \,
        \dotgenq \of{t} \,
        ,
    \\
  \pd[2]{
    \inxi{\curve} \of{\pathcoord, t}
  }{
    t
  }
    & = \inxi{\shapefuns} \of{\pathcoord} \,
        \ddotgenq \of{t} \,
        .
\end{align*}

We can then obtain the new, fixed-domain integral formulation for our energies considering the rule of substitution by integration given interval~${ I \subseteq \mathds{R} }$ for a differentiable function~$\varphi \colon \interval{a}{b} \to I$ with integrable derivative, and a continuous function~$f \colon I \to \mathds{R}$
%
\begin{align*}
  \int\limits_{ \varphi \of{a} }^{ \varphi \of{b} }{
    f \of{u} \,
    \td{u}
  }
    & = \int\limits_{a}^{b}{
        f \of{ \varphi \of{x} } \,
        \varphi^{ \prime } \of{x} \,
        \td{x}
      } \,
\end{align*}
%
with~$u = \varphi \of{x}$ and~$\varphi^{\prime} \of{x} \td{x} = \td{ u }$.
Omitting~$\inxi{\parentheses{}}$ for better readability, it follows with the substitution for integration~$\td{\lpathcoord} = \cablen \td{\pathcoord}$ for the internal, external, and dynamic forces of the system
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:forces}
\begin{align}
  \begin{split}
  \forcevec_{\msexternal}
    & = \int\limits_{0}^{1}{
          \unitdensity \,
          \crosssection \,
          \transpose{
            \shapefuns \of{\pathcoord}
          } \,
          \gravityvec \,
          \cablen \,
          \td{\pathcoord}
        } \,
        ,
    \\
  \forcevec_{\msinternal}
    & = \int\limits_{0}^{1}{
          \parentheses*{
            \transpose{
              \shapefuns \of{\pathcoord}
            } \,
            \stress \of{\pathcoord}
            +
            \frac{
              1
            }{
              \cablen
            } \,
            \transpose{
              \crossp*{
                \p{\shapefuns} \of{\pathcoord} \,
                \genq
              }{
                \pd{
                  \rotvector \of{\pathcoord}
                }{
                  \genq
                }
              }
            } \,
            \stress \of{\pathcoord}
          } \,
          \cablen \,
          \td{\pathcoord}
        }
        +
        \dotso
    \\
    & \phantom{=}
        +
        \int\limits_{0}^{1}{
          \frac{
            1
          }{
            \cablen
          } \,
          \transpose{
            \pd{
              \p{\rotvector} \of{\pathcoord}
            }{
              \genq
            }
          } \,
          \torque \of{\pathcoord} \,
          \cablen \,
          \td{\pathcoord}
        } \,
        ,
    \\
    \forcevec_{\msdynamic}
      & = \int\limits_{0}^{1}{
            \unitdensity \,
            \crosssection \,
            \transpose*{
              \shapefuns \of{\pathcoord} \,
              \ddotgenq \,
              -
              \frac{
                \ddotcablen \,
                \cablen
                -
                2 \,
                \pow[2]{\dotcablen}
              }{
                \cablen
              } \,
              \pathcoord \,
              \p{\shapefuns} \,
              \genq
            } \,
            \shapefuns \of{\pathcoord} \,
            \cablen \,
            \td{\pathcoord}
          }
          +
          \dotsb
      \\
      %
      & \phantom{=}
        +
        \int\limits_{0}^{1}{
            \unitdensity \,
            \crosssection \,
            \transpose*{
              -
              2 \,
              \frac{
                \dotcablen
              }{
                \cablen
              } \,
              \pathcoord \,
              \p{\shapefuns} \,
              \dotgenq
              +
              \pow*[2]{
                \pathcoord \,
                \frac{
                  \dotcablen
                }{
                  \cablen
                }
              } \,
              \pp{\shapefuns}
            } \,
            \shapefuns \of{\pathcoord} \,
            \cablen \,
            \td{\pathcoord}
          } \,
          ,
  \end{split}
\end{align}
\end{subequations}

Similarly, the energies expressed in terms of the discrete displacement function given the fixed-boundary integral read
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:energies}
\begin{align}
  \energypotential_{\subgravity}
    & = \int\limits_{0}^{1}{
          \unitdensity \,
          \crosssection \,
          \gravity \,
          \transpose{
            \evecz
          } \,
          \shapefuns \of{\pathcoord} \,
          \genq \,
          \cablen \,
          \td{\pathcoord}
        } \,
        ,
        \label{eqn:spatial-dynamics:discretization:energies:gravity}
    \\
  \energypotential_{\strain*}
    & = \frac{1}{2} \,
          \int\limits_{0}^{1}{
          \transpose{
            \stress
          } \,
          \matr{C}^{\strainmeasure} \,
          \stress \,
          \cablen \,
          \td{\pathcoord}
        } \,
        ,
        \label{eqn:spatial-dynamics:discretization:energies:strain}
    \\
  \energypotential_{\bend*}
    & = \frac{1}{2} \,
          \int\limits_{0}^{1}{
          \transpose{
            \torque
          } \,
          \matr{C}^{\bendingmeasure} \,
          \torque \,
          \cablen \,
          \td{\pathcoord}
        } \,
        .
        \label{eqn:spatial-dynamics:discretization:energies:bend}
\end{align}
\end{subequations}

It may be pointed out that deformation measures of curvature~$\curvature$ and torsion~$\torsion$ given in~\cref{eqn:spatial-dynamics:kinematics:curvature-torsion} are geometric properties of the curve itself, as such these are invariant under a change of parametrization thus
%
\begin{align*}
  \inlambda{\curvature} \of{\lpathcoord}
    & = \inxi{\curvature} \of{\pathcoord} \,
      ,
    & \inlambda{\torsion} \of{\lpathcoord}
    & = \inxi{\torsion} \of{\pathcoord} \,
      .
\end{align*}

Combining~\cref{eqn:spatial-dynamics:discretization:forces} and omitting the explicit time-dependency of the generalized coordinates as well as their time-derivatives, we obtain the general dynamics equation for the constrained cable as
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:final-ode-residual}
\begin{align}
  \Mass \of{\genq} \,
  \ddotgenq
    & = \forcevec_{\msexternal} \of{ \genq, \dotgenq }
        -
        \forcevec_{\msinternal} \of{ \genq } \,
        ,
    \\
  \zeros
    & = \posconstraints \of{\genq, t} \,
        ,
    \\
\shortintertext{with}
  \posconstraints \of{\genq, t}
    & = \begin{bmatrix}
        \posconstraints_{\subproximal} \of{\genq, t}
          \\
        \posconstraints_{\subdistal} \of{\genq, t}
      \end{bmatrix} \,
        .
\end{align}
\end{subequations}
%
