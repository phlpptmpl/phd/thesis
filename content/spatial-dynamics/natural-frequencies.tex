%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/natural-frequencies.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Natural Frequencies}\label{sec:spatial-dynamics:natural-frequencies}

One reason for considering B-splines is their ability to approximate continuous spatial curves~${\curve \of{\pathcoord} \in C^{2}}$ with only two parameters.
Since B-splines are piecewise polynomial curves, we can change both the number of pieces composing our B-spline as well as we can change the local polynomial degree.
In addition, as given in~\cref{sec:spatial-dynamics:basis-functions} presenting the investigated basis functions, the control points of a curve defined by B-splines only have local support \textIe changes in one control point\textemdash in our case the control points are generalized coordinates\textemdash affects the curve shape only locally.
In other terms, the generalized coordinates are only locally coupled to a limited number of their preceding and succeeding control points which depends only on the local polynomial degree.
Theoretically, this B-spline property leads to making the system numerically less stiff thus better predicting the natural frequencies~(see the example on composite \beziercurves in~\cref{sec:spatial-dynamics:basis-functions}).

%% nurbs cable, fixed segment, varying degree
\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--natural-frequencies--segment-10--tension-free}
  \includegraphics[%
    ]{cosserat-rod/bsplines/natural-frequencies/segment-10/tension-free}
  \tikzexternaldisable%
  \caption[%
    Natural frequencies of B-spline cable of different degree.
  ]{%
    Natural frequencies of B-spline cable with number of segments~${\numsegments = 10}$ and local polynomial degree~${\polydeg \in \numset{3;4;5;6}}$ compared to natural frequencies of the tension-free beam.
    Data are tabularized in~\cref{app:chap:natural-frequencies}.
  }
  \label{fig:spatial-dynamics:natural-frequencies:bsplines:varying-degree}
\end{figure}

In a first evaluation, we split our cable into~${\numsegments = 10}$ segments each defined through local B-splines of varying polynomial degree.
\Cref{fig:spatial-dynamics:natural-frequencies:bsplines:varying-degree} shows decreasing relative differences in the prediction of natural frequencies of the simply-supported beam as the local polynomial degree is increased.
Further, the approximation error between numerical and analytical solution always stays below the threshold of~$\Delta \naturalfrequency_{k} < 1$ for all natural frequencies, the first~$7$ natural frequencies are bounded by a relative difference of~${ \Delta^{\mc{rel}} \naturalfrequency_{k} \leq \num{1e-2} }$, $\irange{7}{k}$, yielding an absolute error of~${\Delta^{\mc{abs}} \naturalfrequency_{k} \leq \SI{0.18}{\radian\per\second}}$, equivalently~${\Delta^{\mc{abs}} \naturalfrequency_{k} \approxeq \SI{0.02864789}{\hertz}}$.

%% nurbs cable, fixed segment, varying degree
\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--natural-frequencies--degree-04--tension-free}
  \includegraphics[%
    ]{cosserat-rod/bsplines/natural-frequencies/degree-04/tension-free}
  \tikzexternaldisable%
  \caption[%
    Natural frequencies of B-spline cable of different number of segments.
  ]{%
    Natural frequencies of B-spline cable with local polynomial degree~${\polydeg = 4}$ and number of segments~${\numsegments \in \numset{1;5;10;20}}$ compared to natural frequencies of the tension-free beam.
    Data are tabularized in~\cref{app:chap:natural-frequencies}.
  }
  \label{fig:spatial-dynamics:natural-frequencies:bsplines:varying-segment}
\end{figure}

In a second evaluation, we set the local polynomial degree to fixed~${\polydeg = 4}$ and vary the number of segments~(see~\cref{fig:spatial-dynamics:natural-frequencies:bsplines:varying-segment}).
We assume the impact of increasing number of segments more significant than raising local polynomial degree since increased segmentation allows to form more complex curves.
With number of segments~$\numsegments \leq 5$, worsening of approximation of natural frequencies is entailed with maximum underestimations of~${ \abs{\Delta \naturalfrequency_{k}} \approxeq \SI{0.98}{\radian\per\second} }$ or equivalently of ${ \abs{\Delta \naturalfrequency_{k}} \approxeq \SI{0.15597184}{\hertz} }$; still comparatively low.
Once the number of segments outgrows~$\numsegments \geq 2 \, \polydeg - 1$, approximation of lower-order natural frequencies converges toward differences of~$\Delta \naturalfrequency_{k} \lessapprox \SI{1e-6}{\radian\per\second} \approxeq \SI{1.59154943e-07}{\hertz}$.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--natural-frequencies--convergence-rate}
  \includegraphics[%
    ]{cosserat-rod/bsplines/natural-frequencies/convergence-rate}
  \tikzexternaldisable%
  \caption[%
    Convergence rate of natural frequencies of B-spline cable.
  ]{%
    Convergence rate of natural frequencies~$\naturalfrequency_{k}$, $\parentheses*{ k \in \numset{1;2;4;8} }$ for local polynomial degree~$\polydeg \in \numset{3;4;5;6}$ confirming our expectation of lower error for higher local polynomial degree~$\polydeg$ and quicker approximation with increasing number of segments~$\numsegments$.
  }
  \label{fig:spatial-dynamics:natural-frequencies:bsplines:convergence-rate}
\end{figure}

Varying of local polynomial degree~$\polydeg$ affects approximation of natural frequencies likewise as variation of number of segments does.
However, noticeable improvement can only be achieved by high-order local polynomial degree, which does not provide the same convenience as increased number of segments.
It is thus beneficial to vary the number of segments, not only because it allows for representing more complex geometric curves but also because it provides for quick convergence of the lower-order natural frequencies with only few segments~(see~\cref{fig:spatial-dynamics:natural-frequencies:bsplines:convergence-rate}).
As such, we fix the local polynomial degree for the remainder of this thesis to~$\polydeg \equiv 4$ as a good trade-off between approximation quality, numerical stability, but also continuity of the cable~(we require at least~$\curve \of{ \pathcoord, t } \in C^{3}$ to have continuous torsion \textIe~$\polydeg \geq 4$).

%% nurbs cable, fixed segment, fixed degree, varying tension
\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{cosserat-rod--bsplines--natural-frequencies--varying-tension-d04-s10}
  \includegraphics[%
    ]{cosserat-rod/bsplines/natural-frequencies/varying-tension-d04-s10}
  \tikzexternaldisable%
  \caption[%
    Natural frequencies of B-spline cable of varying axial tension.
  ]{%
    Natural frequencies of B-spline cable with number of segments~${\numsegments = 10}$ and local polynomial degree~${\polydeg = 4}$ compared to natural frequencies of the axially loaded beam with tension varying in~${\stress*_{\ms{axial}} \in \SIset{0;100;1500;3000}{\newton}}$.%
  }
  \label{fig:spatial-dynamics:natural-frequencies:bsplines:varying-tension}
\end{figure}

Increasing axial beam stress within the range of interest~${\cabforce \in \SIinterval{100}{3000}{\newton}}$ has negligible effect on the approximation of  natural frequencies.
For higher order natural frequencies, we do see a decrease in the error as axial tension increases with the maximum error being lower than~$\Delta^{\mc{abs}} \naturalfrequency_{k} < \SI{4}{\percent}$, a decrease of approximately \SI{63}{\percent}.
At the same time, lower order natural frequencies are approximated equally well as in the axially tension-free case.
