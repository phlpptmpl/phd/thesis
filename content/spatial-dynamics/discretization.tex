%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/discretization.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discretization}\label{sec:spatial-dynamics:discretization}

To solve the infinite dimensional problem obtained from~\cref{sec:spatial-dynamics:kinematics}, we assume the kinematics can be discretized by means of a discrete displacement function satisfying geometric constraints at the proximal and distal cable end such that we can write the spatial curve~$\curve \of{ \lpathcoord, t }$  as
%
\begin{align}
  \curve \of{ \lpathcoord, t }
    & = \discretedisplacement \of{\lpathcoord, \genq \of{t}} \,
        ,
        \label{eqn:spatial-dynamics:discretization:discrete-displacement-function}
\end{align}
%
for which we introduce time-dependent generalized coordinates~$\genq \of{t}$.
Introducing a discrete displacement function implies constraints on the kinematic properties of the curve between the discrete points.
In the simplest case, we may impose a linear blending function between two discrete points~$\genq_{1}$ and~$\genq_{2}$ such that the resulting curve shape reads
%
\begin{align*}
  \curve \of{ \lpathcoord, t }
    & = \parentheses{1 - \lpathcoord} \,
        \genq_{1} \of{t}
        +
        \lpathcoord \,
        \genq_{2} \of{t} \,
        .
\end{align*}
%
We can directly see from above equation that the discrete displacement function can be written as linear combination of some later-to-defined blending functions~$\shapefuns \of{\lpathcoord}$ and the generalized coordinates~$\genq \of{t}$.
In above example, we would have~$\shapefuns = \left[ 1 - \lpathcoord, \lpathcoord \right]$ and~$\genq \of{t} = \transpose{ \left[ \genq*_{1} \of{t}, \genq*_{2} \of{t} \right] }$.
More generally, this approach yields the discrete displacement field given through
%
\begin{align}
  \curve \of{ \lpathcoord, t }
    & = \discretedisplacement \of{\lpathcoord, \genq \of{t}}
      = \shapefuns \of{\lpathcoord} \,
        \genq \of{t} \,
        .
        \label{eqn:spatial-dynamics:discretization:curve-shape-funs}
\end{align}

Let us assume from here on~$\shapefuns \of{\lpathcoord} \in \mathds{R}^{3 \times 3 \, \numshapefuns}$ and thus~$\genq \of{t} \in \mathds{R}^{3 \, \numshapefuns \times 1}$, such that we obtain a three-dimensional curve with three, not necessarily equal, sets of generalized coordinates.
Alternatively, we may write our curve in components as
%
\begin{align*}
\begin{split}
  \curve \of{ \lpathcoord, t }
    =
      \begin{bmatrix}
          x \of{ \lpathcoord, t }
            \\
          y \of{ \lpathcoord, t }
            \\
          z \of{ \lpathcoord, t }
        \end{bmatrix}
    & =
        \begin{bmatrix}
          \shapefuns_{x} \of{\lpathcoord} \,
            \genq_{x} \of{t}
            \\
          \shapefuns_{y} \of{\lpathcoord} \,
            \genq_{y} \of{t}
            \\
          \shapefuns_{z} \of{\lpathcoord} \,
            \genq_{z} \of{t}
        \end{bmatrix}
      = \underbrace{
          \begin{bmatrix}
            \shapefuns_{x} \of{\lpathcoord}
              & \zeros
              & \zeros
              \\
            \zeros
              & \shapefuns_{y} \of{\lpathcoord}
              & \zeros
              \\
            \zeros
              & \zeros
              & \shapefuns_{z} \of{\lpathcoord}
          \end{bmatrix}
        }_{
          \shapefuns \of{\lpathcoord}
        }
        \,
        \underbrace{
          \begin{bmatrix}
            \genq_{x} \of{t}
              \\
            \genq_{y} \of{t}
              \\
            \genq_{z} \of{t}
          \end{bmatrix}
        }_{
          \genq \of{t}
        } \,
        .
\end{split}
\end{align*}

Following the kinematics in~\cref{sec:spatial-dynamics:kinematics}, we require not only the discrete curve but also its first three derivatives \textWrt path coordinate~$\lpathcoord$ and time~$t$ due to the measurement of torsion~\cref{eqn:spatial-dynamics:kinematics:curvature-torsion:torsion}, such that we obtain
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:curve-derivatives}
\begin{align}
  \p{\curve}
  = \pd{
    \discretedisplacement \of{\lpathcoord, \genq}
  }{
    \lpathcoord
  }
    & = \p{\shapefuns} \of{\lpathcoord} \,
        \genq \of{t} \,
        ,
    \\
  \pp{\curve}
  = \pd[2]{
    \discretedisplacement \of{\lpathcoord, \genq}
  }{
    \lpathcoord
  }
    & = \pp{\shapefuns} \of{\lpathcoord} \,
        \genq \of{t} \,
        ,
    \\
  \ppp{\curve}
  = \pd[3]{
    \discretedisplacement \of{\lpathcoord, \genq}
  }{
    \lpathcoord
  }
    & = \ppp{\shapefuns} \of{\lpathcoord} \,
        \genq \of{t} \,
        ,
    \\
  \dotcurve
  = \od{
    \discretedisplacement \of{\lpathcoord, \genq}
  }{
    t
  }
    & = \shapefuns \of{\lpathcoord} \,
        \dotgenq \of{t} \,
        ,
    \\
  \ddotcurve
  = \od[2]{
    \discretedisplacement \of{\lpathcoord, \genq}
  }{
    t
  }
    & = \shapefuns \of{\lpathcoord} \,
        \ddotgenq \of{t} \,
        .
\end{align}
\end{subequations}
%
Consequently, our virtual displacements require accounting for the discrete constraints resulting in
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:virtual-displacements}
\begin{align}
  \virtual{\curve}
    & = \pd{
          \curve
        }{
          \genq
        } \,
        \virtual{\genq} \,
        ,
    \\
  \virtual{\p{\curve}}
    & = \pd{
          \p{\curve}
        }{
          \genq
        } \,
        \virtual{\genq} \,
        ,
    \\
  \virtual{\rotvector}
    & = \pd{
          \rotvector
        }{
          \genq
        } \,
        \virtual{\genq} \,
        ,
    \\
  \virtual{\p{\rotvector}}
    & = \pd{
          \p{\rotvector}
        }{
          \genq
        } \,
        \virtual{\genq} \,
        ,
\end{align}
\end{subequations}
%
with the rotation vector~$\rotvector$ igiven in~\cref{eqn:spatial-dynamics:kinematics:rotation-vector} and its derivative~$\p{\rotvector}$ given in~\cref{eqn:spatial-dynamics:kinematics:rotation-vector-partial}.

Lastly, we require resolving the integral forms over the cable body currently given as Riemann integral over body~$\cablebody$.
Assuming a cable of constant cross-section~$\crosssection$ under all external and internal forces and of constant unit density~$\unitdensity$, we obtain for the Rieman integral
%
\begin{align*}
  \int\limits_{\cablebody}^{}{}
    & = \int\limits_{M}^{}{
          \td{m}
        }
      = \int\limits_{0}^{\cablen}{
          \unitdensity \,
          \crosssection \,
          \td{\lpathcoord}
        } \,
      .
\end{align*}
%

By substituting respective derivatives of the discrete displacement function from~\cref{eqn:spatial-dynamics:discretization:curve-derivatives} and the virtual displacements~\cref{eqn:spatial-dynamics:discretization:virtual-displacements} into the virtual works from~\cref{eqn:spatial-dynamics:equations-of-motion:dynamic,eqn:spatial-dynamics:equations-of-motion:external,eqn:spatial-dynamics:equations-of-motion:internal} and respecting the explicit boundaries of integration, we obtain the virtual works as function of our sought-for forces reading
%
\begin{subequations}\label{eqn:spatial-dynamics:discretization:virtual-works}
\begin{align}
  \virtualworkdynamic
    & = \int\limits_{0}^{\cablen}{
          \unitdensity \,
          \crosssection \,
          \transpose*{
            \shapefuns \,
            \ddotgenq
          } \,
          \shapefuns \,
          \td{\lpathcoord}
        } \,
        \virtual{\genq}
      &
        = \transpose{
          \forcevec_{\msdynamic}
        } \,
        \virtual{\genq} \,
        ,
    \\
  \virtualworkexternal
    & = \int\limits_{0}^{\cablen}{
          \unitdensity \,
          \crosssection \,
          \transpose{
            \gravityvec
          } \,
          \shapefuns \,
          \td{\lpathcoord}
        } \,
        \virtual{\genq}
      &
        = \transpose{
          \forcevec_{\msexternal}
        } \,
        \virtual{\genq} \,
        ,
    \\
  \virtualworkinternal
    & = \int\limits_{0}^{\cablen}{
          \parentheses*{
            \transpose{
              \stress
            } \,
            \shapefuns
            +
            \transpose{
              \stress
            } \,
            \crossp*{
              \parentheses*{
                \p{\shapefuns} \,
                \genq
              }
            }{
              \pd{
                \rotvector
              }{
                \genq
              }
            }
            +
            \transpose{
              \torque
            } \,
            \pd{
              \p{\rotvector}
            }{
              \genq
            }
          } \,
          \td{\lpathcoord}
        } \,
        \virtual{\genq}
      &
        = \transpose{
          \forcevec_{\msinternal}
        } \,
        \virtual{\genq} \,
        .
\end{align}
\end{subequations}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/discretization/time-varying-length}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/discretization/gaussian-quadrature}
