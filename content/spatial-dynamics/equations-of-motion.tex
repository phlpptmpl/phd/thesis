%!TEX root=../../thesis.tex
%!TEX file=content/spatial-dynamics/equations-of-motion.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Equations of Motion}\label{sec:spatial-dynamics:equations-of-motion}

Due to its nature, deriving the equations of motion using D'Alembert's Principle is more convenient for continua and generally applicable without considering specialties as making the transition from Lagrangian mechanics to continuum mechanics.
We follow the principle of virtual work to obtain the equations of motion of the cable model.

Within Newtonian mechanics, two types of forces can be identified: those that can do work, and those that cannot.
A force that can do work is destined to perform infinitesimal work~$\virtualwork_{\ms{w}} = \transpose{ \forcevec_{\ms{w}} } \, \virtualdisplacement$ along an infinitesimal displacement~$\virtualdisplacement$.
These forces include conservative forces \textEg gravity, or non-conservative forces \textEg friction.
Forces that cannot do work~(passive forces if one wants to call them such) are generally constraint forces such as normal or tension forces.
It may be clear that such a constraint force~$\forcevec_{\ms{p}}$ will not perform work~$\virtualwork_{\ms{p}} = \transpose{ \forcevec_{\ms{p}} } \, \virtualdisplacement$ as the infinitesimal displacement~$\virtualdisplacement$ would violate the constraints.

D'Alembert's Principle may find its origin in the works on static equilibrium of levers by Aristotle.
The formulation used in modern days is based on Jean Bernoulli and is given for a system composed of~$N$ particles in static equilibrium with vanishing virtual works such that
%
\begin{align}
  \virtualwork
    = \sum\limits_{i = 1}^{N}{
        \transpose{
          \forcevec_{i}
        } \,
        \virtualdisplacement_{i}
      }
    & = 0 \,
        ,
        \label{eqn:spatial-dynamics:equations-of-motion:virtual-work:principle}
\end{align}
%
for all of the physical constraints satisfying virtual displacements~$\parentheses{ \virtualdisplacement_{1}, \dotsc, \virtualdisplacement_{N} }$.
Later, Jean-Baptiste le Rond d’Alembert extended the above principle by Bernoulli to include inertial forces~$\mass_{i} \, \ddotvect{x}_{i}$ in the principle of virtual work rendering it
%
\begin{align}
  \sum\limits_{i = 1}^{N}{
      \transpose{
        \parentheses*{
          \forcevec_{i} - \mass_{i} \od[2]{ \vect{x}_{i} }{ t }
        }
      } \,
      \virtualdisplacement_{i}
    }
      & = 0 \,
          .
          \label{eqn:spatial-dynamics:equations-of-motion:virtual-work:dalembert-principle}
\end{align}

We can read d'Alembert's Principle from~\cref{eqn:spatial-dynamics:equations-of-motion:virtual-work:dalembert-principle} such that work done by all active forces acting on a system of~$N$ particles is algebraically equal to the work done by all the acceleration forces.
To further extend d'Alembert's principle to the use case in this thesis, we split the total amount of virtual work~$\virtualwork$ into three distinct virtual%
\begin{inparaenum}[1)]
  \item internal work~$\virtualworkinternal$~(see~\cref{sec:spatial-dynamics:equations-of-motion:internal})
  \item external work~$\virtualworkexternal$~(see~\cref{sec:spatial-dynamics:equations-of-motion:external}), and 
  \item dynamic work~$\virtualworkdynamic$~(see~\cref{sec:spatial-dynamics:equations-of-motion:dynamic})
\end{inparaenum}%
such that they compose as
%
\begin{align}
  \virtualwork
    & = \virtualworkinternal
        -
        \virtualworkexternal
        +
        \virtualworkdynamic
        \,
        .
        \label{eqn:spatial-dynamics:equations-of-motion:virtual-work:total-virtual-work}
\end{align}

By appropriate substitution of~\cref{eqn:spatial-dynamics:equations-of-motion:virtual-work:dalembert-principle} in~\cref{eqn:spatial-dynamics:equations-of-motion:virtual-work:total-virtual-work} we obtain the equations of motion form of d'Alembert's principle reading
%
\begin{align*}
  \zeros
    & = \transpose*{
            \forcevec_{\msinternal}
            -
            \forcevec_{\msexternal}
            +
            \forcevec_{\msdynamic}
        } \,
        \virtualdisplacement \,
        .
\end{align*}
%
Assuming that we find a formulation of all forces given a set of generalized coordinates~$\genq$ and their respective time-derivatives, we can rewrite the equations of motion to a more commonly known form
%
\begin{align*}
  \zeros
    & = \transpose*{
            \forcevec_{\msinternal} \of{\genq}
            -
            \forcevec_{\msexternal} \of{\genq}
            +
            \forcevec_{\msdynamic} \of{\genq, \ddotgenq}
        } \,
        \virtual{\genq}
        \,
        ,
\end{align*}
%
which must hold true, according to Bernoulli and d'Alembert, for every displacement and for all times \textIe~$\forall \, \virtual{\genq}, t$.
This is only valid if the sum of forces vanishes from which we obtain the standard formulation of the equations of motion
%
\begin{align}
  \zeros
    & = \Mass \of{\genq} \,
        \ddotgenq \of{t}
        -
        \forcevec_{\msexternal} \of{ \genq \of{t}, \dotgenq \of{t} }
        +
        \forcevec_{\msinternal} \of{ \genq \of{t} }
        \,
        .
        \label{eqn:spatial-dynamics:equations-of-motion:general-dalembert-ode}
\end{align}
%
\Cref{eqn:spatial-dynamics:equations-of-motion:general-dalembert-ode} no longer contains terms of~$\forcevec_{\msdynamic} \of{\genq, \ddotgenq}$ since we used linearity of the dynamic forces in generalized accelerations~$\ddotgenq$ such that
%
\begin{align*}
  \forcevec_{\msdynamic} \of{\genq, \ddotgenq}
    & = \Mass \of{\genq} \,
        \ddotgenq \,
        .
\end{align*}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion/external}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion/internal}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion/dynamic}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion/energies}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{content/spatial-dynamics/equations-of-motion/constraints}

The equations of motion derived in this section provide a \gls{DAE} in unstrained path coordinate~$\lpathcoord$ and time~$t$ that allows for forward-in-time projection of the dynamics of a Cosserat rod\textemdash or in our case of a cable.
What is lacking is a tractable formulation of the still integral formulation of the dynamics.
To remedy the up to now infinite dimensional problem, we introduce the Rayleigh-Ritz approach in order to discretize the solution space of our cable into a lower-dimensional subspace of possible shapes.
In the end, we want to obtain a discrete \gls{DAE}-system in a set of generalized coordinates and their time-derivatives of which we can perform forward-in-time integration.
