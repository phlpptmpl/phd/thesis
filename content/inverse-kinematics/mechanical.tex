%!TEX root=../../thesis.tex
%!TEX file=content/inverse-kinematics/mechanical.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Physical Determination}\label{sec:inverse-kinematics:physical}

In previous chapters, the cable was considered to be ideal in such ways that it neither has any mass nor any radial flexibility.
Real cables, however, do not confine with these assumptions, as can seen most notably when looking at suspension bridges.
Due to their own weight, cables used in these applications sag visibly drastically and tensing them to a straight line is impossible to achieve.
For \caro-based applications, similar behavior can be experienced.
Yet it is not that prominent an effect, it still needs consideration for both the inverse kinematics as well as forward dynamics on either medium-scale \caros such as the \Expo robots or large-scale robots such as the \Fast \caro.

On the following pages, the inverse kinematics problem will briefly be derived in terms of the terminology typically used on \caros.
A thorough derivation of the equations may be found in 
The derivation is based upon~\autocite{Irvine.1981} and similarly transferred to \caros as~\textcite{Kozak.2006} have performed.

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--catenary--cable}
  \includegraphics[%
      height=0.35\textheight,%
    ]{kinematics/catenary/cable}
  \tikzexternaldisable%
  \caption[%
    Cable with mechanical properties sagging under the influence of gravity.%
  ]{%
    Diagram of a sagging cable acting under gravity fixed at cable drawing point~$\Ai$ with force~$\forcevec_{\Bi}$ applied at its distal attachment point~$\Bi$ with unit density~$\unitdensity$, Young's modulus~$\youngsmodulus$, and unstrained cross-section~$\crosssection$.
  }
  \label{fig:inverse-kinematics:catenary}
\end{figure}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--catenary--vector-loop}
  \includegraphics[%
      width=0.60\linewidth,%
    ]{kinematics/catenary/vector-loop}
  \tikzexternaldisable%
  \caption[%
    Cable with mechanical properties sagging under the influence of gravity.%
  ]{%
    Diagram of a sagging cable acting under gravity fixed at cable drawing point~$\Ai$ with force~$\forcevec_{\Bi}$ applied at its distal attachment point~$\Bi$ with unit density~$\unitdensity$, Young's modulus~$\youngsmodulus$, and unstrained cross-section~$\crosssection$.
  }
  \label{fig:inverse-kinematics:catenary}
\end{figure}

We assume the path coordinate~$\lpathcoord \in \domainlpathcoord \equiv \interval{0}{\cablen_{\subunstrained}}$ to be the unstrained path length and~$\strainedlpathcoord \in \domainstrainedlpathcoord \equiv \interval{0}{\cablen_{\subunstrained} + \delcablen}$ a path abscissae along the strained cable length.
By balancing the forces along axes~$\evecx[\subcable]$ and~$\evecz[\subcable]$, the static equilibrium of a point~$\arbpoint \of*{x, z; \strainedlpathcoord}$ along the cable from~\cref{fig:inverse-kinematics:catenary} can be stated to yield
%
\begin{subequations}\label{eqn:inverse-kinematics:catenary:static-equilibrium}
\begin{align}
  \tension \,
  \parentheses*{
    \od{%
      x
    }{
      \strainedlpathcoord
    }
  }
    & = \force_{x} \, , \\
  \tension \,
  \parentheses*{
    \od{
      z
    }{
      \strainedlpathcoord
    }
  }
    & = \force_{z}
        +
        \unitdensity \,
        \gravity \,
        \parentheses*{
          \lpathcoord
          -
          \cablen_{\subunstrained}
        } \,
        ,
\end{align}
\end{subequations}
%
in which~$\tension$ be the cable tension at point~$\strainedlpathcoord$.
In addition to~\cref{eqn:inverse-kinematics:catenary:static-equilibrium}, the shape of the segment must satisfy the geometric constraint
%
\begin{align}
  \pow{
    \td{x}
  }
  +
  \pow{
    \td{z}
  }
    & = \pow{
          \td{\strainedlpathcoord}
        } \,
        ,
        \label{eqn:inverse-kinematics:catenary:geometric-constraint}
\end{align}
%
which is interpreted as the tangent at the current strained point~$\strainedlpathcoord$.

Substituting~\cref{eqn:inverse-kinematics:catenary:static-equilibrium} into~\cref{eqn:inverse-kinematics:catenary:geometric-constraint}, we find an expression for the tension~$\tension$ as a function of the unstrained cable path~$\lpathcoord$:
%
\begin{align}
  \tension \of{\lpathcoord}
    & = \sqrt{
          \pow{
            \force_{x}
          }
          +
          \pow*[2]{
            \force_{z}
            +
            \unitdensity \,
            \gravity \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            } 
          }
        } \,
        .
\end{align}
%
Assuming on the other hand Hooke's law to apply for the tension, the tension at {point~$\lpathcoord$} must read
%
\begin{align}
  \tension \of{\lpathcoord}
    & = \youngsmodulus \,
        \crosssection \,
        \parentheses*{
          \od{
            \strainedlpathcoord
          }{
            \lpathcoord
          }
          -
          1
        } \,
        .
        \label{eqn:inverse-kinematics:catenary:hookes-law}
\end{align}

Considering~$\od{x}{\lpathcoord} = \od{x}{\strainedlpathcoord} \od{\strainedlpathcoord}{\lpathcoord}$ and likewise~$\od{z}{\lpathcoord} = \od{z}{\strainedlpathcoord} \od{\strainedlpathcoord}{\lpathcoord}$, differential equations for~$x$ and~$z$ in~$\lpathcoord$ can be found:
%
\begin{subequations}\label{eqn:inverse-kinematics:catenary:geometric-differential-equations}
\begin{align}
  \od{
    x
  }{
    \lpathcoord
  }
    & = \frac{
            \force_{x}
          }{
            \youngsmodulus \,
            \crosssection
          }
          +
          \frac{
            \force_{x}
          }{
            \sqrt{
              \pow{
                \force_{x}
              }
              +
              \pow*[2]{
                \force_{z}
                +
                \unitdensity \,
                \gravity \,
                \parentheses*{
                  \lpathcoord
                  -
                  \cablen_{\subunstrained}
                }
              }
            }
          } \,
    \\
  \begin{split}
    \od{
      z
    }{
      \lpathcoord
    }
      & = \frac{
            \force_{z}
          }{
            \youngsmodulus \,
            \crosssection
          }%
          +
          \frac{
            \unitdensity \,
            \gravity \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }{
            \youngsmodulus \,
            \crosssection
          } \\
      & \phantom{=}%
        +
        \frac{
          \force_{z}
          +
          \unitdensity \,
          \gravity \,
          \parentheses*{
            \lpathcoord
            -
            \cablen_{\subunstrained}
          }
        }{ 
          \sqrt{
            \pow{
              \force_{x}
            }
            +
            \pow*[2]{
              \force_{z}
              + 
              \unitdensity \,
              \gravity \,
              \parentheses*{
                \lpathcoord
                -
                \cablen_{\subunstrained}
              }
            }
          }
        } \,
        .
  \end{split}
\end{align}
\end{subequations}

Integrating~\cref{eqn:inverse-kinematics:catenary:geometric-differential-equations} for~$\lpathcoord$ and applying the boundary conditions at~$\lpathcoord = 0$, as seen in~\cref{fig:inverse-kinematics:catenary}, \textIe~$x \of{0} = 0~$ and~$z \of{0} = 0$ yields the equation of the cable sagging under the influence of gravity
%
\begin{subequations}\label{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}
\begin{align}
\begin{split}
  x \of{\lpathcoord}
    & = \frac{
          \force_{x}
        }{
          \youngsmodulus \,
          \crosssection
        } \,
        \lpathcoord%
      + \frac{
          \abs{\force_{x}}
        }{
          \unitdensity \,
          \gravity
        } \,
        \left(
          \parcsinh*{
            \frac{
              \force_{z}
              +
              \unitdensity \,
              \gravity \,
              \parentheses*{
                \lpathcoord
                -
                \cablen_{\subunstrained}
              }
            }{
              \force_{x}
            }
          }
        \right.
        +
        \dotsb \\
    & \phantom{=}
      \left.
        -
        \parcsinh*{
          \frac{
            \force_{z}
            -
            \unitdensity \,
            \gravity \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }{
            \force_{x}
          }
        }
      \right) \,
      ,
\end{split}
    \\
\begin{split}
  z \of{\lpathcoord}
    & = \frac{
          \lpathcoord
        }{
          \youngsmodulus \,
          \crosssection
        } \,
        \parentheses*{
          \force_{z} %
          +
          \unitdensity \,
          \gravity \,
          \parentheses*{
            \frac{
              \lpathcoord
            }{
              2
            }
            -
            \cablen_{\subunstrained}
          }
        }
        +
        \dotsb
        \\
    & \phantom{=}
      +
      \frac{
        1
      }{
        \unitdensity \,
        \gravity
      } \,
      \parentheses*{
        \sqrt{
          \pow{
            \force_{x}
          }
          +
          \pow*[2]{
            \force_{z}
            +
            \unitdensity \,
            \gravity \,
            \parentheses*{
              \lpathcoord
              -
              \cablen_{\subunstrained}
            }
          }
        }%
        -
        \sqrt{
          \pow{
            \force_{x}
          }
          +
          \pow*[2]{
            \force_{z}
            -
            \unitdensity \,
            \gravity \,
            \cablen_{\subunstrained}
          }
        }%
      } \,
      ,
\end{split}
\end{align}
\end{subequations}

To determine the strained length of the cable between proximal and distal point, we integrate~\cref{eqn:inverse-kinematics:catenary:hookes-law} with respect to the~$\lpathcoord$ and evaluate for~$\lpathcoord = \cablen_{\subunstrained}$, yielding the strained length
%
\begin{align*}
  \cablen_{\substrained}
    & = \cablen_{\subunstrained}
        +
        \frac{
          1
        }{
          2 \,
          \unitdensity \,
          \youngsmodulus \,
          \crosssection
        } \,
        \left(
          \force_{z} \,
          \sqrt{
            \pow{
              \force_{x}
            }
            +
            \pow{
              \force_{z}
            }
          }
          +
          \pow{
            \force_{x}
          } \,
          \parcsinh*{
            \frac{
              \force_{z}
            }{
              \abs{\force_{x}}
            }
          }
          +
          \dotsb
        \right.
    \\
    & \phantom{=}
      \left.
        -
        \parentheses*{
          \force_{z}
          -
          \unitdensity \,
          \gravity \,
          \cablen_{\subunstrained}
        } \,
        \sqrt{
          \pow{
            \force_{x}
          }
          +
          \pow*{
            \force_{z}
            -
            \unitdensity \,
            \gravity \,
            \cablen_{\subunstrained}
          }
        }
        -
        \pow{
          \force_{x}
        } \,
        \parcsinh*{
          \frac{
            \force_{z}
            -
            \unitdensity \,
            \gravity \,
            \cablen_{\subunstrained}
          }{
            \force_{x}
          }
        }
      \right) \,
      .
\end{align*}



We can read~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution} as function of two inpdenent variables and one dependent variables, depending on what we assume given and what we are wanting to determine:
%
\begin{description}
  \item[Force and length given]
    In case of a given distal force~$\forcevec_{\Bi}$ and a given unstrained cable length~$\cablen_{\subunstrained}$, the cable shape can be solved for straightforward by simply substituting the values into~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution} and evaluating them along the unstrained path coordinate~$\lpathcoord \in \interval{0}{\cablen_{\subunstrained}}$.
  \item[Length and distsal point given]
    This combination is the most useful for forward dynamics as it allows for calculating the force necesssary to displace the cable such that it reaches a given distal point.
    Likewise it can be used to determine the restoring force said cable bears onto the platform.
  \item[Distal point and force given]
    For determination of the cable length given a distal force~$\forcevec_{\Bi}$ and a distal position~$\Bi$, this combination makes most sense in the control loop along a trajectory.
    Since a valid force distribution balancing the mobile platform at the given position is give, we require only knowing the unstraiend cable length that will attain said position and force.
    In addition, this is the formulation as given for solving the inverse kinetostatic problem of \caros with hefty cables.
\end{description}
%

Of course, we can combine the pulley kinematics from~\cref{sec:inverse-kinematics:geometric:pulley} with the physics-based determination from this section to incorporate the additional cable length due to pulley guidance.
A thorough derivation, though not applicable to a general \caro configuration, is given by~\textcite{Gouttefarde.2014}.
The solution to the pulley-based hefty cable \gls{IKP} becomes more involved as the cable wraps around the pulley and thus its proximal angle of leave is confined.
However, similar to the standard pulley-based \gls{IKP}, the cable also tangentially leaves the pulley adding a kinematic constraint into the equation that can be exploited to simplify the overall solving.Z
