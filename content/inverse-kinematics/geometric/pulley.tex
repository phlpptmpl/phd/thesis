%!TEX root=../../../thesis.tex
%!TEX file=content/inverse-kinematics/geometric/pulley.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Pulley Kinematics}\label{sec:inverse-kinematics:geometric:pulley}

Due to abrasion of the cables being guided over punctiform deflection units, their lifespan reduces drastically, especially when under high tension.
This behavior suggests itself as a sharp corner acts like a knife cutting the cable when it is at rest and scraping it when in motion.
It is then just a question of time till enough fibers ruptured and the cable completely fails.

During the analysis of the workspace on \Segesta, \citeauthor{Verhoeven.2004,Bruckmann.2008,Pott.2012} have mechanically extended the system to include pulleys instead of punctiform eyelets~\autocite{Verhoeven.2004,Bruckmann.2008,Pott.2012}.
The advantage was less fatigue and less ruptured cables, on the downside the geometric length of the cable increases.
At a given nominal cable length, the overall tension increases due to an increased geometric length.
All this affects the system, in particular the workspace and accuracy.
To compensate for the additional distance of the cable on the pulley, in particular two algorithms, both based on geometric assessment of the pulley-cable plane, were derived which will be briefly derived here for later comparison.



%~\autocite{Bruckmann.2008}



%~\autocite{Pott.2012}




\begin{figure}[tbp]
  \centering
  \smaller[1]
  \tikzexternalenable%
  \tikzsetnextfilename{kinematics--pulley--vector-loop}
  \includegraphics[%
      width=0.60\linewidth,%
    ]{kinematics/pulley/vector-loop}
  \tikzexternaldisable%
  \caption[%
    Kinematic loop of the pulley inverse kinematics problem.
  ]{%
    Kinematic loop of the pulley inverse kinematics problem with cable entry point~$\frameanchor_{\cabindex}$, platform pose~$\platpose = \vectset{ \platpos ; \platrot }$, and local platform anchor~$\platanchor_{\cabindex}$ given in~$\coordsys{\symplatform}$.
  }%
  \label{fig:fundamentals:inverse-kinematics:pulley:kinematic-loop}
\end{figure}

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.35\textheight,%
      ]{Side-view of pulley for pulley-based kinematics}
    \caption{%
      Side view of the pulley in its local, winch-rotated coordinate system~$\coordsys{\symwinch}$.%
    }
    \label{fig:inverse-kinematics:geometric:pulley:kinematic-views:side}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \missingfigure[%
        figwidth=\linewidth,%
        figheight=0.35\textheight,%
      ]{Top view of pulley for pulley-based kinematics}
    \caption{%
      Top view of the pulley in its local, winch-rotated coordinate system~$\coordsys{\symwinch}$.%
    }
    \label{fig:inverse-kinematics:geometric:pulley:kinematic-views:top}
  \end{subfigure}
  \caption[%
    Kinematic components of the pulley-based inverse kinematics.
  ]{%
    Kinematic components of the pulley-based inverse kinematics with a side-view~\subref{fig:inverse-kinematics:pulley:kinematic-views:side} and a top-view~\subref{fig:inverse-kinematics:pulley:kinematic-views:top} of the local pulley coordinate system.%
  }
  \label{fig:inverse-kinematics:geometric:pulley:kinematic-views}
\end{figure}

To begin with, we assume that the swiveling angle~$\swivelangle$ of the pulley is known\footnote{For sake of clarity and readability, the cable index~$\parentheses{}_{\cabindex}$ is omitted in this section.} and that all coordinates are now given in the 2D frame~$\coordsys{\symcable} = \parentheses{ C; \evecx[\symcable], \evecz[\symcable] }$.
The transformation from world coordinates~$\coordsys{\symworld}$ to cable coordinate system~$\coordsys{\symcable}$ is given by
%
\begin{align}
  \trafom{\subcable}{\symworld}
    & = \rotmatr_{\mc{w}} \of{ \orientparam } \,
        \rotz{\swivelangle} \,
        ,
\end{align}
%
for which we make~$\orientparam$ the tuple of rotation parameters of the pulley frame~$\coordsys{\symroller}$ \textWrt the world frame~$\coordsys{\symworld}$.
With the transformation leaving all~$y$-coordinates equal to zero~(except for numerical inaccuracies), the following derivation will be give in coordinates~$x$ and~$z$ only.
Given the geometric representation from~\cref{fig:inverse-kinematics:geometric:pulley:tangential-pulley}, we can state the following vector loop:
%
\begin{align}
  \vectft{A}{B}
    & = \vectft{A}{M}
        +
        \vectft{M}{C}
        +
        \vectft{C}{B}
        \,
        ,
        \label{eqn:inverse-kinematics:geometric:pulley:vector-loop}
\end{align}
%
where~$\vectft{A}{M} = \radius \evecx$.
Since the vector from pulley center~$M$ to the cable leave point~$\symcable$ is dependent on only the angle of wrap, we shall state the following relation, which may be simply inferred from the 3D rotation matrix about the~$\evecy$-axis:
%
\begin{align}
  \vectft{M}{C}
    & = -\radius \,
        \begin{bmatrix}
          \cos{\wrapangle} \\
          -\sin{\wrapangle}
        \end{bmatrix} \,
        ,
        \label{eqn:inverse-kinematics:geometric:pulley:ai-to-mi}
\end{align}
%
giving us the pulley leave point directly as a function of the wrap angle~$\wrapangle$.
Lastly, we shall exploit the tangent~$\vect{t}_{\subcable}$ on the pulley, which can be easily deferred from the relation
%
\begin{subequations}
\begin{align}
  \vect{t}_{\subcable}
    & = \frac{
          1
        }{
          \norm{
            \vectft{M}{C}
          }
        } \,
        \od{
          \vectft{M}{C}
        }{
          \wrapangle
        } \\
  \vect{t}_{\subcable}
    & =
      \begin{bmatrix}
        \sin{\wrapangle} \\
        \cos{\wrapangle}
      \end{bmatrix} \,
      ,
      \label{eqn:inverse-kinematics:geometric:pulley:tangent-formula}
\end{align}
\end{subequations}
%
which gives us the tangent to the pulley at point~$C$ pointing in positive direction of~$\wrapangle$.
In fact, the tangent at~$C$ is collinear with the cable thus giving us
%
\begin{align}
  \vectft{C}{B}
    & = \lambda \,
        \vect{t}_{\subcable} \,
        .
        \label{eqn:inverse-kinematics:geometric:pulley:tangent-cable-equation}
\end{align}

Taking further into consider that the cable leaves the pulley tangentially \textIe its angle to the radial connection equals~$\SI{90}{\degree}$, we can apply the right-angle triangle formula of the triangle~$\triangle_{ \left( M B C \right) }$ reading
%
\begin{align}
  \pow{\radius} + \pow{\lambda}
    & = \norm{
          \vectft{M}{B}
        } \,
        ,
\end{align}
%
yielding for the unknown cable length~$\lambda$ in the workspace
%
\begin{align}
  \lambda
    & = \sqrt{
          \pow{
            \vectft{M}{B}
          }
          -
          \pow{
            \radius
          }
        } \,
        .
        \label{eqn:inverse-kinematics:geometric:pulley:workspace-cable-length}
\end{align}
%
We ommit the negative solution to the square root of~\cref{eqn:inverse-kinematics:geometric:pulley:workspace-cable-length} since the cable length cannot be negative and would otherwise not satisfy the constraint~\cref{eqn:inverse-kinematics:geometric:pulley:geometric:cable-equation}.
Substituting~\cref{eqn:inverse-kinematics:geometric:pulley:ai-to-mi,eqn:inverse-kinematics:geometric:pulley:geometric:formula,eqn:inverse-kinematics:geometric:pulley:geometric:cable-equation,eqn:inverse-kinematics:geometric:pulley:workspace-cable-length} into~\cref{eqn:inverse-kinematics:geometric:pulley:vector-loop} yields
%
\begin{align}
  \vectft{A}{B}
  -
  \radius \,
  \evecx
    & = -
        \begin{bmatrix}
          \lambda
            & -\radius \\
          \radius
            & \lambda
        \end{bmatrix}
        \begin{bmatrix}
          \sin \wrapangle \\
          \cos \wrapangle
        \end{bmatrix} \,
        .
        \label{eqn:inverse-kinematics:geometric:pulley:first-equation}
\end{align}
%
%~\cref{eqn:inverse-kinematics:geometric:pulley:first-equation}
This equation can now be converted to separate the sine and cosine of the wrapping angle on the left hand side yielding
%
\begin{align}
  \begin{bmatrix}
    \sin \wrapangle \\
    \cos \wrapangle  
  \end{bmatrix}
    & = \frac{
          1
        }{
          \pow{
            \lambda
          }
          +
          \pow{
            \radius
          }
        } \,
        \begin{bmatrix}
          \lambda
            & \radius \\
          -\radius
            & \lambda
        \end{bmatrix} \,
        \parentheses*{
          \vectft{A}{B}[\mc{xz}]
          -
          \radius \,
          \evecx
        } \,
        ,
        \label{eqn:inverse-kinematics:geometric:pulley:sin-cos-equation}
\end{align}
%
which can be solved for the unknown wrap angle~$\wrapangle$ using the inverse tangent or, for easier numerical handling, the inverse tangent function with two arguments:
%
\begin{align}
  \wrapangle
    & = \arctant{%
          \lambda \,
          \parentheses*{
            \vectft{A}{B}[\mc{x}]
            -
            \radius
          }
          +
          \radius \,
          \vectft{A}{B}[\mc{z}]%
        }{%
          -\radius \,
          \parentheses*{
            \vectft{A}{B}[\mc{x}]
            -
            \radius
          }
          +
          \lambda \,
          \vectft{A}{B}[\mc{z}]%
        } \,
        .
        \label{eqn:inverse-kinematics:geometric:pulley:wrapangle}
\end{align}

One caveat of this approach comes from use of the inverse tangent with two arguments to solve~\cref{eqn:inverse-kinematics:geometric:pulley:wrapangle} as it is defined to provide angles in the interval~$\wrapangle \interval[open right]{-\pi}{\pi}$.
If the horizontal position of the cable end point is left of the outer end of the pulley \textIe {$\vectft{A}{B}[\mc{x}] \leq 2 \, \radius$}, the wrap angle will be negative and needs to be subtracted from~$2 \, \pi$.
However, as the assumptions state\amend{We should state the assumptions when introducing the problem} the cable end point to always be farther away from the pulley \textIe~$\vectft{A}{B}[\mc{x}] > 2 \, \radius$, this case does not have to be considered explicitly.
For the sake of completeness, the full algorithm for determining the wrap angle using~\cref{eqn:inverse-kinematics:geometric:pulley:wrapangle} would read
%
\begin{align}
  \wrapangle
    & = \arctant{%
          \lambda \,
          \parentheses*{
            \vectft{A}{B}[\mc{x}]
            -
            \radius
          }
          + 
          \radius \,
          \vectft{A}{B}[\mc{z}]%
        }{%
          -\radius \,
            \parentheses*{
              \vectft{A}{B}[\mc{x}]
              -
              \radius
            }
            +
            \lambda \,
            \vectft{A}{B}[\mc{z}]%
        }
      + \begin{cases}
          0 \,
          ,
            & \vectft{A}{B}[x] > 2 \,
              \radius \,
              ,
            \\
          2 \,
          pi \,
          ,
            & \vectft{A}{B}[x] \leq 2 \,
              \radius \,
              .
        \end{cases}
        .
\end{align}
