%!TEX root=../../../thesis.tex
%!TEX file=content/inverse-kinematics/geometric/standard.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Standard Kinematics}\label{sec:inverse-kinematics:geometric:standard}

\begin{figure}[tbp]
  \centering
  \smaller[1]
  \missingfigure[%
      figwidth=\linewidth,%
      figheight=6cm,%
    ]{%
      Standard boxed kinematic vector loop of an arbitrary platform.
    }
  \caption{%
    Vector loop of the geometric inverse kinematics for the cable length~$\cablen_{\cabindex} = \norm{\cabdirn_{\cabindex}}$ where the unknown cable vector~$\cabdirn_{\cabindex}$ can be determined through the position of its proximal point~$\Bi$ and its distal point~$\Ai$.
  }
  \label{fig:inverse-kinematics:geometric:standard:vector-loop}
\end{figure}

Highly influenced by their rigid-link counterparts, the inverse kinematics is most simply based on the geometric distance between the proximal and distal point of each actuating cable~(see~\cref{fig:inverse-kinematics:geometric:standard:vector-loop}).
This very simple determination of the setpoints of cable lengths is valid for every \caro motion pattern.
Knowing the platform pose~$\platpose = \vectset{ \platpos ; \platrot }$, we can determine the vector starting at the center of the world frame~$\coordsys{\symworld}$ via the platform cable anchor and the world anchor as
%
\begin{align*}
  \zeros
    & = \platpos
        +
        \platrot \,
        \bi
        +
        \cabdirn_{\cabindex}
        -
        \ai
        \,
        ,
\end{align*}
%
which may be solved for the unknown vector~$\cabdirn_{\cabindex}$ yielding
%
\begin{align}
  \cabdirn_{\cabindex}
    & = \ai
        -
        \parentheses{
          \platpos
          +
          \platrot \,
          \bi
        } \,
        ,
        \label{eqn:inverse-kinematics:geometric:standard:vector-loop}
\end{align}
%
and for the cable length in question
%
\begin{align}
  \cablen_{\cabindex}
    = \sqrt{
        \transpose{
          \cabdirn_{\cabindex}
        } \,
        \cabdirn_{\cabindex}
      }
    & = \norm{
          \cabdirn_{\cabindex}
        }
      = \norm{
          \ai
          -
          \parentheses{
            \platpos
            +
            \platrot \,
            \bi
          }
        } \,
        .
        \label{eqn:inverse-kinematics:geometric:standard:cable-length}
\end{align}

It may be apparent to the discerning reader that both~\cref{eqn:inverse-kinematics:geometric:standard:vector-loop,eqn:inverse-kinematics:geometric:standard:cable-length} are valid if and only if the cable does not promote elastic behavior.
Otherwise, the elasticity would result in either an unstrained length~$\cablen_{\cabindex, \symunstrained}$ smaller than the geometric distance \textIe~$\cablen_{\cabindex, \symunstrained} \leq \cablen_{\cabindex}$ while still~$\cablen_{\cabindex, \symstrained} = \cablen_{\cabindex}$, or, even worse, sagging response of the cable where the strained length~$\cablen_{\cabindex, \symstrained}$ will be larger than the geometric distance \textIe~$\cablen_{\cabindex, \symstrained} \geq \cablen_{\cabindex}$.
In the latter case, no a priori knowledge on the unstrained cable length~$\cablen_{\cabindex, \symunstrained}$ may be available\textemdash -it must be found through a kinetostatic analysis as the strain-inducing stress on the cable must equilibrate the applied stress~(see~\cref{sec:fundamentals:inverse-kinematics:mechanical}).

The geometric inverse kinematics formulation given in~\cref{eqn:inverse-kinematics:geometric:standard:cable-length} assumes ideal, punctiform attachment anchors of the cable on both the platform and the frame, which realistically can never be achieved let alone the cable diameter is non-negligible.
