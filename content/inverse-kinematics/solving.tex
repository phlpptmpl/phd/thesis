%!TEX root=../../thesis.tex
%!TEX file=content/inverse-kinematics/solving.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Solving the Inverse Kinematics Problem}\label{sec:inverse-kinematics:solving}

\talkabout[inline]{My solution to force based inverse kinematics}

For proper operation of \caros, the cable set length must be determined given a platform pose~$\platpose$.
Different methodologies can be used which split into two main categories:
%
\begin{inparaenum}[1)]
  \item Geometric methods, and
  \item Kinetostatic methods.
\end{inparaenum}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Geometric Solving}


Geometric methods are based purely on the geometric properties of the pose and the \caro and use algorithms as presented in~\cref{sec:inverse-kinematics:geometric:standard,sec:inverse-kinematics:geometric:pulley}.
The equations can be solved straightforward by simply substituting the robot's geometric dimensions.
As these methods consider the cables virtually nonexistent\textemdash negligible cable mass and infinite stiffness \textIe~$\youngsmodulus \to \infty$\textemdash , there is also no need to account for elasticity or alike.
As such, the calculated values can directly be commanded to the \caro drives.
In addition, under consideration of the kinematic dualism linking the operational space velocities to the joint space velocities by means of the structure matrix, the necessary command values of joint velocities can be calculated directly knowing the desired operational space trajectory.
Lastly, with the geometric definitions of the \caro kinematics being completely free of any time history, we obtain a stateless description of the motion.
However, this is also the most likely cause of divergence between theory and experiments for we can observe effects that clearly show stateful responses \textEg when cable vibration occurs.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Kinetostatic Solving}


When talking about kinetostatic solving of the kinematics, the problem of determining cable lengths is usually combined with the problem of finding a valid force distribution~(see~\cref{sec:fundamentals:cable-force-distribution}).
Depending on the underlying cable model, these equations can be solved independent of each other.
For example, when considering only an elastic or viscoelastic cable model, we first determine a valid force distribution given the structure matrix at the current pose and then use this force distribution to determine the unstrained cable length.
With the elastic cable model from~\cref{eqn:fundamentals:dynamics:elastic-force}, the~$\cabindex$-th cable unstrained length~$\cablen_{\cabindex, \subunstrained}$ can be calculated from
%
\begin{align*}
  \cablen_{\cabindex, \subunstrained}
    & = \frac{
          \youngsmodulus \,
          \crosssection
        }{
          \cabforce_{\cabindex}
          +
          \youngsmodulus \,
          \crosssection
        } \,
        \cablen_{\cabindex, \substrained} \,
        ,
\end{align*}
%
resulting in tensile force~$\cabforce_{\cabindex}$ on the platform due to strained cable length~$\cablen_{\cabindex, \substrained}$, which in turn is the solution obtained from solving the \gls{IKP}.
We see that the kinetostatic problem is decoupled into two subsequent problems which can be solved independent\footnote{To be precise, even the simple case of a viscoelastic cable model and the standard kinematics, the solution of each sub-problem cannot be determined completely independent of each other as the selection of the inverse kinematics formulation affects the structure matrix, which is used for calculating the force distribution.
However, for the matter of this section, the problems can be considered decoupled.}.

When choosing to consider hefty cables in the inverse kinematics, the problem of finding a cable length no longer becomes independent of the problem of finding a valid force distribution.
Cogently arguing, the force applied onto the cable at its proximal end must not only account for keeping the platform statically equilibrated, but must also keep the cable itself equilibrated.
Very simply put, the vertical force at the proximal end must balance both the platform weight and the weight of the cable\textemdash logically the force increases as the cable length increases.
Solving for the minimal cable length given the cable spanned between two points, the distal force nominally in the range of \SIrange[round-mode=off]{100}{3000}{\newton}, and mechanical properties~(unit density~$\unitdensity = \SI{0.813}{\kilo\gram\per\cubic\meter}$, equivalent mass per length~$\SI{23}{\gram\per\meter}$, Young's modulus~$\youngsmodulus = \SI{1337}{\giga\pascal}$), we find that the cable length increases exponentially as the applied force decreases~(see~\cref{fig:inverse-kinematics:solving:different-lengths}).
In addition, cable sag becomes a challenging problem, especially when considering cable-cable interference, with the mass-induced sag reaches values of up to~$\SI{0.75}{\meter}$ vertical distance to the straight line cable.

\begin{figure}[tbp]
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--catenary--catenary-vs-geometriclength}
    \includegraphics[%
        width=1.00\linewidth,%
        axisratio=1.00,%
        % height=0.35\textheight,%
      ]{kinematics/catenary/catenary-vs-geometric_length}
    \tikzexternaldisable%
    \caption{%
        Logarithmic display of the difference of cable lengths from standard kinematics and mechanical kinematics when varying distal cable forces in their allowed range.
      }
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \smaller[1]
    \tikzexternalenable%
    \tikzsetnextfilename{kinematics--catenary--catenary-vs-geometricsag}
    \includegraphics[%
        width=1.00\linewidth,%
        axisratio=1.00,%
        % height=0.35\textheight,%
      ]{kinematics/catenary/catenary-vs-geometric_sag}
    \tikzexternaldisable%
    \caption{%
        Comparison of cable sag for different cable forces reaching a maximum sag of \SI{0.6449}{\meter} for the minimum of allowed nominal distal cable forces.%
      }
  \end{subfigure}
  \caption{%
      Effect of nominal distal cable force on the resulting cable length under consideration of cable sag.
    }
  \label{fig:inverse-kinematics:solving:different-lengths}
\end{figure}

In case of the solution to the inverse kinetostatic problem, we must simultaneously solve both~\cref{eqn:fundamentals:statics:static-equilibrium,eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution}.
Care needs to be taken as the forces on the cable distal point in~\cref{eqn:fundamentals:inverse-kinematics:mechanical:closed-form-solution} refer to the local coordinate frame~$\coordsys{\symcable}$, whereas~\cref{eqn:fundamentals:statics:static-equilibrium} is given in world coordinates of~$\coordsys{\symworld}$.
In addition, while the existence of solutions to the kinetostatic equation for \caros with hefty cables likewise depends on the level of redundancy of the robot, we need to additionally consider how the force distribution shall be laid out \textEg using the closed-form solution presented in~\cref{sec:fundamentals:cable-force-distribution}, or any other force distribution algorithm.
The significance of considering hefty cables is apparent when the \caro under investigation is a suspended \caro.
Since these robots do not feature antagonistic pairs of cables, cable sag cannot be compensated for by simply increasing the mean cable force in all cables.
Such systems suffer more drastically under the effects of hefty cables causing the platform to be lifted upwards due to drag of the cables undergoing sagging deformation.
This effect is apparent on the \Expo \caros~\autocite{Tempel.2015g}, as well as on the \SI{500}{\meter}-aperture \Fast \caro~\autocite{Kozak.2006,Nan.2011}, where deflections in the pose of absolute up to~$\SI{4}{\meter}$ and in the cable lengths of absolute up to~$\SI{7}{\meter}$ occur due to cable sag.
